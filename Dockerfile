FROM node:16.13.0 as builder
ARG VERSION
WORKDIR /app

COPY package.json ./
COPY package-lock.json ./
COPY .npmrc /root/.npmrc
COPY strada-sw-menusidebar-0.0.1.tgz ./


RUN npm install --legacy-peer-deps

RUN rm -f .npmrc

COPY . ./

RUN node_modules/.bin/ng build --prod


FROM nginx:alpine

COPY --from=builder /app/dist/strada  /usr/share/nginx/html

COPY default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80

# When the container starts, replace the env.json with values from environment variables
CMD ["/bin/sh",  "-c",  "envsubst < /usr/share/nginx/html/assets/environments/config.template.json > /usr/share/nginx/html/assets/environments/config.json && nginx -g 'daemon off;'"]
