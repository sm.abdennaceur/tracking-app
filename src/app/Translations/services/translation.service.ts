import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {TranslateLoader} from "@ngx-translate/core";
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {
  languageUser
  account : string;
  transalte= new BehaviorSubject(undefined);
  transalte$ = this.transalte.asObservable();
  constructor(
    private httpClient: HttpClient
  ) {}
  setLanguage( language: String){
    this.languageUser = language;
  }
  getLanguage() {
    return this.languageUser;
  }
  settransalte(account){
    this.account = account;
    this.transalte.next(account);
  }


}
