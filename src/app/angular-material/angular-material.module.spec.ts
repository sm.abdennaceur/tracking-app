import { TestBed } from '@angular/core/testing';
import { AngularMaterialModule } from './angular-material.module';
describe('AngularMaterialModule', () => {
  let pipe: AngularMaterialModule;
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [AngularMaterialModule] });
    pipe = TestBed.get(AngularMaterialModule);
  });
  it('can load instance', () => {
    expect(pipe).toBeTruthy();
  });
});
