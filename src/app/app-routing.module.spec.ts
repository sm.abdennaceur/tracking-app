import { TestBed } from '@angular/core/testing';
import { AppRoutingModule } from './app-routing.module';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
describe('AppRoutingModule', () => {
  let pipe: AppRoutingModule;
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [AppRoutingModule] });
    pipe = TestBed.get(AppRoutingModule);
  });
  it('can load instance', () => {
    expect(pipe).toBeTruthy();
  });
  it('components defaults to: [AdminLayoutComponent]', () => {
    expect(AppRoutingModule.components).toEqual([AdminLayoutComponent]);
  });
});
