import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FunctionAuthGuard, SigninOidcPageComponent, SignoutOidcPageComponent } from '@strada/lib-authentification';

const routes: Routes = [
 /* {
    path: 'login',
    component: SigninOidcPageComponent,
  },
  {
    path: 'Logout',
    component: SignoutOidcPageComponent
  },*/
  {
    path: 'login',
    data: { breadcrumb: 'Login' },
    loadChildren: () => import('./login/login.module').then((m) => m.LoginModule),
  },

  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: '',
    loadChildren: () => import('./layouts/admin/layout.module').then((m) => m.AdminModule),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)
],
  exports: [RouterModule],
})
export class AppRoutingModule {
  static components = [];
}
