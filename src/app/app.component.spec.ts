import { ComponentFixture, TestBed } from "@angular/core/testing";
import { NO_ERRORS_SCHEMA } from "@angular/core";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { PdfService } from "./core/services/pdf.service";
import { AppComponent } from "./app.component";
describe("AppComponent", () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  beforeEach(() => {
    const routerStub = () => ({
      events: { pipe: () => ({ subscribe: (f) => f({}) }) },
    });
    const activatedRouteStub = () => ({});
    const titleStub = () => ({});
    const pdfServiceStub = () => ({ updateMessage: (title) => ({}) });
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [AppComponent],
      providers: [
        { provide: Router, useFactory: routerStub },
        { provide: ActivatedRoute, useFactory: activatedRouteStub },
        { provide: Title, useFactory: titleStub },
        { provide: PdfService, useFactory: pdfServiceStub },
      ],
    });
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });
  it("can load instance", () => {
    expect(component).toBeTruthy();
  });
  it("title defaults to: strada", () => {
    expect(component.title).toEqual("strada");
  });
  // describe('ngOnInit', () => {
  //   it('makes expected calls', () => {
  //     const pdfServiceStub: PdfService = fixture.debugElement.injector.get(
  //       PdfService
  //     );
  //     spyOn(pdfServiceStub, 'updateMessage').and.callThrough();
  //     component.ngOnInit();
  //     expect(pdfServiceStub.updateMessage).toHaveBeenCalled();
  //   });
  // });
});
