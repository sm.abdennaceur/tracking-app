import { Component, OnInit, Input } from '@angular/core';
import { Router, RouteConfigLoadEnd, ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { PdfService } from './core/services/pdf.service';
import { TranslateService } from '@ngx-translate/core';
import { TranslationService } from './Translations/services/translation.service';
// begin : Methods translation() which is defined in the translation.js
declare function languageSelectedUser(languageUser): any;
declare function translationSelected(): any;
//end : Methods translation() which is defined in the translation.js
import * as config from '../assets/environments/config.json';
import { AppParameters } from '../environments/environment.prod';
import { ConfigService } from './core/services/environment.service';
import { ConfdataServce } from './core/services/env.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'strada';
  // translation
  languageUser: any;
  @Input() data: any;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private pdfService: PdfService,
    private confdataServce: ConfdataServce,
    private configService: ConfigService,
    public translate: TranslateService,
    public translation: TranslationService
  ) {
    const browserLang = translate.getBrowserLang();
    this.languageUser = this.translation.getLanguage();
    translate.use(browserLang.match(/this.languageUser/) ? browserLang : 'en');
    languageSelectedUser(translate.currentLang);
  }
  parametres = {
    decalageMinuteHeureEte: 60,
    fuseauxHoraire: '+01:00',
    heureEteHiverActif: true,
    heureNuitDebut: '21:00',
    heureNuitFin: '06:00',
    heureUTC: false,
    id: 'ac1636f6-c11b-4123-a7e5-88a61a4c7c80',
    utilisateurId: 'db69b6f5-d245-4ab3-8915-acf400d82471',
  };
  ngOnInit() {
    localStorage.setItem('user_params', JSON.stringify(this.parametres));

    this.confdataServce.setConfg(this.configService.getConfiguration());

    this.router.events.pipe(filter((event) => event instanceof RouteConfigLoadEnd)).subscribe((event: any) => {
      this.data = event.route.data;
      //  this.titleService.setTitle(event.route.data.title);

      // this.pdfService.updateMessage(event.route.data.title);
    });
  }
}
