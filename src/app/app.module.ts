import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { registerLocaleData } from '@angular/common';
// import { LoginModule } from './login/login.module';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { DashboardModule } from './dashboard/dashboard.module';
// import { EmployeesModule } from './employees/employees.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import localeFr from '@angular/common/locales/fr';
import localeEn from '@angular/common/locales/en';
import localeEs from '@angular/common/locales/es';
import localePr from '@angular/common/locales/prg';

import { DragScrollModule } from 'ngx-drag-scroll';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

import { ColorPickerModule } from 'ngx-color-picker';
import { CalendarModule } from 'primeng/calendar';
// changes here

// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from 'firebase/app';

// Add the Performance Monitoring library
import 'firebase/performance';
import { DragDropDirective } from './drag-drop.directive';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './core/auth/jwt.interceptor';
// import { SettingService } from './core/services/settingService.service';
import { ConfigService } from '../app/login/services/environment.service';
import { DialoRazComponent } from './shared/dialo-raz/dialo-raz.component';
// import { SvgIconComponent } from 'angular-svg-icon';
import { AngularMaterialModule } from './angular-material/angular-material.module';
// import { EmployeeTravailComponent } from './employees/blocks/employee-travail/employee-travail.component';
// import { PopupPresencesHistoriqueComponent } from './employees/blocks/employee-travail/popup-presences-historique/popup-presences-historique.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';

registerLocaleData(localeFr, 'fr');
registerLocaleData(localeEn, 'en');
registerLocaleData(localeEs, 'es');
registerLocaleData(localePr, 'pr');

// TODO: Replace the following with your app's Firebase project configuration

var firebaseConfig = {
  apiKey: 'AIzaSyAyVulaZyfUDzeWy2W1_ZP92e1GHoOzRN8',
  authDomain: 'stradatms.firebaseapp.com',
  databaseURL: 'https://stradatms.firebaseio.com',
  projectId: 'stradatms',
  storageBucket: 'stradatms.appspot.com',
  messagingSenderId: '531181604703',
  appId: '1:531181604703:web:6a1c1c8a770029fb5bf0ea',
  measurementId: 'G-6785K6DP7R',
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Initialize Performance Monitoring and get a reference to the service
const perf = firebase.performance();
export function ConfigLoader(configService: ConfigService) {
  return () => configService.load('../assets/environments/config.json');
}
import { BreadcrumbComponent } from '@tmsApp/shared/breadcrumb/breadcrumb.component';
import {
  AuthentificationEnvService,
  AuthGuard,
  FunctionAuthGuard,
  HttpInterceptorService,
  LibAuthentificationModule
} from '@strada/lib-authentification';
import { LoginModule } from './login/login.module';
// import { BreadcrumbComponent } from '@tmsApp/shared/breadcrumb/breadcrumb.component';


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    AppComponent,
    AppRoutingModule.components,
    // BreadcrumbComponent,
    // PopupPresencesHistoriqueComponent,
  ],
  imports: [
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    MatDatepickerModule,
    MatDialogModule,
    MatPaginatorModule,
    MatTableModule,
    MatCheckboxModule,
    MatIconModule,
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    CoreModule,
    SharedModule,
    DashboardModule,
    AngularSvgIconModule.forRoot(),
    TranslateModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
   // LibAuthentificationModule.forRoot({configUrl: './assets/env/env.json'}), // url du fichers contenant les infos nécessaire

    BrowserAnimationsModule,
    DragScrollModule,
    ColorPickerModule,
    FormsModule,
    CalendarModule,
    HttpClientModule,
  ],
  exports: [ColorPickerModule, AngularMaterialModule],
  providers: [
     ConfigService,
     {
       provide: APP_INITIALIZER,
       useFactory: ConfigLoader,
       deps: [ConfigService],
       multi: true,
     },
 /*   {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    },  */
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
  
  /*  {
      provide: APP_INITIALIZER,
      useFactory: (envConfigService: AuthentificationEnvService) => () => envConfigService.load().toPromise(),
      deps: [AuthentificationEnvService],
      multi: true
    },*/
    
    { provide: LOCALE_ID, useValue: 'fr-FR' }
    
  ],

  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class AppModule {}
