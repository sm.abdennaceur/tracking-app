import { ENV } from './../env.config';
import { IPasswrodStrength } from '../../shared/interfaces';
import { AppParameters } from '../../../environments/environment.prod';

interface AuthConfig {
  CLIENT_ID: string;
  CLIENT_DOMAIN: string;
  REALM: string;
  LOGIN_URL: string;
  AUDIENCE?: string;
  REDIRECT?: string;
  SCOPE?: string;
  CONNECTION?: string;
  BASE_URL?: string;
}
interface ManagementAppConfig {
  CLIENT_ID: string;
  CLIENT_SECERET: string;
}

export const AUTH_CONFIG: AuthConfig = {
  CLIENT_ID: AppParameters.auth0ClientId,
  CLIENT_DOMAIN: AppParameters.baseUrl, // e.g., you.auth0.com
  REALM: 'Username-Password-Authentication',
  LOGIN_URL: window.location.origin + '/',
  SCOPE: AppParameters.auth0Scope,
  CONNECTION: 'Username-Password-Authentication',
  BASE_URL: AppParameters.baseUrl,
  AUDIENCE: AppParameters.auth0Audience,
};

export const MANAGEMENT_CONFIG: ManagementAppConfig = {
  CLIENT_ID: AppParameters.auth0ConfigId,
  CLIENT_SECERET: AppParameters.auth0ClientSecret,
};

export const PASSWORD_STRENGTH_LABELS: IPasswrodStrength[] = [
  {
    name: 'none',
    pattern: /.+/,
    label: '',
  },
  {
    name: 'low',
    pattern: /(?=.{8,})/,
    label: '8 caractères minimum',
  },
  {
    name: 'fair',
    pattern: /(?=.*[a-z])(?=.*[A-Z]).(?=.{8,})/,
    label: 'Minuscules (a-z), majuscules (A-Z) et chiffres (0-9)',
  },
  {
    name: 'good',
    pattern: /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,32}/,
    label: 'Minuscules (a-z), majuscules (A-Z) et chiffres (0-9)',
  },
  {
    name: 'excellent',
    pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,30}$/,
    label: 'au moins un caractère spécial (! @ # $% ^ & *)',
  },
];
