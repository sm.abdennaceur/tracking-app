import { async } from "@angular/core/testing";
import { Injectable } from "@angular/core";
import { Observable, of as observableOf, throwError } from "rxjs";

import { AuthService } from "./auth.service";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";

@Injectable()
class MockRouter {
  navigate = jasmine.createSpy("navigate");
}

@Injectable()
class MockHttpClient {
  post() {}
}

describe("AuthService", () => {
  let service;

  beforeEach(() => {
    // service = new AuthService({}, {});
  });

  it("should run #setLoggedIn()", async () => {
    // service.loggedIn$ = service.loggedIn$ || {};
    // service.loggedIn$.next = jest.fn();
    // service.setLoggedIn({});
    // expect(service.loggedIn$.next).toHaveBeenCalled();
  });

  it("should run #login()", async () => {
    // service._auth0 = service._auth0 || {};
    // service._auth0.authorize = jest.fn();
    // service.login();
    // expect(service._auth0.authorize).toHaveBeenCalled();
  });

  it("should run #loginCurrentUI()", async () => {
    // service.resetError = jest.fn();
    // service._auth0 = service._auth0 || {};
    // service._auth0.login = jest.fn();
    // service.loginError = service.loginError || {};
    // service.loginError.push = jest.fn();
    // service.loginCurrentUI({
    //   id: {},
    //   password: {}
    // });
    // expect(service.resetError).toHaveBeenCalled();
    // expect(service._auth0.login).toHaveBeenCalled();
    // expect(service.loginError.push).toHaveBeenCalled();
  });

  it("should run #handleAuth()", async () => {
    // service._auth0 = service._auth0 || {};
    // service._auth0.parseHash = jest.fn().mockReturnValue([
    //   {
    //     "error": {}
    //   },
    //   {
    //     "accessToken": {}
    //   }
    // ]);
    // service._getProfile = jest.fn();
    // service.router = service.router || {};
    // service.router.navigate = jest.fn();
    // window.location.hash = {};
    // service.handleAuth();
    // expect(service._auth0.parseHash).toHaveBeenCalled();
    // expect(service._getProfile).toHaveBeenCalled();
    // expect(service.router.navigate).toHaveBeenCalled();
  });

  it("should run #_getProfile()", async () => {
    // service._auth0 = service._auth0 || {};
    // service._auth0.client = {
    //   userInfo: function() {}
    // };
    // service._setSession = jest.fn();
    // service._getProfile({
    //   accessToken: {}
    // });
    // expect(service._setSession).toHaveBeenCalled();
  });

  it("should run #_setSession()", async () => {
    // service.setLoggedIn = jest.fn();
    // service._setSession({
    //   expiresIn: {},
    //   accessToken: {}
    // }, {});
    // // expect(service.setLoggedIn).toHaveBeenCalled();
  });

  it("should run #_clearExpiration()", async () => {
    // service._clearExpiration();
  });

  it("should run #logout()", async () => {
    // service._clearExpiration = jest.fn();
    // service._auth0 = service._auth0 || {};
    // service._auth0.logout = jest.fn();
    // service.logout();
    // // expect(service._clearExpiration).toHaveBeenCalled();
    // // expect(service._auth0.logout).toHaveBeenCalled();
  });

  it("should run #renewToken()", async () => {
    // service._auth0 = service._auth0 || {};
    // service._auth0.checkSession = jest.fn();
    // service._getProfile = jest.fn();
    // service._clearExpiration = jest.fn();
    // service.renewToken();
    // expect(service._auth0.checkSession).toHaveBeenCalled();
    // expect(service._getProfile).toHaveBeenCalled();
    // expect(service._clearExpiration).toHaveBeenCalled();
  });

  it("should run #isAuthenticated()", async () => {
    // service.isAuthenticated();
  });

  it("should run #resetError()", async () => {
    // service.resetError();
  });

  it("should run #requestResetPassword()", async () => {
    // service._auth0 = service._auth0 || {};
    // service._auth0.changePassword = jest.fn();
    // service.requestResetPassword({});
    // expect(service._auth0.changePassword).toHaveBeenCalled();
  });

  it("should run #getPasswordStrength()", async () => {
    // service.http = service.http || {};
    // service.http.post = jest.fn().mockReturnValue(observableOf('post'));
    // service.http.get = jest.fn();
    // service.getPasswordStrength();
    // expect(service.http.post).toHaveBeenCalled();
    // expect(service.http.get).toHaveBeenCalled();
  });

  it("should run #getPasswordStrengthDetails()", async () => {
    // service.getPasswordStrengthDetails({});
  });
});
