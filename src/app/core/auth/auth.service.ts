import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { AUTH_CONFIG, MANAGEMENT_CONFIG, PASSWORD_STRENGTH_LABELS } from '../auth/auth.config';
import * as auth0 from 'auth0-js';
import { ENV } from '../env.config';
import { IUserCredentials, IPasswrodStrength } from '../../shared/interfaces';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { tap, concatMap, catchError } from 'rxjs/operators';
import { ConfigService } from '../services/environment.service';
import { ConfdataServce } from '../services/env.service';
import * as crypto from 'crypto-js';
import { DeviceDetectorService } from 'ngx-device-detector';
import { EmployeesService } from '../services/employees.service';
import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  // Create Auth0 web auth instance
  //  _auth0 = new auth0.WebAuth({
  //   clientID: AUTH_CONFIG.CLIENT_ID,
  //   domain: AUTH_CONFIG.CLIENT_DOMAIN,
  //   audience: AUTH_CONFIG.AUDIENCE,
  //   responseType: "token id_token",
  //   scope: AUTH_CONFIG.SCOPE,
  //   redirectUri:window.location.href  returnTo: AUTH_CONFIG.LOGIN_URL,
  // });

  accessToken: string;
  userProfile: any;
  idToken: string;
  expiresAt: number;
  // Create a stream of logged in status to communicate throughout app
  loggedIn: boolean = false;
  loggedIn$ = new BehaviorSubject<boolean>(this.loggedIn);
  loggingIn: boolean;
  fetchingProfile: boolean;
  loginError: any[] = [];
  secretKey = '36a3301753e578166348caae8096945b';

  email: any;
  langueParams: any;
  constructor(
    private router: Router,
    private http: HttpClient,
    private configService: ConfigService,
    private confdataServce: ConfdataServce,
    private deviceService: DeviceDetectorService // private employeeService: EmployeesService, // private profileService: ProfileService
  ) {
    this.AppParameters = this.configService.getConfiguration();

    this.confdataServce.conf$.subscribe((res) => {
      if (res != undefined) {
        this.AppParameters = res;
        if (JSON.parse(localStorage.getItem('expires_at')) < Date.now() && localStorage.getItem('expires_at')) {
          this.renewToken();
        } else {
          this.setLoggedIn(true);
        }
      }
    });
  }
  AppParameters: any;
  setLoggedIn(value: boolean) {
    // Update login status subject
    this.loggedIn$.next(value);
    this.loggedIn = value;
  }

  login() {
    // Auth0 authorize request
    new auth0.WebAuth({
      clientID: this.AppParameters.auth0ClientId,
      domain: this.AppParameters.auth0BaseUrl,
      audience: this.AppParameters.auth0Audience,
      responseType: 'token id_token',
      scope: this.AppParameters.auth0Scope,
      redirectUri: window.location.origin + '/',
      returnTo: window.location.origin + '/',
    }).authorize();
    this.handleAuth();
  }

  loginCurrentUI(user: IUserCredentials) {
    this.loggingIn = true;
    this.resetError();
    new auth0.WebAuth({
      clientID: this.AppParameters.auth0ClientId,
      domain: this.AppParameters.auth0BaseUrl,
      audience: this.AppParameters.auth0Audience,
      responseType: 'token id_token',
      scope: this.AppParameters.auth0Scope,
      redirectUri: window.location.origin + '/',
      returnTo: window.location.origin + '/',
    }).login(
      {
        realm: AUTH_CONFIG.REALM,
        username: user.id,
        password: user.password,
      },
      (err, res) => {
        this.loggingIn = false;
        if (err) this.loginError.push(err);
      }
    );
  }
  handleAuth() {
    // When Auth0 hash parsed, get profile
    new auth0.WebAuth({
      clientID: this.AppParameters.auth0ClientId,
      domain: this.AppParameters.auth0BaseUrl,
      audience: this.AppParameters.auth0Audience,
      responseType: 'token id_token',
      scope: this.AppParameters.auth0Scope,
      redirectUri: AUTH_CONFIG.LOGIN_URL,
      returnTo: window.location.origin + '/',
    }).parseHash((err, authResult) => {
      if (authResult && authResult.accessToken) {
        window.location.hash = '';
        this._getProfile(authResult);
      } else if (err) {
        console.error(`Error authenticating: ${err.error}`);
      }
    });
  }
  // handleAuth() {
  //   // When Auth0 hash parsed, get profile
  //   new auth0.WebAuth({
  //     clientID: this.AppParameters.auth0ClientId,
  //     domain: this.AppParameters.auth0BaseUrl,
  //     audience: this.AppParameters.auth0Audience,
  //     responseType: 'token id_token',
  //     scope: this.AppParameters.auth0Scope,
  //     redirectUri: window.location.href,
  //     returnTo: window.location.origin + "/",
  //   }).parseHash((err, authResult) => {
  //     if (authResult && authResult.accessToken) {
  //       window.location.hash = '';
  //       // this._getProfile(authResult);
  //       const main = async () => {
  //         try {
  //         const city = await  this._getProfile(authResult);
  //         }
  //         catch (error) {
  //         }

  //     };
  //     main()
  //         .then(() =>{;

  //         }
  //    )
  //         .catch(() => console.error('Failed!'));
  //     } else if (err) {
  //       console.error(`Error authenticating: ${err.error}`);
  //     }

  //   });
  // }
  // _getProfile (authResult) {
  //   this.fetchingProfile = true;

  //   const getEcho = async () => {
  //     try {
  //       const resultA = await  new auth0.WebAuth({
  //         clientID: this.AppParameters.auth0ClientId,
  //         domain: this.AppParameters.auth0BaseUrl,
  //         audience: this.AppParameters.auth0Audience,
  //         responseType: 'token id_token',
  //         scope: this.AppParameters.auth0Scope,
  //         redirectUri:window.location.origin + "/",
  //         returnTo: window.location.origin + "/",
  //       }).client.userInfo(authResult.accessToken, (err, profile) => {
  //         if (profile) {
  //           this._setSession(authResult, profile);
  //         } else if (err) {
  //           this.logout();
  //         }
  //       });;
  //       } catch (error) {
  //      }
  //   };
  //   getEcho();
  //  }

  private _getProfile(authResult) {
    this.fetchingProfile = true;
    // Use access token to retrieve user's profile and set session
    new auth0.WebAuth({
      clientID: this.AppParameters.auth0ClientId,
      domain: this.AppParameters.auth0BaseUrl,
      audience: this.AppParameters.auth0Audience,
      responseType: 'token id_token',
      scope: this.AppParameters.auth0Scope,
      redirectUri: window.location.origin + '/',
      returnTo: window.location.origin + '/',
    }).client.userInfo(authResult.accessToken, (err, profile) => {
      if (profile) {
        this._setSession(authResult, profile);
      } else if (err) {
        console.warn(`Error retrieving profile: ${err.error}`);
        this.logout();
      }
    });
  }

  private _setSession(authResult, profile?) {
    this.expiresAt = authResult.expiresIn * 1000 + new Date().getTime();
    // Store expiration in local storage to access in constructor
    localStorage.setItem('expires_at', JSON.stringify(this.expiresAt));
    localStorage.setItem('Strada_Token', authResult.idToken);
    localStorage.setItem('picture_url', profile.picture);

    this.accessToken = authResult.accessToken;
    this.userProfile = profile;
    this.idToken = authResult.idToken;
    // Update login status in loggedIn$ stream
    this.setLoggedIn(true);
    this.fetchingProfile = false;
    this.router.navigate(['/']);
  }

  private _clearExpiration() {
    // Remove token expiration from localStorage
    localStorage.removeItem('expires_at');
    localStorage.removeItem('Strada_Token');
    localStorage.removeItem('picture_url');
  }

  logout() {
    // Remove data from localStorage
    localStorage.setItem('logout', new Date().getTime().toString());

    this._clearExpiration();
    // End Auth0 authentication session
    new auth0.WebAuth({
      clientID: this.AppParameters.auth0ClientId,
      domain: this.AppParameters.auth0BaseUrl,
      audience: this.AppParameters.auth0Audience,
      responseType: 'token id_token',
      scope: this.AppParameters.auth0Scope,
      redirectUri: window.location.href,
      returnTo: window.location.origin + '/',
    }).logout({
      clientId: this.AppParameters.auth0ClientId,
      returnTo: ENV.BASE_URI,
    });
  }

  get tokenValid(): boolean {
    // Check if current time is past access token's expiration

    return new Date().getTime() < JSON.parse(localStorage.getItem('expires_at'));
  }

  renewToken() {
    // Check for valid Auth0 session
    new auth0.WebAuth({
      clientID: this.AppParameters.auth0ClientId,
      domain: this.AppParameters.auth0BaseUrl,
      audience: this.AppParameters.auth0Audience,
      responseType: 'token id_token',
      scope: this.AppParameters.auth0Scope,
      redirectUri: window.location.href,
      returnTo: window.location.origin + '/',
    }).checkSession(
      {
        scope: 'openid profile email',
      },
      (err, authResult) => {
        if (authResult && authResult.accessToken) {
          this._getProfile(authResult);
        } else {
          this._clearExpiration();
          this.logout();
        }
      }
    );
  }

  isAuthenticated(): boolean {
    return new Date().getTime() < JSON.parse(localStorage.getItem('expires_at'));
  }

  resetError() {
    this.loginError = [];
  }

  requestResetPassword(email: string): Promise<any> {
    return new Promise((resolve, reject) => {
      new auth0.WebAuth({
        clientID: this.AppParameters.auth0ClientId,
        domain: this.AppParameters.auth0BaseUrl,
        audience: this.AppParameters.auth0Audience,
        responseType: 'token id_token',
        scope: this.AppParameters.auth0Scope,
        redirectUri: window.location.href,
        returnTo: window.location.origin + '/',
      }).changePassword(
        {
          connection: AUTH_CONFIG.CONNECTION,
          email: email,
        },
        function (err, resp) {
          if (err) {
            reject(err.message);
          } else {
            resolve(resp);
          }
        }
      );
    });
  }

  getPasswordStrength(): Observable<any> {
    var token;
    return this.http
      .post<any>(AUTH_CONFIG.BASE_URL + '/oauth/token', {
        client_id: MANAGEMENT_CONFIG.CLIENT_ID,
        client_secret: MANAGEMENT_CONFIG.CLIENT_SECERET,
        audience: AUTH_CONFIG.BASE_URL + '/api/v2/',
        grant_type: 'client_credentials',
      })
      .pipe(
        tap((res) => {
          token = res.access_token;
        }),
        concatMap((res: { access_token: string }) => {
          return this.http.get(AUTH_CONFIG.BASE_URL + '/api/v2/connections', {
            headers: {
              Authorization: 'Bearer ' + res.access_token,
            },
          });
        })
      );
  }

  createToken() {
    const deviceInfo = this.deviceService.getDeviceInfo();
    let data = {
      token: localStorage.getItem('Strada_Token'),
      user: jwt_decode(localStorage.getItem('Strada_Token')),
      deviceinfo: deviceInfo,
      expires_at: localStorage.getItem('expires_at'),
      picture_url: localStorage.getItem('picture_url'),
      date: new Date(),
    };
    let result = this.encrypt(JSON.stringify(data));

    return result.replace(/\+/g, 'p1L2u3S').replace(/\//g, 's1L2a3S4h').replace(/=/g, 'e1Q2u3A4l');
  }

  getPasswordStrengthDetails(strenghtCode: string): IPasswrodStrength {
    return PASSWORD_STRENGTH_LABELS.find((x) => x.name === strenghtCode);
  }
  encrypt(value: string): string {
    return crypto.AES.encrypt(value, this.secretKey.trim()).toString();
  }

  decrypt(textToDecrypt: string) {
    return crypto.AES.decrypt(textToDecrypt, this.secretKey.trim()).toString(crypto.enc.Utf8);
  }
  compareDates(date1, date2) {
    var dif = date1.getTime() - new Date(date2).getTime();

    var Seconds_from_T1_to_T2 = dif / 1000;
    return Math.abs(Seconds_from_T1_to_T2);
  }
  tokenManegement(token) {
    try {
      let data = JSON.parse(this.decrypt(token));
      let cond1 = JSON.stringify(data.deviceinfo) == JSON.stringify(this.deviceService.getDeviceInfo());
      let cond2 = this.compareDates(new Date(), data.date) < 20;
      if (cond1 == true) {
        this.expiresAt = data.expires_at;
        // Store expiration in local storage to access in constructor
        localStorage.setItem('expires_at', JSON.stringify(parseInt(this.expiresAt.toString())));
        localStorage.setItem('Strada_Token', data.token);
        localStorage.setItem('picture_url', data.picture_url);

        this.accessToken = data.token;
        this.userProfile = data.user;
        // Update login status in loggedIn$ stream
        this.setLoggedIn(true);
        this.fetchingProfile = false;
        this.router.navigate(['/']);
      } else {
        this.router.navigate(['/login']);
      }
    } catch {
      this.logout();
    }
  }
  // getDecodedAccessToken(token: string): any {
  //   try {
  //     return jwt_decode(token);
  //   } catch (Error) {
  //     return null;
  //   }
  // }
  // // email: any;
  // // langueParams: any;

  // getUserLanguage(params){
  //   this.email = this.getDecodedAccessToken(localStorage.getItem('Strada_Token')).email;
  //   this.employeeService.getUserDetailsV2(this.email).subscribe((idLangue: any) =>{
  //     this.profileService.getLanguages().subscribe( langues =>{

  //     let index = langues.findIndex((langue) => {
  //       return langue.id == idLangue.languageId;
  //     });
  //     this.langueParams = langues[index];
  //   },
  //   (err) => {
  //     // this.langueParams = {
  //     //   id: '61ef2bc1-b4b5-4538-ac02-be804d7f2a80',
  //     //   code: 'es',
  //     //   label: 'Espagnol',
  //     // };

  //     this.langueParams = {
  //       id: '85766a9d-4c7e-4032-9573-8315fa612ec1',
  //       code: 'fr',
  //       label: 'Français',
  //     };

  //   })
  // })
  // }
  private handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
  // getPlatformLangues(config) {
  //    let endpoint = 'https://apiua.dev.stradatms.net/api/Language'
  //   let url =  endpoint;
  //   //let url = config.apiUrl + endpoint;
  //   let params = new HttpParams();
  //   return this.http.get(url, { params: params }).pipe(catchError(this.handleError));
  // }
  getPlatformLangues(config) {
    let endpoint = 'api/ua/Language';
    let url = this.AppParameters.adminApiUrl + endpoint;
    // let url = "https://apiua.dev.stradatms.net/api/Language";

    let params = new HttpParams();

    return this.http.get(url, { params: params }).pipe(catchError(this.handleError));
  }

  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }

  getUserDetails(config, email): Observable<any[]> {
    const endpoint = 'api/v2/user/User/GetByEmail?email=';
    var url = config.apiUrl + endpoint + email;

    let parameters = new HttpParams();
    const header = new Headers();
    header.append('Access-Control-Allow-Origin', '*');
    return this.http
      .get<any[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getUserLanguage(params) {
    let idUserSelected;
    this.email = this.getDecodedAccessToken(localStorage.getItem('Strada_Token')).email;
    this.getUserDetails(params, this.email)
      .toPromise()
      .then((user) => {
        return (idUserSelected = (user as any).languageId);
      })

      .then((idLangue) => {
        this.getPlatformLangues(params)
          .toPromise()
          .then(
            (langues) => {
              let index = (langues as Array<any>).findIndex((langue) => {
                return langue.id == idLangue;
              });

              this.langueParams = langues[index];

              this.configCalandersLanguage();
            },
            (err) => {
              // this.langueParams = {
              //   id: '61ef2bc1-b4b5-4538-ac02-be804d7f2a80',
              //   code: 'es',
              //   label: 'Espagnol',
              // };

              this.langueParams = {
                id: '85766a9d-4c7e-4032-9573-8315fa612ec1',
                code: 'fr',
                label: 'Français',
              };
            }
          );
      });
  }

  configCalandersLanguage() {
    //I18n.language = this.langueParams.code;
  }

  setLangue() {
    this.confdataServce.conf$.subscribe((res) => {
      //if (res != undefined) {

      this.getUserLanguage(res);
      // }
    });
  }
}
