import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
 
 
@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private auth: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
 
 

             request = request.clone({
                setHeaders: {
                   // Authorization: `Bearer ${JSON.parse(sessionStorage.getItem((sessionStorage.key(0)))).access_token}`,
                   Authorization: `Bearer ${localStorage.getItem("Strada_Token")}`,
                }
            });
        
        return next.handle(request);
    }
}

 