import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { PdfService } from '../../core/services/pdf.service';
import { BreadcrumbsService } from '../services/breadcrumbs.service';
import { NavbarService } from '../services/navbar.service';
import { faChevronDown, faPlus } from '@fortawesome/free-solid-svg-icons';
import { INavPrameters } from '../../shared/interfaces';

import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
// import { fab } from '@fortawesome/free-brands-svg-icons';

import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { filter, distinctUntilChanged } from 'rxjs/operators';
import { SessionServce } from '../services/session.service';
import { TranslateService } from '@ngx-translate/core';
import { TranslationService } from '../../Translations/services/translation.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  selected = 'en';

  faChevronDown = faChevronDown;
  private titleModule: any;
  subscription: any;
  messageFromSibling: any;
  params: INavPrameters;
  //Language
  languageUser: any;
  tab: any[] = [];
  constructor(
    public titleService: Title,
    private pdfService: PdfService,
    public router: Router,
    public breadcrumbsService: BreadcrumbsService,
    public navbar: NavbarService,
    public translate: TranslateService,
    public translation: TranslationService,
    private sessionServce: SessionServce
  ) {
    const browserLang = translate.getBrowserLang();

    // end translation
    library.add(far, fas);
  }

  updateStatus: boolean = false;
  collaborateursdetails: boolean = false;
  ngOnInit() {
    let role = this.sessionServce.getsession();

    // this.params = this.navbar.getNavBar();
    this.navbar.currentPrameters.subscribe((message) => {
      this.params = message;
    });
    this.titleService.getTitle();
    this.subscription = this.pdfService.getMessage().subscribe((mymessage) => (this.messageFromSibling = mymessage));
    // this.params = this.navbar.getNavBar();
    this.navbar.currentPrameters.subscribe((message) => {
      this.params = message;
    });
    this.titleService.getTitle();
    this.subscription = this.pdfService.getMessage().subscribe((mymessage) => (this.messageFromSibling = mymessage));

    //   Empty NavBar when route changes
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        distinctUntilChanged()
      )
      .subscribe(() => {
        // this.navbar.setNavBar({})
      });
  }
  doAction(target, event) {
    this.navbar.actionsSubjects[target].callback(event);
  }
  doAction1(target) {}

  // translation
  translateUser(language: string) {
    this.languageUser = language;
    this.translation.setLanguage(this.languageUser);
    this.translation.settransalte(this.languageUser);
  }
}
