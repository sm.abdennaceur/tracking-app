import { TestBed } from "@angular/core/testing";
import { BreadcrumbsService } from "./breadcrumbs.service";
describe("BreadcrumbsService", () => {
  let service: BreadcrumbsService;
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [BreadcrumbsService] });
    service = TestBed.get(BreadcrumbsService);
  });

  it("can load instance", () => {
    expect(service).toBeTruthy();
  });

  describe("getTitle()", () => {
    it("should return the title", () => {
      service.title = "Test Title";
      let returnedTitle = service.getTitle();
      expect(returnedTitle).toEqual("Test Title");
    });
  });

  describe("getSubTitle()", () => {
    it("should return the setSubSubTitle", () => {
      service.subTitle = "Test subTitle";
      let returnedTitle = service.getSubTitle();
      expect(returnedTitle).toEqual("Test subTitle");
    });
  });

  describe("setTitle()", () => {
    it("should set the title", () => {
      service.setTitle("UpdatedNewTitle");
      expect(service.title).toEqual("UpdatedNewTitle");
    });
  });

  describe("setSubTitle()", () => {
    it("should set the subTitle", () => {
      service.setSubTitle("UpdatedNewSubtitle");
      expect(service.subTitle).toEqual("UpdatedNewSubtitle");
    });
  });

  describe("getSubSubTitle()", () => {
    it("should return the subSubTitle", () => {
      service.subSubTitle = "Test subSubTitle";
      let returnedTitle = service.getSubSubTitle();
      expect(returnedTitle).toEqual("Test subSubTitle");
    });
  });

  describe("setSubSubTitle()", () => {
    it("should set the setSubSubTitle", () => {
      service.setSubSubTitle("UpdatedNewsetSubSubTitle");
      expect(service.subSubTitle).toEqual("UpdatedNewsetSubSubTitle");
    });
  });

  describe("getIconeName()", () => {
    it("should return the iconeName", () => {
      service.iconeName = "Test iconeName";
      let returnedTitle = service.getIconeName();
      expect(returnedTitle).toEqual("Test iconeName");
    });
  });

  describe("setIcone()", () => {
    it("should set the iconeName", () => {
      service.setIcone("Test iconeName");
      expect(service.iconeName).toEqual("Test iconeName");
    });
  });
});
