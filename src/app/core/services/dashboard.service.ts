import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import {
  IChronoTachygraphValidity,
  IChronoTachygraphUnload,
  IPaginatedTachygraphs,
  IDriverUnload,
} from '../../shared/interfaces';
import { AppParameters } from '../../../environments/environment.prod';
import { ConfigService } from '@tmsApp/login/services/environment.service';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor(private http: HttpClient, private configService: ConfigService) {
    this.AppParameters = this.configService.config;
  }
  AppParameters: any;
  getUnloadList(): Observable<IDriverUnload[]> {
    const endpoint = 'Conducteur/LastCardUnloads';
    let url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IDriverUnload[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getCardList(): Observable<any[]> {
    const endpoint = 'Card/List';
    let url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<any[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }
  getChronotachygraphUnloadList(): Observable<IPaginatedTachygraphs> {
    const endpoint = 'Tachograph/Page';
    let url = this.AppParameters.apiUrl + endpoint;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IPaginatedTachygraphs>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getChronotachygraphList(): Observable<IChronoTachygraphValidity[]> {
    const endpoint = 'VehicleCalibration/LastValidities';
    let url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IChronoTachygraphValidity[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  //ERROR HANDLER
  private handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
