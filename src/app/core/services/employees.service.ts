import { EventEmitter, Injectable } from '@angular/core';
import {
  ITravelExpenses,
  IAnomalieName,
  IInfractionTypeName,
  IInfractionByEmployeeResponse,
  ICarte,
  IDriverEvent,
  IDriverEventByType,
  IVehicleUtilisation,
  IVehicleUtilisationRow,
  driverDayUtilisation,
  driverHoursUtilisation,
  driverKlmsUtilisation,
  IParticularCondition,
  IEmployee,
  IActivityEmployee,
  IPlanningEmployee,
  IDriverPlanningUtilisation,
  ILeaveRequest,
  IUserDetailsData,
  VehicleSataic,
  IEmployeeUtilisationSummary,
  DriverCard,
  IAddEmployee,
  TimeParams,
  IConducteurUseVehicles,
  IINfractionsListByDriver,
  IActivityTypeInfo,
  IActivitesManullesModifications,
} from '../../shared/interfaces';
import { BehaviorSubject, Observable, throwError } from 'rxjs';

import { HttpClient, HttpParams } from '@angular/common/http';
import * as moment from 'moment';
import { AppParameters } from '../../../environments/environment.prod';
import { ConfigService } from '@tmsApp/login/services/environment.service';

import { catchError, finalize, tap, map } from 'rxjs/operators';

import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpEventType,
} from '@angular/common/http';
import { ConfdataServce } from './env.service';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class EmployeesService {
  activityTypes = [
    { value: 'driving', label: 'Conduite', id: '3' },
    { value: 'work', label: 'Travail', id: '2' },
    { value: 'mad', label: 'Mise a disposition', id: '1' },
    { value: 'cutOff', label: 'Coupure', id: '5' },
    { value: 'ac', label: 'A.C', id: '6' },
    { value: 'de', label: 'D.E', id: '4' },
    { value: 'absence', label: 'Absence', id: '7' },
    { value: 'repos', label: 'Repos', id: '0' },
  ];
  getAcitivityTpe(act, full = false): any {
    if (!full)
      return this.activityTypes.find((el) => el.id === act) && this.activityTypes.find((el) => el.id === act).value;
    return this.activityTypes.find((el) => el.id === act);
  }
  dataUpdated: EventEmitter<any> = new EventEmitter();
  setLatestDataDateActivite(data) {
    this.dataUpdated.emit(data);
  }
  feesIconsList = {
    'F M_RPM': 'fork',
    'E M_RPM': 'fork',
    'F M_RPS': 'fork',
    'E M_RPS': 'fork',
    'F M_RUM': 'fork',
    'E M_RUM': 'fork',
    'F M_RUS': 'fork',
    'E M_RUS': 'fork',
    'F M_NUS': 'fork',
    'E M_NUS': 'fork',
    'F M_SPM': 'fork',
    'E M_SP': 'fork',
    'F M_CC': 'sandwich',
    'E M_CC': 'sandwich',
    'E V_CHA': 'sandwich',
    'F M_GD1M': 'fork',
    'E M_GD1M': 'fork',
    'F M_GD1S': 'fork',
    'E M_GD1S': 'fork',
    DEC_LUN: 'sleep',
    'F M_GD2': 'fork',
    'E M_GD2': 'fork',
    'F V_RPM': 'fork',
    'E V_RPM': 'fork',
    'F V_CD': 'sleep',
    'F V_RPS': 'nightClock',
    'E V_RPS': 'nightClock',
    'F V_RUM': 'fork',
    'E V_RUM': 'fork',
    'F V_RUS': 'nightClock',
    'E V_RUS': 'nightClock',
    'F V_SP': '',
    'E V_SP': '',
    'F V_CC': 'sandwich',
    'E V_CC': 'sandwich',
    'F V_CHA': 'fork',
    ZZFRAIS5: 'sleep',
    'E V_CD': 'sleep',
    'F V_RJ': 'sleep',
    'E V_RJ': 'sleep',
    'F M_SPS': 'nightClock',
    'F M_NUM': 'nightClock',
    'E M_NUM': 'nightClock',
    FMA_1: '',
    QUICK: '',
    ZZFRAIS1: '',
    ZZFRAIS2: '',
    ZZFRAIS3: '',
    ZZFRAIS4: '',
    AIX_DIM: '',
    AIX_JF: '',
    AIX_JT: '',
    BET_WE: '',
    CHO_AST: '',
    FJON_DIM: '',
    NAN_TC: '',
    NAN_TL: '',
    RP_SPE: 'fork',
    CC_SPE: 'sandwich',
    DEc_SPE: 'sleep',
    ARM_RP: 'fork',
    ARM_CC: 'sandwich',
    ARM_288: '',
    ARM_DF: '',
    SOT_DIM: '',
    SOT_DIMCDD: '',
    SOT_RPUN: '',
    NUI_WE: '',
    LAN_PAN: '',
  };

  listEmployees = [];
  planningEmployee: IPlanningEmployee[] = [
    {
      employeeId: 1,
      firstName: 'Bairam ',
      lastName: 'Frootan',
      state: false,
      nextDateTachy: new Date('06/01/2020'),
      lastDateEmptyTacho: new Date('06/01/2020'),
      serviceHours: 25,
      serviceDays: 5,
      absenceDays: 2,
      KmNumber: 2563,
      TotalFrais: '189 €(5Cc-2Rps)',
      TotalInfractions: 0,
      statisticsDay: [
        {
          day: new Date('2014-04-01T00:00:00'),
          serviceTime: '8:20',
          TotalKm: 300,
          TotalFrais: 2,
          infractions: false,
          echeance: true,
          LeaveRequest: true,
          LeaveRequestType: true,
          Pontage: true,
        },
        {
          day: new Date('2014-04-02T00:00:00'),
          serviceTime: '8:20',
          TotalKm: 300,
          TotalFrais: 3,
          infractions: false,
          echeance: true,
          LeaveRequest: true,
          LeaveRequestType: true,
          Pontage: true,
        },
      ],
    },
    {
      employeeId: 2,
      firstName: 'test ',
      lastName: 'test',
      state: false,
      nextDateTachy: new Date('06/01/2020'),
      lastDateEmptyTacho: new Date('06/01/2020'),
      serviceHours: 25,
      serviceDays: 5,
      absenceDays: 2,
      KmNumber: 2563,
      TotalFrais: '189 €(5Cc-2Rps)',
      TotalInfractions: 0,
      statisticsDay: [
        {
          day: new Date('2014-04-01T00:00:00'),
          serviceTime: '8:20',
          TotalKm: 300,
          TotalFrais: 2,
          infractions: false,
          echeance: true,
          LeaveRequest: true,
          LeaveRequestType: true,
          Pontage: true,
        },
        {
          day: new Date('2014-04-02T00:00:00'),
          serviceTime: '8:20',
          TotalKm: 250,
          TotalFrais: 3,
          infractions: false,
          echeance: true,
          LeaveRequest: true,
          LeaveRequestType: true,
          Pontage: true,
        },
      ],
    },
  ];

  constructor(
    private http: HttpClient,
    private configService: ConfigService,
    private router: Router,
    private auth: AuthService,
    private confdataServce: ConfdataServce
  ) {
    this.AppParameters = this.configService.getConfiguration();
  }
  AppParameters: any;
  getActivitiesEmployee(id, startDate, endDate): Observable<IActivityEmployee[]> {
    const params = `?id=${id}&dateDebut=${startDate}&dateFin=${endDate}`;
    const endpoint = 'EmployeeDay/List';
    const url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    const header = new Headers();
    header.append('Access-Control-Allow-Origin', '*');
    return this.http
      .get<IActivityEmployee[]>(url + params, { params: parameters })
      .pipe(catchError(this.handleError));

    // return this.activitiesEmployee;
  }
  getFuseauTime(): Observable<TimeParams[]> {
    const endpoint = 'api/set/FuseauHoraire/List';
    const url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    const header = new Headers();
    header.append('Access-Control-Allow-Origin', '*');
    return this.http
      .get<TimeParams[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }
  // A vérifier

  getHistorizedActivitiesEmployee(id, startDate, endDate): Observable<IActivityEmployee[]> {
    const params = `?conducteurId=${id}&dateDebut=${startDate}&dateFin=${endDate}`;
    const endpoint = 'Conducteur/ListJourConducteurModifies';
    const url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    const header = new Headers();
    header.append('Access-Control-Allow-Origin', '*');
    return this.http
      .get<IActivityEmployee[]>(url + params, { params: parameters })
      .pipe(catchError(this.handleError));

    // return this.activitiesEmployee;
  }

  // A rectifier
  assignaDriverToDrive(ActiviteId: string[], ConducteurId: string): Observable<any> {
    let endpoint = 'Conducteur/AffectConducteurToActivite?';
    if (ActiviteId.length > 0) {
      endpoint += 'ActiviteIds=' + ActiviteId[0];
      for (let i = 1; i < ActiviteId.length; i++) {
        endpoint += ',' + ActiviteId[i];
      }
    }
    endpoint += '&ConducteurId=' + ConducteurId;
    let url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    return this.http
      .post<any>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }
  getListTravelExpenses(): Observable<ITravelExpenses[]> {
    const endpoint = 'IndemnityBonus/IndemnityList';
    const url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    const header = new Headers();
    header.append('Access-Control-Allow-Origin', '*');
    return this.http
      .get<ITravelExpenses[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getUserDetails(email): Observable<ITravelExpenses[]> {
    const endpoint = 'api/User/';
    var url = this.AppParameters.apiUrl + endpoint + email;

    let parameters = new HttpParams();
    const header = new Headers();
    header.append('Access-Control-Allow-Origin', '*');
    return this.http
      .get<ITravelExpenses[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }
  getReposCatchError(email): Observable<any> {
    const endpoint = 'api/v2/user/User/GetByEmail?email=';
    var url = this.AppParameters.apiUrl + endpoint + email;

    return this.http.get<any>(url).pipe(
      catchError((err) => {
        console.error(err);
        // localStorage.setItem('UAMtcheckerr', (this.selectedDate.getTime() + 60000).toString());
        // localStorage.removeItem('Strada_Token');
        // this.router.navigate(['/login']);

        this.auth.logout();

        return throwError(err);
      })
    );
  }
  selectedDate: Date = new Date();

  getUserDetailsV2(email) {
    let lastResponse: HttpEvent<any>;
    let error: HttpErrorResponse;
    const endpoint = 'api/v2/user/User/GetByEmail?email=';
    var url = this.AppParameters.apiUrl + endpoint + email;

    return this.http.get<any>(url).pipe(
      tap((response: HttpEvent<any>) => {
        lastResponse = response;
        if (response) {
          if (response.type === HttpEventType.Response) {
          }
        } 
      }),
      catchError((err: any) => {
        error = err;
        // localStorage.setItem('UAMtcheckerr', (this.selectedDate.getTime() + 60000).toString());
        // localStorage.removeItem('Strada_Token');

        this.auth.logout();

        this.router.navigate(['/login']);
        if (err.status == 400) {
        }
        return throwError(err);
      }),
      finalize(() => {
        if (lastResponse.type === HttpEventType.Sent && !error) {
          // localStorage.setItem('UAMtcheckerr', (this.selectedDate.getTime() + 60000).toString());
          // localStorage.removeItem('Strada_Token');

          this.auth.logout();

          // this.router.navigate(['/login']);
        }
      })
    );
  }

  getFeeIcon(code) {
    return this.feesIconsList[code];
  }

  getDatesActiviteByConducteur(ConducteurId: string, DateDebut: string, DateFin: string): Observable<any[]> {
    const endpoint = 'Activite/DatesActiviteByConducteur';
    let url = this.AppParameters.apiUrl + endpoint;
    url += '?ConducteurId=' + ConducteurId + '&DateDebut=' + DateDebut + '&DateFin=' + DateFin;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    return this.http
      .get<[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getListEmployee(): Observable<IEmployee[]> {
    const endpoint = 'Employee/List';
    let url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IEmployee[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getemployeeDetails(id): Observable<IEmployee> {
    const endpoint = 'Employee/Details?collaborateurId=' + id;
    let url = this.AppParameters.apiUrl + endpoint; //"https://feature-stms-778.dev.apitms.stradatms.net/" + endpoint;
    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IEmployee>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  SearchListemployees(
    index = 0,
    pageSize = 12,
    search = '',
    state = undefined,
    driversOnly: boolean = true
  ): Observable<any> {
    let stateEmployee: boolean;
    if (state === 1) {
      stateEmployee = true;
    } else if (state === 2) {
      stateEmployee = false;
    }
    const filter = stateEmployee !== undefined ? `&InUse=${stateEmployee}` : '';
    const endpoint = `${
      driversOnly ? 'Conducteur' : 'Employee'
    }/Page?PageIndex=${index}&PageSize=${pageSize}&FullName=${search}${filter}`;
    let url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    return this.http.get(url, { params: parameters }).pipe(catchError(this.handleError));
  }

  getEmployeeVehiclePlanning(startDate, endDate): IPlanningEmployee[] {
    return this.planningEmployee;
  }

  // ApiGateway code adaptaion needed
  getEmployeeTimesPlanning(startDate, endDate): Observable<IDriverPlanningUtilisation[]> {
    let endpoint = 'DriverPlanning/GroupList?';

    let url = this.AppParameters.apiUrl + endpoint + 'startDate=' + startDate + '&endDate=' + endDate;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IDriverPlanningUtilisation[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  SearchListDrivers(index = 0, pageSize = 20, search = ''): Observable<any> {
    const endpoint = `Driver/Page?PageIndex=${index}&PageSize=${pageSize}&FullName=${search}`;
    let url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    return this.http.get(url, { params: parameters }).pipe(catchError(this.handleError));
  }

  // A vérifier
  getListInfractionsNames(): Observable<IInfractionTypeName[]> {
    const endpoint = 'InfractionType/List';
    let url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    return this.http
      .get<IInfractionTypeName[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getEvenementConducteurType(): Observable<IInfractionTypeName[]> {
    const endpoint = 'DriverEvenement/Types';
    let url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    return this.http
      .get<IInfractionTypeName[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getInfractionListByType(startDate, endDate, infractions): Observable<IInfractionTypeName[]> {
    let endpoint = 'InfractionByType/List?startDate=' + startDate + '&endDate=' + endDate;
    infractions.forEach((element) => {
      endpoint += '&Codes=' + element;
    });
    let url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    return this.http
      .get<IInfractionTypeName[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getDriverEventsByType(startDate, endDate, anomalies): Observable<IDriverEventByType[]> {
    let endpoint = 'DriverEvenement/ListByLibelle';
    if (anomalies.length) {
      endpoint += '?labels=' + anomalies[0];
    }
    for (let i = 1; i < anomalies.length; i++) {
      endpoint += ',' + anomalies[i];
    }
    let url = this.AppParameters.apiUrl + endpoint + '&startDate=' + startDate + '&endDate=' + endDate;

    let parameters = new HttpParams();
    return this.http
      .get<IDriverEventByType[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getInfractionsByEmployee(
    employees,
    startDate,
    endDate,
    state = undefined
  ): Observable<IInfractionByEmployeeResponse[]> {
    let endpoint = 'InfractionByEmployee/List?startDate=' + startDate + '&endDate=' + endDate;

    employees.forEach((element) => {
      endpoint += '&EmployeeIds=' + element;
    });
    let url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IInfractionByEmployeeResponse[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getCardListByEmployee(employees, state = true): Observable<ICarte[]> {
    let endpoint = 'Card/List';
    if (employees.length) {
      endpoint += '?driverIds=' + employees[0].id;
    }
    for (let index = 1; index < employees.length; index++) {
      endpoint += ',' + employees[index].id;
    }
    if (employees.length) {
      endpoint += '&OnlyValid=' + state;
    }

    let url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<ICarte[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getDriverEventByEmployee(employees, startDate, endDate): Observable<IDriverEvent[]> {
    let endpoint = 'DriverEvenement/ListByDriver';
    if (employees.length) {
      endpoint += '?driverIds=' + employees[0].id;
    }
    for (let index = 1; index < employees.length; index++) {
      endpoint += ',' + employees[index].id;
    }

    let url = this.AppParameters.apiUrl + endpoint + '&startDate=' + startDate + '&endDate=' + endDate;
    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IDriverEvent[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getListVehicleUtilisation(startDate, endDate, vehicles): Observable<IVehicleUtilisationRow[]> {
    let endpoint = 'VehicleUtilisation/Summary?';
    vehicles.forEach((element) => {
      endpoint += 'VehicleIds=' + element + '&';
    });

    let url = this.AppParameters.apiUrl + endpoint + 'startDate=' + startDate + '&endDate=' + endDate;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IVehicleUtilisationRow[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getEmployeeUtilisationSummary(startDate, endDate, employees): Observable<IEmployeeUtilisationSummary[]> {
    let endpoint = 'EmployeeUtilisation/Summary?';
    if (employees.length > 0) {
      endpoint += 'employeeIds=' + employees[0];
      for (let i = 1; i < employees.length; i++) {
        endpoint += ',' + employees[i];
      }
      endpoint += '&startDate=' + startDate + '&endDate=' + endDate;
    } else {
      endpoint += 'startDate=' + startDate + '&endDate=' + endDate;
    }

    let url = this.AppParameters.apiUrl + endpoint;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IEmployeeUtilisationSummary[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getactivitepointeuse(startDate, endDate, employees): Observable<IEmployeeUtilisationSummary[]> {
    let endpoint = 'ActivitePointeuse/RapportActivitesPointeuses?';
    if (employees.length > 0) {
      endpoint += 'collaborateurIds=' + employees[0];
      for (let i = 1; i < employees.length; i++) {
        endpoint += ',' + employees[i];
      }
      endpoint += '&dateActiviteDebut=' + startDate + '&dateActiviteFin=' + endDate;
    } else {
      endpoint += 'dateActiviteDebut=' + startDate + '&dateActiviteFin=' + endDate;
    }

    let url = this.AppParameters.apiUrl + endpoint;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IEmployeeUtilisationSummary[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getSummaryActivitepointeuseDecoupledWeekly(startDate, endDate, employees): Observable<IEmployeeUtilisationSummary[]> {
    let endpoint = 'Collaborateur/SummaryActivitePointeuseMonthFiltre?';
    if (employees.length > 0) {
      endpoint += 'collaborateurIds=' + employees[0];
      for (let i = 1; i < employees.length; i++) {
        endpoint += ',' + employees[i];
      }
      endpoint += '&startDate=' + startDate + '&endDate=' + endDate;
    } else {
      endpoint += 'startDate=' + startDate + '&endDate=' + endDate;
    }

    let url = this.AppParameters.apiUrl + endpoint;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IEmployeeUtilisationSummary[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getDriverUtilisation(startDate, endDate, vehicles): Observable<driverDayUtilisation[]> {
    let endpoint = 'DriverDay/Summary?';
    if (vehicles.length > 0) {
      endpoint += 'id=' + vehicles[0];
    }
    for (let i = 1; i < vehicles.length; i++) {
      endpoint += ',' + vehicles[i];
    }

    let url = this.AppParameters.apiUrl + endpoint + '&startDate=' + startDate;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<driverDayUtilisation[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getDriverTimesUtilisation(startDate, endDate, vehicles): Observable<driverHoursUtilisation[]> {
    let endpoint = 'DriverDay/Times?';
    if (vehicles.length) {
      endpoint += 'conducteurId=' + vehicles[0];
    }
    for (let i = 1; i < vehicles.length; i++) {
      endpoint += ',' + vehicles[i];
    }

    let url = this.AppParameters.apiUrl + endpoint + '&startDate=' + startDate + '&endDate=' + endDate;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<driverHoursUtilisation[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getDriverKlmsUtilisation(startDate, endDate, vehicles): Observable<driverKlmsUtilisation[]> {
    let endpoint = 'DriverUse/List?';
    if (vehicles.length) {
      endpoint += 'driverIds=' + vehicles[0];
    }
    for (let i = 1; i < vehicles.length; i++) {
      endpoint += ',' + vehicles[i];
    }

    let url = this.AppParameters.apiUrl + endpoint + '&startDate=' + startDate + '&endDate=' + endDate;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<driverKlmsUtilisation[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  //ERROR HANDLER
  private handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }

  verifiedDay(id: string, startDate: any): Observable<any> {
    // const url = this.AppParameters.apiUrl
    const url = this.AppParameters.apiUrl;
    const endPoint = 'Pointing/List';
    const params = `?conducteurId=${id}&startDate=${encodeURIComponent(
      moment(startDate).format()
    )}&endDate=${encodeURIComponent(moment(startDate).format())}`;
    return this.http.get(url + endPoint + params).pipe(catchError(this.handleError));
  }

  verifyDay(id: string, startDate: any): Observable<any> {
    const url = this.AppParameters.apiUrl;
    const endPoint = 'Pointing/Pointing';
    const params = {
      driverId: id,
      pointingDate: encodeURIComponent(moment(startDate).format()),
      pointingTime: moment(startDate).format('HH:mm'),
    };
    return this.http.put(url + endPoint, params).pipe(catchError(this.handleError));
  }
  unVerifyDay(id: string, startDate: any): Observable<any> {
    const url = this.AppParameters.apiUrl;
    const endPoint = 'Pointing/Depointing';
    const params = {
      id: id,
      depointingDate: encodeURIComponent(moment(startDate).format()),
      depointingTime: moment(startDate).format('HH:mm'),
    };
    return this.http.post(url + endPoint, params).pipe(catchError(this.handleError));
  }
  submitActivityComment(id, comment: string) {
    const endpoint = 'EmployeeDay/Comment';
    const params = `?Id=${id}&Comment=${comment}`;
    const url = this.AppParameters.apiUrl + endpoint + params;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http.post(url, { params: parameters }).pipe(catchError(this.handleError));
  }

  /* Conditions particuliéres par employée */
  getParticularConditionsByEmployee(employees, startDate, endDate): Observable<IParticularCondition[]> {
    let endpoint = 'Driver/SpecialCondition?';
    employees.length > 0 ? (endpoint += 'conducteurIds=' + employees[0].id) : null;
    for (let index = 1; index < employees.length; index++) {
      endpoint += ',' + employees[index].id;
    }
    employees.length > 0
      ? (endpoint += '&dateDebut=' + startDate + '&dateFin=' + endDate)
      : (endpoint += 'dateDebut=' + startDate + '&dateFin=' + endDate);

    let url = this.AppParameters.apiUrl + endpoint;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IParticularCondition[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  /* Infractions List par employée */
  getInfractionsListByEmployee(id, startDate, endDate): Observable<IINfractionsListByDriver[]> {
    const params = `?ConducteurId=${id}&DateDebut=${startDate}&DateFin=${endDate}`;
    const endpoint = 'InfractionFrais/InfractionsActivite';
    const url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    const header = new Headers();
    header.append('Access-Control-Allow-Origin', '*');
    return this.http
      .get<IINfractionsListByDriver[]>(url + params, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  addEmployee(employee: IAddEmployee): Observable<any> {
    let endpoit = 'Collaborateur/CreateCollaborateur';
    let url = this.AppParameters.apiUrl + endpoit;
    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    // const body = {
    //   adresseId: employee.adresseId,
    //   profileId: employee.profileId,
    //   matricule: employee.matricule,
    //   nationalite: employee.nationalite,
    //   dateNaissance: employee.dateNaissance,
    //   nomJeuneFille: employee.nomJeuneFille,
    //   noms: employee.noms,
    //   prenoms: employee.prenoms,
    //   nomAffichage: employee.nomAffichage,
    //   langueUtiliseeTravail: employee.langueUtiliseeTravail,
    //   dateEmbauche: employee.dateEmbauche,
    //   mail: employee.mail,
    //   fonction: employee.fonction,
    //   numeroSecuriteSociale: employee.numeroSecuriteSociale,
    //   groupActiviteId: employee.groupActiviteId,
    // };
    return this.http.post<any>(url, employee, { params: parameters });
  }
  // .pipe(catchError(this.handleError));
  editEmployeeDetails(employee): Observable<any> {
    /* ***************Edit Comment 4 lines Fiche Salariés************************ */
    // const hiringDate = new Date(employee.work.hiringDate);
    // hiringDate.setDate(hiringDate.getDate() + 1);
    // const startDate = new Date(employee.work.startDate);
    // startDate.setDate(startDate.getDate() + 1);
    // https://web-gw-demo1.dev.stradatms.net/Employee/Details?id=ead71e39-bc07-4f76-b8a0-b4e9fc0a0c13

    let endpoint = 'Employee/Details';
    // let url = AppParameters.apiUrl + endpoint;
    let url = this.AppParameters.apiUrl + endpoint + '?id=' + employee.id;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    console.dir(employee);
    const body = {
      displayName: employee.fullName || '',
      adress: employee.adress || '',
      maidenName: employee.maidenName || '',
      function: employee.post || '',
      phone: employee.phone || '',
      mail: employee.email || '',
      zipCode: employee.zipCode || '',
      city: employee.city || '',
      groupActiviteId: employee.groupActiviteId,
      motDePasseBorne: employee.motDePasseBorne + '' || '',
      identifiantBorne: employee.identifiantBorne || '',
    };

    return this.http
      .post<any>(url, body, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  emptyDataBase(): Observable<any> {
    let url = this.AppParameters.apiUrl;
    const endPoint = 'api/File/EmptyDatabase';
    const params = {};
    return this.http.post(url + endPoint, params).pipe(catchError(this.handleError));
  }

  getEmployeeSummary(id: string, startDate: string, endDate: string): Observable<VehicleSataic> {
    const url =
      this.AppParameters.apiUrl +
      `Activity/CollaborateurSummary?conducteurId=${id}&startDate=${startDate}&endDate=${endDate}`;
    return this.http.get<VehicleSataic>(url).pipe(catchError(this.handleError));
  }

  /* Get Cards History  */
  getDriverCardHistory(ids: string[]): Observable<DriverCard[]> {
    const endpoint = 'Card/List?';
    const params = ids.reduce((acc, curr) => {
      return '&DriverIds=' + curr;
    }, '');
    const url = this.AppParameters.apiUrl + endpoint + params;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<any>(url, { params: parameters })
      .pipe(
        map((res) => {
          if (res && res.length > 0) return res[0].cards;
          return [];
        }),
        catchError(this.handleError)
      );
  }
  getEmployeeUtilisationVehicule(startDate, endDate, employeeID): Observable<IConducteurUseVehicles[]> {
    let endpoint = 'Conducteur/GetUtilisationVehicules?';
    endpoint += 'conducteurId=' + employeeID;
    endpoint += '&startDate=' + startDate;
    endpoint += '&endDate=' + endDate;
    let url = this.AppParameters.apiUrl + endpoint;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IConducteurUseVehicles[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getLeavesRequests(): ILeaveRequest[] {
    return [
      {
        id: '1213',
        displayName: 'Saga Lindén',
        requestDate: '28/11/2019',
        leaveDate: {
          from: '21/12/2019',
          to: '28/12/2019',
        },
        duration: 7,
        nature: 'Congés payés',
        alreadyTaken: 8.5,
        sold: 25.5,
        absencesSoldes: true,
      },
      {
        id: '456',
        displayName: 'Rafaa Ferid',
        requestDate: '28/11/2009',
        leaveDate: {
          from: '21/12/2009',
          to: '02/03/2012',
        },
        duration: 365,
        nature: 'Congés payés',
        alreadyTaken: 6.0,
        sold: 2.0,
        absencesSoldes: true,
      },
      {
        id: '12143',
        displayName: 'Saga Lindén',
        requestDate: '28/11/2019',
        leaveDate: {
          from: '21/12/2019',
          to: '28/12/2019',
        },
        duration: 7,
        nature: 'Congés payés',
        alreadyTaken: 8.0,
        sold: 5.0,
        absencesSoldes: true,
      },
      {
        id: '6676',
        displayName: 'Rafaa Ferid',
        requestDate: '28/11/2009',
        leaveDate: {
          from: '21/12/2009',
          to: '02/03/2012',
        },
        duration: 365,
        nature: 'Congés payés',
        alreadyTaken: 6.0,
        sold: 2.0,
        absencesSoldes: true,
      },
      {
        id: '1213',
        displayName: 'Saga Lindén',
        requestDate: '28/11/2019',
        leaveDate: {
          from: '21/12/2019',
          to: '28/12/2019',
        },
        duration: 7,
        nature: 'Congés payés',
        alreadyTaken: 8.0,
        sold: 5.0,
        absencesSoldes: true,
      },
      {
        id: '456',
        displayName: 'Rafaa Ferid',
        requestDate: '28/11/2009',
        leaveDate: {
          from: '21/12/2009',
          to: '02/03/2012',
        },
        duration: 365,
        nature: 'Congés payés',
        alreadyTaken: 6.0,
        sold: 2.0,
        absencesSoldes: true,
      },
      {
        id: '12143',
        displayName: 'Saga Lindén',
        requestDate: '28/11/2019',
        leaveDate: {
          from: '21/12/2019',
          to: '28/12/2019',
        },
        duration: 7,
        nature: 'Congés payés',
        alreadyTaken: 8.0,
        sold: 5.0,
        absencesSoldes: true,
      },
      {
        id: '6676',
        displayName: 'Rafaa Ferid',
        requestDate: '28/11/2009',
        leaveDate: {
          from: '21/12/2009',
          to: '02/03/2012',
        },
        duration: 365,
        nature: 'Congés payés',
        alreadyTaken: 6.0,
        sold: 2.0,
        absencesSoldes: true,
      },
      {
        id: '1213',
        displayName: 'Saga Lindén',
        requestDate: '28/11/2019',
        leaveDate: {
          from: '21/12/2019',
          to: '28/12/2019',
        },
        duration: 7,
        nature: 'Congés payés',
        alreadyTaken: 8.0,
        sold: 5.0,
        absencesSoldes: true,
      },
      {
        id: '456',
        displayName: 'Rafaa Ferid',
        requestDate: '28/11/2009',
        leaveDate: {
          from: '21/12/2009',
          to: '02/03/2012',
        },
        duration: 365,
        nature: 'Congés payés',
        alreadyTaken: 6.0,
        sold: 2.0,
        absencesSoldes: true,
      },
      {
        id: '12143',
        displayName: 'Saga Lindén',
        requestDate: '28/11/2019',
        leaveDate: {
          from: '21/12/2019',
          to: '28/12/2019',
        },
        duration: 7,
        nature: 'Congés payés',
        alreadyTaken: 8.0,
        sold: 5.0,
        absencesSoldes: true,
      },
      {
        id: '6676',
        displayName: 'Rafaa Ferid',
        requestDate: '28/11/2009',
        leaveDate: {
          from: '21/12/2009',
          to: '02/03/2012',
        },
        duration: 365,
        nature: 'Congés payés',
        alreadyTaken: 6.0,
        sold: 2.0,
        absencesSoldes: true,
      },
    ];
  }

  getAbcenseListOfADay(): any[] {
    return [
      {
        driverId: 1049,
        displayName: 'Christian Hery Celestin',
        leaveDates: 'Du 06/04/2014 au 09/04/2014',
        duration: 3,
        reasonCode: 'FCO',
        comment: 'Raisons personnelles',
      },
      {
        driverId: 1049,
        displayName: 'Christian Hery Celestin',
        leaveDates: 'Du 06/04/2014 au 09/04/2014',
        duration: 3,
        reasonCode: 'FCO',
        comment: 'Raisons personnelles',
      },
      {
        driverId: 1049,
        displayName: 'Christian Hery Celestin',
        leaveDates: 'Du 06/04/2014 au 09/04/2014',
        duration: 3,
        reasonCode: 'FCO',
        comment: 'Raisons personnelles',
      },
      {
        driverId: 1049,
        displayName: 'Christian Hery Celestin',
        leaveDates: 'Du 06/04/2014 au 09/04/2014',
        duration: 3,
        reasonCode: 'FCO',
        comment: 'Raisons personnelles',
      },
      {
        driverId: 1049,
        displayName: 'Christian Hery Celestin',
        leaveDates: 'Du 06/04/2014 au 09/04/2014',
        duration: 3,
        reasonCode: 'FCO',
        comment: 'Raisons personnelles',
      },
      {
        driverId: 1049,
        displayName: 'Christian Hery Celestin',
        leaveDates: 'Du 06/04/2014 au 09/04/2014',
        duration: 3,
        reasonCode: 'FCO',
        comment: 'Raisons personnelles',
      },
    ];
  }

  GetprofilGroup(): Observable<IInfractionTypeName[]> {
    const endpoint = 'TypeActivite/GroupeActiviteList';
    let url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    return this.http
      .get<IInfractionTypeName[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  getTrackedConducteurs(startDate, endDate, conducteurs) {
    let endpoint = 'Activite/ListPositionsGPSActiviteByConducteur?';
    if (conducteurs.length > 0) {
      endpoint += 'ConducteurIds=' + conducteurs[0];
      for (let i = 1; i < conducteurs.length; i++) {
        endpoint += ',' + conducteurs[i];
      }
      endpoint += '&DateDebut=' + startDate + '&DateFin=' + endDate;
    } else {
      endpoint += 'DateDebut=' + startDate + '&DateFin=' + endDate;
    }

    let url = this.AppParameters.apiUrl + endpoint;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<any[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  postCollabeDisable(dataCollab) {
    const endpoint = 'Collaborateur/Disable';
    let url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    return this.http.post(url, dataCollab).pipe(catchError(this.handleError));
  }

  postCollabeEnable(dataCollab) {
    const endpoint = 'Collaborateur/Enable';
    let url = this.AppParameters.apiUrl + endpoint;
    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');
    return this.http.post(url, dataCollab, { params: parameters }).pipe(catchError(this.handleError));
  }

  getListManualActivities(): Observable<IActivityTypeInfo[]> {
    let endpoint = 'TypeActivite/TypeActiviteList';
    let url = this.AppParameters.apiUrl + endpoint;

    let parameters = new HttpParams();
    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IActivityTypeInfo[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  postCreateActivitySequence(
    vehiculeId,
    activityEmployeeSelected,
    ELEMENT_DATA,
    texteLibre: string = 'Texte Libre',
    debutKLm: number = 0,
    finKlm: number = 0,
    totalKlm: number = 0,
    dayCreationFormatToupdate: string
  ): Observable<any> {
    const url = this.AppParameters.apiUrl;
    const endPoint = 'Conducteur/CreateListActiviteManuelle';

    const params = {
      dateActivite: dayCreationFormatToupdate,
      conducteurId: activityEmployeeSelected.driverId,
      vehiculeId: vehiculeId,
      activiteManuelles: ELEMENT_DATA,
      kilometreDebut: debutKLm,
      kilometreFin: finKlm,
      kilometreTotal: totalKlm,
      commentaire: texteLibre,
    };
    return this.http.post(url + endPoint, params).pipe(catchError(this.handleError));
  }

  /* *************Undo Sequence Modification***************** */
  postUndoModificationSequence(undoPayload): Observable<any> {
    const url = this.AppParameters.apiUrl;
    const endPoint = `Conducteur/AnnulerSessionModification?sessionModificationActiviteId=${undoPayload.sessionModificationActiviteId}`;

    const params = {
      dateAnnulation: undoPayload.date,
      userAnnulationId: undoPayload.userID,
      motifAnnulation: undoPayload.motif,
    };
    return this.http.post(url + endPoint, params).pipe(catchError(this.handleError));
  }

  /* *************Redo last Sequence Modification***************** */
  postRedoModificationSequence(redoPayload): Observable<any> {
    const url = this.AppParameters.apiUrl;
    const endPoint = 'Conducteur/ActiverDerniereSessionModificationActiviteAnnulee';

    const params = {
      conducteurId: redoPayload.conducteurID,
      jour: redoPayload.jour,
    };

    return this.http.post(url + endPoint, params).pipe(catchError(this.handleError));
  }

  /* *************Undo @ALL Sequence Modification***************** */
  postUndoALLModificationSequence(undoALLPayload): Observable<any> {
    const url = this.AppParameters.apiUrl;
    const endPoint = 'Conducteur/AnnulerToutesSessionsModificationsActivite';

    const params = {
      userAnnulationId: undoALLPayload.userID,
      conducteurId: undoALLPayload.driverID,
      jour: undoALLPayload.jour,
      motifAnnulation: undoALLPayload.motif,
    };
    return this.http.post(url + endPoint, params).pipe(catchError(this.handleError));
  }

  /* *************GET List ActivitesManuels (Modifications sessions)***************** */
  getListActivitesManuellesModifs(
    conducteurId: string,
    startDate: string,
    endDate: string
  ): Observable<IActivitesManullesModifications[]> {
    let endpoint = 'Activite/ActivitesManuelles';
    let url = this.AppParameters.apiUrl + endpoint;

    let parameters = new HttpParams();

    if (conducteurId) {
      parameters = parameters.set('ConducteurIds', conducteurId);
    }
    if (startDate) {
      parameters = parameters.set('DateDebut', startDate);
    }
    if (endDate) {
      parameters = parameters.set('DateFin', endDate);
    }

    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IActivitesManullesModifications[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  /* *************GET the Last sessionModificationActivite by day and driver***************** */
  getLastSessionModification(conducteurId: string, date: string): Observable<IActivitesManullesModifications[]> {
    let endpoint = 'Activite/LastSessionModificationActiviteByConducteurAndJour';
    let url = this.AppParameters.apiUrl + endpoint;

    let parameters = new HttpParams();

    if (conducteurId) {
      parameters = parameters.set('ConducteurId', conducteurId);
    }
    if (date) {
      parameters = parameters.set('Jour', date);
    }

    const headers = new Headers();
    headers.append('Access-Control-Allow-Origin', '*');

    return this.http
      .get<IActivitesManullesModifications[]>(url, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  /* *************Save modification sequences (Activités scindées)***************** */
  postSaveScinderActivities(scinderPayload): Observable<any> {
    const url = this.AppParameters.apiUrl;
    const endPoint = 'Activite/UpdateActivites';

    const params = {
      conducteurId: scinderPayload.conducteurId,
      rows: scinderPayload.rows,
    };

    return this.http.post(url + endPoint, params).pipe(catchError(this.handleError));
  }
  requiredprofile = new BehaviorSubject(undefined);
  required: any;
  required$ = this.requiredprofile.asObservable();

  setrequiredprofile(required) {
    this.required = required;
    this.requiredprofile.next(required);
  }

  getrequiredprofile() {
    return Object.assign({}, this.required);
  }
}
