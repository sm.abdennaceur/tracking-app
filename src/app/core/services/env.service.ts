import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfdataServce {

  tenant = new BehaviorSubject(undefined);
  tenants: any;
  tenant$ = this.tenant.asObservable();

  conf = new BehaviorSubject(undefined);
  progressBar: any;
  conf$ = this.conf.asObservable();

  profildetails = new BehaviorSubject(undefined);
  profil: any;
  Profile$ = this.conf.asObservable();

  constructor() { }

  setConfg(progress) {
    this.progressBar = progress;
    this.conf.next(progress);
  }

  getConfg() {
    console.log(this.progressBar)
    return Object.assign({}, this.progressBar);
  }

  settenant(progress) {
    this.tenants = progress;
    this.tenant.next(progress);
  }

  getenant() {
    return Object.assign({}, this.tenants);
  }

  setProfil(profile) {
    this.profil = profile;
    this.profildetails.next(profile);
  }

  getProfil() {
    return Object.assign({}, this.profil);
  }

}
