import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ConfigService {
  private config: any;

  constructor(private http: HttpClient) {
  }

  load(url: string) {
    return new Promise((resolve) => {
      this.http.get(url)
        .subscribe(config => {
          this.config = config;
          resolve(config);
        });
    });
  }

  getConfiguration() {
    return this.config;
  }
}
