import { TestBed } from '@angular/core/testing';
import { ExcelService } from './excel.service';
describe('ExcelService', () => {
  let service: ExcelService;
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [ExcelService] });
    service = TestBed.get(ExcelService);
  });
  it('can load instance', () => {
    expect(service).toBeTruthy();
  });
});
