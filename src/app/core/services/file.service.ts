import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { promise } from "protractor";
import { Observable } from "rxjs";
import { AuthService } from "../auth/auth.service";
import * as shajs from "sha.js";
import { AppParameters } from '../../../environments/environment.prod';
import { ConfigService } from './environment.service';
@Injectable({
  providedIn: "root",
})
export class FileService {
  constructor(private auth: AuthService, private http: HttpClient,private configService: ConfigService) {
    this.AppParameters = this.configService.getConfiguration();

  }
  AppParameters:any
  // uploadReadFile(file: FileUpload) {
  //   // var reader = new FileReader();
  //   // reader.onload = (e) => {
  //   //   var text: string = reader.result.toString();

  //   if (!file.sent && !file.hasError)
  //     this.uploadFile(file).subscribe(
  //       () => {
  //         file.sent = true;
  //       },
  //       () => (file.hasError = true)
  //     );
  //       // };
  //   // reader.readAsText(file.bolb);
  // }

  uploadFile(file: FileUpload): Observable<any> {
    const url = this.AppParameters.apiUrl + "api/File/TachoFileUpload";
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.auth.idToken}`,
    });
    const fd = new FormData();
    fd.append("file", file.bolb);
    return this.http.post(url, fd, { headers });
  }

  // checksum(str, algorithm?, encoding?) {
  //   return shajs(algorithm || "sha256")
  //     .update(str)
  //     .digest(encoding || "hex");
  // }
}
