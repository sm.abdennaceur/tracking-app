import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
    providedIn: 'root'
})
export class FilterdataServce {

    constructor() { }
    conf = new BehaviorSubject(undefined);
    filter: any;
    conf$ = this.conf.asObservable();

    setConfg(progress) {
        this.filter = progress;
        this.conf.next(progress);
    }

    getConfg() {
        return Object.assign({}, this.filter);
    }

}
