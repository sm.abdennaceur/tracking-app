import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MenuEntry } from '@strada/sw-menusidebar';
import jwt_decode from 'jwt-decode';
import { AuthService } from '../../../app/login/services/auth.service';
import { SessionServce } from './session.service';

@Injectable({
  providedIn: 'root',
})
export class MenusidebarService {
  menuEntries: MenuEntry[] = [];
  universeEntries: MenuEntry[] = [];
  listUniverses: any = [];

  info;
  test;
  constructor(
    public translateService: TranslateService,
    public authService: AuthService,
    private sessionServce: SessionServce
  ) {
    console.log(localStorage.getItem('picture_url'));
    this.info = this.sessionServce.getsession();
  }
  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }

  getSidebarEntries(): MenuEntry[] {
    var userEntrySubEntries2: MenuEntry[] = [];
    if (localStorage.getItem('universes') != null) {
      this.listUniverses = JSON.parse(localStorage.getItem('universes'));
    }
    if (this.menuEntries.findIndex((m) => m.label === this.translateService.instant('switch')) < 0) {
      userEntrySubEntries2.push(this.addItem('Strada', '/strada', '', null, true, true, false));
      userEntrySubEntries2.push(
        this.addItem(
          ' TMS',
          this.controlRoutes('TMS', '/tms'),
          '',
          null,
          true,
          this.controlUniverses('TMS'),

          false,
          true
        )
      );
      userEntrySubEntries2.push(
        this.addItem(
          'Efact',
          this.controlRoutes('EFACT', '/efact'),
          '',
          null,
          true,
          this.controlUniverses('EFACT'),
          false
        )
      );
      userEntrySubEntries2.push(
        this.addItem(
          'Time',
          this.controlRoutes('TIME', '/time'),
          '',
          null,
          true,
          this.controlUniverses('TIME'),
          false,
          true
        )
      );

      userEntrySubEntries2.push(this.addItem('Grille Tarifaire', '/pricer', '', null, true, true, false));

      this.menuEntries.push(this.addItem('switch', '', 'icone-change.svg', userEntrySubEntries2, true, true, false));
    }

    if (this.menuEntries.findIndex((m) => m.label === this.translateService.instant('Dashboard')) < 0) {
      this.menuEntries.push(this.addItem('Dashboard', '/', 'icon-home.svg', null, true, true, false));
    }

    if (this.menuEntries.findIndex((m) => m.label === this.translateService.instant('Tracking')) < 0) {
      userEntrySubEntries2=[]
      userEntrySubEntries2.push(
        this.addItem(
          'google maps',
          '/track',
          '',
          null,
          true,
          true,
          false
        )
      );
  /*    userEntrySubEntries2.push(
        this.addItem(
          'leaflet maps',
          '/track-Leaflet',
          '',
          null,
          true,
          false,
          false,
          true
        )
      );
      userEntrySubEntries2.push(
        this.addItem(
          'Here maps',
          '/track-hereMap',
          '',
          null,
          true,
          false,
          false,
          true
        )
      );*/
      
      this.menuEntries.push(this.addItem('Tracking', '', 'map.svg',userEntrySubEntries2 , true, true, false));
    }
    if (this.menuEntries.findIndex((m) => m.label === this.translateService.instant("Center d'information")) < 0) {
      this.menuEntries.push(
        this.addItem("Center d'information", '/Center_information', 'info-ol.svg', null, true, true, false)
      );
    }
    return this.menuEntries;
  }

  controlUniverses(code) {
    let activate: boolean = false;
    this.listUniverses.map((item) => {
      if (item.codeUniverse == code) {
        activate = item.isActivate;
      }
    });
    return activate;
  }
  controlRoutes(code, defaultRoute) {
    let route: string = defaultRoute;
    this.listUniverses.map((item) => {
      if (item.codeUniverse == code) {
        if (item.isActivate == false) route = './';
      }
    });

    return route;
  }
  public addItem(
    label: string,
    path: string,
    icon: string,
    subEntries: null | MenuEntry[],
    show: boolean,
    isActive: boolean,
    isProfile: boolean,
    isSwitch?: boolean
  ): MenuEntry {
    return {
      label: this.translateService.instant(label),
      link: path,
      icon: icon,
      subEntries: subEntries,
      show: show,
      isActive: isActive,
      isProfile: isProfile,
      isSwitch: isSwitch,
    };
  }
}
