import { Injectable, Output, EventEmitter } from '@angular/core';
import { INavPrameters, IFilter, INavBarDate, IStateFilter, IEmployee } from '../../shared/interfaces';
import { BehaviorSubject } from 'rxjs';
import { isDefined } from '@angular/compiler/src/util';
import { SearchService } from './navbar/search.service';
const datekey = 'selectedDate';
@Injectable({
  providedIn: 'root',
})
export class NavbarService {
  @Output() click: EventEmitter<Event> = new EventEmitter();
  @Output() clickSaveBtn: EventEmitter<Event> = new EventEmitter();
  @Output() exportPDFClick: EventEmitter<Event> = new EventEmitter();
  @Output() exportXLSClick: EventEmitter<Event> = new EventEmitter();
  @Output() change: EventEmitter<INavBarDate> = new EventEmitter();
  @Output() newTypeClick: EventEmitter<Event> = new EventEmitter();

  parameters: INavPrameters = {};
  parametersSubject = new BehaviorSubject<INavPrameters>(this.parameters);
  currentPrameters = this.parametersSubject.asObservable();
  listEmployees: IEmployee[] = [];

  filter: IFilter[] = [];
  filtersSubject = new BehaviorSubject<IFilter[]>(this.filter);
  currentFilter = this.filtersSubject.asObservable();

  public actionsSubjects: any = {};
  actionsListners = {};
  state: IStateFilter = 'all';
  date: INavBarDate = {
    interval: null,
    startDate: null,
    endDate: null,
  };
  dateSubject = new BehaviorSubject<INavBarDate>(this.date);
  currentDate = this.dateSubject.asObservable();

  stateSubject = new BehaviorSubject<IStateFilter>(this.state);
  currentState = this.stateSubject.asObservable();
  constructor(private searchService: SearchService) {
    this.initDate();

    this.currentPrameters.subscribe((config) => {
      if (config && !config.search) {
        this.searchService.setValue('');
      }
    });
  }
  initDate() {
    const dateJosn = localStorage.getItem(datekey);

    if (dateJosn) {
      const date = JSON.parse(dateJosn);
      date.startDate = new Date(date.startDate);
      date.endDate = new Date(date.endDate);

      this.setDate(date);
    } else {
      this.setDate({
        interval: 'day',
        startDate: new Date(),
        endDate: new Date(),
      });
    }
  }

  public setNavBar(args: INavPrameters): void {
    if (isDefined(args.actionsList))
      args.actionsList.forEach((el) => {
        this.actionsSubjects[el.slug] = {
          emitter: new EventEmitter(),
          callback: (event: Event) => {
            this.actionsSubjects[el.slug].emitter.emit(event);
          },
        };
      });
    this.parameters = args;
    this.parametersSubject.next(args);
  }
  setFilters(filter: IFilter[]) {
    this.filtersSubject.next(filter);
  }
  clicked(event: Event) {
    this.click.emit(event);
  }
  clickedSaveBtn(event: Event) {
    this.clickSaveBtn.emit(event);
  }
  saveBtnIsLoading(loading: boolean) {
    this.parametersSubject.next({
      ...this.parameters,
      loadingSaveBtn: loading,
    });
  }
  setDate(date: INavBarDate) {
    if (date.interval === undefined) {
      date.interval = this.date.interval;
    } else {
      this.date.interval = date.interval;
    }
    localStorage.setItem(datekey, JSON.stringify(date));
    this.dateSubject.next(date);
    this.dateChanged();
  }
  dateChanged() {
    this.change.emit(this.date);
  }
  exportPDFClicked(event: Event) {
    this.exportPDFClick.emit(event);
  }
  exportXLSClicked(event: Event) {
    this.exportXLSClick.emit(event);
  }

  setState(state: IStateFilter) {
    this.state = state;
    this.stateSubject.next(this.state);
  }
  conf = new BehaviorSubject(undefined);
  popup: any;
  conf$ = this.conf.asObservable();

  setpopup(list) {
    this.popup = list;
    this.conf.next(list);
  }
  getpopup() {
    return Object.assign({}, this.popup);
  }
  newTypeClicked(event: Event) {
    this.newTypeClick.emit(event);
  }
}
