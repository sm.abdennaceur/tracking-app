import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class SearchService {
  private searchSubject = new BehaviorSubject<string>("");
  public searchEvent: Observable<string> = this.searchSubject.asObservable();
  value: string = "";
  constructor() {}

  public setValue = function (value) {
    this.searchSubject.next(value);
    this.value = value;
  };
}
