import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IUserProfileParamaters } from '../../shared/interfaces';
import { ConfigService } from '@tmsApp/login/services/environment.service';
import { AppParameters } from '../../../environments/environment.prod';

@Injectable({
  providedIn: 'root',
})
export class ParamatersService {
  AppParameters: any;

  constructor(private http: HttpClient, private configService: ConfigService) {
    this.AppParameters = this.configService.getConfiguration();
  }

  getUserParamaters(userId: string): Observable<IUserProfileParamaters> {
    let endpoint = 'api/set/Parametres/ParametresUtilisateur';
    let url = this.AppParameters.apiUrl + endpoint;
    let params = new HttpParams();
    if (userId) {
      params = params.set('utilisateurId', userId);
    }
    return this.http
      .get<IUserProfileParamaters>(url, { params: params })
      .pipe(catchError(this.handleError));
  }

  testParamatersAreCreated(userId: string): Observable<boolean> {
    let endpoint = 'api/set/Parametres/IsCreated';
    let url = this.AppParameters.apiUrl + endpoint;
    let params = new HttpParams();
    if (userId) {
      params = params.set('utilisateurId', userId);
    }
    return this.http
      .get<boolean>(url, { params: params })
      .pipe(catchError(this.handleError));
  }

  createUserParamaters(userId: string) {
    let endpoint = 'api/set/Parametres/Create';
    let url = this.AppParameters.apiUrl + endpoint;
    url += '?utilisateurId=' + userId;
    let params = new HttpParams();

    return this.http.post(url, { params: params }).pipe(catchError(this.handleError));
  }

  updateUserParamaters(obj: IUserProfileParamaters, userId: string): Observable<any> {
    let endpoint = 'api/set/Parametres/Update';
    let url = this.AppParameters.apiUrl + endpoint;
    url += '?utilisateurId=' + userId;
    let parameters = new HttpParams();

    const body = {
      heureUTC: obj.heureUTC,
      fuseauxHoraire: obj.fuseauxHoraire,
      heureEteHiverActif: obj.heureEteHiverActif,
      decalageMinuteHeureEte: obj.decalageMinuteHeureEte,
      // heureNuitDebut: obj.heureNuitDebut,
      // heureNuitFin: obj.heureNuitFin,
      // minuteNuitDebut: obj.minuteNuitDebut,
      // minuteNuitFin: obj.minuteNuitFin,
    };

    return this.http
      .post<any>(url, body, { params: parameters })
      .pipe(catchError(this.handleError));
  }

  //ERROR HANDLER
  private handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
