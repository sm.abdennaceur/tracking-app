import { TestBed } from '@angular/core/testing';
import { PdfService } from './pdf.service';
describe('PdfService', () => {
  let service: PdfService;
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [PdfService] });
    service = TestBed.get(PdfService);
  });
  it('can load instance', () => {
    expect(service).toBeTruthy();
  });
});
