import { Injectable } from '@angular/core';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PdfService {
  private titleComponent = new Subject<string>();
  constructor() { }

  public exportAsPdfFile(cols: any[], rows: any[], pdfFileName: string,equalWidthCols:boolean = false): void {
    var doc = new jsPDF((cols.length>4)?'l':'p','mm',"a4");
   if(!equalWidthCols && cols.length>5){
     let columnStyles=[]
     cols.forEach(el=>{
       const width= (el.length)<11?el.length*3.5:(el.length*2<40)? el.length*2:'auto';
       columnStyles.push({cellWidth: width})
      })
      const  config={styles:{cellWidth:'auto'},columnStyles:columnStyles};
      doc.autoTable(cols, rows,config);
    }else{
      doc.autoTable(cols, rows);
    }
    doc.save(pdfFileName + '_export_' + new Date().getTime());
  }

  public exportMultipleAsPdfFile(data: any[], pdfFileName: string): void {
    var doc = new jsPDF('l','mm',"a4");
      data.forEach(el=>{
        doc.autoTable(el.cols, el.rows,{styles:{fontSize:7,cellWidth:'auto',minCellWidth:12}});
      })
      doc.setFontSize(5)
    doc.save(pdfFileName + '_export_' + new Date().getTime());
  }

  getMessage(): Observable<string> {
    return this.titleComponent.asObservable();
 }
 updateMessage(title: string) {
  this.titleComponent.next(title);
}

}
