/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BaseService } from './base-service';
import { StradaAdminUsersAccessApiConfiguration } from './strada-admin-users-access-api-configuration';
import { StrictHttpResponse } from './strict-http-response';
import { RequestBuilder } from './request-builder';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

// import { UserTypeRow } from '../models/user-type-row';

@Injectable({
  providedIn: 'root',
})
export class DashbordApiService extends BaseService {
  constructor(
    config: StradaAdminUsersAccessApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * Path part for operation userTypeGet
   */
  static readonly UserTypeGetPath = '/api/UserType';

  /**
   * Gets all user type.
   *
   *
   *
   * This method provides access to the full `HttpResponse`, allowing access to response headers.
   * To access only the response body, use `userTypeGet()` instead.
   *
   * This method doesn't expect any request body.
   */
  userTypeGet$Response(params?: {

  }): Observable<StrictHttpResponse<Array<any>>> {

    const rb = new RequestBuilder(this.rootUrl, DashbordApiService.UserTypeGetPath, 'get');
    if (params) {


    }
    return this.http.request(rb.build({
      responseType: 'json',
      accept: 'application/json'
    })).pipe(
      filter((r: any) => r instanceof HttpResponse),
      map((r: HttpResponse<any>) => {
        return r as StrictHttpResponse<Array<any>>;
      })
    );
  }

  /**
   * Gets all user type.
   *
   *
   *
   * This method provides access to only to the response body.
   * To access the full response (for headers, for example), `userTypeGet$Response()` instead.
   *
   * This method doesn't expect any request body.
   */
  userTypeGet(params?: {

  }): Observable<Array<any>> {

    return this.userTypeGet$Response(params).pipe(
      map((r: StrictHttpResponse<Array<any>>) => r.body)
    );
  }

}
