/* tslint:disable */
import { Injectable } from '@angular/core';

/**
 * Global configuration
 */
@Injectable({
  providedIn: 'root',
})
export class StradaAdminUsersAccessApiConfiguration {
  rootUrl: string = '';
}

/**
 * Parameters for `StradaAdminUsersAccessApiModule.forRoot()`
 */
export interface StradaAdminUsersAccessApiConfigurationParams {
  rootUrl?: string;
}
