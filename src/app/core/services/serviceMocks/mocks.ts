import {
  IDriverEvent,
  IDriverEventByType,
  IDriverPlanningUtilisation,
  IInfractionTypeName,
} from '../../../shared/interfaces';

export const mockListEmployeesData: any[] = [
  {
    id: 'd2b80b56-ab53-4274-af18-035e18ce6666',
    firstName: 'Karim',
    lastName: 'Hamdi',
    fullName: 'Karim Hamdi',
    phone: '0021670345915',
    mobilePhone: '0021650027884',
    profile: 'Default',
    state: 'Inactif',
    warning: false,
    address: '  ',
    geolocalisationFees: false,
    birthDate: '1995-07-21T00:00:00+00:00',
    hiringDate: '2010-01-01T00:00:00+00:00',
    workUsedLanguage: 'fr',
    licenceNumber: null,
    deliveredLicenceAutorityName: null,
    deliveredLicenceNationName: null,
    country: null,
    mail: null,
    maidenName: null,
    nationality: 'Tunisie',
    socialSecurityNumber: null,
    serialNumber: '1000000006370',
  },
  {
    id: 'd2b80b56-ab53-4274-af18-035e18ce5555',
    firstName: 'Rafaa',
    lastName: 'Ferid',
    fullName: null,
    phone: null,
    mobilePhone: null,
    profile: 'Default',
    state: 'Inactif',
    warning: false,
    address: '  ',
    geolocalisationFees: false,
    birthDate: '1995-10-30T00:00:00+00:00',
    hiringDate: '2005-01-01T00:00:00+00:00',
    workUsedLanguage: 'fr',
    licenceNumber: null,
    deliveredLicenceAutorityName: null,
    deliveredLicenceNationName: null,
    country: null,
    mail: null,
    maidenName: null,
    nationality: 'Tunisie',
    socialSecurityNumber: null,
    serialNumber: '1000000008323',
  },
];

export const mockDetailsEmployeesGet: any = {
  id: 'd2b80b56',
  lastName: 'Karim',
  firstName: 'Hamdi',
  maidenName: null,
  fullName: 'Karim Haldi',
  post: null,
  profile: 'Default',
  phone: null,
  adress: '  ',
  zipCode: null,
  city: null,
  country: null,
  email: null,
  warning: false,
  state: false,
  card: { cn: null, validityStart: null, validityEnd: null, unload: null },
  drivingLicenseCard: { number: null, autority: null, country: null },
};

export const mockIndemnityBonusGet: any = [
  {
    code: 'FirstMockCode',
    libelle: 'Karim',
    unitValue: 13.56,
    foreign: false,
  },
  {
    code: 'SecondMockCode',
    libelle: 'Hamdi',
    unitValue: 16,
    foreign: true,
  },
];

export const mockPaginationEmployees: any = {
  items: [
    {
      id: '1eeed1',
      firstName: 'Karim',
      lastName: 'Hamdi',
      fullName: 'KH',
      phone: null,
      mobilePhone: null,
      profile: 'Default',
      state: 'Inactif',
      warning: false,
      address: '  ',
      geolocalisationFees: false,
      birthDate: '1988-07-21T00:00:00+00:00',
      hiringDate: '0001-01-01T00:00:00+00:00',
      workUsedLanguage: 'fr',
      licenceNumber: null,
      deliveredLicenceAutorityName: null,
      deliveredLicenceNationName: null,
      country: null,
      mail: null,
      maidenName: null,
      nationality: 'France',
      socialSecurityNumber: null,
      serialNumber: '1000000006370',
    },
    {
      id: '2fggff4',
      firstName: 'Raffa',
      lastName: 'Ferid',
      fullName: null,
      phone: null,
      mobilePhone: null,
      profile: 'Default',
      state: 'Inactif',
      warning: false,
      address: '  ',
      geolocalisationFees: false,
      birthDate: '1963-10-30T00:00:00+00:00',
      hiringDate: '0001-01-01T00:00:00+00:00',
      workUsedLanguage: 'fr',
      licenceNumber: null,
      deliveredLicenceAutorityName: null,
      deliveredLicenceNationName: null,
      country: null,
      mail: null,
      maidenName: null,
      nationality: 'France',
      socialSecurityNumber: null,
      serialNumber: '1000000008323',
    },
  ],
  pageIndex: 0,
  totalCount: 6,
  itemsCount: 6,
  pageCount: 1,
  hasPreviousPage: false,
  hasNextPage: true,
};

export const mockListAnomaliesEmployees: any = [
  {
    type: 1,
    libelle: 'FirstMockedData',
  },
  {
    type: 0,
    libelle: 'SecondMockedData',
  },
];

export const mockActivitiesEmployees: any = [
  {
    day: '2010-10-15T15:28:23.222Z',
    driverId: '3fa85f64',
    displayName: 'Karim Hamdi',
    totalDrive: 0,
    totalWork: 0,
    totalMad: 0,
    totalBreak: 0,
    totalService: 0,
    totalDe: 0,
    totalAc: 0,
    totalNight: 0,
    totalAbsence: 0,
    totalMileage: 0,
    sequences: [
      {
        id: '5717',
        vehicleId: '4562',
        activityCode: 'code',
        activityLibelle: 'code',
        startTime: 'code',
        endTime: 'code',
        duration: 0,
      },
    ],
  },
];

export const mockEmployeesPlanning: any = [
  {
    driverId: '3fa85f64',
    displayName: 'Karim',
    lastLockingDate: '2020-10-19T19:40:41.670Z',
    lastUnloadingTacho: '2020-10-19T19:40:41.670Z',
    cardRenewDate: '2020-10-19T19:40:41.670Z',
  },
];

export const mockInfractionTypes: IInfractionTypeName[] = [
  {
    code: 'AM',
    libelle: 'Amplitude',
  },
  {
    code: '2S',
    libelle: 'Conduite 2 semaines',
  },
];

export const mockAnomaliesbyType: IDriverEventByType[] = [
  {
    type: 24,
    libelle: 'First one',
    evenements: [
      {
        id: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
        driverId: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
        driverFullName: 'Karim Hamdi',
        // libelle: "query",
        startDate: new Date(2020 - 10 - 10),
        endDate: new Date('2020-10-20T08:20:17.605Z'),
        type: 0,
      },
    ],
  },
];

export const mockDriverEventsByEmployees: IDriverEvent[] = [
  {
    driverId: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
    fullName: 'string',
    evenements: [
      {
        id: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
        driverId: '3fa85f64-5717-4562-b3fc-2c963f66afa6',
        driverFullName: 'Karim Hamdi',
        libelle: 'string',
        startDate: new Date('2020-10-20T12:31:33.598Z'),
        endDate: new Date('2020-10-20T12:31:33.598Z'),
        type: 0,
      },
    ],
  },
];
