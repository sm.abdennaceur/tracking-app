import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
    providedIn: 'root'
})
export class SessionServce {

    constructor() { }
    session = new BehaviorSubject(undefined);
    sessionsetails: any;
    session$ = this.session.asObservable();

    setsession(sessionsetails) {
        this.sessionsetails = sessionsetails;
        this.session.next(sessionsetails);
    }

    getsession() {
        return Object.assign({}, this.sessionsetails);
    }

}
