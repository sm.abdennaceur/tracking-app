import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
 

@Injectable({
    providedIn: "root",
  })
export class LocaleService {

  //Chosse Locale From This Link
   constructor(  
    ) { }
    conf = new BehaviorSubject(undefined);
     conf$ = this.conf.asObservable();

  

  


  private _locale: string;

  set locale(value: string) {
    this._locale = value;
     this.conf.next(value);
  }
  get locale(): string {
    return Object.assign({}, this._locale );

    // return this._locale || 'en';
  }

  public registerCulture(culture: string) {
  
    switch (culture) {
      case 'fr': {
        this._locale = 'fr';
         break;
      }
      case 'en': {
        this._locale = 'en';
         break;
      }

      default: {
        this._locale = 'en';
         break;
      }
    }
  }
}