// tslint:disable
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import {
  Pipe,
  PipeTransform,
  Injectable,
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA,
  Directive,
  Input,
  Output,
} from "@angular/core";
import { isPlatformBrowser } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { Observable, of as observableOf, throwError } from "rxjs";

import { Component, ElementRef } from "@angular/core";
import { SidebarComponent } from "./sidebar.component";
import { Router } from "@angular/router";
import { AuthService } from "../auth/auth.service";
import {
  faChevronDown,
  faChevronLeft,
  faChevronRight,
  faChevronUp,
  faCircle,
} from "@fortawesome/free-solid-svg-icons";

@Injectable()
class MockElementRef {
  nativeElement = {};
  // nativeElement = {};
}

@Injectable()
class MockRouter {
  navigate() {}
}

@Injectable()
class MockAuthService {}

@Directive({ selector: "[oneviewPermitted]" })
class OneviewPermittedDirective {
  @Input() oneviewPermitted;
}

@Pipe({ name: "translate" })
class TranslatePipe implements PipeTransform {
  transform(value) {
    return value;
  }
}

@Pipe({ name: "phoneNumber" })
class PhoneNumberPipe implements PipeTransform {
  transform(value) {
    return value;
  }
}

@Pipe({ name: "safeHtml" })
class SafeHtmlPipe implements PipeTransform {
  transform(value) {
    return value;
  }
}

describe("SidebarComponent", () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        SidebarComponent,
        TranslatePipe,
        PhoneNumberPipe,
        SafeHtmlPipe,
        OneviewPermittedDirective,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [
        { provide: ElementRef, useClass: MockElementRef },
        { provide: Router, useClass: MockRouter },
        { provide: AuthService, useClass: MockAuthService },
      ],
    })
      .overrideComponent(SidebarComponent, {})
      .compileComponents();
    fixture = TestBed.createComponent(SidebarComponent);
    component = fixture.debugElement.componentInstance;
  });

  afterEach(() => {
    component.ngOnDestroy = function () {};
    fixture.destroy();
  });

  it("should run #constructor()", async () => {
    expect(component).toBeTruthy();
  });

  it("faChevronLeft defaults to: faChevronLeft", () => {
    expect(component.faChevronLeft).toEqual(faChevronLeft);
  });
  it("faChevronRight defaults to: faChevronRight", () => {
    expect(component.faChevronRight).toEqual(faChevronRight);
  });
  it("faChevronDown defaults to: faChevronDown", () => {
    expect(component.faChevronDown).toEqual(faChevronDown);
  });
  it("faChevronUp defaults to: faChevronUp", () => {
    expect(component.faChevronUp).toEqual(faChevronUp);
  });
  it("faCircle defaults to: faCircle", () => {
    expect(component.faCircle).toEqual(faCircle);
  });
  it("pictureUrl defaults to: empty", () => {
    expect(component.pictureUrl).toEqual("");
  });
});
