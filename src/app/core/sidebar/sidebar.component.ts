import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { faChevronLeft, faCircle, faChevronRight, faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons';

import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { SessionServce } from '../services/session.service';

const misc: any = {
  navbarMenuVisible: 0,
  activeCollapse: true,
  disabledCollapseInit: 0,
};

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  faChevronLeft = faChevronLeft;
  faChevronRight = faChevronRight;
  faChevronDown = faChevronDown;
  faChevronUp = faChevronUp;
  faCircle = faCircle;
  pictureUrl = '';

  mobileMenuVisible: any = 0;
  private nativeElement: Node;
  private toggleButton: any;
  private sidebarVisible: boolean;
  private route: Subscription;

  @ViewChild('app-navbar', { static: false }) button: any;

  constructor(
    private element: ElementRef,
    private router: Router,
    private auth: AuthService,
    private sessionServce: SessionServce
  ) {
    this.nativeElement = element.nativeElement;
  }
  USER: boolean = false;
  INVIT: boolean = false;
  Dashboard: boolean = false;
  Rapports: boolean = false;
  Parametres: boolean = false;
  GraphiqueAcivite: boolean = false;
  Vitessestachygraphe: boolean = false;
  GraphiqueAcivitecollaborateur: boolean = false;
  excesVitesses: boolean = false;
  Listecartes: boolean = false;
  accederMenuDonneeBase: boolean = false;
  accederParametresInterfaces: boolean = false;
  ajoutDocumentVehicule: boolean = false;

  accederEcransReporting: boolean = false;
  importerFichier: boolean = false;

  restituerFichier: boolean = false;
  userName;
  ngOnInit() {
    this.pictureUrl = localStorage.getItem('picture_url');

    let role = this.sessionServce.getsession();
    this.userName = role.userName;
    console.log(role);
  }

  goToLink(url: string) {
    window.open(url, '_blank');
  }

  minimizeSidebar(event) {
    const target = event.target.querySelector('p');
    event.target.classList.remove('nav-item-hover');
    target.classList.remove('nav-link-title');
    const collapsable = event.target.querySelector('div.collapse');
    if (collapsable) {
      collapsable.classList.remove('show');
    }

    // we simulate the window Resize so the charts will get updated in realtime.
    const simulateWindowResize = setInterval(() => {
      window.dispatchEvent(new Event('resize'));
    }, 180);

    // we stop the simulation of Window Resize after the animations are completed
    setTimeout(() => {
      clearInterval(simulateWindowResize);
    }, 1000);
  }
  userLogout() {
    this.auth.logout();
  }

  extendSidebar(event) {
    const target = event.target.querySelector('p');
    event.target.classList.add('nav-item-hover');
    misc.sidebarMiniActive = false;
    const collapsable = event.target.querySelector('div.collapse');
    if (collapsable) {
      collapsable.classList.add('show');
      collapsable.querySelector('p').classList.add('nav-link-title');
    } else {
      target.classList.add('nav-link-title');
    }

    // we simulate the window Resize so the charts will get updated in realtime.
    const simulateWindowResize = setInterval(() => {
      window.dispatchEvent(new Event('resize'));
    }, 180);

    // we stop the simulation of Window Resize after the animations are completed
    setTimeout(() => {
      clearInterval(simulateWindowResize);
    }, 1000);
  }
}
