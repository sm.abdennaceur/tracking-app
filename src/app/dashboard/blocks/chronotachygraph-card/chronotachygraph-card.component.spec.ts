// tslint:disable
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import {
  Pipe,
  PipeTransform,
  Injectable,
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA,
  Directive,
  Input,
  Output,
} from "@angular/core";
import { isPlatformBrowser } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { Observable, of as observableOf, throwError } from "rxjs";

import { Component } from "@angular/core";
import { ChronotachygraphCardComponent } from "./chronotachygraph-card.component";

@Directive({ selector: "[oneviewPermitted]" })
class OneviewPermittedDirective {
  @Input() oneviewPermitted;
}

@Pipe({ name: "translate" })
class TranslatePipe implements PipeTransform {
  transform(value) {
    return value;
  }
}

@Pipe({ name: "phoneNumber" })
class PhoneNumberPipe implements PipeTransform {
  transform(value) {
    return value;
  }
}

@Pipe({ name: "safeHtml" })
class SafeHtmlPipe implements PipeTransform {
  transform(value) {
    return value;
  }
}

describe("ChronotachygraphCardComponent", () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        ChronotachygraphCardComponent,
        TranslatePipe,
        PhoneNumberPipe,
        SafeHtmlPipe,
        OneviewPermittedDirective,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [],
    })
      .overrideComponent(ChronotachygraphCardComponent, {})
      .compileComponents();
    fixture = TestBed.createComponent(ChronotachygraphCardComponent);
    component = fixture.debugElement.componentInstance;
  });

  afterEach(() => {
    component.ngOnDestroy = function () {};
    fixture.destroy();
  });

  it("should run #constructor()", async () => {
    expect(component).toBeTruthy();
  });

  it("should run #ngOnInit()", async () => {
    component.ngOnInit();
  });

  xit("should run #ngOnChanges()", async () => {
    component.unloadData = component.unloadData || {};
    component.unloadData = ["unloadData"];
    component.unloadData.push = jest.fn();
    component.reArrangeUnloadData = jest.fn().mockReturnValue({
      classifyUloadData: function () {},
    });
    component.reArrangeValidityData = jest.fn().mockReturnValue({
      classifyValidityData: function () {},
    });
    component.ngOnChanges({
      unloadData: {},
      validityData: {},
    });
    // expect(component.unloadData.push).toHaveBeenCalled();
    // expect(component.reArrangeUnloadData).toHaveBeenCalled();
    // expect(component.reArrangeValidityData).toHaveBeenCalled();
  });

  xit("should run #reArrangeUnloadData()", async () => {
    component.simplifiedData = component.simplifiedData || {};
    component.simplifiedData.push = jest.fn();
    component.unloadData = component.unloadData || {};
    component.unloadData = ["unloadData"];
    component.reArrangeUnloadData();
    // expect(component.simplifiedData.push).toHaveBeenCalled();
  });

  xit("should run #reArrangeValidityData()", async () => {
    component.simplifiedValidityData = component.simplifiedValidityData || {};
    component.simplifiedValidityData.push = jest.fn();
    component.validityData = component.validityData || {};
    component.validityData = ["validityData"];
    component.reArrangeValidityData();
    // expect(component.simplifiedValidityData.push).toHaveBeenCalled();
  });

  xit("should run #classifyUloadData()", async () => {
    component.simplifiedData = component.simplifiedData || {};
    component.simplifiedData = ["simplifiedData"];
    component.cardSlider = component.cardSlider || {};
    component.cardSlider.steps = {
      i: {},
    };
    component.card = component.card || {};
    component.card.dataList = {
      push: function () {},
    };
    component.card.date = "date";
    component.card.displayName = "displayName";
    component.classifyUloadData();
  });

  xit("should run #classifyValidityData()", async () => {
    component.simplifiedValidityData = component.simplifiedValidityData || {};
    component.simplifiedValidityData = ["simplifiedValidityData"];
    component.validitySlider = component.validitySlider || {};
    component.validitySlider.steps = {
      i: {},
    };
    component.validity = component.validity || {};
    component.validity.dataList = {
      push: function () {},
    };
    component.validity.date = "date";
    component.validity.displayName = "displayName";
    component.classifyValidityData();
  });
});
