import { Component, OnInit, Input, ViewChild, SimpleChanges } from '@angular/core';
import { IChronoTachygraphValidity, IChronoTachygraphUnload } from '../../../shared/interfaces';
import { TranslateService } from '@ngx-translate/core';

import * as moment from 'moment';
import { TimeParamsService } from '../../../shared/time-service/time-params.service';

@Component({
  selector: 'chronotachygraph-card',
  templateUrl: './chronotachygraph-card.component.html',
  styleUrls: ['./chronotachygraph-card.component.scss'],
})
export class ChronotachygraphCardComponent implements OnInit {
  @Input() unloadData: IChronoTachygraphUnload[];
  @Input() validityData: IChronoTachygraphValidity[];
  simplifiedData = [];
  simplifiedValidityData = [];
  @ViewChild('cardslider', { static: false }) cardSlider;
  @ViewChild('validityslider', { static: false }) validitySlider;
  card = [
    { value: 15, displayName: '', date: '', dataList: [] },
    { value: 45, displayName: '', date: '', dataList: [] },
    { value: 75, displayName: '', date: '', dataList: [] },
    { value: 95, displayName: '', date: '', dataList: [] },
  ];
  validity = [
    { value: 3, displayName: '', date: '', dataList: [] },
    { value: 9, displayName: '', date: '', dataList: [] },
    { value: 15, displayName: '', date: '', dataList: [] },
    { value: 21, displayName: '', date: '', dataList: [] },
    { value: 27, displayName: '', date: '', dataList: [] },
  ];
  constructor(public timeParams: TimeParamsService, public translate: TranslateService) {}

  ngOnInit() {}
  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.

    if (changes.unloadData) {
      // this.unloadData.push({
      //   id: "201",
      //   dateNextUnloading: "2020-11-02T00:00:00",
      //   dateLastUnloading: "2020-10-12T00:00:00",
      //   displayName: "1 tachy unload TEST",
      //   entityLocation: "karoimcdsd",
      //   events: 0,
      //   registration: "1 unload tachy TEST"
      // });
      // this.unloadData.push({
      //   id: "202",
      //   dateNextUnloading: "2020-11-02T00:00:00",
      //   dateLastUnloading: "2020-10-01T00:00:00",
      //   displayName: "2 tachy unload TEST",
      //   entityLocation: "karoimcdsd",
      //   events: 0,
      //   registration: "2 unload tachy TEST"
      // });
      // this.unloadData.push({
      //   id: "203",
      //   dateNextUnloading: "2020-11-02T00:00:00",
      //   dateLastUnloading: "2020-09-12T00:00:00",
      //   displayName: "3 tachy unload TEST",
      //   entityLocation: "karoimcdsd",
      //   events: 0,
      //   registration: "3 unload tachy TEST"
      // });
      // this.unloadData.push({
      //   id: "204",
      //   dateNextUnloading: "2020-11-02T00:00:00",
      //   dateLastUnloading: "2020-08-12T00:00:00",
      //   displayName: "4 tachy unload TEST",
      //   entityLocation: "karoimcdsd",
      //   events: 0,
      //   registration: "4 unload tachy TEST"
      // });
      // this.unloadData.push({
      //   id: "204",
      //   dateNextUnloading: "2020-11-02T00:00:00",
      //   dateLastUnloading: "2018-08-12T00:00:00",
      //   displayName: "Test Dépassé",
      //   entityLocation: "karoimcdsd",
      //   events: 0,
      //   registration: "Test Dépasssé"
      // });
      this.reArrangeUnloadData().classifyUloadData();
    }
    if (changes.validityData) {
      // this.validityData.push({
      //   id: "201",
      //   nextDate: "2021-01-01T00:00:00",
      //   registration: "1 tachy Validity TEST",
      // });
      // this.validityData.push({
      //   id: "202",
      //   nextDate: "2021-10-05T00:00:00",
      //   registration: "2 tachy Validity TEST",
      // });
      // this.validityData.push({
      //   id: "203",
      //   nextDate: "2021-08-11T00:00:00",
      //   registration: "3 tachy Validity TEST",
      // });
      // this.validityData.push({
      //   id: "204",
      //   nextDate: "2022-05-01T00:00:00",
      //   registration: "4 tachy Validity TEST",
      // });
      // this.validityData.push({
      //   id: "205",
      //   nextDate: "2018-11-03T00:00:00",
      //   registration: "Inférieur Validity TEST",
      // });
      // this.validityData.push({
      //   id: "205",
      //   nextDate: "2020-11-05T13:27:00",
      //   registration: "Inférieur Validity TEST with time",
      // });
      // this.validityData.push({
      //   id: "205",
      //   nextDate: "2026-11-03T00:00:00",
      //   registration: "Supérieure Validity TEST",
      // });
      // this.validityData.push({
      //   id: "205",
      //   nextDate: "2021-11-06T01:00:00",
      //   registration: "Test Date du jour TEST",
      // });
      this.reArrangeValidityData().classifyValidityData();
    }
  }

  reArrangeUnloadData() {
    const nowTime = new Date().getTime();

    this.simplifiedData = [];

    this.unloadData.forEach((el) => {
      const time = new Date(el.dateLastUnloading).getTime();
      el.dateLastUnloading = this.changeDate(el.dateLastUnloading);
      this.simplifiedData.push({
        days: Math.abs(nowTime - time) / (1000 * 60 * 60 * 24),
        lastUnload: el.dateLastUnloading,
        displayName: el.displayName,
        date: el.dateLastUnloading,
        id: el.id,
      });
    });

    return this;
  }

  reArrangeValidityData() {
    // const nowTime = new Date().getTime();
    const nowDate = new Date();
    this.simplifiedValidityData = [];

    this.validityData.forEach((el) => {
      el.nextDate = this.changeDate(el.nextDate);
      // const time = new Date(el.nextDate).getTime();
      const date = new Date(el.nextDate).getTime();
      this.simplifiedValidityData.push({
        days: moment(date).diff(nowDate, 'months', true),
        lastUnload: el.nextDate,
        displayName: el.registration,
        date: el.nextDate,
        id: el.vehicleId,
      });
    });

    return this;
  }
  changeDate(dateToChange) {
    let date;
    let paramUser = JSON.parse(localStorage.getItem('user_params'));
    if (paramUser.heureUTC == true) {
      date = this.timeParams.convertToLocalTime(dateToChange.toString());
    } else if (paramUser.heureUTC == false) {
      if (String(paramUser.fuseauxHoraire.charAt(0) == '-' || String(paramUser.fuseauxHoraire.charAt(0) == '+'))) {
        date = this.timeParams.convertToUTC(
          String(paramUser.fuseauxHoraire.charAt(0)),
          dateToChange?.toString(),
          Number(String(paramUser.fuseauxHoraire).substring(1, 3)),
          Number(String(paramUser.fuseauxHoraire).substring(4, 6)),
          Number(paramUser.decalageMinuteHeureEte)
        );
      } else {
        date = this.timeParams.convertToUTC(
          '+',
          dateToChange.toString(),
          Number(String(paramUser.fuseauxHoraire).substring(0, 2)),
          Number(String(paramUser.fuseauxHoraire).substring(3, 5)),
          Number(paramUser.decalageMinuteHeureEte)
        );
      }
    }
    return date;
  }
  classifyUloadData() {
    if (this.cardSlider) {
      this.simplifiedData.forEach((el) => {
        let i = 0;
        while (el.days > this.cardSlider.steps[i] && this.card.length > i - 1) {
          i++;
        }
        this.card[i - 1].dataList.push(el);
        if ((new Date(this.card[i - 1].date).getTime() || 0) < new Date(el.date).getTime()) {
          this.card[i - 1].date = el.date.split('T')[0];
          this.card[i - 1].displayName = el.displayName;
        }
      });
    }
  }
  classifyValidityData() {
    this.simplifiedValidityData.forEach((el) => {
      let i = 0;
      if (this.validitySlider) {
        while (el.days > this.validitySlider.steps[i] && this.validity.length > i - 1) {
          i++;
        }

        i = this.validity.length - i;
        if (i === 0) {
          i++;
        }

        this.validity[i - 1].dataList.push(el);
        if ((new Date(this.validity[i - 1].date).getTime() || 0) < new Date(el.date).getTime()) {
          this.validity[i - 1].date = el.date.split('T')[0];
          this.validity[i - 1].displayName = el.displayName;
        }
      }
    });
  }
}
