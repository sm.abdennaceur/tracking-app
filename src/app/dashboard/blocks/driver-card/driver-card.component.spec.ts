import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import {
  Pipe,
  PipeTransform,
  Injectable,
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA,
  Directive,
  Input,
  Output,
} from "@angular/core";
import { isPlatformBrowser } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { Observable, of as observableOf, throwError } from "rxjs";

import { Component } from "@angular/core";
import { DriverCardComponent } from "./driver-card.component";

@Directive({ selector: "[oneviewPermitted]" })
class OneviewPermittedDirective {
  @Input() oneviewPermitted;
}

@Pipe({ name: "translate" })
class TranslatePipe implements PipeTransform {
  transform(value) {
    return value;
  }
}

@Pipe({ name: "phoneNumber" })
class PhoneNumberPipe implements PipeTransform {
  transform(value) {
    return value;
  }
}

// https://gwapi.dev.stradatms.net/EmployeeUtilisation/Summary?employeeIds=cb0fcf9a-c970-461c-8ff9-4398c5b029fe,0c5c34fa-d332-4a71-af34-2fb6bcf3f413,0f629fbf-d0a3-4a85-8e31-6c0fbe7718da,a64e4e0c-71f8-44eb-8612-e3ca55e1d46c,a8c64402-a323-4ceb-811d-fdb837807c6f%2C,bab891a5-a97e-41a9-96bb-6ecc4dd890d9&startDate=2020-04-04&endDate=2020-04-04
// https://gwapi.dev.stradatms.net/EmployeeUtilisation/Summary?employeeIds=cb0fcf9a-c970-461c-8ff9-4398c5b029fe,0c5c34fa-d332-4a71-af34-2fb6bcf3f413,0f629fbf-d0a3-4a85-8e31-6c0fbe7718da,a64e4e0c-71f8-44eb-8612-e3ca55e1d46c,a8c64402-a323-4ceb-811d-fdb837807c6f,5848c1d4-bd0c-4df3-bca5-1f08e2dc98a5,bab891a5-a97e-41a9-96bb-6ecc4dd890d9&startDate=2010-10-01T00%3A00%3A00%2B02%3A00&endDate=2020-10-31T23%3A59%3A00%2B01%3A00

@Pipe({ name: "safeHtml" })
class SafeHtmlPipe implements PipeTransform {
  transform(value) {
    return value;
  }
}

describe("DriverCardComponent", () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        DriverCardComponent,
        TranslatePipe,
        PhoneNumberPipe,
        SafeHtmlPipe,
        OneviewPermittedDirective,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [],
    })
      .overrideComponent(DriverCardComponent, {})
      .compileComponents();
    fixture = TestBed.createComponent(DriverCardComponent);
    component = fixture.debugElement.componentInstance;
  });

  afterEach(() => {
    component.ngOnDestroy = function () {};
    fixture.destroy();
  });

  it("should run #constructor()", async () => {
    expect(component).toBeTruthy();
  });

  it("should run #ngOnInit()", async () => {
    component.ngOnInit();
  });

  xit("should run #ngOnChanges()", async () => {
    component.unloadData = component.unloadData || {};
    component.unloadData.push = jest.fn();
    component.reArrangeUnloadData = jest.fn().mockReturnValue({
      classifyUloadData: function () {},
    });
    component.reArrangeValidityData = jest.fn().mockReturnValue({
      classifyValidityData: function () {},
    });
    component.ngOnChanges({
      unloadData: {},
      validityData: {},
    });
    // expect(component.unloadData.push).toHaveBeenCalled();
    // expect(component.reArrangeUnloadData).toHaveBeenCalled();
    // expect(component.reArrangeValidityData).toHaveBeenCalled();
  });

  xit("should run #reArrangeUnloadData()", async () => {
    component.simplifiedData = component.simplifiedData || {};
    component.simplifiedData.push = jest.fn();
    component.unloadData = component.unloadData || {};
    component.unloadData = ["unloadData"];
    component.reArrangeUnloadData();
    // expect(component.simplifiedData.push).toHaveBeenCalled();
  });

  it("should run #undefined()", async () => {
    // Error: ERROR this JS code is invalid, "user.cards.forEach((el)"
    //     at Function.getFuncReturn (C:\Users\Diginov\AppData\Roaming\npm\node_modules\ngentest\src\util.js:333:13)
    //     at C:\Users\Diginov\AppData\Roaming\npm\node_modules\ngentest\src\util.js:421:30
    //     at Array.forEach (<anonymous>)
    //     at Function.getFuncParamObj (C:\Users\Diginov\AppData\Roaming\npm\node_modules\ngentest\src\util.js:404:26)
    //     at Function.getFuncArguments (C:\Users\Diginov\AppData\Roaming\npm\node_modules\ngentest\src\util.js:355:30)
    //     at Function.getFuncReturn (C:\Users\Diginov\AppData\Roaming\npm\node_modules\ngentest\src\util.js:340:34)
    //     at FuncTestGen.setMockData (C:\Users\Diginov\AppData\Roaming\npm\node_modules\ngentest\src\func-test-gen.js:159:31)
    //     at FuncTestGen.setMockData (C:\Users\Diginov\AppData\Roaming\npm\node_modules\ngentest\src\func-test-gen.js:90:12)
    //     at C:\Users\Diginov\AppData\Roaming\npm\node_modules\ngentest\index.js:70:17
    //     at Array.forEach (<anonymous>)
  });

  xit("should run #classifyUloadData()", async () => {
    component.simplifiedData = component.simplifiedData || {};
    component.simplifiedData = ["simplifiedData"];
    component.cardSlider = component.cardSlider || {};
    component.cardSlider.steps = {
      i: {},
    };
    component.card = component.card || {};
    component.card.dataList = {
      push: function () {},
    };
    component.card.date = "date";
    component.card.displayName = "displayName";
    component.classifyUloadData();
  });

  xit("should run #classifyValidityData()", async () => {
    component.simplifiedValidityData = component.simplifiedValidityData || {};
    component.simplifiedValidityData = ["simplifiedValidityData"];
    component.validitySlider = component.validitySlider || {};
    component.validitySlider.steps = {
      i: {},
    };
    component.validity = component.validity || {};
    component.validity.dataList = {
      push: function () {},
    };
    component.validity.date = "date";
    component.validity.displayName = "displayName";
    component.classifyValidityData();
  });
});
