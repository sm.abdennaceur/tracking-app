import { Component, OnInit, Input, SimpleChanges, ViewChild } from '@angular/core';
import { EmployeesService } from '../../../core/services/employees.service';
import { IDriverUnload, IDriverValidity, IEmployee } from '../../../shared/interfaces';
import { TimeParamsService } from '../../../shared/time-service/time-params.service';

@Component({
  selector: 'driver-card',
  templateUrl: './driver-card.component.html',
  styleUrls: ['./driver-card.component.scss'],
})
export class DriverCardComponent implements OnInit {
  @Input() unloadData: IDriverUnload[];
  @Input() validityData: IDriverValidity[];
  simplifiedData = [];
  simplifiedValidityData = [];
  @ViewChild('cardslider', { static: false }) cardSlider;
  @ViewChild('validityslider', { static: false }) validitySlider;
  card = [
    { value: 3.5, displayName: '', date: '', dataList: [] },
    { value: 10.5, displayName: '', date: '', dataList: [] },
    { value: 17.5, displayName: '', date: '', dataList: [] },
    { value: 24.5, displayName: '', date: '', dataList: [] },
    { value: 29, displayName: '', date: '', dataList: [] },
  ];
  validity = [
    { value: 0.5, displayName: '', date: '', dataList: [] },
    { value: 1.5, displayName: '', date: '', dataList: [] },
    { value: 2.5, displayName: '', date: '', dataList: [] },
    { value: 3.5, displayName: '', date: '', dataList: [] },
    { value: 4.5, displayName: '', date: '', dataList: [] },
    { value: 5.5, displayName: '', date: '', dataList: [] },
  ];
  constructor(private employeeService: EmployeesService, public timeParams: TimeParamsService) {}

  ngOnInit() {}
  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.

    if (changes.unloadData) {
      // this.unloadData.push({
      //   lastUnloadDate: "2020-11-02T00:00:00",
      //   displayName: "1 driver dechargement TEST",
      //   driverId: "20125",
      // })
      // this.unloadData.push({
      //   lastUnloadDate: "2020-10-26T00:00:00",
      //   displayName: "2 driver dechargement TEST",
      //   driverId: "20126",
      // })
      // this.unloadData.push({
      //   lastUnloadDate: "2020-10-19T00:00:00",
      //   displayName: "3 driver dechargement TEST",
      //   driverId: "20127",
      // })
      // this.unloadData.push({
      //   lastUnloadDate: "2020-10-12T00:00:00",
      //   displayName: "4 driver dechargement TEST",
      //   driverId: "20128",
      // })
      // this.unloadData.push({
      //   lastUnloadDate: "2020-10-06T00:00:00",
      //   displayName: "5 driver dechargement TEST",
      //   driverId: "20129",
      // })

      this.reArrangeUnloadData().classifyUloadData();
    }
    if (changes.validityData) {
      // let cards: any[] = [
      //   {
      //     id: "cardOne",
      //     validityEndDate: "2024-12-01T00:00:00",
      //     validityStartDate: "2014-10-09T00:00:00",
      //   },
      //   {
      //     id: "cardTwo",
      //     validityEndDate: "2023-11-08T00:00:00",
      //     validityStartDate: "2014-10-09T00:00:00",
      //   },
      //   {
      //     id: "cardThree",
      //     validityEndDate: "2022-11-08T00:00:00",
      //     validityStartDate: "2014-10-09T00:00:00",
      //   },
      //   {
      //     id: "cardFour",
      //     validityEndDate: "2021-12-08T00:00:00",
      //     validityStartDate: "2014-10-09T00:00:00",
      //   },
      //   {
      //     id: "cardFive",
      //     validityEndDate: "2021-10-03T00:00:00",
      //     validityStartDate: "2014-10-09T00:00:00",
      //   },
      //   {
      //     id: "cardSix",
      //     validityEndDate: "2020-11-03T00:00:00",
      //     validityStartDate: "2014-10-09T00:00:00",
      //   },
      // ]
      // this.validityData.push({
      //   driverFullName: "Test Karim",
      //   driverId: "4512",
      //   cards: cards
      // })
      this.reArrangeValidityData().classifyValidityData();
    }
  }
  changeDate(dateToChange) {
    let date;
    let paramUser = JSON.parse(localStorage.getItem('user_params'));
    if (paramUser.heureUTC == true) {
      date = this.timeParams.convertToLocalTime(dateToChange.toString());
    } else if (paramUser.heureUTC == false) {
      if (String(paramUser.fuseauxHoraire.charAt(0) == '-' || String(paramUser.fuseauxHoraire.charAt(0) == '+'))) {
        date = this.timeParams.convertToUTC(
          String(paramUser.fuseauxHoraire.charAt(0)),
          dateToChange.toString(),
          Number(String(paramUser.fuseauxHoraire).substring(1, 3)),
          Number(String(paramUser.fuseauxHoraire).substring(4, 6)),
          Number(paramUser.decalageMinuteHeureEte)
        );
      } else {
        date = this.timeParams.convertToUTC(
          '+',
          dateToChange.toString(),
          Number(String(paramUser.fuseauxHoraire).substring(0, 2)),
          Number(String(paramUser.fuseauxHoraire).substring(3, 5)),
          Number(paramUser.decalageMinuteHeureEte)
        );
      }
    }
    return date;
  }
  reArrangeUnloadData() {
    const nowTime = new Date().getTime();
    this.simplifiedData = [];
    this.unloadData.forEach((el) => {
      const time = new Date(el.lastUnloadDate).getTime();
      const timeExpiry = new Date(el.lastUnloadDate).getTime();
      el.lastUnloadDate = this.changeDate(el.lastUnloadDate);
      el.expirationDate = this.changeDate(el.expirationDate);
      this.simplifiedData.push({
        days: (nowTime - timeExpiry) / (1000 * 60 * 60 * 24),
        daysUnload: (nowTime - time) / (1000 * 60 * 60 * 24),
        lastUnload: el.lastUnloadDate,
        displayName: el.displayName,
        date: el.expirationDate,
        driverId: el.driverId,
        id: '',
      });
    });
    this.employeeService.getListEmployee().subscribe((employees) => {
      const AllEmployees = employees as IEmployee[];
      const IdsEmployees = AllEmployees.map((x) => x.id);
      this.simplifiedData.forEach((x, index) => {
        AllEmployees.forEach((y, indexy) => {
          if (x.driverId == y.driverId) {
            this.simplifiedData[index].id = AllEmployees[indexy].id;
          }
        });
      });
    });
    return this;
  }
  getEmployees() {}

  reArrangeValidityData() {
    const nowTime = new Date().getTime();
    this.simplifiedValidityData = [];

    this.validityData.forEach((user) => {
      user.cards.forEach((el) => {
        const time = new Date(el.validityEndDate).getTime();
        el.validityEndDate = this.changeDate(el.validityEndDate);
        this.simplifiedValidityData.push({
          days: -((nowTime - time) / (1000 * 60 * 60 * 24 * 365)),
          lastUnload: el.validityEndDate,
          displayName: user.driverFullName,
          date: el.validityEndDate,
          driverId: user.driverId,
          id: '',
        });
      });
    });

    this.employeeService.getListEmployee().subscribe((employees) => {
      const AllEmployees = employees as IEmployee[];
      const IdsEmployees = AllEmployees.map((x) => x.id);
      this.simplifiedValidityData.forEach((x, index) => {
        AllEmployees.forEach((y, indexy) => {
          if (x.driverId == y.driverId) {
            this.simplifiedValidityData[index].id = AllEmployees[indexy].id;
          }
        });
      });
    });

    return this;
  }
  classifyUloadData() {
    if (this.cardSlider) {
      this.simplifiedData.forEach((el) => {
        let i = 0;
        while (el.days > this.cardSlider.steps[i] && this.card.length > i - 1) {
          i++;
        }
        this.card[i - 1].dataList.push(el);
        if ((new Date(this.card[i - 1].date).getTime() || 0) < new Date(el.date).getTime()) {
          this.card[i - 1].date = el.date.split('T')[0];
          this.card[i - 1].displayName = el.displayName;
        }
      });
    }
  }
  classifyValidityData() {
    this.simplifiedValidityData.forEach((el) => {
      let i = 0;
      if (this.validitySlider) {
        while (el.days > this.validitySlider.steps[i] && this.validity.length > i - 1) {
          i++;
        }
        i = this.validity.length - i;
        if (i === 0) {
          i++;
        }

        this.validity[i - 1].dataList.push(el);
        if ((new Date(this.validity[i - 1].date).getTime() || 0) < new Date(el.date).getTime()) {
          this.validity[i - 1].date = el.date.split('T')[0];
          this.validity[i - 1].displayName = el.displayName;
        }
      }
    });
  }
}
