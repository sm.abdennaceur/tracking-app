import { Component, OnInit, Input, ViewChild, TemplateRef, ViewContainerRef } from '@angular/core';
import { Overlay, OverlayConfig, OverlayContainer } from '@angular/cdk/overlay';
import { ComponentPortal, TemplatePortal } from '@angular/cdk/portal';
import { NavbarService } from '../../../core/services/navbar.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

export interface Dimensions {
  width?: number;
  height?: number;
  maxHeight?: number;
  maxWidth?: number;
}

@Component({
  selector: 'slider-timeline',
  templateUrl: './slider-timeline.component.html',
  styleUrls: ['./slider-timeline.component.scss'],
})
export class SliderTimelineComponent implements OnInit {
  @Input() numberMax: number;
  @Input() step: number;
  @Input() items: any = [];
  @Input() dropDownTitle: string = '';
  @Input() dateLabel: string = '';
  @Input() unite: string = 'jours';
  @Input() validityEmployeesSlider: boolean;
  @Input() driverCardunloading: boolean;
  @Input() driverCardvalidities: boolean;
  @Input() tachygraphesunloading: boolean;
  @Input() tachygraphesvalidities: boolean;
  @ViewChild('templatePortalContent', { static: false }) templatePortalContent: TemplateRef<any>;
  private unsubscribe = new Subject();

  steps: any = [];
  menuData: any;
  constructor(
    public overlay: Overlay,
    private _viewContainerRef: ViewContainerRef,
    private OverlayContainer: OverlayContainer,
    private navbar: NavbarService,
    public router: Router
  ) {}

  ngOnInit() {
    this.generateSteps();
    this.menuData = {
      menuItems: [],
    };
  }
  ngOnDestroy(): void {
    this._viewContainerRef.clear();
  }
  generateSteps() {
    for (let index = 0; index < this.numberMax; index += this.step) {
      this.steps.push(index);
    }
    // if(this.step % this.numberMax != 0){
    //   this.steps.push(this.numberMax)
    // }
  }
  getLeftPosition(step: any) {
    if (this.validityEmployeesSlider) {
      if (step > this.numberMax - 1) {
        return ((this.numberMax - step) / (this.numberMax * 1.0)) * 100;
      }
      return ((this.numberMax - step - 1) / (this.numberMax * 1.0)) * 100;
    }
    return (step / (this.numberMax * 1.0)) * 100;
  }

  getLeftStepPosition(step: any) {
    return (step / (this.numberMax * 1.0)) * 100;
  }
  getClass(item) {
    if (this.validityEmployeesSlider) {
      // item = this.numberMax-item;
      item = this.numberMax - item - 1;
      if (item < this.steps[this.steps.length - 2]) {
        return 'safe';
      } else if (item < this.steps[this.steps.length - 1]) {
        return 'warning';
      } else {
        return 'danger';
      }
    }

    if (item < this.steps[this.steps.length - 2]) {
      return 'safe';
    } else if (item < this.steps[this.steps.length - 1]) {
      return 'warning';
    } else {
      return 'danger';
    }
  }

  getMenuPosition(target: HTMLElement, width) {
    const position = {
      top: 0,
      left: 0,
    };
    position.left = target.getBoundingClientRect().left - width / 2 + 10;
    position.top = target.getBoundingClientRect().top + target.offsetHeight + 32;
    return position;
  }
  getPosition(target, width) {
    const left = this.getMenuPosition(target, width).left;
    const top = this.getMenuPosition(target, width).top;
    return {
      positionStrategy: this.overlay
        .position()
        .global()
        .left(left > 0 ? left + 'px' : '0')
        .top(top + 'px'),
      top: top,
      left: left,
    };
  }
  overlayRef;
  openDropDown(event, data: any, d: Dimensions = {}) {
    this.menuData = data;
    const WIDTH = d.width || 250;
    const HEIGHT = d.height;
    const { positionStrategy, top, left } = this.getPosition(event, WIDTH);
    const overlayConfig = new OverlayConfig({
      positionStrategy,
      hasBackdrop: true,
      backdropClass: 'slide-menu-backdrop',
      height: `${HEIGHT}px`,
      width: `${WIDTH}px`,
      maxWidth: `${d.maxWidth}px`,
      maxHeight: d.maxHeight || ' calc( 100vh - ' + (top + 47) + 'px )',
    });
    this.overlayRef = this.overlay.create(overlayConfig);

    const MenuPortal = new TemplatePortal(this.templatePortalContent, this._viewContainerRef);
    this.overlayRef.attach(MenuPortal);
    this.overlayRef.backdropClick().subscribe((_) => {
      this.menuData = null;
      this.overlayRef.dispose();
    });

    const overlayContainerClasses = this.OverlayContainer.getContainerElement().classList;
    overlayContainerClasses.add('global-slide-class');
  }
}
