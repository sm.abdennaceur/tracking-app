// tslint:disable
import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import {
  Pipe,
  PipeTransform,
  Injectable,
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA,
  Directive,
  Input,
  Output,
} from "@angular/core";
import { isPlatformBrowser } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { By } from "@angular/platform-browser";
import { Observable, of as observableOf, throwError } from "rxjs";

import { Component } from "@angular/core";
import { DashboardComponent } from "./dashboard.component";
import { NavbarService } from "../core/services/navbar.service";
import { DashboardService } from "../core/services/dashboard.service";

@Injectable()
class MockNavbarService {}

@Injectable()
class MockDashboardService {}

@Directive({ selector: "[oneviewPermitted]" })
class OneviewPermittedDirective {
  @Input() oneviewPermitted;
}

@Pipe({ name: "translate" })
class TranslatePipe implements PipeTransform {
  transform(value) {
    return value;
  }
}

@Pipe({ name: "phoneNumber" })
class PhoneNumberPipe implements PipeTransform {
  transform(value) {
    return value;
  }
}

@Pipe({ name: "safeHtml" })
class SafeHtmlPipe implements PipeTransform {
  transform(value) {
    return value;
  }
}

describe("DashboardComponent", () => {
  let fixture;
  let component;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [
        DashboardComponent,
        TranslatePipe,
        PhoneNumberPipe,
        SafeHtmlPipe,
        OneviewPermittedDirective,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [
        { provide: NavbarService, useClass: MockNavbarService },
        { provide: DashboardService, useClass: MockDashboardService },
      ],
    })
      .overrideComponent(DashboardComponent, {})
      .compileComponents();
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.debugElement.componentInstance;
  });

  afterEach(() => {
    component.ngOnDestroy = function () {};
    fixture.destroy();
  });

  it("should run #constructor()", async () => {
    expect(component).toBeTruthy();
  });

  xit("should run #itemChange()", async () => {
    component.itemChange({}, {});
  });

  xit("should run #itemResize()", async () => {
    component.itemResize({}, {});
  });

  xit("should run #ngOnInit()", async () => {
    component.navbar = component.navbar || {};
    component.navbar.setNavBar = jest.fn();
    component.setBlocks = jest.fn();
    component.fetchData = jest.fn();
    component.ngOnInit();
    // expect(component.navbar.setNavBar).toHaveBeenCalled();
    // expect(component.setBlocks).toHaveBeenCalled();
    // expect(component.fetchData).toHaveBeenCalled();
  });

  it("should run #setBlocks()", async () => {
    component.dashboard = component.dashboard || {};
    component.setBlocks();
  });

  it("should run #changedOptions()", async () => {
    component.options = component.options || {};
    component.options.api = {
      optionsChanged: function () {},
    };
    component.changedOptions();
  });

  xit("should run #removeItem()", async () => {
    component.dashboard = component.dashboard || {};
    component.dashboard.splice = jest.fn();
    component.dashboard.indexOf = jest.fn();
    component.removeItem({});
    // expect(component.dashboard.splice).toHaveBeenCalled();
    // expect(component.dashboard.indexOf).toHaveBeenCalled();
  });

  it("should run #addItem()", async () => {
    component.addItem();
  });

  xit("should run #fetchData()", async () => {
    component.DS = component.DS || {};
    component.DS.getUnloadList = jest.fn().mockReturnValue(observableOf({}));
    component.DS.getCardList = jest.fn().mockReturnValue(observableOf({}));
    component.DS.getChronotachygraphUnloadList = jest
      .fn()
      .mockReturnValue(observableOf({}));
    component.DS.getChronotachygraphList = jest
      .fn()
      .mockReturnValue(observableOf({}));
    component.driverData = component.driverData || {};
    component.driverData.unloadList = "unloadList";
    component.driverData.validity = "validity";
    component.chronoData = component.chronoData || {};
    component.chronoData.unloadList = "unloadList";
    component.chronoData.validity = "validity";
    component.fetchData();
    // expect(component.DS.getUnloadList).toHaveBeenCalled();
    // expect(component.DS.getCardList).toHaveBeenCalled();
    // expect(component.DS.getChronotachygraphUnloadList).toHaveBeenCalled();
    // expect(component.DS.getChronotachygraphList).toHaveBeenCalled();
  });

  xit("should run #ngOnDestroy()", async () => {
    component.unsubscribe = component.unsubscribe || {};
    component.unsubscribe.next = jest.fn();
    component.unsubscribe.complete = jest.fn();
    component.ngOnDestroy();
    // expect(component.unsubscribe.next).toHaveBeenCalled();
    // expect(component.unsubscribe.complete).toHaveBeenCalled();
  });
});
