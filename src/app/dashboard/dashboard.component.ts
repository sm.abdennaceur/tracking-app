import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../core/services/navbar.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BreadcrumbsService } from '../core/services/breadcrumbs.service';

import { GridsterConfig, GridsterItem } from 'angular-gridster2';
import { DashboardService } from '../core/services/dashboard.service';
import {
  IChronoTachygraphValidity,
  IChronoTachygraphUnload,
  IPaginatedTachygraphs,
  IDriverValidity,
  IDriverUnload,
} from '../shared/interfaces';
import { EmployeesService } from '../core/services/employees.service';
import { ParamatersService } from '../core/services/paramaters.service';
import jwt_decode from 'jwt-decode';
import { AuthService } from '../core/auth/auth.service';
import { TranslateService } from '@ngx-translate/core';

export interface IChronoData {
  validity: IChronoTachygraphValidity[];
  unloadList: IChronoTachygraphUnload[];
}
export interface IDriverData {
  validity: any[];
  unloadList: IDriverUnload[];
}
const BLOCK_HEIGHT = 400;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  private unsubscribe = new Subject();

  options: GridsterConfig;
  dashboard: Array<GridsterItem>;
  containerHaight = 100;
  static itemChange(item, itemComponent) {
    // console.info('itemChanged', item, itemComponent);
  }

  static itemResize(item, itemComponent) {
    // console.info('itemResized', item, itemComponent);
  }

  driverData: IDriverData = {
    unloadList: [],
    validity: [],
  };
  chronoData: IChronoData = {
    unloadList: [],
    validity: [],
  };
  chronotachyList: any = [];

  email;
  constructor(
    public navbar: NavbarService,
    public DS: DashboardService,
    private employeeService: EmployeesService,
    private userparams: ParamatersService,
    public auth: AuthService
  ) {}

  ngOnInit() {
    this.navbar.setNavBar({
      dataSelector: false,
      search: false,
    });
     this.email = this.getDecodedAccessToken(localStorage.getItem('Strada_Token')).email;
   // const item = JSON.parse(sessionStorage.getItem((sessionStorage.key(0)))).profile;
   // this.email = item.email;
     this.getAssociatedData(this.email);
    this.setBlocks();
    this.fetchData();
  }
  setBlocks() {
    this.dashboard = [
      { cols: 1, rows: 1, y: 0, x: 0, resizeEnabled: false, blockType: 'driver' },
      { cols: 1, rows: 1, y: 0, x: 1, resizeEnabled: false, blockType: 'chronotachygraph' },
    ];
    this.options = {
      itemChangeCallback: DashboardComponent.itemChange,
      itemResizeCallback: DashboardComponent.itemResize,
      draggable: {
        enabled: true,
      },
      pushItems: true,
      resizable: {
        enabled: false,
      },
      minCols: this.dashboard.length > 2 ? 3 : 2,
      maxCols: this.dashboard.length > 2 ? 3 : 2,
      minRows: Math.ceil((this.dashboard.length * 1.0) / 3),
      maxRows: Math.ceil((this.dashboard.length * 1.0) / 3),
      maxItemCols: 1,
      minItemCols: 1,
      maxItemRows: 1,
      minItemRows: 1,
      margin: 30,
      outerMargin: true,
      outerMarginTop: 50,
    };
    this.containerHaight = Math.ceil((this.dashboard.length * 1.0) / 3) * BLOCK_HEIGHT;
  }
  changedOptions() {
    this.options.api.optionsChanged();
  }

  removeItem(item) {
    this.dashboard.splice(this.dashboard.indexOf(item), 1);
  }

  addItem() {
    // this.dashboard.push({});
  }

  fetchData() {
    this.DS.getUnloadList().subscribe((data) => {
      this.driverData.unloadList = data;
    });
    this.DS.getCardList().subscribe((data) => {
      this.driverData.validity = data;
    });
    this.DS.getChronotachygraphUnloadList().subscribe((data: IPaginatedTachygraphs) => {
      this.chronoData.unloadList = data.items;
    });
    this.DS.getChronotachygraphList().subscribe((data: IChronoTachygraphValidity[]) => {
      this.chronoData.validity = data;
    });
    // this.chronoData.validity = [];
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
  userData: any;
  getAssociatedData(mail) {
    this.employeeService.getUserDetails(mail).subscribe((data: any) => {
      this.userData = data;
      this.getIfParamatersAreCreated(data.id);
    });
  }

  getIfParamatersAreCreated(testUserId: string) {
    this.userparams.testParamatersAreCreated(testUserId).subscribe((data) => {
      if (data == true) {
        this.getAndSaveUserParams(testUserId);
      } else {
        this.userparams.createUserParamaters(testUserId).subscribe((data) => {
          this.getAndSaveUserParams(testUserId);
        });
      }
    });
  }

  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }
  getAndSaveUserParams(idUser: string) {
    this.userparams
      .getUserParamaters(idUser)
      .toPromise()
      .then((parametres) => {
        localStorage.setItem('user_params', JSON.stringify(parametres));
      });
  }
}
