import { NgModule } from '@angular/core';

import { DashboardRoutingModule } from './dashboard.routing';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { SharedModule } from '../shared/shared.module';
import { DriverCardComponent } from './blocks/driver-card/driver-card.component';
import { ChronotachygraphCardComponent } from './blocks/chronotachygraph-card/chronotachygraph-card.component';
import { GridsterModule } from 'angular-gridster2';
import { CommonModule } from '@angular/common';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { SliderTimelineComponent } from './blocks/slider-timeline/slider-timeline.component';
import { FullscreenOverlayContainer, OverlayContainer } from '@angular/cdk/overlay';
// translations Module
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';

@NgModule({
  declarations: [
    DashboardRoutingModule.components,
    DriverCardComponent,
    ChronotachygraphCardComponent,
    SliderTimelineComponent,
  ],
  imports: [
    DashboardRoutingModule,
    AngularMaterialModule,
    SharedModule,
    GridsterModule,
    CommonModule,
    AngularSvgIconModule,
    TranslateModule,
  ],
  // providers: [{provide: OverlayContainer, useClass: FullscreenOverlayContainer}],
})
export class DashboardModule {}
