import { TestBed } from '@angular/core/testing';
import { DashboardRoutingModule } from './dashboard.routing';
import { DashboardComponent } from './dashboard.component';
describe('DashboardRoutingModule', () => {
  let pipe: DashboardRoutingModule;
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [DashboardRoutingModule] });
    pipe = TestBed.get(DashboardRoutingModule);
  });
  it('can load instance', () => {
    expect(pipe).toBeTruthy();
  });
  it('components defaults to: [DashboardComponent]', () => {
    expect(DashboardRoutingModule.components).toEqual([DashboardComponent]);
  });
});
