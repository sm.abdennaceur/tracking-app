import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { DragDropDirective } from './drag-drop.directive';

@Component({
  template: `
    <div>Without Directive</div>
    <div appDragDrop>Default</div>
  `
})
class TestComponent {}

describe('DragDropDirective', () => {
  let fixture: ComponentFixture<TestComponent>;
  let elementsWithDirective: Array<DebugElement>;
  let bareElement: DebugElement;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DragDropDirective, TestComponent]
    });
    fixture = TestBed.createComponent(TestComponent);
    fixture.detectChanges();
    elementsWithDirective = fixture.debugElement.queryAll(
      By.directive(DragDropDirective)
    );
    bareElement = fixture.debugElement.query(By.css(':not([appDragDrop])'));
  });
  it('should have bare element', () => {
    expect(bareElement).toBeTruthy();
  });
  it('should have 1 element(s) with directive', () => {
    expect(elementsWithDirective.length).toBe(1);
  });
});
