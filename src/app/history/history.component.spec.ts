import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NavbarService } from '../core/services/navbar.service';
import { HistoryComponent } from './history.component';
describe('HistoryComponent', () => {
  let component: HistoryComponent;
  let fixture: ComponentFixture<HistoryComponent>;
  beforeEach(() => {
    const navbarServiceStub = () => ({
      click: { pipe: () => ({ subscribe: f => f({}) }) },
      currentDate: { subscribe: f => f({}) },
      setNavBar: object => ({})
    });
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [HistoryComponent],
      providers: [{ provide: NavbarService, useFactory: navbarServiceStub }]
    });
    fixture = TestBed.createComponent(HistoryComponent);
    component = fixture.componentInstance;
  });
  it('can load instance', () => {
    expect(component).toBeTruthy();
  });
  describe('ngOnInit', () => {
    it('makes expected calls', () => {
      const navbarServiceStub: NavbarService = fixture.debugElement.injector.get(
        NavbarService
      );
      spyOn(navbarServiceStub, 'setNavBar').and.callThrough();
      component.ngOnInit();
      expect(navbarServiceStub.setNavBar).toHaveBeenCalled();
    });
  });
});
