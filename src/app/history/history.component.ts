import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../core/services/navbar.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  
  private unsubscribe = new Subject();
  constructor(public navbar:NavbarService) { 
    this.navbar.click
    .pipe(takeUntil(this.unsubscribe)) // unsubscribe to prevent memory leak
    .subscribe(isOpen => {
      alert("Historyclicked");
  });
    this.navbar.currentDate
    // .pipe(takeUntil(this.unsubscribe)) // unsubscribe to prevent memory leak
    .subscribe(isOpen => {
  });
  }

  ngOnInit() {
   
    this.navbar.setNavBar(
      {
         button:{
           title:"Nouveau véhicule dynamicment",
           faIcon:"plus",
        //   callback:function (){
        //     alert("history");

        //   }
        },
        dataSelector:true,
        actionsList:[
          {
            title:"BD-587-TR",
            faIcon:"",
            // callback:function(){
            //   alert("BD-587-TR"); 
            // }
          },
          {
            title:"RT-458-RR",
            faIcon:"",
            // callback:function(){
            //   alert("RT-458-RR"); 
            // }
          },
          {
            title:"SD-456-RG",
            faIcon:"",
            // callback:function(){
            //   alert("SD-456-RG"); 
            // }
          },
          {
            title:"SD-456-TY",
            faIcon:"",
            // callback:function(){
            //   alert("SD-456-TY"); 
            // }
          },
        ]
      });
  }

  ngOnDestroy(){
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

}
