import { TestBed } from '@angular/core/testing';
import { HistoryModule } from './history.module';
describe('HistoryModule', () => {
  let pipe: HistoryModule;
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [HistoryModule] });
    pipe = TestBed.get(HistoryModule);
  });
  it('can load instance', () => {
    expect(pipe).toBeTruthy();
  });
});
