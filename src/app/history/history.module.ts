import { NgModule } from '@angular/core';

import { HistoryRoutingModule } from './history.routing';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [HistoryRoutingModule.components],
  imports: [ HistoryRoutingModule, AngularMaterialModule, SharedModule ]
})
export class HistoryModule { }
