import { TestBed } from '@angular/core/testing';
import { HistoryRoutingModule } from './history.routing';
import { HistoryComponent } from './history.component';
describe('HistoryRoutingModule', () => {
  let pipe: HistoryRoutingModule;
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [HistoryRoutingModule] });
    pipe = TestBed.get(HistoryRoutingModule);
  });
  it('can load instance', () => {
    expect(pipe).toBeTruthy();
  });
  it('components defaults to: [HistoryComponent]', () => {
    expect(HistoryRoutingModule.components).toEqual([HistoryComponent]);
  });
});
