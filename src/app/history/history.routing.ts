import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HistoryComponent } from './history.component';

const routes: Routes = [
  {

    path: '',
    children: [{
      path: 'history',
      component: HistoryComponent
    }]
  }

];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoryRoutingModule {
  static components = [HistoryComponent];
}
