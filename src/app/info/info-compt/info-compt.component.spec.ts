import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoComptComponent } from './info-compt.component';

describe('InfoComptComponent', () => {
  let component: InfoComptComponent;
  let fixture: ComponentFixture<InfoComptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoComptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoComptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
