import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoComptComponent } from './info-compt/info-compt.component';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: InfoComptComponent,
    // data: {
    //   breadcrumb: 'info',
    // },
  },
];

@NgModule({
  declarations: [InfoComptComponent],

  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule ],
})
export class InfoModule { }

 
 