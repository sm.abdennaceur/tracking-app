import { NavigationStart, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MenuEntry, menuParams } from '@strada/sw-menusidebar/lib/menusidebar/menusidebar.service';
import { MenusidebarService } from '../../core/services/menusidebar.service';
@Component({
  selector: 'app-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss'],
})
export class AdminLayoutComponent implements OnInit {
  menuEntries: MenuEntry[] = [];
  listeUniverses: any = [];
  userMenuParams: menuParams = {
    universe: 'TRACKING',
    backgrounColor: '#3f4662',
  };
  constructor(public sidebarService: MenusidebarService, public route: Router) {}

  ngOnInit() {
    this.menuEntries = [...this.sidebarService.getSidebarEntries()];
    this.route.events.subscribe((e) => {
      if (e instanceof NavigationStart) {
        console.log('here');

        if (
          this.route.url.includes('tracking') &&
          !e.url.includes('/tracking') &&
          e.url != '/' &&
          e.url != '/strada' &&
          e.url != '/tms' &&
          e.url != '/time' &&
          e.url != '/tracking' &&
          e.url != '/efact' &&
          e.url != '/pricer'
        ) {
          this.route.navigate(['tracking/' + e.url]);
        }
      }
    });
  }
}
