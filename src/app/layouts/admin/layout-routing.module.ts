import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../../core/auth/auth-guard.service';
import { AdminLayoutComponent } from './admin-layout.component';
import { AuthGuard } from '@strada/lib-authentification';

const routes: Routes = [
  {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [AuthGuardService],
    children: [
      {
        path: '',
        loadChildren: () => import('../../dashboard/dashboard.module').then((m) => m.DashboardModule),
        data: { breadcrumb: 'L_Breadcrumb_Dashboard' },
      },

      {
        path: 'track',
        data: { breadcrumb: 'Tracking' },

        loadChildren: () => import('../../tracking/tracking.module').then((m) => m.TrackingModule),
      },
      {
        path: 'track-Leaflet',
        data: { breadcrumb: 'Tracking' },

        loadChildren: () => import('../../tracking-leaflet-map/tracking-leaflet-map.module').then((m) => m.TrackingLeafletMapModule),
      },
      {
        path: 'track-hereMap',
        data: { breadcrumb: 'Tracking' },

        loadChildren: () => import('../../tracking-here-map/tracking-here-map.module').then((m) => m.TrackingHereMapModule),
      },
      // {
      //   path: '',
      //   data: { breadcrumb: 'Historique' },
      //   loadChildren: () => import('../../history/history.module').then((m) => m.HistoryModule),
      // },
      {
        path: 'Center_information',
        data: { breadcrumb: "Center d'information" },
        loadChildren: () => import('../../info/info.module').then((m) => m.InfoModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
