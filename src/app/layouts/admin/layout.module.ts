import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChartModule } from 'primeng/chart';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { LibMenusidebarModule } from '@strada/sw-menusidebar';
import { SharedModule } from '../../shared/shared.module';
import { NavbarComponent } from '../../core/navbar/navbar.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { AdminRoutingModule } from './layout-routing.module';
import { AdminLayoutComponent } from './admin-layout.component';
import { SidebarComponent } from '../../core/sidebar/sidebar.component';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient } from '@angular/common/http';
import { AngularMaterialModule } from '@tmsApp/login/services/angular-material.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [AdminLayoutComponent, SidebarComponent, NavbarComponent],
  imports: [ 
    CommonModule,
    ChartModule,
    AngularSvgIconModule.forRoot(),
    AdminRoutingModule,
    TranslateModule,
    AngularMaterialModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    LibMenusidebarModule,
    SharedModule,
    FontAwesomeModule,
  ],
  providers: [],
  exports: [TranslateModule, SharedModule],
})
export class AdminModule {}
