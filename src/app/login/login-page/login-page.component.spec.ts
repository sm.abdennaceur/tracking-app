import { TestBed, ComponentFixture } from "@angular/core/testing";
import {
  HttpClientTestingModule,
  HttpTestingController,
} from "@angular/common/http/testing";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { LoginPageComponent } from "./login-page.component";
import { AngularMaterialModule } from "src/app/angular-material/angular-material.module";
import { RouterModule } from "@angular/router";
import { AuthService } from "src/app/core/auth/auth.service";
// import { LoginPageComponent, User } from "./login-page.component";
describe("Component: Login", () => {
  let component: LoginPageComponent;
  let fixture: ComponentFixture<LoginPageComponent>;
  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    // refine the test module by declaring the test component
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        FormsModule,
        AngularMaterialModule,
        RouterModule.forRoot([]),
        HttpClientTestingModule,
      ],
      declarations: [LoginPageComponent],
      providers: [AuthService],
    });

    // create component and test fixture
    fixture = TestBed.createComponent(LoginPageComponent);

    // get test component from the fixture
    component = fixture.componentInstance;
    component.ngOnInit();
  });

  it("form invalid when empty", () => {
    expect(component.userForm.valid).toBeFalsy();
  });

  it("email field validity", () => {
    let errors = {};
    let email = component.userForm.controls["email"];
    expect(email.valid).toBeFalsy();

    // Email field is required
    errors = email.errors || {};
    expect(errors["required"]).toBeTruthy();

    // Set email to something
    email.setValue("test");
    errors = email.errors || {};
    expect(errors["required"]).toBeFalsy();
    expect(errors["pattern"]).toBeTruthy();

    // Set email to something correct
    email.setValue("admin@strada.com");
    errors = email.errors || {};
    expect(errors["required"]).toBeFalsy();
    expect(errors["pattern"]).toBeFalsy();
  });

  it("password field validity", () => {
    let errors = {};
    let password = component.userForm.controls["password"];

    // Password field is required
    errors = password.errors || {};
    expect(errors["required"]).toBeTruthy();

    // Set password to something
    // password.setValue("strada");
    // errors = password.errors || {};
    // expect(errors["required"]).toBeFalsy();
    // expect(errors["pattern"]).toBeTruthy();

    // Set password to something correct
    password.setValue("Strada123");
    errors = password.errors || {};
    expect(errors["required"]).toBeFalsy();
    expect(errors["pattern"]).toBeFalsy();
  });

  it("submitting a form emits a user", () => {
    expect(component.userForm.valid).toBeFalsy();
    component.userForm.controls["email"].setValue("admin@strada.com");
    component.userForm.controls["password"].setValue("stradA123");
    expect(component.userForm.valid).toBeTruthy();

    //   // Trigger the login function
    //   component.submit(component.userForm);

    //   // Now we can check to make sure the emitted value is correct
    //   // expect(component.user.email).toBe('admin@strada.com');
    //   // expect(component.user.password).toBe("strada123");
  });
});
