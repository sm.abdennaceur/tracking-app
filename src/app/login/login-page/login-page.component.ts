import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
  FormGroupDirective,
  NgForm,
} from '@angular/forms';
import { Router } from '@angular/router';
import { ErrorStateMatcher } from '@angular/material/core';
import { AUTH_CONFIG } from '../services/auth.config';
import { AuthService } from '../services/auth.service';

/** Error when invalid control is dirty, touched, or submitted. */
// export class MyErrorStateMatcher implements ErrorStateMatcher {
//   isErrorState(
//     control: FormControl | null,
//     form: FormGroupDirective | NgForm | null
//   ): boolean {
//     const isSubmitted = form && form.submitted;
//     return !!(
//       control &&
//       control.invalid &&
//       ((control.dirty && control.touched) || isSubmitted)
//     );
//   }
// }

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit {
  public userForm: FormGroup;
  // matcher = new MyErrorStateMatcher();
  passwordStrength: any = {
    name: 'none',
    pattern: /.+/,
    label: '',
  };
  constructor(
    private router: Router,
    public auth: AuthService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.createForm();
    this.auth.getPasswordStrength().subscribe((res) => {
      // console.log(res);
      this.passwordStrength = this.auth.getPasswordStrengthDetails(
        this.getPasswordStrength(res)
      );
      this.createForm();
    });
  }

  createForm() {
    // console.log(this.passwordStrength);

    this.userForm = this.fb.group({
      email: [
        '',
        [
          Validators.required,
          Validators.pattern(
            /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          ),
        ],
      ],
      password: [
        '',
        Validators.compose([
          Validators.pattern(this.passwordStrength.pattern),
          Validators.required,
        ]),
      ],
    });
  }

  submit(userForm: FormGroup) {
    const email = userForm.value.email;
    const pwd = userForm.value.password;
    this.auth.loginCurrentUI({
      id: email,
      password: pwd,
    });
    userForm.statusChanges.subscribe((state) => {
      this.auth.resetError();
    });
  }

  getPasswordStrength(options) {
    return options.find((x) => x.name === AUTH_CONFIG.REALM).options
      .passwordPolicy;
  }
}
