import { SilentLoginComponent } from "./silent-login/silent-login.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { LoginComponent } from "./login.component";
import { ResetPasswordComponent } from "./reset-password/reset-password.component";
import { LoginPageComponent } from "./login-page/login-page.component";
import { RequestResetPasswordComponent } from "./request-reset-password/request-reset-password.component";

const routes: Routes = [
  {
    path: "",
    component: LoginComponent,
    children: [
      { path: "reset-password", component: RequestResetPasswordComponent },
      { path: "", component: LoginPageComponent },
      // { path: 'reset-password', component: ResetPasswordComponent },
    ],
  },
  { path: ":id", component: SilentLoginComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginRoutingModule {
  static components = [LoginComponent];
}
