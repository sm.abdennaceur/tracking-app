import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import jwt_decode from 'jwt-decode';
// import { EmployeesService } from '../core/services/employees.service';
// import { ParamatersService } from '../core/services/paramaters.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  email: string;
  userData: any;

  constructor(
    private route: ActivatedRoute,
    public auth: AuthService,
    private router: Router // private employeeService: EmployeesService, // private userparams: ParamatersService
  ) {
    if (this.checkParams()) {
      this.auth.handleAuth();
    }

    this.auth.loggedIn$.subscribe((log) => {
      console.log(this.auth.isAuthenticated());

      if (log || this.auth.isAuthenticated()) {
        // console.log(log);
        // console.log(this.auth.userProfile.email);
        // this.email = this.auth.userProfile.email;
        // this.getAssociatedData(this.email)
        this.router.navigate(['/']);
      }
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      console.log(params); //log the entire params object
      console.log(params['id']); //log the value of id
    });
  }

  getAssociatedData(mail) {
    // this.employeeService.getUserDetails(mail).subscribe((data: any) => {
    //   this.userData = data;
    // console.log("@@ this employeeService.userData", data);
    // console.log(this.userData)
    // this.getUserParamatees(data.id);
    //   this.getIfParamatersAreCreated(data.id);
    // });
  }

  getIfParamatersAreCreated(testUserId: string) {
    // this.userparams.testParamatersAreCreated(testUserId).subscribe((data) => {
    // console.log("%c@testParamatersAreCreated Data", "color: red", data);
    // console.log("%c@UsserParams testUserId", "color: red", testUserId);
    // if (!data) {
    //   this.userparams.createUserParamaters(testUserId).subscribe((data) => {
    // console.log("%c @@DefaultParamsCreated", "color: green", data);
    //     });
    //   }
    // });
  }

  checkParams() {
    return window.location.toString().includes('access_token');
  }
}
