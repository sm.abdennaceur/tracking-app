import { SilentLoginComponent } from './silent-login/silent-login.component';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from './login-routing.module';
import { AngularMaterialModule } from '../login/services/angular-material.module';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { RequestResetPasswordComponent } from './request-reset-password/request-reset-password.component';
@NgModule({
  imports: [
    ReactiveFormsModule,
    LoginRoutingModule,
    AngularSvgIconModule,
    FormsModule,
    AngularMaterialModule,
    CommonModule,
  ],
  declarations: [
    LoginRoutingModule.components,
    ResetPasswordComponent,
    LoginPageComponent,
    RequestResetPasswordComponent,
    SilentLoginComponent,
  ],
  exports: [AngularMaterialModule],
})
export class LoginModule {}
