import { AuthService } from "./../services/auth.service";
import { Component, OnInit } from "@angular/core";
@Component({
  selector: "app-logout",
  templateUrl: "./logout.component.html",
  styleUrls: ["./logout.component.sass"],
})
export class LogoutComponent implements OnInit {
  constructor(public authService: AuthService) {}

  ngOnInit() {
    this.authService.logout();
  }
}
