import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
  FormGroupDirective,
  NgForm,
} from '@angular/forms';
import { Router } from '@angular/router';
import { ErrorStateMatcher } from '@angular/material/core';
import { MustMatch } from './must-match-helper.validator';

/** Error when invalid control is dirty, touched, or submitted. */
// export class MyErrorStateMatcher implements ErrorStateMatcher {
//   isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
//     const isSubmitted = form && form.submitted;
//     return !!(control && control.invalid && ( (control.dirty && control.touched) || isSubmitted));
//   }
// }

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit {
  private userForm: FormGroup;
  // matcher = new MyErrorStateMatcher();

  constructor(private router: Router, private fb: FormBuilder) {}

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.userForm = this.fb.group(
      {
        password: ['', [Validators.required, Validators.minLength(8)]],
        confirmPassword: ['', [Validators.required, Validators.minLength(8)]],
      },
      {
        validators: MustMatch('password', 'confirmPassword'),
      }
    );
  }

  submit(userForm: FormGroup) {
    const confirmPassword = userForm.value.confirmPassword;
    const pwd = userForm.value.password;
    if (pwd === confirmPassword) {
      this.router.navigate([`/`]);
    }
  }

  cancel() {
    this.router.navigate([`/login`]);
  }
}
