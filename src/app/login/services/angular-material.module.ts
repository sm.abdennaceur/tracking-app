import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatOptionModule } from '@angular/material/core';
// import {MatGridListModule} from '@angular/material/grid-list';
// import { MatFormFieldModule } from '@angular/material/form-field';
import { DateRangePickerModule } from '@syncfusion/ej2-angular-calendars';
import {
  MatRangeDatepickerModule,
  MatRangeNativeDateModule,
} from 'mat-range-datepicker';
import {
  MatMomentDateModule,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import { MatTableExporterModule } from 'mat-table-exporter';

import { NgModule } from '@angular/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatRippleModule } from '@angular/material/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTreeModule } from '@angular/material/tree';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  imports: [
    MatProgressSpinnerModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatDividerModule,
    MatProgressBarModule,
    ScrollingModule,
    MatRippleModule,
    MatToolbarModule,
    MatTreeModule,
    MatStepperModule,
    MatMomentDateModule,
    MatButtonModule,
    MatSnackBarModule,
    MatCheckboxModule,
    MatListModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule,
    MatTableExporterModule,
    MatAutocompleteModule,
    MatOptionModule,
    MatMenuModule,
    MatButtonToggleModule,
    MatRadioModule,
    MatSelectModule,
    MatTooltipModule,
    MatChipsModule,
    MatListModule,
    DragDropModule,
    MatSliderModule,
    MatExpansionModule,
    MatDialogModule,
    MatRangeDatepickerModule,
    MatRangeNativeDateModule,
    DateRangePickerModule,
    MatGridListModule,
  ],
  exports: [
    MatProgressSpinnerModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatDividerModule,
    MatProgressBarModule,
    ScrollingModule,
    MatRippleModule,
    MatToolbarModule,
    MatTreeModule,
    MatStepperModule,
    MatMomentDateModule,
    MatButtonModule,
    MatSnackBarModule,
    MatCheckboxModule,
    MatListModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule,
    MatTableExporterModule,
    MatAutocompleteModule,
    MatOptionModule,
    MatMenuModule,
    MatButtonToggleModule,
    MatRadioModule,
    MatSelectModule,
    MatTooltipModule,
    MatChipsModule,
    MatListModule,
    DragDropModule,
    MatSliderModule,
    MatExpansionModule,
    MatDialogModule,
    MatRangeDatepickerModule,
    MatRangeNativeDateModule,
    DateRangePickerModule,
    MatGridListModule,
  ],
  providers: [
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
  ],
})
export class AngularMaterialModule {}
