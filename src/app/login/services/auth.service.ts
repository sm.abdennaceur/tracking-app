import jwt_decode from 'jwt-decode';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { AUTH_CONFIG, MANAGEMENT_CONFIG, PASSWORD_STRENGTH_LABELS } from './auth.config';
import * as auth0 from 'auth0-js';
import { ENV } from './env.config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, concatMap } from 'rxjs/operators';

import { ConfigService } from '../services/environment.service';
import { ConfdataServce } from '../services/env.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import * as crypto from 'crypto-js';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  // Create Auth0 web auth instance
  //  _auth0 = new auth0.WebAuth({
  //   clientID: AUTH_CONFIG.CLIENT_ID,
  //   domain: AUTH_CONFIG.CLIENT_DOMAIN,
  //   audience: AUTH_CONFIG.AUDIENCE,
  //   responseType: "token id_token",
  //   scope: AUTH_CONFIG.SCOPE,
  //   redirectUri: AUTH_CONFIG.LOGIN_URL,
  //   returnTo: AUTH_CONFIG.LOGIN_URL,
  // });

  accessToken: string;
  userProfile: any;
  idToken: string;
  expiresAt: number;
  // Create a stream of logged in status to communicate throughout app
  loggedIn: boolean = false;
  loggedIn$ = new BehaviorSubject<boolean>(this.loggedIn);
  loggingIn: boolean;
  fetchingProfile: boolean;
  secretKey = '36a3301753e578166348caae8096945b';

  loginError: any[] = [];
  constructor(
    private router: Router,
    private http: HttpClient,
    private configService: ConfigService,
    private confdataServce: ConfdataServce,
    private deviceService: DeviceDetectorService
  ) {
    // this.AppParameters = this.configService.getConfiguration();
    // this.confdataServce.conf$.subscribe((res) => {
    //   console.log(res);
    //   if (res != undefined) {
    //     this.AppParameters = res;
    //     if (JSON.parse(localStorage.getItem('expires_at')) > Date.now()) {
    //       this.renewToken();
    //     }
    //   }
    // });
  }
  AppParameters: any;
  setLoggedIn(value: boolean) {
    // Update login status subject
    this.loggedIn$.next(value);
    this.loggedIn = value;
  }

  login() {
    // Auth0 authorize request
    this.AppParameters = this.configService.config;
    new auth0.WebAuth({
      clientID: this.AppParameters.auth0ClientId,
      domain: this.AppParameters.auth0BaseUrl,
      audience: this.AppParameters.auth0Audience,
      responseType: 'token id_token',
      scope: this.AppParameters.auth0Scope,
      redirectUri: AUTH_CONFIG.LOGIN_URL,
      returnTo: AUTH_CONFIG.LOGIN_URL,
    }).authorize();
  }

  loginCurrentUI(user: any) {
    this.loggingIn = true;
    this.resetError();
    this.AppParameters = this.configService.config;
    console.dir(this.AppParameters);

    new auth0.WebAuth({
      clientID: this.AppParameters.auth0ClientId,
      domain: this.AppParameters.auth0BaseUrl,
      audience: this.AppParameters.auth0Audience,
      responseType: 'token id_token',
      scope: this.AppParameters.auth0Scope,
      redirectUri: AUTH_CONFIG.LOGIN_URL,
      returnTo: AUTH_CONFIG.LOGIN_URL,
    }).login(
      {
        realm: AUTH_CONFIG.REALM,
        username: user.id,
        password: user.password,
      },
      (err, res) => {
        this.loggingIn = false;
        if (err) this.loginError.push(err);
        if (res) console.log(res);
      }
    );
  }
  handleAuth() {
    // When Auth0 hash parsed, get profile
    this.AppParameters = this.configService.config;

    new auth0.WebAuth({
      clientID: this.AppParameters.auth0ClientId,
      domain: this.AppParameters.auth0BaseUrl,
      audience: this.AppParameters.auth0Audience,
      responseType: 'token id_token',
      scope: this.AppParameters.auth0Scope,
      redirectUri: AUTH_CONFIG.LOGIN_URL,
      returnTo: AUTH_CONFIG.LOGIN_URL,
    }).parseHash((err, authResult) => {
      if (authResult && authResult.accessToken) {
        window.location.hash = '';
        this.getProfile(authResult);
      } else if (err) {
        console.error(`Error authenticating: ${err.error}`);
      }
      setTimeout(() => {
        this.router.navigate(['/']);
      });
    });
  }
  public getProfile(authResult) {
    this.fetchingProfile = true;
    // Use access token to retrieve user's profile and set session
    this.AppParameters = this.configService.config;

    new auth0.WebAuth({
      clientID: this.AppParameters.auth0ClientId,
      domain: this.AppParameters.auth0BaseUrl,
      audience: this.AppParameters.auth0Audience,
      responseType: 'token id_token',
      scope: this.AppParameters.auth0Scope,
      redirectUri: AUTH_CONFIG.LOGIN_URL,
      returnTo: AUTH_CONFIG.LOGIN_URL,
    }).client.userInfo(authResult.accessToken, (err, profile) => {
      console.log(profile);
      console.log(err);

      if (profile) {
        this._setSession(authResult, profile);
      } else if (err) {
        console.warn(`Error retrieving profile: ${err.error}`);
        this.logout();
      }
    });
  }

  private _setSession(authResult, profile?) {
    this.expiresAt = authResult.expiresIn * 1000 + Date.now();
    // Store expiration in local storage to access in constructor
    localStorage.setItem('expires_at', JSON.stringify(this.expiresAt));
    localStorage.setItem('Strada_Token', authResult.idToken);
    this.accessToken = authResult.accessToken;
    this.userProfile = profile;
    this.idToken = authResult.idToken;
    // Update login status in loggedIn$ stream
    this.setLoggedIn(true);
    this.fetchingProfile = false;
  }

  private _clearExpiration() {
    // Remove token expiration from localStorage
    localStorage.removeItem('expires_at');
    localStorage.removeItem('Strada_Token');
  }

  logout() {
    // Remove data from localStorage
    this._clearExpiration();
    this.AppParameters = this.configService.config;

    // End Auth0 authentication session
    new auth0.WebAuth({
      clientID: this.AppParameters.auth0ClientId,
      domain: this.AppParameters.auth0BaseUrl,
      audience: this.AppParameters.auth0Audience,
      responseType: 'token id_token',
      scope: this.AppParameters.auth0Scope,
      redirectUri: AUTH_CONFIG.LOGIN_URL,
      returnTo: AUTH_CONFIG.LOGIN_URL,
    }).logout({
      clientId: AUTH_CONFIG.CLIENT_ID,
      returnTo: ENV.BASE_URI,
    });
  }

  get tokenValid(): boolean {
    // Check if current time is past access token's expiration
    return Date.now() < JSON.parse(localStorage.getItem('expires_at'));
  }

  renewToken() {
    // Check for valid Auth0 session
    this.AppParameters = this.configService.config;

    new auth0.WebAuth({
      clientID: this.AppParameters.auth0ClientId,
      domain: this.AppParameters.auth0BaseUrl,
      audience: this.AppParameters.auth0Audience,
      responseType: 'token id_token',
      scope: this.AppParameters.auth0Scope,
      redirectUri: AUTH_CONFIG.LOGIN_URL,
      returnTo: AUTH_CONFIG.LOGIN_URL,
    }).checkSession(
      {
        scope: 'openid profile email',
      },
      (err, authResult) => {
        console.log(authResult);

        if (authResult && authResult.accessToken) {
          this.getProfile(authResult);
        } else {
          this._clearExpiration();
          this.logout();
        }
      }
    );
  }

  isAuthenticated(): boolean {
    return JSON.parse(localStorage.getItem('expires_at')) > Date.now();
  }

  resetError() {
    this.loginError = [];
  }

  requestResetPassword(email: string): Promise<any> {
    this.AppParameters = this.configService.config;

    return new Promise((resolve, reject) => {
      new auth0.WebAuth({
        clientID: this.AppParameters.auth0ClientId,
        domain: this.AppParameters.auth0BaseUrl,
        audience: this.AppParameters.auth0Audience,
        responseType: 'token id_token',
        scope: this.AppParameters.auth0Scope,
        redirectUri: AUTH_CONFIG.LOGIN_URL,
        returnTo: AUTH_CONFIG.LOGIN_URL,
      }).changePassword(
        {
          connection: AUTH_CONFIG.CONNECTION,
          email: email,
        },
        function (err, resp) {
          if (err) {
            // console.log(err.message);
            reject(err.message);
          } else {
            // console.log(resp);
            resolve(resp);
          }
        }
      );
    });
  }

  getPasswordStrength(): Observable<any> {
    var token;
    return this.http
      .post<any>(AUTH_CONFIG.BASE_URL + '/oauth/token', {
        client_id: MANAGEMENT_CONFIG.CLIENT_ID,
        client_secret: MANAGEMENT_CONFIG.CLIENT_SECERET,
        audience: AUTH_CONFIG.BASE_URL + '/api/v2/',
        grant_type: 'client_credentials',
      })
      .pipe(
        tap((res) => {
          // console.log(res);
          token = res.access_token;
        }),
        concatMap((res: { access_token: string }) => {
          return this.http.get(AUTH_CONFIG.BASE_URL + '/api/v2/connections', {
            headers: {
              Authorization: 'Bearer ' + res.access_token,
            },
          });
        })
      );
  }

  getPasswordStrengthDetails(strenghtCode: string): any {
    return PASSWORD_STRENGTH_LABELS.find((x) => x.name === strenghtCode);
  }
  encrypt(value: string): string {
    return crypto.AES.encrypt(value, this.secretKey.trim()).toString();
  }

  decrypt(textToDecrypt: string) {
    return crypto.AES.decrypt(textToDecrypt, this.secretKey.trim()).toString(crypto.enc.Utf8);
  }
  createToken() {
    const deviceInfo = this.deviceService.getDeviceInfo();
    let data = {
      token: localStorage.getItem('Strada_Token'),
      user: jwt_decode(localStorage.getItem('Strada_Token')),
      deviceinfo: deviceInfo,
      expires_at: localStorage.getItem('expires_at'),
      date: new Date(),
    };
    let result = this.encrypt(JSON.stringify(data));

    return result.replace(/\+/g, 'p1L2u3S').replace(/\//g, 's1L2a3S4h').replace(/=/g, 'e1Q2u3A4l');
  }

  compareDates(date1, date2) {
    var dif = date1.getTime() - new Date(date2).getTime();

    var Seconds_from_T1_to_T2 = dif / 1000;
    return Math.abs(Seconds_from_T1_to_T2);
  }
  tokenManegement(token) {
    try {
      let data = jwt_decode(token);
      console.log(data);
      this.expiresAt = (data as any).exp;
      // Store expiration in local storage to access in constructor
      localStorage.setItem(
        'expires_at',
        JSON.stringify(parseInt(JSON.stringify(this.expiresAt)) * 1000 + new Date().getTime())
      );
      localStorage.setItem('Strada_Token', token);
      this.accessToken = (data as any).token;
      this.userProfile = (data as any).user;
      // Update login status in loggedIn$ stream
      this.setLoggedIn(true);
      this.fetchingProfile = false;
      this.router.navigate(['/track']);
    } catch {
      this.logout();
    }
  }
}
