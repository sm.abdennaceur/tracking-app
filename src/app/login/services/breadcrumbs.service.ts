import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbsService {
  title: string = "";
  subTitle: string = "";
  subSubTitle: string = "";
  iconeName: string = "";
  constructor() { }
  getTitle() {
    return this.title;
  }

  setTitle(title) {
    this.title = title;
  }
  getSubTitle() {
    return this.subTitle;
  }

  setSubTitle(subTitle) {
    this.subTitle = subTitle;
  }
  getSubSubTitle() {
    return this.subSubTitle;
  }

  setSubSubTitle(subSubTitle) {
    this.subSubTitle = subSubTitle;
  }
  setIcone(icon) {
    this.iconeName = icon;
  }
  getIconeName(){
    return this.iconeName;
  }
}
