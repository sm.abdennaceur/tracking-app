import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
    providedIn: 'root'
})
export class ConfdataServce {

    constructor() { }
    conf = new BehaviorSubject(undefined);
    progressBar: any;
    conf$ = this.conf.asObservable();

    setConfg(progress) {
        this.progressBar = progress;
        this.conf.next(progress);
    }

    getConfg() {
        return Object.assign({}, this.progressBar);
    }

}
