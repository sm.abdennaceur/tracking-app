import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private auth: AuthService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {



if(!request.url.includes('https://api-adresse.data.gouv.fr/')){

  request = request.clone({
    setHeaders: {
        Authorization: `Bearer ${JSON.parse(sessionStorage.getItem((sessionStorage.key(0)))).access_token}`,
    }
});

}
        return next.handle(request);
    }
}

