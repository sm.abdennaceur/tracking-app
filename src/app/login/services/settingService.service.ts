import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
// import * as config   from '../../../assets/environments/config.json'

@Injectable({
    providedIn: 'root'
  })
export class SettingService  {

    constructor(private http: HttpClient) {
  
    }
  
    public getJSON(): Observable<any> {
        return this.http.get("../../../assets/environments/config.json");
    }
    // public getSetting(){
    //     // use setting here
        
    // }
  }