import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-silent-login',
  templateUrl: './silent-login.component.html',
  styleUrls: ['./silent-login.component.sass'],
})
export class SilentLoginComponent implements OnInit {
  constructor(public authService: AuthService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      console.log(params); //log the entire params object
      console.log(params['id'].toString()); //log the value of id
      this.authService.tokenManegement(params['id']);
    });
  }
}
