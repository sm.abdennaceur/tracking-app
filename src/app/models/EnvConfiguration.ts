export interface EnvConfiguration {
    apiUrl: string;
    authenticationEndpoint: string;
    authority: string;
    client_id: string;
    response_type: string;
    scope: string;
    filterProtocolClaims: boolean;
    loadUserInfo: boolean;
    audience: string;
    checkSessionInterval: number;
    refreshToken: string;
    redirectUrl: string;

}
