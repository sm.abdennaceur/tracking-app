interface FileUpload {
  hasError?: boolean;
  name: string;
  sent: boolean;
  driver: string;
  bolb?: File;
  status?: string;
  uploaded?: string;
}
interface SepratedFiles {
  fc?: FileUpload[];
  fv?: FileUpload[];
}
