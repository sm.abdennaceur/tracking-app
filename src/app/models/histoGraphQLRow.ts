export class HistoGraphQLRow {}

export class Data {
  trackings: Tracking;
}

export class Tracking {
  nodes: Array<Node>;
}

export class Node {
  evenements: Array<Evenement>;
}

export class GeoLocalisation {
  date?: string;
  position?: Position;
  compteurKm?: number;
  isEnMouvement?: boolean;
}

export class Position {
  latitude?: number;
  longitude?: number;
  vitesse?: number;
}

export class Personne {
  activite?: number;
}

export class Tachygraphe {
  compteurKm?: number;
  vRN?: string;
  vIN?: string;
  conducteur?: Personne;
}

export class Evenement {
  geoLocalisation?: GeoLocalisation;
  tachygraphe?: Tachygraphe;
}
