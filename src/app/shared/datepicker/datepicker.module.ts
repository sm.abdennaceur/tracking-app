import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { AngularMaterialModule } from '../../angular-material/angular-material.module';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [DatePickerComponent],
  imports: [CommonModule, NgbModule, FormsModule, AngularSvgIconModule, AngularMaterialModule, TranslateModule],
  exports: [DatePickerComponent],
})
export class DatepickerModule {}
