import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialo-raz',
  templateUrl: './dialo-raz.component.html',
  styleUrls: ['./dialo-raz.component.scss'],
})
export class DialoRazComponent implements OnInit {
  description: string;
  title: string;

  constructor(private dialogRef: MatDialogRef<DialoRazComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.description = data.description;
    this.title = data.title;
  }

  ngOnInit() {}

  save() {
    this.dialogRef.close();
  }

  close() {
    this.dialogRef.close();
  }
}
