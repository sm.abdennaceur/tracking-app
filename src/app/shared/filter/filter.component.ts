import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { faChevronDown,faChevronUp, faSearch, faSlidersH} from '@fortawesome/free-solid-svg-icons';
import { Filter, IFilter} from "../interfaces";
import { Router, NavigationEnd  } from '@angular/router';
import { NavbarService } from '../../core/services/navbar.service';



export interface ICustomFilter {
  id: number,
  title: string,
  checked?: boolean,
  listFilters: IFilter[]
};

// jQuery Sign $
declare let $: any;
@Component({
  selector: 'filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  public filterForm: FormGroup;
 

  faChevronDown = faChevronDown;
  faChevronUp = faChevronUp;
  faSearch= faSearch;
  faSliders=faSlidersH;
  @ViewChild('dataModal', { static: false }) dataModal: any;
  @Input() filterList :  Filter;
  public listSelectedFilter: IFilter[] = [];
  public listSelectedFilterForModal: IFilter[] = [];
  public title: string;
  public edit = false;
  public listSimpleFilters: IFilter[] = []
  public Loaded :boolean;
    // { id: 1, title: 'Longue distance', checked: false },
    // { id: 2, title: 'Courte distance', checked: false },
    // { id: 3, title: 'Autocariste', checked: false },
    // { id: 4, title: 'Interim', checked: false },
    // { id: 5, title: 'Sédentaire', checked: false }
  // ];
  public listCustumFilters: ICustomFilter[] = [
    {
      id: 1,
      title: "Mon filtre perso 1",
      checked: false,
      listFilters: [
        { id: 1, title: "Longue distance", checked: true },
        { id: 2, title: "Courte distance", checked: true },
      ]
    },
    {
      id: 2,
      title: "Le filtre à Jean-René",
      checked: false,
      listFilters: [
        { id: 4, title: "Interim", checked: true },
        { id: 5, title: "Sédentaire", checked: true }
      ]
    }
  ];
  public titleModal = 'Ajouter un filtre personnalisé';
  public titleButton = 'Ajouter';
  constructor(private fb: FormBuilder, private router:Router,public navbar:NavbarService) {
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd) { 
       setTimeout(
        ()=>this.createFilterList())
      }
    });
  }
  async createFilterList(){
    this.listSimpleFilters=[]
    this.Loaded=false;
   this.filterList && await this.filterList.list.map((item,i)=>{
      this.listSimpleFilters.push({ id: i, title: item, checked: false })
    })
      this.Loaded=true;

  }
  ngOnInit() {
    this.createFilterList()
    this.createForm();
    
  }

  createForm() {
    this.filterForm = this.fb.group({
      title: ['', [Validators.required]],
      id: [''],
    });
    this.navbar.setFilters(this.listSelectedFilter)

  }

  onChangeSimpleFilterChips(filter: IFilter, event: any) {


    if (event.checked) {
      filter.checked = true;
      this.listSelectedFilter.push(filter);
    } else {
      this.listSelectedFilter = this.listSelectedFilter.filter(x => x.id !== filter.id);
    }
    this.navbar.setFilters(this.listSelectedFilter)
  }

  onChangeCustomFiltersChips(customFilter: ICustomFilter, event: any) {
    if (event.checked) {
      this.listSelectedFilter = customFilter.listFilters;;
      this.listSimpleFilters.map(item => {
        const check = this.listSelectedFilter.find(x => x.id === item.id);
        if (check) item.checked = true;
        return item;
      });
    } else {
      this.listSimpleFilters.map(item => {
        const check = this.listSelectedFilter.find(x => x.id === item.id);
        if (check) item.checked = false;
        return item;
      });

      this.listSelectedFilter = this.listSelectedFilter.filter(x => !customFilter.listFilters.includes(x));
      
    }
    
  }

  showModalCustomFilter() {
    if (this.listSelectedFilter.length > 1) {
      this.listSelectedFilterForModal = this.listSelectedFilter;
      $(this.dataModal.nativeElement).modal('show');
      this.filterForm.reset();
      this.titleModal = 'Ajouter un filtre personnalisé';
      this.titleButton = 'Ajouter';
    }
  }

  submitCustomFilter(filterForm: FormGroup) {

    const payload: any = {
      title: filterForm.value.title,
      listFilters: this.listSelectedFilterForModal
    }


    if (filterForm.value.id) {
      this.listCustumFilters.map(x => {
        if (x.id === filterForm.value.id) {
          x.title = payload.title;
        }
        return x;
      });

    } else {
      const lastId = this.listCustumFilters[this.listCustumFilters.length - 1].id + 1;
      payload.id = lastId;
      this.listCustumFilters.push(payload);
    }


    this.listSimpleFilters.map(item => {
      const check = this.listSelectedFilter.find(x => x.id === item.id);
      if (check) item.checked = false;
      return item;
    });

    this.listSelectedFilter = [];
    this.listSelectedFilterForModal = [];

    $(this.dataModal.nativeElement).modal('hide');

  }

  removeChips(filter: IFilter): void {
    this.listSelectedFilter = this.listSelectedFilter.filter(x => x.id !== filter.id);

    this.listSimpleFilters.map(x => {
      if (x.id === filter.id) x.checked = false;
      return x;
    });
    this.navbar.setFilters(this.listSelectedFilter)
  }


  onEdit(customFilter: ICustomFilter) {
    this.titleModal = 'Modifier titre du filtre personnalisé';
    this.titleButton = 'Modifier';
    this.listSelectedFilterForModal = customFilter.listFilters;
    $(this.dataModal.nativeElement).modal('show');
    this.edit = true;
    this.filterForm = this.fb.group({
      title: [customFilter.title, Validators.required],
      id: [customFilter.id],
    });
  }

  onDelete(customFilter: ICustomFilter) {
    if (confirm('Êtes-vous sûr de supprimer ce filtre personnalisé ?') === true) {
      this.listCustumFilters = this.listCustumFilters.filter(x => x.id !== customFilter.id);
    }
  }

}
