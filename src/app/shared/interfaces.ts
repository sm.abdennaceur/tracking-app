import { StringifyOptions } from 'querystring';

export interface INavBarDate {
  interval?: 'day' | 'week' | 'month';
  startDate: Date;
  endDate: Date;
}
export interface TimeParams {
  id: string,
  fuseau: string,
  villes: string
}
export class IAddVehicle {
  numeroIdentificationVehicule: string;
  immatriculation: string;
  immatriculationAffichage: string;
  paysImmatriculation: string;
  information: string;
  type: string;
  isIntegre: boolean;
  dateFinIntegration: Date;

}
export interface IVehicle {
  id: string;
  _id: string;
  registration: string;
  immatriculation: string;
  countryRegistration: string;
  displayName: string;
  entityLocation: string;
  activite: string;
  idNumber: string;
  state: string;
  indexKm: number;
  fuel: string;
  warning: boolean;
  vehicleDailyKm?: any[];
  totalPeriod: any;
  listDeadlines?: any[];
  calibration: any;
  informationUEV: any;
  locking: any;
  movementSensors: any;
  remoteUnloading: any;
  carBody: any;
  fuelCO2: any;
  geolocalisation: any;
  accessoriesEquipment: any;
  costs: any;
  personnalisation: any;
  notes: string;
  activities?: IActivityVehicle[];
  tachographAnomalies?: ITachographAnomalies[];
  drivesWithoutCard?: IDriveWithoutCard[];
}
export interface IConducteurUseVehicles {
  conducteurId: string;
  nomConducteur: string;
  vehiculeId: string;
  vehicleRegistration: string;
  vehicleFirstUse: Date;
  vehicleLastUse: Date;
  vehicleOdometerBegin: number;
  vehicleOdometerEnd: number;
  vehicleOdometerParcours: number;
}
export interface ITachographAnomalies {
  _id: string;
  type: string;
  title: string;
  startDate: Date;
  endDate: Date;
  startDriver: ITypeDriver[];
  endDriver: ITypeDriver[];
}

export interface ITypeDriver {
  driver?: string;
  numberCard?: string;
}

export interface IDriveWithoutCard {
  _id: string;
  startDate: Date;
  endDate: Date;
  duration: number;
  justification: string;
}

export interface IActivityVehicle {
  _id: string;
  vehiculeId: string;
  vehiculeDisplayName: string;
  date: Date;
  jour?: Date;
  sequences: IPeriodVehicle[];
}

export interface IPeriodVehicle {
  _id: string;
  activity: string;
  timeStart: string;
  timeStop: string;
  duration: number;
  numberKm: number;
  averageSpeed: number;
  maxSpeed: number;
  detection: string;
  startPosition?: { lat: string; lng: string };
  endPosition?: { lat: string; lng: string };
  selected?: boolean;
}

export interface ISpeed {
  _id: string;
  vehiculeId: string;
  vehiculeDisplayName: string;
  date: Date;
  speedPeriod: ISpeedPeriod[];
  speeds?: ISpeedPeriod[];
}

export interface ISpeedPeriod {
  time: string;
  speed: number;
  lat: number;
  lng: number;
}

export interface ISpeedingDriver {
  conducteurId: number | string;
  affichage: string;
  excesVitesse: ISpeedingVehicle[];
}

export interface ISpeedingVehicle {
  affichage: string;
  conducteurId: number | string;
  dateDebut: Date;
  dateFin: Date;
  immatriculation: string;
  numeroDeCarte: string;
  vehiculeId: number | string;
  vitesseMaxi: number;
  vitesseMoyenne: number;
}

export interface IVehiclePlanning {
  _id: string;
  vehicleId: string;
  registration: string;
  state: boolean;
  indexKM: number;
  lastDateEmptyTacho: Date;
  statisticsDay: IStatisticsDay[];
}

export interface IStatisticsDay {
  date: Date;
  numberKilometers: number;
  totalActivity: number;
  tachographCard: boolean;
  speeding: boolean;
  totalRevenue: number;
}

export interface IActivityEmployee {
  id: string;
  driverId: string;
  employeeUserName: string;
  displayName: string;
  date?: Date;
  day: Date;
  absenceDuration: number;
  paidHour: number;
  nightServiceDuration: number;
  sequences?: IPeriodEmployee[];
  comments: any[];
  travelExpenses?: ITravelExpenses[];
  totalDrive?: any;
  // dailyOffense?: IDailyOffense[];
}

export interface IHistorizedActivityEmployee {
  jour: Date,
  conducteurId: string,
  affichage: string,
  totalConduite: number,
  totalDoubleEquipage: number,
  totalService: number,
  totalTravail: number,
  totalMad: number,
  totalRepos: number,
  totalAc: number,
  totalNuit: number,
  totalAbsence: number,
  totalKilometre: number,
  totalFrais: number,
  activites: [
    {
      id: string,
      vehiculeId: string,
      vehiculeImmatriculation: string,
      typeActiviteCode: string,
      typeActiviteLibelle: string,
      heureDebut: {
        ticks: number,
        days: number,
        hours: number,
        milliseconds: number,
        minutes: number,
        seconds: number,
        totalDays: number,
        totalHours: number,
        totalMilliseconds: number,
        totalMinutes: number,
        totalSeconds: number
      },
      heureFin: {
        ticks: number,
        days: number,
        hours: number,
        milliseconds: number,
        minutes: number,
        seconds: number,
        totalDays: number,
        totalHours: number,
        totalMilliseconds: number,
        totalMinutes: number,
        totalSeconds: number
      },
      duree: {
        ticks: number,
        days: number,
        hours: number,
        milliseconds: number,
        minutes: number,
        seconds: number,
        totalDays: number,
        totalHours: number,
        totalMilliseconds: number,
        totalMinutes: number,
        totalSeconds: number
      },
      nombreKilometre: number,
      carteInseree: boolean
    }
  ],
  frais: [
    {
      id: string,
      type: string,
      titre: string,
      etranger: true,
      prix: number,
      date: Date
    }
  ]
}

export interface IPeriodEmployee {
  selected?: boolean;
  id: string;
  vehicleId: string;
  registration: string;
  vehicleImmatriculation: string;
  indexKM: number;
  lastDateEmptyTacho: Date;
  activity: string;
  activityLibelle: string;
  activityCode: string;
  newActivityCode: string;
  startTime: string;
  endTime: string;
  duration: number;
  numberKm: number;
  startPosition?: { lat: string; lng: string };
  endPosition?: { lat: string; lng: string };
  edited?: boolean;
}

export interface ITravelExpenses {
  code: string;
  libelle: string;
  unitValue: number;
  foreign: boolean;
  // nature: string;
}

export interface IDailyOffense {
  title: string;
  date: Date;
}

export interface Filter {
  title: string;
  list: string[];
}
export interface INavAction {
  title: string;
  faIcon?: string;
  slug?: string;
  iconUrl?: string;
  disabled?: boolean;
}
export interface INavReport {
  title: string;
}
export interface INavPrameters {
  actionsList?: INavAction[];
  reportRout?: INavReport[];
  button?: INavAction;
  dataSelector?: boolean;
  periodeSelector?:boolean;
  exportPDF?: boolean;
  exportXLS?: boolean;
  filter?: Filter;
  search?: boolean;
  saveBtn?: boolean;
  saveBtnDisabled?: boolean;
  loadingSaveBtn?: boolean;
  stateFilter?: boolean;
  newType?: boolean
}

export interface IFilter {
  id: number | string;
  title: string;
  checked?: boolean;
}
export type IStateFilter = 'all' | 'actif' | 'inactif';
export interface ITachygraph {
  id: number | string;
  displayName: string;
  registration: string;
  entityLocation: string;
  dateNextUnloading: string;
  dateLastUnloading: string;
  events: number;
  lastUnloadingDays: number;
  nextUnloadingDays: number;
  anomalyList?: any;
}

export interface IPaginatedTachygraphs {
  pageIndex: number;
  totalCount: number;
  itemsCount: number;
  pageCount: number;
  hasPreviousPage: boolean;
  hasNextPage: boolean;
  items: ITachygraph[];
}

export interface IChronoTachygraphValidity {
  nextDate: string;
  id: string;
  registration: string;
  vehicleId:string
}

// export interface IChronoTachygraphValidity {
//   id: string | number;
//   vehicleId: string | number;
//   registration: string;
//   infosStation: string;
//   lastCardUnload: string;
//   oldDate: string;
//   newDate: string;
//   oldTime: string;
//   newTime: string;
//   nextDate: string;
//   oldKm: number;
//   newKm: number;
//   maxSpeed: number;
//   tireCirc: number;
//   tireSize: string;
//   country: number;
//   constanteW: number;
//   constanteK: number;
//   purpose: number;
// }
export interface IChronoTachygraphUnload {
  id: string | number;
  dateNextUnloading: string;
  dateLastUnloading: string;
  displayName: string;
  entityLocation: string;
  events: number;
  registration: string;
}

// export interface IDriverValidity {
//   nextDate: string;
//   registration: string;
//   id: string;
// }
export interface IDriverUnload {
  driverId: string | number;
  displayName: string;
  lastUnloadDate: string;
  expirationDate: string;
}

export interface IDriverValidity {
  driverFullName: string;
  driverId: string | number;
  cards: {
    validityEndDate: string;
    validityStartDate: string;
    id: string | number;
  }[];
}
// export interface IDriverUnload {
//   lastUnloadDate: string;
//   diplayName: string;
//   driverId: string | number;
// }
export class IAddEmployee{
    adresseId:string;
    profileId: string;
    matricule: string;
    nationalite: string;
    dateNaissance: string;
    nomJeuneFille: string;
    noms: string;
    prenoms: string;
    nomAffichage: string;
    langueUtiliseeTravail: string;
    dateEmbauche: string;
    mail: string;
    fonction: string;
    numeroSecuriteSociale: string;
    groupActiviteId: string;
    telephone: string;

}
export interface IEmployee {
  id: string;
  driverId?: string;
  employeeId: string;
  firstName: string;
  lastName: string;
  fullName: string;
  phone: string;
  state: string;
  groupeActiviteLibelle: string;
  groupActiviteId: string;
  warning: boolean;
  checked?: boolean;
  post?: string;
  adress?: string;
  zipCode: string;
  city: string;
  email?: string;
  entity?: string;
  card: {
    // card: {
    cn: string;
    unload: {
      lastUnloadDate: string;
      nextUnloadDate: string;
    };
    validityEnd: string;
    validityStart: string;
    // };
  };
  work?: {
    hiringDate: string;
    startDate: string;
    vehicles: any[];
  };
  infraction?: any[];
  deadline?: any[];
  drivingLicenseCard?: {
    autority: string;
    country: string;
    number: string;
  };
  totalPeriod: {
    driving: number;
    ac: number;
    cutOff: number;
    de: number;
    mad: number;
    work: number;
  };
  maidenName?: string;
}
export interface ITachographAnomaliesPerType {
  id: string;
  type: string;
  libelle: string;
  listVehicle: ITachographAnomaliesVehicles[];
}
export interface ITachographAnomaliesVehicles {
  id: string;
  registration: string;
  displayName: string;
  startDate: Date;
  endDate: Date;
  startDriver: ITypeDriver[];
  endDriver: ITypeDriver[];
  checked?: boolean;
}
export interface IAnomalieName {
  type: string;
  libelle: string;
}
export interface IInfractionTypeName {
  code: string;
  libelle: string;
}
export interface IInfractionByTypeResponse {
  code: string;
  libelle: string;
  infractions: IInfraction[];
}
export interface IInfraction {
  date: Date;
  infractionHour: string;
  employee: string;
  authorizedServiceTime: number;
  realizedServiceTime: number;
  authorizedBreakTime: number;
  realizedBreakTime: number;
  infractionCause: string;
  checked?: boolean;
  state: string;
}

export interface IInfractionByEmployeeResponse {
  driverId: string;
  employeeName: string;
  infractions: IInfractionByEmployee[];
  checked?: boolean;
}

export interface IInfractionByEmployee {
  id: number | string;
  date: Date;
  infractionHour: string;
  infractionType: string;
  authorizedServiceTime: number;
  realizedServiceTime: number;
  infractionLibelle: string;
  authorizedBreakTime: number;
  realizedBreakTime: number;
  infractionCause: string;
}
export interface ICarte {
  driverId: number | string;
  driverFullName: string;
  cards: ICardDetails[];
}

export interface ICardDetails {
  id: number | string;
  cardNumber: string;
  stateDeliverCard: string;
  authorityDeliverCard: string;
  deliveryDate: Date;
  validityStartDate: Date;
  validityEndDate: Date;
  validityEndActive: Date;
  dateLastUnloading: Date;
  isSelected: boolean;
}

export interface ITachygraphEvenet {
  id: number | string;
  driverId: number | string;
  driverFullName: string;
  libelle: string;
  startDate: Date;
  endDate: Date;
  type: number;
  isSelected?: boolean;
}
export interface IDriverEvent {
  driverId: number | string;
  fullName: string;
  evenements: ITachygraphEvenet[];
}
export interface IDriverEventByType {
  type: number;
  libelle: string;
  evenements: IDriverEventType[];
}

export interface IDriverEventType {
  id: number | string;
  driverId: string;
  startDate: Date;
  driverFullName: string;
  endDate: Date;
  type: number;
  checked?: boolean;
}

export interface ISessionRow {
  id: number | string;
  startDateActivity: Date;
  startTimeActivity: String;
  endDateActivity: Date;
  endTimeActivity: string;
  frenchFileName: string;
}

export interface ISessionGroupedByDriver {
  driverId: number | string;
  driverFullName: string;
  sessions: ISessionRow[];
}

export interface ISessionGroupedByVehicle {
  vehicleId: number | string;
  registration: string;
  sessions: ISessionRow[];
}

export interface IVehicleUtilisation {
  driverId: number | string;
  displayName: string;
  vehicleUtilisations: IVehicleUtilisationRow[];
}

export interface IVehicleUtilisationRow {
  id: number | string;
  vehicleId: number | string;
  date: Date | string;
  displayName: string;
  registration: string;
  totalDrive: number;
  totalWork: number;
  totalMad: number;
  totalDE: number;
  totalAC: number;
  totalBreak: number;
  startKmCounter: number;
  endKmCounter: number;
  departureDate: Date;
  arrivalDate: Date;
  departureTime: string;
  arrivalTime: string;
  totalKms: number;
}

// ['date', 'displayName', 'totalDrive', 'totalWork',
//  'totalMad', 'totalDE', 'totalAc', 'serv', 'totalBreak', 'totalKms'];

export interface ApiResonceIVehicleUtilisationRow {
  id: number | string;
  dateFormatted: string | Date;
  displayName: string;
  registration: string;
  totalDrive: number;
  totalWork: number;
  totalMad: number;
  totalDE: number;
  totalAC: number;
  serv: number;
  totalBreak: number;
  totalKms: number;
}
export interface IEmployeeUtilisationSummary {
  employeeId: number | string;
  driverId: number | string;
  displayName: string;
  date: Date;
  startTime: string;
  endTime: string;
  duration: string;
  activityCode: string;
  activityLibelle: string;
  mileage: number;
}

export interface driverDayUtilisation {
  driverId: number | string;
  displayName: string;
  date: Date;
  startTime: string;
  endTime: string;
  activityCode: number;
  activityLibelle: string;
  mileage: number;
  duration: string;
  infractions: IInfractionDriverList[];
}
export interface IInfractionDriverList {
  id: number | string;
  idConducteur: number;
  typeInfraction: string;
  heureInfraction: number;
  jour: Date;
  timestampInfraction: number;
  duree: number;
  causeInfraction: string;
  tempsServiceEffectue: number;
  tempServiceAutorise: number;
  article: string;
  tempsReposAutorise: number;
  compteurAmenagementAutorise: number;
  compteurAmenagementEffectue: number;
  tempsReposeEffectue: number;
  commentaire: string;
  montantMin: number;
  note: number;
  montantMax: number;
  reference: string;
  niveauGravite: number;
  decriptif: string;
  remarque: string;
  textLoi: string;
  explication: string;
  manuelle: true;
  vitesseEffectuee: number;
  vitesseAutorisee: number;
  nePasImprimer: boolean;
  classeContravention: number;
  dateEdition: Date;
  dateRemise: Date;
  heureRemise: string;
  dateSignature: Date;
  heureSignature: string;
}

export interface ApiResponse {
  dateFormatted: string | string;
  startTime: string;
  endTime: string;
  driverId: number | string;
  displayName: string;
  travail: string;
  conduite: string;
  MAD: string;
  coupure: string;
  DE: string;
  AC: string;
  Kms: string;
  CC: string;
  CJ: string;
  RJ: string;
  AM: string;
  TS: string;
  TP: string;
  TE: string;
}

export interface driverHoursUtilisation {
  id: number | string;
  displayName: string;
  date: Date;
  startTime: string;
  endTime: string;
  amplitude: string;
  interruption: string;
  totalAc: string;
  totalDrive: string;
  totalBreak: string;
  totalDe: string;
  totalMad: string;
  totalNight: string;
  totalService: string;
  absenceCode: string;
  absence: string;
  totalMileage: number;
  totalWork: string;
  infractions: IInfractionDriverList[];
  fees: IFeesDriverList[];
}

export interface IFeesDriverList {
  id: number | string;
  code: string;
  nature: string;
  jour: Date;
  localite: string;
  montant: number;
  modification: boolean;
  typeConducteur: number;
  suppression: boolean;
  idConducteur: number;
  lienDll: number;
  article: string;
  etranger: boolean;
  marchandise: boolean;
  valeurUnitaire: number;
  quantite: number;
  type: number;
  calculViaScript: boolean;
}

export interface ApiResponseHours {
  id: number | string;
  date: Date;
  interr: string;
  startTime: string;
  endTime: string;
  amplitude: string;
  serv: string;
  totalWork: string;
  totalDrive: string;
  totalMad: string;
  totalDE: string;
  totalAc: string;
  totalBreak: string;
  displayName: string;
  absc: string;
  codeAbsc: string;
  night: string;
  infractions: string;
  fees: string;
  totalKms: number;
}

export interface driverKlmsUtilisation {
  id: number | string;
  driverId: number | string;
  vehicleId: number | string;
  displayName: string;
  registration: string;
  startKmCounter: number;
  endKmCounter: number;
  departureDate: string;
  /************* update arrivalTime ***departureTime */
  departureTime: string;
  arrivalDate: string;
  arrivalTime: string;
  dataBlockCounterView: number;
  departureTimestamp: number;
  arrivalTimestamp: number;
  consolidation: boolean;
  mileage: number;
}

export interface ApiResponseKlms {
  id: number | string;
  displayName: string;
  vehicule: string;
  dateFormatted: string;
  hours: string;
  index: number;
  dateFormatted2: string;
  hours2: string;
  index2: number;
  Kms: number;
}

export interface IVehicleKlmsUtilisation {
  id: number | string;
  conducteurId: number | string;
  vehiculeId: number | string;
  affichage: string;
  immatriculation: string;
  compteurDebutKm: number;
  compteurFinKm: number;
  dateDepart: string;
  heureDepart: string;
  dateArrivee: string;
  heureArrivee: string;
  kmsTotal: number;
}

export interface ApiResponseKlmsVehicle {
  id: number | string;
  driver: string;
  vehicule: string;
  registration: string;
  dateFormatted: string;
  hours: string;
  index: number;
  dateFormatted2: string;
  hours2: string;
  index2: number;
  Kms: number;
}

export interface IInfractionDriverList {
  id: number | string;
  idConducteur: number;
  typeInfraction: string;
  heureInfraction: number;
  jour: Date;
  timestampInfraction: number;
  duree: number;
  causeInfraction: string;
  tempsServiceEffectue: number;
  tempServiceAutorise: number;
  article: string;
  tempsReposAutorise: number;
  compteurAmenagementAutorise: number;
  compteurAmenagementEffectue: number;
  tempsReposeEffectue: number;
  commentaire: string;
  montantMin: number;
  note: number;
  montantMax: number;
  reference: string;
  niveauGravite: number;
  decriptif: string;
  remarque: string;
  textLoi: string;
  explication: string;
  manuelle: true;
  vitesseEffectuee: number;
  vitesseAutorisee: number;
  nePasImprimer: boolean;
  classeContravention: number;
  dateEdition: Date;
  dateRemise: Date;
  heureRemise: string;
  dateSignature: Date;
  heureSignature: string;
}

export interface IFeesDriverList {
  id: number | string;
  code: string;
  nature: string;
  jour: Date;
  localite: string;
  montant: number;
  modification: boolean;
  typeConducteur: number;
  suppression: boolean;
  idConducteur: number;
  lienDll: number;
  article: string;
  etranger: boolean;
  marchandise: boolean;
  valeurUnitaire: number;
  quantite: number;
  type: number;
  calculViaScript: boolean;
}

export interface IPlanningEmployee {
  employeeId: number | string;
  firstName: string;
  lastName: string;
  state: boolean;
  nextDateTachy: Date;
  lastDateEmptyTacho: Date;
  serviceHours: number;
  serviceDays: number;
  absenceDays: number;
  KmNumber: number;
  TotalFrais: string;
  TotalInfractions: number;
  statisticsDay: IEmployeeStatistics[];
}
export interface IEmployeeStatistics {
  day: Date;
  serviceTime: string;
  TotalKm: number;
  TotalFrais: number;
  infractions: boolean;
  echeance: boolean;
  LeaveRequest: boolean;
  LeaveRequestType: boolean;
  Pontage: boolean;
}

export interface IDriverPlanningUtilisation {
  driverId: number | string;
  displayName: string;
  lastLockingDate: string;
  lastUnloadingTacho: string;
  cardRenewDate: string;
  driverPlanningRows: IstatisticsDay[];
}

export interface IstatisticsDay {
  driverId: number | string;
  displayName: string;
  date: Date;
  endTime: string;
  startTime: string;
  amplitude: string;
  interruption: string;
  totalAc: string;
  totalDrive: string;
  totalBreak: string;
  totalDe: string;
  totalMad: string;
  totalNight: string;
  totalService: string;
  totalWork: string;
  absenceCode: string;
  absence: string;
  totalMileage: number;
  deadline: boolean;
  pointed: boolean;
  infractions: IInfractionDriverList[];
  fees: IFeesDriverList[];
}

export interface IPlanningEmployeeApiResponse {
  employeeId: number | string;
  displayName: string;
  state: boolean;
  nextDateTachy: string;
  lastDateEmptyTacho: string;
  serviceHours: number;
  serviceDays: number;
  absenceDays: number;
  KmNumber: number;
  TotalFrais: number;
  TotalInfractions: number;

  lastLockingDate: string;
  lastUnloadingTacho: string;
  cardRenewDate: string;
  day: number;

  statisticsDay: IstatisticsDay[];
}
export interface IAbsenceTypeList {
  id: number | string;
  code: string;
  libelle: string;
  color: number;
  profiles: IAssociatedProfiles[];
}

export interface IAssociatedProfiles {
  id: number | string;
  libelle: string;
}

export interface ApiRespenseAbscenceTypes {
  id: number | string;
  libAbs: string;
  codeAbs: string;
  colorAbs: number;
  concernedProfiles: string;
  profiles: IAssociatedProfiles[];
}
export interface IModificationsType {
  id: number | string;
  libelle: string;
  type: number;
  codeAbs: string;
  creationDate: Date;
  modificationDate: Date;
}

export interface IApiResponceModificationsType {
  id: number | string;
  libAbs: string;
}
export interface IEnumeration {
  id: number | string;
  libelle: string;
  type: number;
  codeAbs: string;
  creationDate: Date;
  modificationDate: Date;
}

export interface IApiResponceEnumeration {
  id: number | string;
  libAbs: string;
}
export interface ICalibrationsVehicle {
  id: number | string;
  vehicleId: number | string;
  registration: string;
  infosStation: string;

  oldDate: Date;
  newDate: Date;
  oldTime: string;
  newTime: string;
  nextDate: Date;

  oldKm: number;
  newKm: number;
  maxSpeed: number;
  tireCirc: number;
  tireSize: string;
  country: number;
  constantW: number;
  constantK: number;
  purpose: number;
}

export interface IApiResponceCalibration {
  date: string;
  oldCounter: number;
  country: number;
  station: string;
  tireSize: string;
  circonférenceTire: number;
  maxSpeed: number;
  dateNextControl: string;
  constantW: number;
  constantK: number;
  registration: string;
}
export interface IVehicleLocking {
  id: number | string;
  vehicleId: number | string;
  startDate: Date;
  startTime: string;
  endDate: Date;
  endTime: string;
  companyName: string;
  companyAddress: string;
  companyCountry: string;
  companyCardNumeber: string;
}

export interface IApiResponceLocking {
  entreprise: string;
  cardNumber: string;
  country: string;
  station: string;
  start: string;
  end: string;
}
export interface IParticularCondition {
  id: number | string;
  driverId: number | string;
  displayName: string;
  conditionDateTime: Date;
  conditionDateTimeFin: Date;
  //
  conditionDate?: Date;
  condtionTime?: string;
  conditionType?: string;
  type?: number;
  state?: number;
  manual?: boolean;
}

export interface ISpecialConditionsApiResponse {
  id: number | string;
  driverId: number | string;
  displayName: string;
  conditions: IConditionsDetails[];
}

export interface IConditionsDetails {
  id: number | string;
  conditionDate: Date;
  condtionTime: string;
  conditionType: string;
  type: number;
  state: number;
  manual: boolean;
  isSelected: boolean;
}

export interface IDeadlineTypeList {
  id: number | string;
  deadlineTypeName: string;
  displayName: string;
  concerned: string;
  type: number;
  typeLibelle: string;
}

export interface IApiResponceDeadlineTypeList {
  id: number | string;
  lib: string;
  concerned: string;
  deadlineType: string;
}

export interface ILeaveRequest {
  id: string;
  driverId?: number;
  displayName: string;
  requestDate: string;
  leaveDate: {
    from: string;
    to: string;
  };
  leaveDates?: string;
  duration: number;
  nature: string;
  alreadyTaken: number;
  sold: number;
  absencesSoldes: boolean;
  reasonCode?: string;
  comment?: string;
}

export interface IBreadCrumb {
  url: string;
  iconUrl?: string;
  label: string;
}

export interface IUserDetailsData {
  id: number | string;
  name: string;
  surname: string;
  password: string;
  role: string;
  address: string;
  zipCode: string;
  city: string;
  country: string;
  mail: string;
  phoneNumber: string;
  preferredLanguage: string;
}

export interface IUserCredentials {
  id: string;
  password: string;
}
export interface IPasswrodStrength {
  name: string;
  pattern: RegExp;
  label: string;
}

export interface DriverCard {
  id: string;
  cardNumber: string;
  stateDeliverCard: string;
  authorityDeliverCard: string;
  deliveryDate: string;
  validityStartDate: string;
  validityEndDate: string;
  validityEndActive: string;
  dateLastUnloading: string;
}
export interface IDrivingWithoutCard{
    vehiculeId:string;
    registration: string;
    displayName: string;
    drivesWithoutCard:IDriviesWithoutCard[]
}

export interface IDriviesWithoutCard{
        id:string;
        justification: string;
        date: Date;
        startHour: string;
        endHour: string;
        duration: string;
        conducteurId: string;
        conducteurNom: string
}
export interface VehicleSataic {
  ac: number;
  break: number;
  de: number;
  driving: number;
  indexKilometers: number;
  mad: number;
  monthKilometers: number;
  work: number;
  totalMileageDays?: DailyTotal[];
  totalServiceDays?: DailyTotal[];
}
export interface DailyTotal {
  date: Date;
  mileage: number;
}
export interface IResponseRestitutionsStorage {
  files: IFilesRestitutionsStorage[],
  relatedFiles: IFilesRestitutionsStorage[],
}

export interface IFilesRestitutionsStorage {
  id: string;
  fileName: string;
  tachofileType: number;
  checksum: string;
  tachofileDbStorageId: Date;
  tachofileDispatcherId: Date;
  status: string;
  dateDebutTraitement: string;
  dateFinTraitement: string;
  conducteurId: string;
  conducteurName: string;
  vehiculeId: string;
  vehiculeImmat: string;
  dateDebutActivite: Date;
  dateFinActivite: Date;
}


export interface IUserProfileParamaters {
  id: string;
  utilisateurId: string;
  heureUTC: boolean;
  fuseauxHoraire: string;
  heureEteHiverActif: boolean;
  decalageMinuteHeureEte: number;
  heureNuitDebut?: string;
  heureNuitFin?: string;
  minuteNuitDebut?: number;
  minuteNuitFin?: number;
}
export interface ISuppHoursProfile {
    id: string;
    heureSuppLibelle: string;
    uniteTempsLibelle: string;
    nombrePeriodeUniteTemps: number;
    seuilCompensation: {
      ticks: number;
      days: number;
      hours: number;
      milliseconds: number;
      minutes: number;
      seconds: number;
      totalDays: number;
      totalHours: number;
      totalMilliseconds: number;
      totalMinutes: number;
      totalSeconds: number
    };
    nbJoursCompensation: number
}


// "": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
//     "": "string",
//     "": "string",
//     "": "string",
//     "": "2021-03-18T13:14:02.925Z",
//     "": {
//       "ticks": 0,
//       "days": 0,
//       "hours": 0,
//       "milliseconds": 0,
//       "minutes": 0,
//       "seconds": 0,
//       "totalDays": 0,
//       "totalHours": 0,
//       "totalMilliseconds": 0,
//       "totalMinutes": 0,
//       "totalSeconds": 0

export interface IINfractionsListByDriver {
  conducteurId: string;
  conducteurName: string;
  codeInfraction: string;
  libelleInfraction: string;
  dateInfraction: string;
  heureInfraction: {
    ticks: number;
    days: number;
    hours: number;
    milliseconds: number;
    minutes: number;
    seconds: number;
    totalDays: number;
    totalHours: number;
    totalMilliseconds: number;
    totalMinutes: number;
    totalSeconds: number
  };
}

export interface IMenuOptionsInfoProfile {
  id: number;
  display: string;
  value: string;
}
export interface IPayloadCreateNewSuppHours {
  libelle: string;
  uniteTempsId: string;
  nombrePeriodeUniteTemps: number;
  seuilCompensation: Object;
  nbJoursCompensation: number;
}

export interface IEmployeePointeuse {
  id: number | string;
  activiteId: number | string;
  startDate: number | string;
  endDate: number | string;
  startTime: number | string;
  endTime: number | string;
  collaborateurId: string;
  dateActivite: number | string;
  dure: string;
  immatriculationVehicule: number | string;
  lieuArrive: number | string;
  libelleActivite: number | string;
  libelleGroupeActivite:   string;
  libelleLieuActivite:  string;
  libelleTypeActivite: number | string;
  libelleTypeActivitePointeuse: string;
  lieuDepart: number | string;
  nomAfichageCollaborateur: number | string;
  nombreKilometre: number | string;
  typeActiviteId: number | string;
  typeActivitePointeuseId: number | string;
  typeLieuActiviteId: number | string;
  vehiculeId: number | string;
  heureDebut:number | string;
  heureFine:number | string;
  activite:number | string;
  developement: number | string;
  reunion: number | string;
  deplacemnt: number | string;
  livraison: number | string;
  latitude: number;
  longitude: number;
}

export interface IEmployeePointeuseFiltered {
  collaborateurId: string;
  collaborateurNom: string;
  weekNumber : string;
  startDateWeekly : string;
  endDateWeekly : string;
  dureWeekly : string;
  developementWeekly : string;
  reunionWeekly : string;
  deplacemntWeekly : string;
  livraisonWeekly : string;}

export interface IFilteredActivityPointeuseByMonth {
  year: number;
  weekNumber: number;
  weekStartDate: string;
  weekEndDate: string;
  employeeSummaries: IEmployeesSummariesPointeuse[];
}

export interface IEmployeesSummariesPointeuse {
  collaborateurId: string;
  activitesSummaries: IActivitiesSummariesFiltered[];
}

export interface IActivitiesSummariesFiltered {
  typeActiviteId: string;
  typeActiviteLibelle: string;
  totalDuree: ITotalDureePointeuseFiltered;
}

export interface ITotalDureePointeuseFiltered {
  ticks: number;
  days: number;
  hours: number;
  milliseconds: number;
  minutes: number;
  seconds: number;
  totalDays: number;
  totalHours: number;
  totalMilliseconds: number;
  totalMinutes: number;
  totalSeconds: number;
}

export interface heureDebutActivite {
  hasValue: string;
  value: heureDebutActivitevalue[]
}

export interface heureDebutActivitevalue {
  days: number
  hours: number
  milliseconds: number
  minutes: number
  seconds: number
  ticks: number
  totalDays: number
  totalHours: number
  totalMilliseconds: number
  totalMinutes: number
  totalSeconds: number
}


export interface IUnitTime {
    uniTempsId: string;
    code: string;
    libelle: string;
}


export interface IActivityTypePointeuse {
  id: string;
  code: string;
  libelle: string;
  icone: string;
  codeCouleur: string;
  groupeActiviteId: string;
}

export interface IActivityTypepointeuseParamaters {
  code: string;
  libelle: string;
  icone: string;
  codeCouleur: string;
  // groupeActiviteId: string[];
}

export interface IPreviewGraphEmployee {
  id: number | string;
  activityCode: string;
  activityLibelle: string;
  startTime: string;
  endTime: string;
  duration: number;
}

export interface IDriverListe{
  id: string,
  driverId: string,
  vehicleId: string,
  displayName: string,
  registration: string,
  startKmCounter: number,
  endKmCounter: number,
  departureDate: string,
  departureTime: string,
  arrivalDate: string,
  arrivalTime: string,
  mileage: number,
  state: string
}

export interface IActivityModification {
  previousActivity: string;
  startingHour: string;
  endingHour: string;
  newActivity: string;
}

export interface IActivityModificationTableRow {
  activityId: string;
  previousActivity: string;
  startingHour: string;
  endingHour: string;
  newActivity: string;
  motif: any;
  freeText: any;
}

export interface ILaguage{
  id: string
  code: string,
  label: string
}
export interface ILaguageUpdate{
  userId: string
  languageId: string,
}

export interface IActivityTypeInfo {
  id: string;
  code: string;
  libelle: string;
  icone: string;
  codeCouleur: string;
  groupeActiviteId: string;
}

export interface IActivitesManullesModifications {
  id: string,
  sessionModificationActiviteId: string,
  conducteurId: string,
  vehiculeId: string,
  conducteurAffichage: string,
  immatriculation: string,
  typeActivite: string,
  dateActivite: Date,
  heureDebut: string,
  duree: string,
  nbKilometre: number
}
