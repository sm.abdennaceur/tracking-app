import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'list-radio-button',
  templateUrl: './list-radio-button.component.html',
  styleUrls: ['./list-radio-button.component.scss']
})
export class ListRadioButtonComponent implements OnInit {

  selectedState: string = 'all';
  @Input() items: string;
  @Input() title: string;
  @Output() changeState = new EventEmitter();
  @Output() changeRadioButton = new EventEmitter();
  selectedEmployee: boolean = false;
  constructor() { }

  ngOnInit() {
  }


  onChangeState(event: any) {
    this.changeState.emit(event.value);
    this.selectedEmployee = false;
  }

  onChangeRadioButton(item: any) {
    this.changeRadioButton.emit(item);
  }

}