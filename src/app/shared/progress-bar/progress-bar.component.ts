import { Component, Input, OnInit, SimpleChanges } from "@angular/core";

@Component({
  selector: "progress-bar",
  templateUrl: "./progress-bar.component.html",
  styleUrls: ["./progress-bar.component.scss"],
})
export class ProgressBarComponent implements OnInit {
  @Input() doneCount: number;
  @Input() totalNumber: number;
  progress: number;
  constructor() {}
  ngOnInit() {}
  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    if (this.totalNumber)
      this.progress = 100 * (this.doneCount / this.totalNumber);
    else this.progress = 0;
  }
}
