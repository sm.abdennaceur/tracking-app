import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  HostListener,
  ViewChild,
  SimpleChanges,
} from "@angular/core";
import { faChevronDown, faSearch } from "@fortawesome/free-solid-svg-icons";

 
@Component({
  selector: "search-check-box",
  templateUrl: "./search-check-box.component.html",
  styleUrls: ["./search-check-box.component.scss"],
})
export class SearchCheckBoxComponent implements OnInit {
  currentVehicle: string = "";
  currentEmployee: string = "";

  searchValue = "";
  vehiclesListPageIndex = 0;
  public vehiclesList: any[] = [];
  initVehiclesList;
  isLaoding: boolean = false;
  flag = false;

  faChevronDown = faChevronDown;
  faSearch = faSearch;

  selectedState: string = "all";
  @Input() maxHeight: number
  @Input() category: string;
  @Input() singleSelection: string;

  @Input() items;
  @Input() title: string;
  @Input() showSelect: boolean;
  @Input() backdropClicks: boolean;
  @Input() showSelectAll: boolean;
  @Input() allLoaded: boolean = false;
  @Input() openMenuAndSelectAll: boolean;
  @Input() infractionsScreen: boolean;
  @Input() employeesAnomaliesByType: boolean;

  @Output() changeState = new EventEmitter();
  @Output() changeCheckbox = new EventEmitter();
  @Output() loadMoreClicked = new EventEmitter();
  @Output() searchInput = new EventEmitter();

  SelectedTypes = [];

  @HostListener("body:scroll", ["$event"])
  onWindowScroll(evt) {}

  @ViewChild("menuTrigger", { static: true })
  public menuTrigger: any;
  constructor() {}

  ngOnInit() {
    document
      .querySelector(".wrapper")
      .addEventListener("scroll", this.handleScroll);
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.openMenu();
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    if (this.openMenuAndSelectAll) {
      this.selectAll({ checked: true });
    }
  }
  get myStyle(): any{
    if(this.maxHeight == undefined ) {
      return {
        'height': '937px'
      }
    }
    else {
      // if (this.maxHeight < 222) {
      //   return {
      //     'height': '937px'
      //   }
      // }
        return {
          'max-height':this.maxHeight.toString().concat(...'px')
        }
    }
  }

  openMenu() {
    this.menuTrigger.openMenu();
  }

  handleScroll(event) {
    const scroll = event.target.scrollTop;
    const target = <HTMLElement>document.querySelector(".list-search-menu");
    target.style.transform = `translateY(${-scroll}px)`;
  }

  onChangeState(event: any) {
    this.selectedState = event.value;
    this.changeState.emit(event.value);
  }

  onChangeCheckbox(item: any, index, event: any) {}

  onRowclicked(row: any, index, event: any) {
    event.preventDefault();
    if (!row.checked) {
      this.SelectedTypes.push(row);

      this.singleSelection == "vehicule"
        ? (this.currentVehicle = row.registration)
        : (this.currentEmployee = row.name);
    } else {
      this.singleSelection == "vehicule"
        ? (this.currentVehicle = null)
        : (this.currentEmployee = null);
      this.SelectedTypes = this.SelectedTypes.filter((el) => {
        const cond =
          JSON.stringify({ ...el, checked: true }) !=
          JSON.stringify({ ...row, checked: true });
        return cond;
      });
    }
    row.checked = !row.checked;
    this.items[index].checked = row.checked;
    const payload: any = {
      items: [row],
      event: { checked: row.checked },
    };
    this.changeCheckbox.emit(payload);
  }

  loadMore(event) {
    this.loadMoreClicked.emit(event);
  }

  readInput(event) {
    this.searchInput.emit(event);
  }

  selectAll(event: any) {
    this.SelectedTypes = [];
    const payload: any = {
      items: this.items.map((el) => {
        if (event.checked) {
          this.SelectedTypes.push({ ...el, checked: true });
        }
        return { ...el, checked: event.checked ? true : false };
      }),
      event: event,
    };

    this.changeCheckbox.emit(payload);
  }
}
