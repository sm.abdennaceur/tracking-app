import { Component, OnInit, Input } from '@angular/core';
import { SearchService } from '../../core/services/navbar/search.service';

@Component({
  selector: 'search-zone',
  templateUrl: './search-zone.component.html',
  styleUrls: ['./search-zone.component.scss'],
})
export class SearchZoneComponent implements OnInit {
  constructor(private seachService: SearchService) {}
  value: string = '';
  @Input() disabled: boolean = false;
  ngOnInit() {
    this.value = this.seachService.value;
  }
  emitSearchValue(event) {
    this.seachService.setValue(event);
  }
}
