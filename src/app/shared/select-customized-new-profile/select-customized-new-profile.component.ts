import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { IMenuOptionsInfoProfile } from '../interfaces';

@Component({
  selector: 'select-customized-new-profile',
  templateUrl: './select-customized-new-profile.component.html',
  styleUrls: ['./select-customized-new-profile.component.scss']
})
export class SelectCustomizedNewProfileComponent implements OnInit {

  @Input() activitelibelle; //list of options to be displayed
  @Input() indemnitySelect: boolean;
  @Input() defaultLib: string = "L_SelectCustomizedNewProfile_ChooseTheTypeOfTimeBase";
  @Input() styleAddComment:boolean=false
  @Output() settingSelectedOption = new EventEmitter<IMenuOptionsInfoProfile>();


  menuIcon:boolean=false;
  selectedOption: IMenuOptionsInfoProfile = {
    id: 11,
    display: this.defaultLib,
    value: ""
  };

  constructor() { }

  ngOnInit() {
    this.selectedOption.display = this.defaultLib;
  }

  iconboolean(matMenu?){
    this.menuIcon=!this.menuIcon
    // console.log(this.indemnitySelect);
  }

  selectNewOption(itemSelected) {
    // change selected Option when the user clicks
    this.selectedOption = itemSelected;
    // console.log("%c Selected Option is :", "color: pink", this.selectedOption);
    this.settingSelectedOption.emit(this.selectedOption);
  }

}
