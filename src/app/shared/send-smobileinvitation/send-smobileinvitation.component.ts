import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
export interface IInvitation {
  Collaborateur: string;
  Statut: string;
  Motif: string;
}
@Component({
  selector: 'app-send-smobileinvitation',
  templateUrl: './send-smobileinvitation.component.html',
  styleUrls: ['./send-smobileinvitation.component.scss'],
})
export class SendSmobileinvitationComponent implements OnInit {
  listDriver: any[] = [];
  columnsWithoutSelect: any[] = ['Collaborateur', 'Statut', 'Motif'];
  dataForm: IInvitation[] = [];
  listId: any[] = [];
  public dataSource = new MatTableDataSource<IInvitation>([]);

  constructor(
    public dialogRef: MatDialogRef<SendSmobileinvitationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    console.log('dalalalalalala', data);
    this.listDriver = data['list'].map((x) => x.firstName);
    this.listId = data['list'].map((y) => y.id);
    for (var i = 0; i < 3; i++) {
      this.dataForm.push({ Collaborateur: this.listDriver[0], Statut: 'Succes', Motif: '' });
    }
    for (var i = 0; i < 2; i++) {
      this.dataForm.push({ Collaborateur: this.listDriver[0], Statut: 'Erreur', Motif: 'Pas d’adresse mail' });
    }
    this.dataSource = new MatTableDataSource<IInvitation>(this.dataForm);
    console.log('listadriver', this.listDriver);
  }

  ngOnInit() {}
}
