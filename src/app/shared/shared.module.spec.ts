import { TestBed } from '@angular/core/testing';
import { SharedModule } from './shared.module';
import { HeaderCalendarComponent } from './vehicle-calendar/header-calendar/header-calendar.component';
describe('SharedModule', () => {
  let pipe: SharedModule;
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [SharedModule] });
    pipe = TestBed.get(SharedModule);
  });
  it('can load instance', () => {
    expect(pipe).toBeTruthy();
  });
});
