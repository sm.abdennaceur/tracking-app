import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AngularMaterialModule } from '../angular-material/angular-material.module';

import { MatButtonToggleModule } from '@angular/material/button-toggle';
//import { BreadcrumbComponent } from '../shared/breadcrumb/breadcrumb.component';

import { DragDropDirective } from '../drag-drop.directive';

import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
// import { Group } from './reports-data-table/reports-data-table.component';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
// import { PreviewVehicleComponent } from "../ /preview-vehicle/preview-vehicle.component";
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRippleModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { FilterComponent } from './filter/filter.component';
import { DialoRazComponent } from './dialo-raz/dialo-raz.component';
import { HeaderCalendarComponent } from './vehicle-calendar/header-calendar/header-calendar.component';
import { ListRadioButtonComponent } from './list-radio-button/list-radio-button.component';
import { SendSmobileinvitationComponent } from './send-smobileinvitation/send-smobileinvitation.component';
import { SelectCustomizedNewProfileComponent } from './select-customized-new-profile/select-customized-new-profile.component';
import { SearchZoneComponent } from './search-zone/search-zone.component';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { SearchCheckBoxComponent } from './search-check-box/search-check-box.component';

@NgModule({
  imports: [
    CommonModule,
    AngularSvgIconModule,
    AngularMaterialModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MatButtonToggleModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBU3fyx7thOvVj4K53uNR3N-c9cOUR_62g',
    }),
    AgmDirectionModule,
    Ng2SearchPipeModule,
    TranslateModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatMenuModule,
  ],

  declarations: [
    DragDropDirective,
    BreadcrumbComponent,
    FilterComponent,
    DialoRazComponent,
    HeaderCalendarComponent,
    ListRadioButtonComponent,
    SendSmobileinvitationComponent,
    SelectCustomizedNewProfileComponent,
    SearchZoneComponent,
    ProgressBarComponent,
    SearchCheckBoxComponent
  ],
  exports: [
    DragDropDirective,
    AutocompleteLibModule,
    BreadcrumbComponent,
    MatButtonModule,
    MatFormFieldModule,
    MatMenuModule,
    MatInputModule,
    MatRippleModule,
  ],
  providers: [],
})
export class SharedModule {}
