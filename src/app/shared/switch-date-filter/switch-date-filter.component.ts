import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  ViewChild,
  NgModule,
  Renderer2,
} from "@angular/core";
import { MatDatepickerInputEvent } from "@angular/material/datepicker";
import { NavbarService } from "../../core/services/navbar.service";
import { HeaderCalendarComponent } from "../vehicle-calendar/header-calendar/header-calendar.component";
import * as moment from "moment/moment";
import { NgbCalendar, NgbDate } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "switch-date-filter",
  templateUrl: "./switch-date-filter.component.html",
  styleUrls: ["./switch-date-filter.component.scss"],
})
export class SwitchDateFilterComponent implements OnInit {
  dateRange: any = {};
  dayDate;
  fromDate: NgbDate = null;
  toDate: NgbDate = null;
  startTime: string = "00:00";
  endTime: string = "23:59";
  view: string = "day";
  timePicker: boolean = true;
  @Output() changeView = new EventEmitter();
  @Input() periodeSelector: boolean

  constructor(
    public navbar: NavbarService,
    private renderer: Renderer2,
    private calendar: NgbCalendar
  ) {
    this.fromDate = <NgbDate>{
      year: 2010,
      month: 10,
      day: 11,
    };
    this.toDate = <NgbDate>{
      year: 2010,
      month: 10,
      day: 11,
    };
    this.dayDate = this.fromDate;
  }
  ngOnInit() {
    this.navbar.currentDate.subscribe(data=>{
      this.view = data.interval || 'day';
    });

    this.dateRange.startDate = new Date();
    this.dateRange.endDate = new Date();
    this.fromDate = <NgbDate>{
      year: this.dateRange.startDate.getFullYear(),
      month: this.dateRange.startDate.getMonth(),
      day: this.dateRange.startDate.getDate(),
    };
    this.navbar.currentDate.subscribe((date) => {
      if (
        date.interval !== this.view ||
        date.startDate !== this.dateRange.startDate ||
        date.endDate !== this.dateRange.endDate
      ) {
        this.dateRange.startDate = date.startDate;
        this.dateRange.endDate = date.endDate;
        this.fromDate = <NgbDate>{
          year: this.dateRange.startDate.getFullYear(),
          month: this.dateRange.startDate.getMonth() + 1,
          day: this.dateRange.startDate.getDate(),
        };
        this.dayDate = this.fromDate;
        this.toDate = <NgbDate>{
          year: this.dateRange.endDate.getFullYear(),
          month: this.dateRange.endDate.getMonth() + 1,
          day: this.dateRange.endDate.getDate(),
        };
        this.view = date.interval;
      }
    });
  }
  NgbDateSelected(date) {
    this.dayDate = date.fromDate;
    this.dateRange = date;
    this.navbar.setDate(this.dateRange);
  }

  changeViewByInterval(view: string) {
    this.view = view;
    switch (view) {
      case "day":
        this.dateRange.startDate = new Date(this.dayDate);
        this.dateRange.endDate = new Date(this.dayDate);
        this.fromDate = this.dayDate;
        this.toDate = this.dayDate;
        break;
      case "week":
        this.fromDate = this.calendar.getPrev(
          this.dayDate,
          "d",
          this.calendar.getWeekday(this.dayDate) - 1
        );
        this.toDate = this.calendar.getNext(this.fromDate, "d", 6);
        break;
      case "month":
        this.fromDate = this.calendar.getPrev(
          this.dayDate,
          "d",
          this.dayDate.day - 1
        );
        this.toDate = this.calendar.getPrev(
          this.calendar.getNext(this.fromDate, "m", 1),
          "d",
          1
        );
        break;
    }
    setTimeout(() => {
      this.dateRange.interval = view;
      this.dateRange.startDate = new Date(
        this.fromDate.year +
          "-" +
          this.fromDate.month +
          "-" +
          this.fromDate.day +
          " " +
          this.startTime
      );
      this.dateRange.endDate = new Date(
        this.toDate.year +
          "-" +
          this.toDate.month +
          "-" +
          this.toDate.day +
          " " +
          this.endTime
      );
      const params = { view: view, dateRange: this.dateRange };
      this.changeView.emit(params);
      this.navbar.setDate(this.dateRange);
    });
  }
}
