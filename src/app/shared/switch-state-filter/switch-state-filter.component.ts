import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../../core/services/navbar.service';
import { IStateFilter } from '../interfaces';

@Component({
  selector: 'switch-state-filter',
  templateUrl: './switch-state-filter.component.html',
  styleUrls: ['./switch-state-filter.component.scss'],
})
export class SwitchStateFilterComponent implements OnInit {
  state: IStateFilter = 'all';
  constructor(public navbar: NavbarService) {}

  ngOnInit() {
    this.navbar.currentState.subscribe((state) => {
      this.state = state;
    });
  }

  changeState(state: IStateFilter) {
    this.navbar.setState(state);
  }
}
