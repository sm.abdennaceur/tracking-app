import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

export interface TimeParams {
  id: string;
  fuseau: string;
  villes: string;
}
@Injectable({
  providedIn: 'root',
})
export class TimeParamsService {
  constructor() {}

  convertToLocalTime(date: string) {
    let dateForm = new Date(date);
    var newDate = new Date(dateForm.getTime() + dateForm.getTimezoneOffset() * 60 * 1000);
    var offset = dateForm.getTimezoneOffset() / 60;
    var hours = dateForm.getHours();
    newDate.setHours(hours - offset);
    // if we return newDate, we will get UTC+1
    //return newDate.toISOString();
    return dateForm.toISOString();
    //return date;
  }

  convertToUTC(
    operation: string,
    date: string,
    additionalHoures: number,
    additionalMinutes: number,
    additionalMinutesSummer: number
  ) {
    let isSummerWinter = JSON.parse(localStorage.getItem('user_params')).heureEteHiverActif;
    let dateForm = new Date(date);
    var today = new Date();

    let stdTimezoneOffset = function () {
      var jan = new Date(today.getFullYear(), 0, 1);
      var jul = new Date(today.getFullYear(), 6, 1);
      return Number(jan.getTimezoneOffset()) - Number(jul.getTimezoneOffset());
    };

    let isDstObserved = () => {
      return 0 !== stdTimezoneOffset();
    };

    if (isSummerWinter) {
      if (operation == '-') {
        var now_utc = Date.UTC(
          dateForm.getUTCFullYear(),
          dateForm.getUTCMonth(),
          dateForm.getUTCDate(),
          dateForm.getUTCHours() - additionalHoures,
          dateForm.getUTCMinutes() - additionalMinutes + additionalMinutesSummer,
          dateForm.getUTCSeconds()
        );
      } else {
        var now_utc = Date.UTC(
          dateForm.getUTCFullYear(),
          dateForm.getUTCMonth(),
          dateForm.getUTCDate(),
          dateForm.getUTCHours() + additionalHoures,
          dateForm.getUTCMinutes() + additionalMinutes + additionalMinutesSummer,
          dateForm.getUTCSeconds()
        );
      }
    } else {
      if (operation == '-') {
        var now_utc = Date.UTC(
          dateForm.getUTCFullYear(),
          dateForm.getUTCMonth(),
          dateForm.getUTCDate(),
          dateForm.getUTCHours() - additionalHoures,
          dateForm.getUTCMinutes() - additionalMinutes,
          dateForm.getUTCSeconds()
        );
      } else {
        var now_utc = Date.UTC(
          dateForm.getUTCFullYear(),
          dateForm.getUTCMonth(),
          dateForm.getUTCDate(),
          dateForm.getUTCHours() + additionalHoures,
          dateForm.getUTCMinutes() + additionalMinutes,
          dateForm.getUTCSeconds()
        );
      }
      // } else if (!isDstObserved() && isSummerWinter) {
      //   console.log("false true" )

      //   if (operation == '-') {
      //     var now_utc = Date.UTC(
      //       dateForm.getUTCFullYear(),
      //       dateForm.getUTCMonth(),
      //       dateForm.getUTCDate(),
      //       dateForm.getUTCHours() - additionalHoures,
      //       dateForm.getUTCMinutes() - additionalMinutes + additionalMinutesSummer,
      //       dateForm.getUTCSeconds()
      //     );
      //   } else {
      //     var now_utc = Date.UTC(
      //       dateForm.getUTCFullYear(),
      //       dateForm.getUTCMonth(),
      //       dateForm.getUTCDate(),
      //       dateForm.getUTCHours() + additionalHoures,
      //       dateForm.getUTCMinutes() + additionalMinutes + additionalMinutesSummer,
      //       dateForm.getUTCSeconds()
      //     );
      //   }
      // } else if (isDstObserved() && !isSummerWinter) {
      //   console.log("true false" )

      //   if (operation == '-') {
      //     var now_utc = Date.UTC(
      //       dateForm.getUTCFullYear(),
      //       dateForm.getUTCMonth(),
      //       dateForm.getUTCDate(),
      //       dateForm.getUTCHours() - additionalHoures,
      //       dateForm.getUTCMinutes() - additionalMinutes ,
      //       dateForm.getUTCSeconds()
      //     );
      // } else {
      //   var now_utc = Date.UTC(
      //     dateForm.getUTCFullYear(),
      //     dateForm.getUTCMonth(),
      //     dateForm.getUTCDate(),
      //     dateForm.getUTCHours() + additionalHoures,
      //     dateForm.getUTCMinutes() + additionalMinutes ,
      //     dateForm.getUTCSeconds()
      //   );
      // }
    }

    var newDate = new Date(dateForm.getTime() + dateForm.getTimezoneOffset() * 60 * 1000);
    var offset = dateForm.getTimezoneOffset() / 60;
    var hours = dateForm.getHours();
    newDate.setHours(hours - offset);
    return new Date(now_utc).toISOString();
    //return date;
  }
}
