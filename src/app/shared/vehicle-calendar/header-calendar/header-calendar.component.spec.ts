import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';

import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { HeaderCalendarComponent } from './header-calendar.component';
import { MatCalendar, MatDatepickerIntl } from '@angular/material/datepicker';
import { DateAdapter } from '@angular/material/core';
describe('HeaderCalendarComponent', () => {
  let component: HeaderCalendarComponent;
  let fixture: ComponentFixture<HeaderCalendarComponent>;
  beforeEach(() => {
    const changeDetectorRefStub = () => ({ markForCheck: () => ({}) });
    const matCalendarStub = () => ({
      stateChanges: { subscribe: (f) => f({}) },
      activeDate: {},
      currentView: {},
      minDate: {},
      maxDate: {},
    });
    const matDateFormatsStub = () => ({ display: { monthYearA11yLabel: {} } });
    const dateAdapterStub = () => ({
      format: (arg, monthYearA11yLabel) => ({}),
      createDate: (arg, arg2, number) => ({}),
      getYear: (activeDate) => ({}),
      getMonth: (activeDate) => ({}),
      addCalendarYears: (activeDate, arg1) => ({}),
      addCalendarMonths: (activeDate, arg1) => ({}),
    });
    const matDatepickerIntlStub = () => ({});
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [HeaderCalendarComponent],
      providers: [
        { provide: ChangeDetectorRef, useFactory: changeDetectorRefStub },
        { provide: MatCalendar, useFactory: matCalendarStub },
        { provide: DateAdapter, useFactory: dateAdapterStub },
        { provide: MatDatepickerIntl, useFactory: matDatepickerIntlStub },
      ],
    });
    spyOn(HeaderCalendarComponent.prototype, 'getLabel');
    fixture = TestBed.createComponent(HeaderCalendarComponent);
    component = fixture.componentInstance;
  });
  it('can load instance', () => {
    expect(component).toBeTruthy();
  });
  it('faChevronLeft defaults to: faChevronLeft', () => {
    expect(component.faChevronLeft).toEqual(faChevronLeft);
  });
  it('faChevronRight defaults to: faChevronRight', () => {
    expect(component.faChevronRight).toEqual(faChevronRight);
  });
  describe('constructor', () => {
    it('makes expected calls', () => {
      expect(HeaderCalendarComponent.prototype.getLabel).toHaveBeenCalled();
    });
  });
  // describe('getLabel', () => {
  //   it('makes expected calls', () => {
  //     const dateAdapterStub: DateAdapter = fixture.debugElement.injector.get(
  //       DateAdapter
  //     );
  //     spyOn(dateAdapterStub, 'format').and.callThrough();
  //     spyOn(dateAdapterStub, 'createDate').and.callThrough();
  //     spyOn(dateAdapterStub, 'getYear').and.callThrough();
  //     spyOn(dateAdapterStub, 'getMonth').and.callThrough();
  //     (<jasmine.Spy>component.getLabel).and.callThrough();
  //     component.getLabel();
  //     expect(dateAdapterStub.format).toHaveBeenCalled();
  //     expect(dateAdapterStub.createDate).toHaveBeenCalled();
  //     expect(dateAdapterStub.getYear).toHaveBeenCalled();
  //     expect(dateAdapterStub.getMonth).toHaveBeenCalled();
  //   });
  // });
  // describe('previousClicked', () => {
  //   it('makes expected calls', () => {
  //     const dateAdapterStub: DateAdapter = fixture.debugElement.injector.get(
  //       DateAdapter
  //     );
  //     (<jasmine.Spy>component.getLabel).calls.reset();
  //     spyOn(dateAdapterStub, 'addCalendarYears').and.callThrough();
  //     spyOn(dateAdapterStub, 'addCalendarMonths').and.callThrough();
  //     component.previousClicked();
  //     expect(component.getLabel).toHaveBeenCalled();
  //     expect(dateAdapterStub.addCalendarYears).toHaveBeenCalled();
  //     expect(dateAdapterStub.addCalendarMonths).toHaveBeenCalled();
  //   });
  // });
  // describe('nextClicked', () => {
  //   it('makes expected calls', () => {
  //     const dateAdapterStub: DateAdapter = fixture.debugElement.injector.get(
  //       DateAdapter
  //     );
  //     (<jasmine.Spy>component.getLabel).calls.reset();
  //     spyOn(dateAdapterStub, 'addCalendarYears').and.callThrough();
  //     spyOn(dateAdapterStub, 'addCalendarMonths').and.callThrough();
  //     component.nextClicked();
  //     expect(component.getLabel).toHaveBeenCalled();
  //     expect(dateAdapterStub.addCalendarYears).toHaveBeenCalled();
  //     expect(dateAdapterStub.addCalendarMonths).toHaveBeenCalled();
  //   });
  // });
  describe('selectYear', () => {
    it('makes expected calls', () => {
      (<jasmine.Spy>component.getLabel).calls.reset();
      component.selectYear();
      expect(component.getLabel).toHaveBeenCalled();
    });
  });
});
