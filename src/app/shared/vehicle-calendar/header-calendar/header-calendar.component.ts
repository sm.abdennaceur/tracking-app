import {
  Component,
  ViewEncapsulation,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Inject,
  Optional,
  forwardRef,
  Output,
  EventEmitter,
} from '@angular/core';
import { DateAdapter, MatDateFormats, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatCalendar, MatDatepickerIntl } from '@angular/material/datepicker';

import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header-calendar',
  templateUrl: './header-calendar.component.html',
  styleUrls: ['./header-calendar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderCalendarComponent {
  faChevronLeft = faChevronLeft;
  faChevronRight = faChevronRight;
  label: string;

  constructor(
    private intl: MatDatepickerIntl,
    @Inject(forwardRef(() => MatCalendar)) public calendar: MatCalendar<Date>,
    @Optional() private dateAdapter: DateAdapter<Date>,
    @Optional() @Inject(MAT_DATE_FORMATS) private dateFormats: MatDateFormats,
    changeDetectorRef: ChangeDetectorRef
  ) {
    this.calendar.stateChanges.subscribe(() => changeDetectorRef.markForCheck());
    this.label = this.getLabel();
  }
  get prevButtonLabel(): string {
    return this.intl.prevMonthLabel;
  }

  get nextButtonLabel(): string {
    return this.intl.nextMonthLabel;
  }

  getLabel() {
    return this.dateAdapter.format(
      this.dateAdapter.createDate(
        this.dateAdapter.getYear(this.calendar.activeDate),
        this.dateAdapter.getMonth(this.calendar.activeDate),
        1
      ),
      this.dateFormats.display.monthYearA11yLabel
    );
  }

  previousClicked(): void {
    switch (this.calendar.currentView) {
      case 'multi-year':
        this.calendar.activeDate = this.dateAdapter.addCalendarYears(this.calendar.activeDate, -1);
        break;
      default:
        this.calendar.activeDate = this.dateAdapter.addCalendarMonths(this.calendar.activeDate, -1);
        this.emitDate();
        break;
    }
    this.label = this.getLabel();
  }

  nextClicked(): void {
    switch (this.calendar.currentView) {
      case 'multi-year':
        this.calendar.activeDate = this.dateAdapter.addCalendarYears(this.calendar.activeDate, 1);
        break;
      default:
        this.calendar.activeDate = this.dateAdapter.addCalendarMonths(this.calendar.activeDate, 1);
        this.emitDate();
        break;
    }
    this.label = this.getLabel();
  }

  previousEnabled(): boolean {
    if (!this.calendar.minDate) {
      return true;
    }
    return !this.calendar.minDate || !this._isSameView(this.calendar.activeDate, this.calendar.minDate);
  }

  nextEnabled(): boolean {
    return !this.calendar.maxDate || !this._isSameView(this.calendar.activeDate, this.calendar.maxDate);
  }

  private _isSameView(date1: Date, date2: Date): boolean {
    return (
      this.dateAdapter.getYear(date1) === this.dateAdapter.getYear(date2) &&
      this.dateAdapter.getMonth(date1) === this.dateAdapter.getMonth(date2)
    );
  }
  selectYear() {
    this.calendar.currentView = 'multi-year';
    this.label = this.getLabel();
  }

  emitDate() {
    this.calendar.selectedChange.emit(this.calendar.activeDate);
  }
}
