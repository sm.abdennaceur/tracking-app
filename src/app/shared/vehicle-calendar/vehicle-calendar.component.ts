import {
  Component,
  ViewChild,
  Host,
  ChangeDetectorRef,
  Inject,
  EventEmitter,
  AfterViewInit,
  NgZone,
  OnChanges,
  Input,
  Renderer2,
  ElementRef,
  OnDestroy,
  Output,
  SimpleChange,
  SimpleChanges,
} from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS, MatDateFormats, NativeDateAdapter } from '@angular/material/core';
import * as moment from 'moment';
import { Subscription } from 'rxjs';

import { HeaderCalendarComponent } from './header-calendar/header-calendar.component';
import { DatePipe } from '@angular/common';
import { CdkDragHandle } from '@angular/cdk/drag-drop';
import * as $ from 'jquery';
import { TranslateService } from '@ngx-translate/core';
import { MatCalendar } from '@angular/material/datepicker';

const DAYS_PER_WEEK = 7;

export class CustomDateAdapter extends NativeDateAdapter {
  // private localeData = moment.localeData('fr-FR');
  date = new Date();

  getFirstDayOfWeek(): number {
    return 1;
  }

  // getDayOfWeekNames(style: 'long' | 'short' | 'narrow'): string[] {
  //   switch (style) {
  //     case 'long':
  //       return this.localeData.weekdays();
  //     case 'short':
  //       return this.localeData.weekdaysShort();
  //     case 'narrow':
  //       return this.localeData.weekdaysShort();
  //   }
  // }
}

@Component({
  selector: 'app-vehicle-calendar',
  templateUrl: './vehicle-calendar.component.html',
  styleUrls: ['./vehicle-calendar.component.scss'],
  providers: [{ provide: DateAdapter, useClass: CustomDateAdapter }],
})
export class VehicleCalendarComponent implements OnDestroy, AfterViewInit, OnChanges {
  header = HeaderCalendarComponent;

  @ViewChild(MatCalendar, { static: true }) calendar: MatCalendar<Date>;
  @Input() vehicleDataCalendar: any[] = [];
  @Input() withIndicator: boolean = true;
  @Input() employeeData: boolean = false;
  @Input() unit: string = 'km';
  today: Date;
  @Output() changedDate = new EventEmitter();
  @Output() monthChanged = new EventEmitter();
  selectedDate = new Date();
  activeDate: Date;
  listenClickNextBtnFunc: any;
  listenClickPrevBtnFunc: any;
  private subscriptions: Subscription[] = [];

  constructor(
    @Inject(MAT_DATE_FORMATS) private dateFormats: MatDateFormats,
    private element: ElementRef,
    private renderer: Renderer2,
    private adapter: DateAdapter<Date>,
    private translate: TranslateService,
    private dateAdapter: DateAdapter<Date>
  ) {
    this.today = this.adapter.today();
    this.activeDate = this.adapter.today();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.calendar.stateChanges.next();
    if (changes.vehicleDataCalendar) this.getDates();
    //this.dateAdapter.setLocale('fr');
    this.dateAdapter.setLocale(this.translate.currentLang);
  }

  ngAfterViewInit() {
    let btnPrev = null;
    let btnNext = null;

    btnPrev = this.element.nativeElement.querySelector('.calendar-previous-button');
    btnNext = this.element.nativeElement.querySelector('.calendar-next-button');

    if (btnNext) {
      this.listenClickNextBtnFunc = this.renderer.listen(btnNext, 'click', (event: MouseEvent) => {
        if (this.calendar.currentView === 'month') {
          // this.activeDate = this.adapter.addCalendarMonths(this.activeDate, 1);
          this.handelCalenderOutOfMonths();
          // this.getDates();
        }
      });
    }

    if (btnPrev) {
      this.listenClickPrevBtnFunc = this.renderer.listen(btnPrev, 'click', (event: MouseEvent) => {
        if (this.calendar.currentView === 'month') {
          // this.activeDate = this.adapter.addCalendarMonths(this.activeDate, -1);
          // this.getDates();
          this.handelCalenderOutOfMonths();
        }
      });
    }

    if (this.calendar.currentView === 'month') {
      // setTimeout(() => this.getDates());
    }
    this.handelCalenderOutOfMonths();
  }
  handelCalenderNextOutOfMonths() {
    const field = $(".mat-calendar-body tr[role='row']");
    let html;
    const count = $(field)[field.length - 1].childElementCount;
    for (let i = 1; i <= 7 - count; i++) {
      html = `<td class="mat-calendar-body-cell mat-disabled-cell  ng-star-inserted" role="button" >
      <div class="mat-calendar-body-cell-content rest-point"><span class="day">${i}</span>
      </div>
      </td>`;
      $($(field)[field.length - 1]).append(html);
    }

    $(".mat-calendar-body tr:not([role='row'])").remove();
    if (field.length < 6) {
      const table = $('.mat-calendar-body');
      let content = `<tr>`;
      for (let index = 7 - count + 1; index < 15 - count; index++) {
        content += `
          <td class="1 mat-calendar-body-cell mat-disabled-cell  ng-star-inserted" role="button">
          <div class="mat-calendar-body-cell-content rest-point"><span class="day">${index}</span>
          </div>
          </td>
        `;
      }
      table.append(`
         ${content}
         </tr>
         `);
    }
  }

  LastDayOfAMonth() {
    const month = this.calendar.activeDate.getMonth();
    const year = this.calendar.activeDate.getFullYear();
    return new Date(year, month, 0).getDate();
  }
  addOutOfMonthFields(i: number) {
    const field = $('.mat-calendar-body-label')[i];
    const nbOfEmptyFields = $(field).attr('colspan');
    const lastDayOfPreviousMonth = this.LastDayOfAMonth();
    $(field).attr('colspan', 0);
    $(field).html(`
    <div class="mat-calendar-body-cell-content rest-point">
      <span class="day">${lastDayOfPreviousMonth - parseInt(nbOfEmptyFields) + 1}</span>
    </div>`);
    $(field).removeClass();
    $(field).addClass('mat-calendar-body-cell mat-disabled-cell ng-star-inserted');
    for (let index = 0; index < parseInt(nbOfEmptyFields) - 1; index++) {
      $(field).after(`
      <td class="mat-calendar-body-cell mat-disabled-cell  ng-star-inserted" role="button" >
        <div class="mat-calendar-body-cell-content rest-point">
        <span class="day">${lastDayOfPreviousMonth - index}</span>
        </div>
      </td>`);
    }
  }
  monthSelected(event) {
    this.activeDate = event;
    this.monthChanged.emit(event);
    this.changedDate.emit(event);
    // this.getDates();
    this.handelCalenderOutOfMonths();
  }
  handelCalenderOutOfMonths() {
    if ($('.mat-calendar-body-label').length && parseInt($('.mat-calendar-body-label').attr('colspan')) < 7) {
      this.addOutOfMonthFields(0);
    } else {
      this.addOutOfMonthFields(1);
    }
    this.handelCalenderNextOutOfMonths();
  }

  ngOnDestroy() {
    if (this.listenClickNextBtnFunc) {
      this.listenClickNextBtnFunc();
    }
    if (this.listenClickPrevBtnFunc) {
      this.listenClickPrevBtnFunc();
    }

    this.subscriptions.map((s) => s.unsubscribe());
  }

  getDates() {
    const dates = this.getMonthsDates();
    dates.forEach((d) => this.addPointsToDates(d));
  }

  getMonthsDates() {
    const firstOfMonth = this.adapter.createDate(
      this.adapter.getYear(this.activeDate),
      this.adapter.getMonth(this.activeDate),
      1
    );
    const firstWeekOffset =
      (DAYS_PER_WEEK + this.adapter.getDayOfWeek(firstOfMonth) - this.adapter.getFirstDayOfWeek()) % DAYS_PER_WEEK;
    const daysInMonth = this.adapter.getNumDaysInMonth(this.activeDate);
    const dates = [];
    for (let i = 0, cell = firstWeekOffset; i < daysInMonth; i++, cell++) {
      if (cell === DAYS_PER_WEEK) {
        cell = 0;
      }
      const date = this.adapter.createDate(
        this.adapter.getYear(this.activeDate),
        this.adapter.getMonth(this.activeDate),
        i + 1
      );
      dates.push(date);
    }
    return dates;
  }

  addPointsToDates(date: Date) {
    var datePipe = new DatePipe('fr-FR');
    if (this.vehicleDataCalendar) {
      const vehicleData = this.vehicleDataCalendar.map((d: any) => {
        if (this.employeeData) {
          return {
            dateTime: String(new Date(datePipe.transform(d.date, 'MM/dd/yyyy')).getTime()),
            data: d.service,
          };
        }
        return {
          dateTime: String(new Date(datePipe.transform(d.date, 'MM/dd/yyyy')).getTime()),
          data: d.mileage,
        };
      });

      const hadData = vehicleData.find((x) => x.dateTime === String(date.getTime()));
      if (hadData == undefined) {
        const aria = this.adapter.format(date, this.dateFormats.display.dateA11yLabel);

        this.addPoint({ dateTime: date, data: '' }, aria, date);
      }
      const aria = this.adapter.format(date, this.dateFormats.display.dateA11yLabel);
      if (hadData) {
        this.addPoint(hadData, aria, date);
      }
    }
  }

  addPoint(hadData: any, aria: string, date: Date) {
    const el = this.element.nativeElement.querySelector(`[aria-label="${aria}"]`);
    if (this.calendar.currentView === 'month') {
      if (hadData && el) {
        if (hadData.data === 0) {
          el.querySelector('.mat-calendar-body-cell-content').classList.add('rest-point');
        }
        const data = this.getRenderWithData(hadData.data, date);

        el.querySelector('.mat-calendar-body-cell-content').innerHTML = '';
        el.querySelector('.mat-calendar-body-cell-content').innerHTML = data;
      }
    }
  }

  getRenderWithData(data, date) {
    if (this.withIndicator) {
      let displayData: string = '';
      if (this.employeeData && data && data != '00:00') {
        displayData = data.slice(0, 2) + ' ' + this.unit + ' ' + data.slice(3, 5);
      }
      if (!this.employeeData && data && data != '0') {
        displayData = data + this.unit;
      }
      // console.log("data", data);
      // console.log("displayData", displayData);

      return `<span class="day">${date.getDate()}</span>
      <span class="data" >${displayData}</h6>`;
    } else {
      return `<span class="day">${date.getDate()}</span>`;
    }
  }
  handleCahnge(event) {
    this.selectedDate = event;
    this.activeDate = event;
    this.changedDate.emit(event);
    this.handelCalenderOutOfMonths();
  }
}
