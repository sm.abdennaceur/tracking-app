import { TestBed } from '@angular/core/testing';
import { VehicleCalendarModule } from './vehicule-calendar.module';
describe('VehicleCalendarModule', () => {
  let pipe: VehicleCalendarModule;
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [VehicleCalendarModule] });
    pipe = TestBed.get(VehicleCalendarModule);
  });
  it('can load instance', () => {
    expect(pipe).toBeTruthy();
  });
});
