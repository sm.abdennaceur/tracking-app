import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { VehicleCalendarComponent } from './vehicle-calendar.component';
import { CUSTOM_DATE_FORMATS, CustomDatePickerAdapter } from '../../shared/date-adapter/date-adapter';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DateAdapter, MatNativeDateModule, MAT_DATE_FORMATS } from '@angular/material/core';

@NgModule({
  declarations: [VehicleCalendarComponent],

  imports: [CommonModule, FontAwesomeModule, MatDatepickerModule, MatNativeDateModule],
  providers: [
    { provide: DateAdapter, useClass: CustomDatePickerAdapter },
    { provide: MAT_DATE_FORMATS, useValue: CUSTOM_DATE_FORMATS },
  ],
  exports: [VehicleCalendarComponent],
  entryComponents: [],
})
export class VehicleCalendarModule {}
