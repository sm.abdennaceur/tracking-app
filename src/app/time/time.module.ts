import { NgModule } from '@angular/core';
 import { RouterModule, Routes } from '@angular/router';
import { TimeComponent } from './time/time.component';


const routes: Routes = [
  {
    path: '',
    component: TimeComponent,
 
  },
];

@NgModule({
  declarations: [TimeComponent],

  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule ],
})
export class TimeModule { }

 
 