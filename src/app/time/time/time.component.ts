import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth/auth.service';
import { ConfigService } from '../../core/services/environment.service';
@Component({
  selector: 'app-time',
  templateUrl: './time.component.html',
  styleUrls: ['./time.component.scss'],
})
export class TimeComponent implements OnInit {
  constructor(public router: Router, public authService: AuthService, private configService: ConfigService) {
    this.AppParameters = this.configService.getConfiguration();
  }
  AppParameters;

  ngOnInit(): void {
    //app-time.dev.stradatms.net/
    let apilDomain = 'https://' + this.AppParameters.apiUrlDomain + '/login/';
    window.location.replace(apilDomain.toString() + this.authService.createToken());
  }
}
