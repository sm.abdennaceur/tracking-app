import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfService } from '../../tracking/Services/TrackingServices';
import { element } from 'protractor';
import { mapService } from '../../tracking-leaflet-map/services/map-service';
import { ModalComponent } from '../../tracking/Modal/modal/modal.component';
import * as signalR from '@microsoft/signalr';

import {
  ConducteurRechercherItem,
  Evenement,
  GeoLocalisation,
  InfoConducteur,
  InfoVehicule,
  ModelDetailVehicule,
  Personne,
  Position,
  Tachygraphe,
  VehiculeMarker,
  vehiculeModel,
  VehiculePayload,
  VehiculeRechercherItem,
} from '../../tracking/model/Models';
import {
  ConducteurVehicule,
  Immatriculation,
  PositionVehicule,
  VehiculeModel,
} from '../../tracking/model/VehiculeModels';
import { formatDate } from '@angular/common';
import { MatIconRegistry } from '@angular/material/icon';
import { HubConnection } from '@microsoft/signalr';
import { BehaviorSubject } from 'rxjs';
import { ConfigService } from '@tmsApp/login/services/environment.service';
import { IconeService } from '../../tracking/Services/IconeService';
import { GQLService } from '../../tracking/Services/graph-ql.service';
import { MapFunctionService } from '../../tracking/Services/ServicesMapGoogle/map-function.service';
const H = window['H'];
@Component({
  selector: 'app-my-map',
  templateUrl: './my-map.component.html',
  styleUrls: ['./my-map.component.scss'],
})
export class MyMapComponent implements OnInit, AfterViewInit {
  public vehiculepayload: VehiculePayload = null;
  public TourneesmarkerArray = [];
  public mapFn: MapFunctionService;
  public JsonCulumeActiviteInput: any = {
    c: '00/00',
    t: '00/00',
    m: '00/00',
    cp: '00/00',
  };
  address = null;
  showAddress = false;
  platform: any;
  search: any;
  mapp;
  lat;
  lng;
  public arrowIcon: any = {
    path: '',
    fillColor: 'black',
    fillOpacity: 1,
    strokeColor: 'red',
    strokeWeight: 1,
    scale: 3,
  };
  public datehistorique: any = '00-00-0000';
  public datacalendar: any;
  public datecalendarend: any;
  query: string;
  public isSPI: any = false;
  public aprox: Array<any> = [];
  vehicule;
  userSpiRow;
  public timer: Array<any> = [];
  public listpointsHistorique: any;
  public countlistpointsHistorique: any = 0;
  ui;
  isFullScree: boolean = false;
  mapStyle;
  fullscreenStyle;
  menu;
  PositionStyle;
  public countplayer = 0;
  public timehistorique: any = '00-00-00';
  DisplayMenuContextuel: boolean = false;
  public RechSHOW: any = false;
  public RechSHOW2: any = 0;
  positionMenuX = 0;
  positionMenuY = 0;
  infoendroit = [];
  aproxinfo = [];
  public recherchlist: Array<any> = [];
  public showsniper = false;
  public listv: VehiculeMarker[] = [];
  public inputdd: ModelDetailVehicule = new ModelDetailVehicule();
  public CurrentV: any;
  public way: Array<any> = [];
  public listVehiculesItem: Array<VehiculeModel> = [];
  public polylineArray = [];
  public renderArray = [];
  public stationmarkerArray = [];
  @ViewChild('iconplayer', { static: true }) iconplayer: ElementRef;
  @ViewChild('alert') alert;
  @ViewChild('range', { static: true }) range: ElementRef;

  public sidebar: any;
  public color: Array<string> = ['#ff2e96', '#6e00c2', '#0000c2', '#00c2c2', '#721d1d', '#00FF00', '#FFFF00'];
  liveMode: any = false;
  mrk: any;
  public graphservice: GQLService = null;
  public constructor(
    private mapService: mapService,
    private iconeService: IconeService,
    private modalService: NgbModal,
    public service: ConfService,
    public configService: ConfigService,
    public mapfn: MapFunctionService,
    graph: GQLService
  ) {
    this.mapFn = mapfn;
    this.graphservice = graph;
    this.platform = new H.service.Platform({
      // apikey: 'PK4uDnXXN_ynCkkwZPaDQU9kKp76AC8UJ04Eqk0GTEY', //julien
      apikey: '40MRmFHA7Ag27NMPfjTxiTyjBiRtTYL-J4-om_4IgNU',
    });
    this.AppParameters = this.configService.config;
  }
  fullscreen() {
    this.isFullScree = !this.isFullScree;
    if (this.isFullScree == true) {
      this.sidebar.style.display = 'none';
      this.mapStyle.style.marginLeft = '0px';
      this.PositionStyle.style.left = '25px';
      this.fullscreenStyle.style.left = '25px';
    } else {
      this.sidebar.style.display = 'block';
      this.mapStyle.style.marginLeft = '55px';
      this.PositionStyle.style.left = '78px';
      this.fullscreenStyle.style.left = '78px';
    }
  }
  zoomCurrentPosition() {
    this.mapp.setZoom(7);
  }
  changemode(event) {
    this.liveMode = event.target.checked;
    //  var menudetail = document.getElementById("Mdetail");
    var a = document.getElementById('sliderange');
    if (this.liveMode) {
      setTimeout(() => {
        this.listv.forEach((el) => {
          if (this.CurrentV != null)
            if (el.id == this.CurrentV.idVehicule) {
              // el.marker.setMap(this.map);
              this.mapp.addObject(el.marker);
              el.marker.setVisibility(true);
            }
        });
      });
      this.mapp.removeLayer(this.clusteringLayer);
      if (this.routeLinee != null) {
        this.routeLinee.setVisibility(false);
      }
      this.listv.forEach((el) => {
        if (this.CurrentV != null)
          if (el.id != this.CurrentV.idVehicule) {
            // el.marker.setMap(null);
            // this.map.removeObject(el.marker);
            // this.map.addObject(el.marker);
            // el.marker.setVisibility(true)
            // this.routeLinee = null;
          }
      });

      this.listv.forEach((el) => {
        el.marker.setVisibility(false);

        // this.map.removeObject(el.marker);
      });

      a.hidden = false;
      // this.ClusterMaker.setMap(null);
      //    menudetail.hidden=true;
    } else {
      //    menudetail.hidden=false;
      this.clearrrtimer(0);
      this.CurrentV = null;
      this.RechSHOW = 0;
      this.RechSHOW2 += 1;
      a.hidden = true;
      this.listpointsHistorique = [];
      this.countlistpointsHistorique = 0;
      this.datehistorique = '00-00-0000';
      this.timehistorique = '00-00-00';
      this.showsniper = false;
      if (this.routeLinee != null) {
        this.mapp.removeObject(this.routeLinee);
        this.routeLinee = null;
      }
      // this.polylineArray = [];
      this.renderArray.forEach((el) => {
        // el.setMap(null);
        this.mapp.removeObject(el);
      });
      this.renderArray = [];
      this.stationmarkerArray.forEach((el) => {
        // el.setMap(null);
        this.mapp.removeObject(el);
      });
      this.stationmarkerArray = [];
      if (this.mrk) {
        // this.mrk.setMap(null);
        this.mrk.setVisibility(false);
        // this.map.removeOject(this.mrk);
      }
      this.mapp.addLayer(this.clusteringLayer);
      if (this.routeLinee != null) {
        this.routeLinee.setVisibility(true);
      }
      // setTimeout(() => {
      //   this.listv.forEach((el) => {
      //     this.map.addObject(el.marker);
      //     el.marker.setVisibility(true);
      //   });
      // });

      // this.ClusterMaker.setMap(this.map1);
    }
  }

  async getValueFromcalender(event) {
    this.datacalendar = formatDate(event.dateRange.startDate, 'yyyy-MM-dd', 'en_US');
    this.datecalendarend = formatDate(event.dateRange.endDate, 'yyyy-MM-dd', 'en_US');
    if (event.view == 'day') {
      this.datecalendarend = this.datacalendar;
    }
    // var selectvalue = document.getElementById('Vselect') as HTMLSelectElement;
    if (this.CurrentV != null) {
      if (this.mrk) {
        // this.mrk.setMap(null);
        // this.map.removeObject(this.mrk);
        this.mrk.setVisibility(false);
        this.clearrrtimer(0);
      }

      // var value = selectvalue.options[selectvalue.selectedIndex].value
      this.SVC(this.CurrentV);
    }
  }
  colorr;
  noisePoint = [];
  adress = '';
  ff(noisePoint) {
    var randomcolor;
    if (noisePoint.a.data.data.data.Conducteur.Activite == 0) {
      randomcolor = '#354360';
    }
    if (noisePoint.a.data.data.data.Conducteur.Activite == 2) {
      randomcolor = '#17a2b8';
    }
    if (noisePoint.a.data.data.data.Conducteur.Activite == 3) {
      randomcolor = '#fd7e14';
    }
    if (noisePoint.a.data.data.data.Conducteur.Activite == 4) {
      randomcolor = '#FF0000';
    }
    if (noisePoint.a.data.data.data.Conducteur.Activite == 5) {
      randomcolor = '#006400';
    }

    this.colorr = randomcolor;
    var customIcon = new H.map.Icon(
      'data:image/svg+xml;charset=UTF-8,' +
        encodeURIComponent(
          this.mapFn.replaceHoverIcon(
            noisePoint.a.data.vrn,
            randomcolor,
            noisePoint.a.data.data.data.Position.DateReception,
            'H',
            noisePoint.a.data.data.type,
            this.adress
          )
        ),
      {
        anchor: { x: 11, y: 30 },
        crossOrigin: true,
      }
    );

    var noiseMarker = new H.map.Marker(noisePoint.getPosition(), {
      icon: customIcon,
      min: noisePoint.getMinZoom(),
    });
    noiseMarker.setData(noisePoint);
    // noiseMarker.setZIndex(999);
    noiseMarker.addEventListener('pointerenter', (hov) => {
      this.mapService
        .getAddress(noisePoint.a.data.data.data.Position.Lng, noisePoint.a.data.data.data.Position.Lat)
        .then((res) =>
          res.json().then((address) => {
            this.adress = address.address.LongLabel;
          })
        );
      if (noisePoint.a.data.data.type == 'U') {
        var customIcon = new H.map.Icon(
          'data:image/svg+xml;charset=UTF-8,' +
            encodeURIComponent(
              this.mapFn.replaceHoverIcon(
                noisePoint.a.data.vrn,
                randomcolor,
                noisePoint.a.data.data.data.Position.DateReception,
                'HOV',
                noisePoint.a.data.data.type,
                this.adress
              )
            ),
          {
            anchor: { x: 11, y: 105 },
            crossOrigin: true,
          }
        );
      } else {

        var customIcon = new H.map.Icon(
          'data:image/svg+xml;charset=UTF-8,' +
            encodeURIComponent(
              this.mapFn.replaceHoverIcon(
                noisePoint.a.data.vrn,
                randomcolor,
                noisePoint.a.data.data.data.Position.DateReception,
                'D',
                noisePoint.a.data.data.type,
                this.adress
              )
            ),
          {
            anchor: { x: 10, y: 105 },
            crossOrigin: true,
          }
        );
      }
      noiseMarker.setIcon(customIcon);
      noiseMarker.setZIndex(999);
    });
    noiseMarker.addEventListener('pointerleave', (hov) => {
      var customIcon = new H.map.Icon(
        'data:image/svg+xml;charset=UTF-8,' +
          encodeURIComponent(
            this.mapFn.replaceHoverIcon(
              noisePoint.a.data.vrn,
              randomcolor,
              noisePoint.a.data.data.data.Position.DateReception,
              'H',
              noisePoint.a.data.data.type,
              this.adress
            )
          ),
        {
          anchor: { x: 11, y: 30 },
          crossOrigin: true,
        }
      );
      noiseMarker.setIcon(customIcon);
    });

    noiseMarker.addEventListener('tap', (ev) => {
      if (this.routeLinee != null) {
        this.mapp.removeObject(this.routeLinee);
        this.routeLinee = null;
      }
      this.NewClickMarker(noisePoint.a.data.data.data);
      this.CurrentV = noisePoint.a.data.data.data;
    });
    return noiseMarker;
  }
  clusteringLayer: any;
  clusteredDataProvider: any;
  async initvehicuel() {
    var a: any = await this.service.getPoints();

    this.showsniper = false;

    await a.vehiculeRow.forEach(async (el) => {
      var Vehiculeitem = new VehiculeModel();
      Vehiculeitem.Conducteur = new ConducteurVehicule();
      Vehiculeitem.Position = new PositionVehicule();
      Vehiculeitem.Immatriculation = new Immatriculation();

      if (el.latitude != null) {
        Vehiculeitem.idVehicule = el.id;
        Vehiculeitem.Conducteur.numCartConducteur = el.numConducteur;
        Vehiculeitem.Position.Lat = Number(el.latitude);
        Vehiculeitem.Position.Lng = Number(el.longitude);
        Vehiculeitem.Position.DateReception = el.dateReception;
        Vehiculeitem.Conducteur.Activite = this.setActivite(el.lastActvite);
        Vehiculeitem.Immatriculation.Vin = el.vin;
        Vehiculeitem.Immatriculation.Vrn = el.vrn;
        this.listVehiculesItem.push(Vehiculeitem);
        // this.NewSetMarkerVehiculeToMap(Vehiculeitem);

        this.NewSetMarkerVehiculeToMap(Vehiculeitem, 'V');
      }
    });

    await a.userSpiRow.forEach(async (el) => {
      var Vehiculeitem = new VehiculeModel();
      Vehiculeitem.Conducteur = new ConducteurVehicule();
      Vehiculeitem.Position = new PositionVehicule();
      Vehiculeitem.Immatriculation = new Immatriculation();
      Vehiculeitem.idVehicule = el.id;
      //Vehiculeitem.Conducteur.numCartConducteur=el.numConducteur;
      Vehiculeitem.Position.Lat = Number(el.latitude);
      Vehiculeitem.Position.Lng = Number(el.longitude);
      Vehiculeitem.Immatriculation.Vin = el.nom;
      Vehiculeitem.Immatriculation.Vrn = el.prenom;
      this.listVehiculesItem.push(Vehiculeitem);
      this.NewSetMarkerVehiculeToMap(Vehiculeitem, 'U');
    });

    this.createCluster(this.listv);
  }
  datap: any;
  createCluster(list) {
    var dataPoints = [];
    list.forEach((item) => {
      dataPoints.push(
        new H.clustering.DataPoint(item.data.Position.Lat, item.data.Position.Lng, null, {
          vrn: item.data.Immatriculation.Vrn,
          data: item,
        })
      );
    });
    this.datap = dataPoints;
    this.clusteredDataProvider = new H.clustering.Provider(dataPoints, {
      clusteringOptions: {
        eps: 32,
        minWeight: 9,
      },
      theme: {
        getClusterPresentation: function (markerCluster: any) {
          var clusterSvgTemplate =
            '<svg xmlns="http://www.w3.org/2000/svg" height="50px" width="50px"><circle cx="25px" cy="25px" r="20" fill="blue" stroke-opacity="0.5" />' +
            '<text x="24" y="32" font-size="14pt" font-family="arial" font-weight="bold" text-anchor="middle" fill="white">{text}</text>' +
            '</svg>';
          var w, h;
          var weight = markerCluster.getWeight();
          var svgString = clusterSvgTemplate.replace('{radius}', (weight * 5).toString());
          svgString = svgString.replace('{text}', weight.toString());

          //Set cluster size depending on the weight
          if (weight <= 6) {
            w = 35;
            h = 35;
          } else if (weight <= 12) {
            w = 50;
            h = 50;
          } else {
            w = 75;
            h = 75;
          }

          var clusterIcon = new H.map.Icon(svgString, {
            size: { w: w, h: h },
            anchor: { x: w / 2, y: h / 2 },
            crossOrigin: true,
          });

          // Create a marker for clusters:
          var clusterMarker = new H.map.Marker(markerCluster.getPosition(), {
            icon: clusterIcon,
            // Set min/max zoom with values from the cluster, otherwise
            // clusters will be shown at all zoom levels:
            min: markerCluster.getMinZoom(),
            max: markerCluster.getMaxZoom(),
          });

          // Bind cluster data to the marker:
          clusterMarker.setData(markerCluster);

          // clusterMarker.addEventListener('pointerenter', function (event: any) {
          //   var point = event.target.getPosition(),
          //     screenPosition = this.map.geoToScreen(point),
          //     t = event.target,
          //     data = t.getData(),
          //     tooltipContent = '';
          //   data.forEachEntry(function (p) {
          //     tooltipContent += p.getPosition().lat + ' ' + p.getPosition().lng + '</br>';
          //   });
          // });
          clusterMarker.addEventListener('tap', (event: any) => {});

          return clusterMarker;
        },
        getNoisePresentation: (noisePoint) => {
          this.noisePoint.push(noisePoint);
          return this.ff(noisePoint);
        },
      },
    });

    this.clusteringLayer = new H.map.layer.ObjectLayer(this.clusteredDataProvider);
    this.mapp.addLayer(this.clusteringLayer);
  }
  randomcolor;
  async NewSetMarkerVehiculeToMap(Vehicule: VehiculeModel, type) {
    if (Vehicule.Immatriculation.Vrn == null || '') {
      return;
    }
    if (Vehicule.Conducteur.numCartConducteur != null) {
      var itemC = new ConducteurRechercherItem();
      itemC = {
        num: Vehicule.Conducteur.numCartConducteur,
        type: 'Conducteur',
        activite: Vehicule.Conducteur.Activite,
        name:
          Vehicule.Conducteur.NomConducteur != ''
            ? Vehicule.Conducteur.NomConducteur
            : Vehicule.Conducteur.numCartConducteur,
      };
      this.recherchlist.push(itemC);
    }
    if (Vehicule.Immatriculation.Vrn != null) {
      var item = new VehiculeRechercherItem();
      item = {
        name: Vehicule.Immatriculation.Vrn,
        type: 'Vehicule',
        activite: Vehicule.Conducteur.Activite,
        isAct: Vehicule.Position.IsEnMouvement,
        vitesse: Vehicule.Position.Vitesse,
      };
      this.recherchlist.push(item);
    }

    var randomIndex = Math.floor(Math.random() * this.color.length);
    this.randomcolor = this.color[randomIndex];
    var point = {
      lat: Vehicule.Position.Lat,
      lng: Vehicule.Position.Lng,
    };
    var customIcon = new H.map.Icon(
      'data:image/svg+xml;charset=UTF-8,' +
        encodeURIComponent(this.replace(Vehicule.Immatriculation.Vrn, this.randomcolor))
    );
    var marker = new H.map.Marker(point, { icon: customIcon });

    marker.addEventListener('tap', (ev) => {
      if (this.routeLinee != null) {
        this.mapp.removeObject(this.routeLinee);
        this.routeLinee = null;
      }

      this.NewClickMarker(Vehicule);
    });

    var vehicule: VehiculeMarker = new VehiculeMarker();
    vehicule.id = Vehicule.idVehicule;
    vehicule.marker = marker;
    vehicule.data = Vehicule;
    vehicule.type = type;
    this.listv.push(vehicule);
    // this.map.addObject(vehicule.marker);
  }
  routeLinee: any = null;
  autoTracingRoute(map, pathCoords, act) {
    // if(this.colorr=="")
    // this.colorr="#354360"

    if (this.routeLinee != null) {
      this.mapp.removeObject(this.routeLinee);
      this.routeLinee = null;
    }
    if (act == 0) {
      this.colorr = '#354360';
    }
    if (act == 2) {
      this.colorr = '#17a2b8';
    }
    if (act == 3) {
      this.colorr = '#fd7e14';
    }
    if (act == 4) {
      this.colorr = '#FF0000';
    }
    if (act == 5) {
      this.colorr = '#006400';
    }

    var linestring = new H.geo.LineString();

    pathCoords.forEach((point) => {
      linestring.pushPoint({ lat: point.location.lat, lng: point.location.lng });
    });
console.log("linestring", linestring);

    var polyline = new H.map.Polyline(linestring, {
      style: {
        lineWidth: 7,
        strokeColor: '#51ce9d',
      },
      zIndex: 1,
    });
    this.routeLinee = new H.map.Group();
    this.routeLinee.addObject(polyline);

    this.mapp.addObject(this.routeLinee);
    this.mapp.getViewModel().setLookAtData({ bounds: polyline.getBoundingBox() });
    this.way = [];
  }

  replace(mat: any, color: any): any {
    var template = [
      '<svg xmlns="http://www.w3.org/2000/svg" width="200.389" height="40.842" viewBox="0 0 152.389 35"><g id="Group_17668" data-name="Group 17668" transform="translate(66.408 0.161)"><g id="Group_17448" data-name="Group 17448" transform="translate(-875.391 -442)"><g id="Rectangle_4" data-name="Rectangle 4" transform="translate(878.371 442)" fill="#fff" stroke="#354360" stroke-width="1"><rect width="83" height="15" rx="7.5" stroke="none"/><rect x="0.5" y="0.5" width="82" height="14" rx="7" fill="none"/></g><text id="AC123AB" transform="translate(915.371 453)" fill="#354360" font-size="9" font-family="Avenir-Medium, Avenir" font-weight="500"><tspan x="0" y="0">{{matricule}}</tspan></text><g id="Group_17584" data-name="Group 17584" transform="translate(-1958.619 3021.504)"><path id="Path_12471" data-name="Path 12471" d="M1355.795,2084.344h2.033a.4.4,0,0,0,.371-.546l-.36-.918a.408.408,0,0,0-.337-.255l-1.673-.139a.393.393,0,0,0-.43.395v1.069A.407.407,0,0,0,1355.795,2084.344Z" transform="translate(1510.592 -4656.992)" fill="none"/><path id="Path_12472" data-name="Path 12472" d="M1277.876,2073.552h.023v-.023a1.731,1.731,0,0,1,3.461,0v.023h4.82a1.731,1.731,0,0,1,3.461-.035.809.809,0,0,0,.662-.79l-.012-2.218a.809.809,0,0,0-.058-.291l-.894-2.23a.822.822,0,0,0-.674-.5l-2.439-.186a.794.794,0,0,0-.859.79v4h-.743v-5.1a.79.79,0,0,0-.79-.79h-5.946a.79.79,0,0,0-.79.79v5.761A.776.776,0,0,0,1277.876,2073.552Zm8.316-5.064a.394.394,0,0,1,.43-.4l1.672.139a.393.393,0,0,1,.337.256l.36.918a.4.4,0,0,1-.372.546h-2.033a.392.392,0,0,1-.395-.4Z" transform="translate(1579.8 -4642.6)" fill="#354360"/><path id="Path_12473" data-name="Path 12473" d="M1288.029,2120a.725.725,0,0,0,.151-.011,1,1,0,0,0,.267-.058,1.3,1.3,0,0,0,.36-.174,1.412,1.412,0,0,0,.616-1.127v-.023a1.363,1.363,0,0,0-.314-.883.747.747,0,0,0-.093-.1l-.012-.011a.756.756,0,0,0-.139-.116,1.573,1.573,0,0,0-.3-.174.414.414,0,0,0-.128-.046,1.822,1.822,0,0,0-.267-.058.621.621,0,0,0-.151-.011,1.247,1.247,0,0,0-.546.116,1.731,1.731,0,0,0-.348.209,1.553,1.553,0,0,0-.2.2,1.265,1.265,0,0,0-.256.476,1.819,1.819,0,0,0-.058.267.614.614,0,0,0-.012.151v.023A1.438,1.438,0,0,0,1288.029,2120Z" transform="translate(1571.401 -4687.676)" fill="#354360"/><path id="Path_12474" data-name="Path 12474" d="M1360.655,2119.96a2.059,2.059,0,0,0,.174-.36,1.807,1.807,0,0,0,.058-.267.61.61,0,0,0,.012-.151v-.035a.321.321,0,0,0-.012-.1.992.992,0,0,0-.058-.267,1.3,1.3,0,0,0-.174-.36,1.4,1.4,0,0,0-2.555.767v.023a1.438,1.438,0,0,0,.406.987,1.387,1.387,0,0,0,.987.406A1.464,1.464,0,0,0,1360.655,2119.96Z" transform="translate(1508.206 -4688.206)" fill="#354360"/></g></g><path id="Path_11171" data-name="Path 11171" d="M2719.464,1517.2c.242.512.508,1.024.774,1.512.145.243.266.488.411.731s.29.488.435.707.29.464.436.683c.29.463.58.878.894,1.317.217.317.435.609.677.9.145.2.29.39.435.561.024.024.024.049.048.049.121.17.266.317.387.488.145.17.29.342.411.512.1.122.218.244.314.366.024.049.073.073.1.122.121.146.266.293.387.439s.242.268.363.39.217.244.338.366a3.829,3.829,0,0,0,.314.317c.628.658,1.015,1.024,1.015,1.024a33.305,33.305,0,0,0,2.538-2.707c.363-.415.725-.878,1.112-1.366,2.683-3.414,5.85-8.584,5.85-13.876a9.658,9.658,0,0,0-.29-2.39,8.752,8.752,0,0,0-.459-1.341,5.26,5.26,0,0,0-.411-.829c-.072-.146-.145-.268-.217-.39a9.347,9.347,0,0,0-.8-1.122,10.153,10.153,0,0,0-1.644-1.585,6.734,6.734,0,0,0-.749-.512,10.539,10.539,0,0,0-1.668-.8c-.29-.1-.58-.2-.894-.268a8.864,8.864,0,0,0-1.4-.244c-.315-.024-.653-.049-.967-.049s-.653.025-.967.049a3.218,3.218,0,0,1-.484.049c-.315.049-.628.122-.919.2a8.539,8.539,0,0,0-1.329.464,5.147,5.147,0,0,0-.822.415,9.915,9.915,0,0,0-2.514,1.975c-.193.22-.387.463-.58.707-.1.122-.169.244-.266.366a8.358,8.358,0,0,0-.677,1.195,9.117,9.117,0,0,0-.362.854l-.146.439a10.064,10.064,0,0,0-.435,2.853,16.824,16.824,0,0,0,1.16,5.853A10.692,10.692,0,0,0,2719.464,1517.2Zm7.663-11.072a3.634,3.634,0,1,1-3.6,3.633A3.627,3.627,0,0,1,2727.127,1506.126Z" transform="translate(-2717.699 -1499.526)" fill="{{Color}}" stroke="#354360" stroke-width="1"/><g id="Rectangle_6101" data-name="Rectangle 6101" transform="translate(-66.408 -0.161)" fill="#fff" stroke="#707070" stroke-width="1" opacity="0.002"> <rect width="77" height="35" stroke="none"/><rect x="0.5" y="0.5" width="76" height="34" fill="none"/></g>   </g></svg>',
    ].join('\n');
    var templatePersonne = [
      `<svg id="Component_41_58" data-name="Component 41 – 58" xmlns="http://www.w3.org/2000/svg" width="200.389" height="40.842" viewBox="0 0 154.389 28.842">
      <g id="Group_17668" data-name="Group 17668" transform="translate(68.408)">
        <g id="Group_17448" data-name="Group 17448" transform="translate(-875.391 -442)">
          <g id="Rectangle_4" data-name="Rectangle 4" transform="translate(878.371 442)" fill="#fff" stroke="#354360" stroke-width="1">
            <rect width="83" height="15" rx="7.5" stroke="none"/>
            <rect x="0.5" y="0.5" width="82" height="14" rx="7" fill="none"/>
          </g>
          <text id="CA123AB" transform="translate(913.371 453.249)" fill="#354360" font-size="9" font-family="Avenir-Medium, Avenir" font-weight="500"><tspan x="0" y="0">{{matricule}}</tspan></text>
          <g id="Group_18890" data-name="Group 18890" transform="translate(897.507 445.249)">
            <path id="Path_14320" data-name="Path 14320" d="M2738.878,777.9a.842.842,0,0,1-.078.072A.842.842,0,0,0,2738.878,777.9Z" transform="translate(-2734.543 -773.165)" fill="#354360"/>
            <path id="Path_14321" data-name="Path 14321" d="M2736.44,780.6c-.013.007-.027.02-.04.026A.132.132,0,0,1,2736.44,780.6Z" transform="translate(-2732.298 -775.689)" fill="#354360"/>
            <path id="Path_14322" data-name="Path 14322" d="M2692.7,771.1a1.986,1.986,0,0,0,.15.235C2692.8,771.257,2692.746,771.179,2692.7,771.1Z" transform="translate(-2691.402 -766.807)" fill="#354360"/>
            <path id="Path_14323" data-name="Path 14323" d="M2695,774.7a1.934,1.934,0,0,0,1.559.783,1.978,1.978,0,0,0,1.011-.28,1.951,1.951,0,0,1-2.571-.5Z" transform="translate(-2693.552 -770.174)" fill="#354360"/>
            <path id="Path_14324" data-name="Path 14324" d="M2691.485,768.67a1.4,1.4,0,0,1-.084-.17C2691.427,768.552,2691.453,768.611,2691.485,768.67Z" transform="translate(-2690.188 -764.376)" fill="#354360"/>
            <path id="Path_14325" data-name="Path 14325" d="M2747.253,778.184h4.794a3.183,3.183,0,0,0-6.047-1.39A3.4,3.4,0,0,1,2747.253,778.184Z" transform="translate(-2741.346 -770.463)" fill="#354360"/>
            <path id="Path_14326" data-name="Path 14326" d="M2744.4,741.4a2.072,2.072,0,0,1,.163.339A2.072,2.072,0,0,0,2744.4,741.4Z" transform="translate(-2739.783 -739.036)" fill="#354360"/>
            <path id="Path_14327" data-name="Path 14327" d="M2744.563,767.1a2.069,2.069,0,0,1-.163.339A2.069,2.069,0,0,0,2744.563,767.1Z" transform="translate(-2739.783 -763.067)" fill="#354360"/>
            <path id="Path_14328" data-name="Path 14328" d="M2734.531,781l-.131.091Z" transform="translate(-2730.427 -776.063)" fill="#354360"/>
            <path id="Path_14329" data-name="Path 14329" d="M2737.118,779l-.117.1A.788.788,0,0,0,2737.118,779Z" transform="translate(-2732.86 -774.193)" fill="#354360"/>
            <path id="Path_14330" data-name="Path 14330" d="M2740.124,775.8c-.039.046-.079.091-.124.137A.862.862,0,0,0,2740.124,775.8Z" transform="translate(-2735.666 -771.201)" fill="#354360"/>
            <path id="Path_14331" data-name="Path 14331" d="M2742.011,773.5a1.265,1.265,0,0,1-.111.15A1.265,1.265,0,0,0,2742.011,773.5Z" transform="translate(-2737.444 -769.051)" fill="#354360"/>
            <path id="Path_14332" data-name="Path 14332" d="M2678.437,801.646a3.022,3.022,0,0,0-2.629-1.546,3.012,3.012,0,0,0-3.007,3.007h6.021A3,3,0,0,0,2678.437,801.646Z" transform="translate(-2672.801 -793.935)" fill="#354360"/>
            <circle id="Ellipse_306" data-name="Ellipse 306" cx="1.944" cy="1.944" r="1.944" transform="translate(5.574 0)" fill="#354360"/>
            <circle id="Ellipse_307" data-name="Ellipse 307" cx="1.944" cy="1.944" r="1.944" transform="translate(1.063 1.431)" fill="#354360"/>
          </g>
        </g>
        <path id="Path_11171" data-name="Path 11171" d="M2719.464,1517.2c.242.512.508,1.024.774,1.512.145.243.266.488.411.731s.29.488.435.707.29.464.436.683c.29.463.58.878.894,1.317.217.317.435.609.677.9.145.2.29.39.435.561.024.024.024.049.048.049.121.17.266.317.387.488.145.17.29.342.411.512.1.122.218.244.314.366.024.049.073.073.1.122.121.146.266.293.387.439s.242.268.363.39.217.244.338.366a3.829,3.829,0,0,0,.314.317c.628.658,1.015,1.024,1.015,1.024a33.305,33.305,0,0,0,2.538-2.707c.363-.415.725-.878,1.112-1.366,2.683-3.414,5.85-8.584,5.85-13.876a9.658,9.658,0,0,0-.29-2.39,8.752,8.752,0,0,0-.459-1.341,5.26,5.26,0,0,0-.411-.829c-.072-.146-.145-.268-.217-.39a9.347,9.347,0,0,0-.8-1.122,10.153,10.153,0,0,0-1.644-1.585,6.734,6.734,0,0,0-.749-.512,10.539,10.539,0,0,0-1.668-.8c-.29-.1-.58-.2-.894-.268a8.864,8.864,0,0,0-1.4-.244c-.315-.024-.653-.049-.967-.049s-.653.025-.967.049a3.218,3.218,0,0,1-.484.049c-.315.049-.628.122-.919.2a8.539,8.539,0,0,0-1.329.464,5.147,5.147,0,0,0-.822.415,9.915,9.915,0,0,0-2.514,1.975c-.193.22-.387.463-.58.707-.1.122-.169.244-.266.366a8.358,8.358,0,0,0-.677,1.195,9.117,9.117,0,0,0-.362.854l-.146.439a10.064,10.064,0,0,0-.435,2.853,16.824,16.824,0,0,0,1.16,5.853A10.692,10.692,0,0,0,2719.464,1517.2Zm7.663-11.072a3.634,3.634,0,1,1-3.6,3.633A3.627,3.627,0,0,1,2727.127,1506.126Z" transform="translate(-2717.699 -1499.526)" fill="{{Color}}" stroke="#354360" stroke-width="1"/>
      </g>
      <g id="Rectangle_6356" data-name="Rectangle 6356" transform="translate(0 0.249)" fill="#fff" stroke="#707070" stroke-width="1" opacity="0">
        <rect width="78" height="12" stroke="none"/>
        <rect x="0.5" y="0.5" width="77" height="11" fill="none"/>
      </g>
    </svg>
    `,
    ].join('\n');
    if (mat == 'Krichen') {
      var svg = templatePersonne.replace('{{matricule}}', mat);
      svg = svg.replace('{{Color}}', '#FFFFFF');
      return svg;
    }
    var svg = template.replace('{{matricule}}', mat);
    svg = svg.replace('{{Color}}', color);
    return svg;
  }
  replaceHoverIcon(mat, color, date, Hover, type, adress) {
    return this.iconeService.replaceHoverIcon(mat, color, date, Hover, type, adress);
  }
  ngAfterViewInit() {
    let defaultLayers = this.platform.createDefaultLayers();
    // let customizedMapSetting = new H.ui.MapSettingsControl({
    //   baseLayers: [{
    //     label: "Normal",
    //     layer: defaultLayers.raster.normal.map
    //   }],
    //   // layers: [{
    //   //   label: "Cluster-1",

    //   // },
    //   // {
    //   //   label: "Cluster-2",

    //   // },
    //   // {
    //   //   label: "Marker-1",

    //   // }]
    // });

    // customizedMapSetting.setAlignment('top-right');
    // this.ui.addControl("customized", customizedMapSetting);
    this.mapp = new H.Map(document.getElementById('mapp'), defaultLayers.raster.normal.map);
    window.addEventListener('resize', () => this.mapp.getViewPort().resize());

    var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(this.mapp));
    this.ui = H.ui.UI.createDefault(this.mapp, defaultLayers);

    var mapSettings = this.ui.getControl('mapsettings');
    var zoom = this.ui.getControl('zoom');
    var scalebar = this.ui.getControl('scalebar');
    scalebar.setAlignment('left-bottom');
    mapSettings.setAlignment('left-bottom');
    zoom.setAlignment('left-bottom');
    navigator.geolocation.getCurrentPosition((position) => {
      this.mapp.setCenter({ lat: position.coords.latitude, lng: position.coords.longitude });
      this.mapp.setZoom(6);
    });

    var dataPoints = [];

    //     //clustering
    //     // dataPoints.push(new H.clustering.DataPoint(element.latitude, element.longitude,index));
    //     // icnss.push(this.createNewMarker(element.vrn))
    //   });

    //   let clusteredDataProvider = new H.clustering.Provider(dataPoints, {
    //     // theme: {
    //     //   getClusterPresentation: function (cluster) {
    //     //     var weight = cluster.getWeight(),
    //     //       radius = weight * 5,
    //     //       diameter = radius * 2,
    //     //       svgString = `<svg xmlns="http://www.w3.org/2000/svg" height="${diameter}" width="${diameter}">'<circle cx="${radius}px" cy="${radius}px" r="${radius}px" fill="red" /> </svg>`
    //     //     // Create an icon
    //     //     let clusterIcon = new H.map.Icon(svgString),
    //     //       // Create a marker for the cluster
    //     //       clusterMarker = new H.map.Marker(cluster.getPosition(), {
    //     //         icon: clusterIcon,
    //     //       });
    //     //     // Bind cluster data to the marker
    //     //     clusterMarker.setData(cluster);
    //     //     return clusterMarker;
    //     //   },
    //     //   getNoisePresentation: function (noisePoint) {
    //     //     console.log('noisePoint',noisePoint.getWeight())
    //     //     console.log("dataPoints",dataPoints);
    //     //     let noiseMarker = new H.map.Marker(noisePoint.getPosition(), {
    //     //       icon: new H.map.Icon(icnss[2]),
    //     //      // min: noisePoint.getMinZoom()
    //     //     });
    //     //     noiseMarker.setData(noisePoint);
    //     //     return noiseMarker;
    //     //   }
    //     // }
    //   });

    //   let layer = new H.map.layer.ObjectLayer(clusteredDataProvider);
    //   this.map.addLayer(layer);

    this.mapp.addEventListener('tap', (evt) => {
      if (evt.originalEvent.which == 3) {
        this.menu.hidden = false;
        let coord = this.mapp.screenToGeo(evt.currentPointer.viewportX, evt.currentPointer.viewportY);
        this.positionMenuX = coord.lat;
        this.positionMenuY = coord.lng;

        // menu.st = false;

        this.menu.style.top = evt.targetPointers[0].viewportY + 'px';
        this.menu.style.left = evt.targetPointers[0].viewportX + 'px';
      } else {
        // this.mapp.setZoom(5);
        this.menu.hidden = true;
        if (this.routeLinee != null) {
          this.routeLinee.setVisibility(false);
          this.mapp.removeObject(this.routeLinee);
          this.routeLinee = null;
        }
        if (this.routeLine != null) {
          this.routeLine.setVisibility(false);
          this.mapp.removeObject(this.routeLine);
          this.routeLine = null;
        }
        if (this.markerDebut != null) {
          this.markerDebut.setVisibility(false);
          this.mapp.removeObject(this.markerDebut);
          this.markerDebut = null;
        }
        if (this.markerFin != null) {
          this.markerFin.setVisibility(false);
          this.mapp.removeObject(this.markerFin);
          this.markerFin = null;
        }
        this.closePop();
      }
    });
  }
  ngOnInit(): void {
    window.addEventListener('contextmenu', (e) => e.preventDefault());

    this.sidebar = document.getElementById('sidebar');
    this.mapStyle = document.getElementById('mapp');
    this.fullscreenStyle = document.getElementById('fullscreen');
    this.PositionStyle = document.getElementById('position');
    this.menu = document.getElementById('menuC');
    this.initvehicuel();
    this.Hubconntection2();
    // this.showsniper = false
  }

  markerDebut;
  markerFin;
  routeLine;
  // setPolyy(event) {
  //   if (event.length == 0) {
  //     this.mapp.setZoom(7);
  //     this.menu.hidden = true;
  //     this.mapp.removeObject(this.markerFin);
  //     this.mapp.removeObject(this.markerDebut);
  //     if (this.routeLine != null) this.mapp.removeObject(this.routeLine);
  //   } else {
  //     let markerIconDebut = this.createNewMarker('Début');
  //     const customMarkerIconDebut = new H.map.Icon(markerIconDebut);
  //     this.markerDebut = new H.map.Marker({ lat: event[0].lat, lng: event[0].lng }, { icon: customMarkerIconDebut });
  //     this.mapp.addObject(this.markerDebut);
  //     let markerIconFin = this.createNewMarker('Fin');
  //     const customMarkerIconFin = new H.map.Icon(markerIconFin);
  //     this.markerFin = new H.map.Marker({ lat: event[1].lat, lng: event[1].lng }, { icon: customMarkerIconFin });
  //     this.mapp.addObject(this.markerFin);
  //     var linestring = new H.geo.LineString();
  //     var routingService = this.platform.getRoutingService();
  //     var routingParameters = {
  //       mode: 'fastest;car',
  //       waypoint0: event[0].lat + ',' + event[0].lng,
  //       waypoint1: event[1].lat + ',' + event[1].lng,
  //       representation: 'display',
  //     };
  //     routingService.calculateRoute(routingParameters, (success) => {
  //       var res = success.response.route[0].shape;
  //       console.log("res", res);
  //       res.forEach(function (point) {
  //         var parts = point.split(',');
  //         linestring.pushPoint({ lat: Number(parts[0]), lng: Number(parts[1]) });
  //         // linestring.pushPoint({ lat: point.lat, lng: point.lng });
  //       });
  //       var polyline = new H.map.Polyline(linestring, {
  //         style: {
  //           lineWidth: 10,
  //           strokeColor: 'rgba(0, 128, 255, 0.7)',
  //         },
  //       });
  //       var routeArrows = new H.map.Polyline(linestring, {
  //         style: {
  //           lineWidth: 10,
  //           fillColor: 'white',
  //           strokeColor: 'rgba(255, 255, 255, 1)',
  //           lineDash: [0, 2],
  //         },
  //       });
  //       this.routeLine = new H.map.Group();
  //       this.routeLine.addObjects([polyline, routeArrows]);
  //       this.mapp.addObject(this.routeLine);
  //       this.mapp.getViewModel().setLookAtData({ bounds: polyline.getBoundingBox() });
  //     });
  //   }
  // }

  ClickOnSearch($event) {
    this.showAddress = false;
  }

  openModal(event) {
    const ModalPOI = this.modalService.open(ModalComponent, { centered: true, size: 'xl' });
    ModalPOI.componentInstance.lng = event.lng;
    ModalPOI.componentInstance.lat = event.lat;
    ModalPOI.componentInstance.mapType = 'Leaflet';
  }

  reverseGeocode(e) {
    this.mapService
      .getAddress(e.lng, e.lat)
      .then((res) => res.json())
      .then((address) => {
        let result = {
          VehiculesProximite: null,
          info: null,
        };
        result.VehiculesProximite = this.getListOfVehiculePoximite(e.lat, e.lng);
        result.info = address;
        this.showAddress = true;
        this.address = result;
      });
  }
  getListOfVehiculePoximite(lat, lng) {
    let listVehicules = [];
    this.vehicule.forEach((Veh) => {
      let result = {
        vehiculeId: null,
        latitude: null,
        longitude: null,
        vRN: null,
        vIN: null,
        numeroCarte: null,
      };

      let distance = this.distance(lat, lng, Veh.latitude, Veh.longitude, 'K');
      if (distance > 0 && distance < 50) {
        (result.vehiculeId = Veh.id),
          (result.latitude = +Veh.latitude),
          (result.longitude = +Veh.longitude),
          (result.vRN = Veh.vrn),
          (result.vIN = Veh.vin),
          (result.numeroCarte = Veh.numConducteur);
        listVehicules.push(result);
      }
    });

    return listVehicules;
  }
  distance(lat1, lon1, lat2, lon2, unit) {
    if (lat1 == lat2 && lon1 == lon2) {
      return 0;
    } else {
      var radlat1 = (Math.PI * lat1) / 180;
      var radlat2 = (Math.PI * lat2) / 180;
      var theta = lon1 - lon2;
      var radtheta = (Math.PI * theta) / 180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = (dist * 180) / Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit == 'K') {
        dist = dist * 1.609344;
      }
      if (unit == 'N') {
        dist = dist * 0.8684;
      }
      return dist;
    }
  }

  createNewMarker(mat: any, markerType?: boolean, color?: string): any {
    let ColorRandom = '';
    if (color) {
      ColorRandom = color;
    } else {
      let randomIndex = Math.floor(Math.random() * this.color.length);
      ColorRandom = this.color[randomIndex];
    }

    let template;
    if (!markerType) {
      template = `<?xml version="1.0" encoding="UTF-8"?>
      <svg xmlns="http://www.w3.org/2000/svg" id="Component_41_58" data-name="Component 41 – 58" width="154.389" height="28.842" viewBox="0 0 154.389 28.842">
        <g id="Group_17668" data-name="Group 17668" transform="translate(68.408)">
          <g id="Group_17448" data-name="Group 17448" transform="translate(-875.391 -442)">
            <g id="Rectangle_4" data-name="Rectangle 4" transform="translate(878.371 442)" fill="#fff" stroke="#354360" stroke-width="1">
              <rect width="83" height="15" rx="7.5" stroke="none"></rect>
              <rect x="0.5" y="0.5" width="82" height="14" rx="7" fill="none"></rect>
            </g>
            <text id="CA123AB" transform="translate(913.371 453.249)" fill="#354360" font-size="9" font-family="Avenir-Medium, Avenir" font-weight="500">
              <tspan x="0" y="0">${mat}</tspan>
            </text>
            <g id="Group_18890" data-name="Group 18890" transform="translate(897.507 445.249)">
              <path id="Path_14320" data-name="Path 14320" d="M2738.878,777.9a.842.842,0,0,1-.078.072A.842.842,0,0,0,2738.878,777.9Z" transform="translate(-2734.543 -773.165)" fill="#354360"></path>
              <path id="Path_14321" data-name="Path 14321" d="M2736.44,780.6c-.013.007-.027.02-.04.026A.132.132,0,0,1,2736.44,780.6Z" transform="translate(-2732.298 -775.689)" fill="#354360"></path>
              <path id="Path_14322" data-name="Path 14322" d="M2692.7,771.1a1.986,1.986,0,0,0,.15.235C2692.8,771.257,2692.746,771.179,2692.7,771.1Z" transform="translate(-2691.402 -766.807)" fill="#354360"></path>
              <path id="Path_14323" data-name="Path 14323" d="M2695,774.7a1.934,1.934,0,0,0,1.559.783,1.978,1.978,0,0,0,1.011-.28,1.951,1.951,0,0,1-2.571-.5Z" transform="translate(-2693.552 -770.174)" fill="#354360"></path>
              <path id="Path_14324" data-name="Path 14324" d="M2691.485,768.67a1.4,1.4,0,0,1-.084-.17C2691.427,768.552,2691.453,768.611,2691.485,768.67Z" transform="translate(-2690.188 -764.376)" fill="#354360"></path>
              <path id="Path_14325" data-name="Path 14325" d="M2747.253,778.184h4.794a3.183,3.183,0,0,0-6.047-1.39A3.4,3.4,0,0,1,2747.253,778.184Z" transform="translate(-2741.346 -770.463)" fill="#354360"></path>
              <path id="Path_14326" data-name="Path 14326" d="M2744.4,741.4a2.072,2.072,0,0,1,.163.339A2.072,2.072,0,0,0,2744.4,741.4Z" transform="translate(-2739.783 -739.036)" fill="#354360"></path>
              <path id="Path_14327" data-name="Path 14327" d="M2744.563,767.1a2.069,2.069,0,0,1-.163.339A2.069,2.069,0,0,0,2744.563,767.1Z" transform="translate(-2739.783 -763.067)" fill="#354360"></path>
              <path id="Path_14328" data-name="Path 14328" d="M2734.531,781l-.131.091Z" transform="translate(-2730.427 -776.063)" fill="#354360"></path>
              <path id="Path_14329" data-name="Path 14329" d="M2737.118,779l-.117.1A.788.788,0,0,0,2737.118,779Z" transform="translate(-2732.86 -774.193)" fill="#354360"></path>
              <path id="Path_14330" data-name="Path 14330" d="M2740.124,775.8c-.039.046-.079.091-.124.137A.862.862,0,0,0,2740.124,775.8Z" transform="translate(-2735.666 -771.201)" fill="#354360"></path>
              <path id="Path_14331" data-name="Path 14331" d="M2742.011,773.5a1.265,1.265,0,0,1-.111.15A1.265,1.265,0,0,0,2742.011,773.5Z" transform="translate(-2737.444 -769.051)" fill="#354360"></path>
              <path id="Path_14332" data-name="Path 14332" d="M2678.437,801.646a3.022,3.022,0,0,0-2.629-1.546,3.012,3.012,0,0,0-3.007,3.007h6.021A3,3,0,0,0,2678.437,801.646Z" transform="translate(-2672.801 -793.935)" fill="#354360"></path>
              <circle id="Ellipse_306" data-name="Ellipse 306" cx="1.944" cy="1.944" r="1.944" transform="translate(5.574 0)" fill="#354360"></circle>
              <circle id="Ellipse_307" data-name="Ellipse 307" cx="1.944" cy="1.944" r="1.944" transform="translate(1.063 1.431)" fill="#354360"></circle>
            </g>
          </g>
          <path id="Path_11171" data-name="Path 11171" d="M2719.464,1517.2c.242.512.508,1.024.774,1.512.145.243.266.488.411.731s.29.488.435.707.29.464.436.683c.29.463.58.878.894,1.317.217.317.435.609.677.9.145.2.29.39.435.561.024.024.024.049.048.049.121.17.266.317.387.488.145.17.29.342.411.512.1.122.218.244.314.366.024.049.073.073.1.122.121.146.266.293.387.439s.242.268.363.39.217.244.338.366a3.829,3.829,0,0,0,.314.317c.628.658,1.015,1.024,1.015,1.024a33.305,33.305,0,0,0,2.538-2.707c.363-.415.725-.878,1.112-1.366,2.683-3.414,5.85-8.584,5.85-13.876a9.658,9.658,0,0,0-.29-2.39,8.752,8.752,0,0,0-.459-1.341,5.26,5.26,0,0,0-.411-.829c-.072-.146-.145-.268-.217-.39a9.347,9.347,0,0,0-.8-1.122,10.153,10.153,0,0,0-1.644-1.585,6.734,6.734,0,0,0-.749-.512,10.539,10.539,0,0,0-1.668-.8c-.29-.1-.58-.2-.894-.268a8.864,8.864,0,0,0-1.4-.244c-.315-.024-.653-.049-.967-.049s-.653.025-.967.049a3.218,3.218,0,0,1-.484.049c-.315.049-.628.122-.919.2a8.539,8.539,0,0,0-1.329.464,5.147,5.147,0,0,0-.822.415,9.915,9.915,0,0,0-2.514,1.975c-.193.22-.387.463-.58.707-.1.122-.169.244-.266.366a8.358,8.358,0,0,0-.677,1.195,9.117,9.117,0,0,0-.362.854l-.146.439a10.064,10.064,0,0,0-.435,2.853,16.824,16.824,0,0,0,1.16,5.853A10.692,10.692,0,0,0,2719.464,1517.2Zm7.663-11.072a3.634,3.634,0,1,1-3.6,3.633A3.627,3.627,0,0,1,2727.127,1506.126Z" transform="translate(-2717.699 -1499.526)" fill="${ColorRandom}" stroke="#354360" stroke-width="1"></path>
        </g>
        <g id="Rectangle_6356" data-name="Rectangle 6356" transform="translate(0 0.249)" fill="#fff" stroke="#707070" stroke-width="1" opacity="0">
          <rect width="78" height="12" stroke="none"></rect>
          <rect x="0.5" y="0.5" width="77" height="11" fill="none"></rect>
        </g>
      </svg>
      `;
    } else {
      template = `<?xml version="1.0" encoding="UTF-8"?>
      <svg xmlns="http://www.w3.org/2000/svg" width="75.389" height="35" viewBox="0 0 152.389 35">
        <g id="Group_17668" data-name="Group 17668" transform="translate(66.408 0.161)">
          <g id="Group_17448" data-name="Group 17448" transform="translate(-875.391 -442)">
            <g id="Rectangle_4" data-name="Rectangle 4" transform="translate(878.371 442)" fill="#fff" stroke="#354360" stroke-width="1">
              <rect width="83" height="15" rx="7.5" stroke="none"></rect>
              <rect x="0.5" y="0.5" width="82" height="14" rx="7" fill="none"></rect>
            </g>
            <text id="AC123AB" transform="translate(915.371 453)" fill="#354360" font-size="9" font-family="Avenir-Medium, Avenir" font-weight="500">
              <tspan x="0" y="0">${mat}</tspan>
            </text>
            <g id="Group_17584" data-name="Group 17584" transform="translate(-1958.619 3021.504)">
              <path id="Path_12471" data-name="Path 12471" d="M1355.795,2084.344h2.033a.4.4,0,0,0,.371-.546l-.36-.918a.408.408,0,0,0-.337-.255l-1.673-.139a.393.393,0,0,0-.43.395v1.069A.407.407,0,0,0,1355.795,2084.344Z" transform="translate(1510.592 -4656.992)" fill="none"></path>
              <path id="Path_12472" data-name="Path 12472" d="M1277.876,2073.552h.023v-.023a1.731,1.731,0,0,1,3.461,0v.023h4.82a1.731,1.731,0,0,1,3.461-.035.809.809,0,0,0,.662-.79l-.012-2.218a.809.809,0,0,0-.058-.291l-.894-2.23a.822.822,0,0,0-.674-.5l-2.439-.186a.794.794,0,0,0-.859.79v4h-.743v-5.1a.79.79,0,0,0-.79-.79h-5.946a.79.79,0,0,0-.79.79v5.761A.776.776,0,0,0,1277.876,2073.552Zm8.316-5.064a.394.394,0,0,1,.43-.4l1.672.139a.393.393,0,0,1,.337.256l.36.918a.4.4,0,0,1-.372.546h-2.033a.392.392,0,0,1-.395-.4Z" transform="translate(1579.8 -4642.6)" fill="#354360"></path>
              <path id="Path_12473" data-name="Path 12473" d="M1288.029,2120a.725.725,0,0,0,.151-.011,1,1,0,0,0,.267-.058,1.3,1.3,0,0,0,.36-.174,1.412,1.412,0,0,0,.616-1.127v-.023a1.363,1.363,0,0,0-.314-.883.747.747,0,0,0-.093-.1l-.012-.011a.756.756,0,0,0-.139-.116,1.573,1.573,0,0,0-.3-.174.414.414,0,0,0-.128-.046,1.822,1.822,0,0,0-.267-.058.621.621,0,0,0-.151-.011,1.247,1.247,0,0,0-.546.116,1.731,1.731,0,0,0-.348.209,1.553,1.553,0,0,0-.2.2,1.265,1.265,0,0,0-.256.476,1.819,1.819,0,0,0-.058.267.614.614,0,0,0-.012.151v.023A1.438,1.438,0,0,0,1288.029,2120Z" transform="translate(1571.401 -4687.676)" fill="#354360"></path>
              <path id="Path_12474" data-name="Path 12474" d="M1360.655,2119.96a2.059,2.059,0,0,0,.174-.36,1.807,1.807,0,0,0,.058-.267.61.61,0,0,0,.012-.151v-.035a.321.321,0,0,0-.012-.1.992.992,0,0,0-.058-.267,1.3,1.3,0,0,0-.174-.36,1.4,1.4,0,0,0-2.555.767v.023a1.438,1.438,0,0,0,.406.987,1.387,1.387,0,0,0,.987.406A1.464,1.464,0,0,0,1360.655,2119.96Z" transform="translate(1508.206 -4688.206)" fill="#354360"></path>
            </g>
          </g>
          <path id="Path_11171" data-name="Path 11171" d="M2719.464,1517.2c.242.512.508,1.024.774,1.512.145.243.266.488.411.731s.29.488.435.707.29.464.436.683c.29.463.58.878.894,1.317.217.317.435.609.677.9.145.2.29.39.435.561.024.024.024.049.048.049.121.17.266.317.387.488.145.17.29.342.411.512.1.122.218.244.314.366.024.049.073.073.1.122.121.146.266.293.387.439s.242.268.363.39.217.244.338.366a3.829,3.829,0,0,0,.314.317c.628.658,1.015,1.024,1.015,1.024a33.305,33.305,0,0,0,2.538-2.707c.363-.415.725-.878,1.112-1.366,2.683-3.414,5.85-8.584,5.85-13.876a9.658,9.658,0,0,0-.29-2.39,8.752,8.752,0,0,0-.459-1.341,5.26,5.26,0,0,0-.411-.829c-.072-.146-.145-.268-.217-.39a9.347,9.347,0,0,0-.8-1.122,10.153,10.153,0,0,0-1.644-1.585,6.734,6.734,0,0,0-.749-.512,10.539,10.539,0,0,0-1.668-.8c-.29-.1-.58-.2-.894-.268a8.864,8.864,0,0,0-1.4-.244c-.315-.024-.653-.049-.967-.049s-.653.025-.967.049a3.218,3.218,0,0,1-.484.049c-.315.049-.628.122-.919.2a8.539,8.539,0,0,0-1.329.464,5.147,5.147,0,0,0-.822.415,9.915,9.915,0,0,0-2.514,1.975c-.193.22-.387.463-.58.707-.1.122-.169.244-.266.366a8.358,8.358,0,0,0-.677,1.195,9.117,9.117,0,0,0-.362.854l-.146.439a10.064,10.064,0,0,0-.435,2.853,16.824,16.824,0,0,0,1.16,5.853A10.692,10.692,0,0,0,2719.464,1517.2Zm7.663-11.072a3.634,3.634,0,1,1-3.6,3.633A3.627,3.627,0,0,1,2727.127,1506.126Z" transform="translate(-2717.699 -1499.526)" fill="${ColorRandom}" stroke="#354360" stroke-width="1"></path>
          <g id="Rectangle_6101" data-name="Rectangle 6101" transform="translate(-66.408 -0.161)" fill="#fff" stroke="#707070" stroke-width="1" opacity="0.002">
            <rect width="77" height="35" stroke="none"></rect>
            <rect x="0.5" y="0.5" width="76" height="34" fill="none"></rect>
          </g>
        </g>
      </svg>`;
    }
    let myIconUrl = 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(template);
    return myIconUrl;
  }
  public ob: Array<any> = [];

  Aproximité(coord) {
    this.aproxinfo = [];
    this.listVehiculesItem.map((e) => {
      var a = this.distance(coord.lat, coord.lng, e.Position.Lat, e.Position.Lng, 'K');
      if (a < 50) {
        this.aproxinfo.push(e);
      }
    });
  }
  @ViewChild('pop', { static: true }) pop: ElementRef;

  closePop() {
    this.pop.nativeElement.classList.remove('show');
    this.pop.nativeElement.style.zIndex = -1;
  }
  async infocoords(coord) {
    this.RechSHOW = false;
    this.Aproximité(coord);
    this.closePop();
    this.pop.nativeElement.style.zIndex = 5;
    this.infoendroit = [];
    this.infoendroit.push('List des adresses :');
    this.infoendroit.push(coord.lat.toFixed(6) + ',' + coord.lng.toFixed(6));
    var listadr = await this.geodecodeinfo(coord.lat, coord.lng);
    this.infoendroit.push(...listadr);
    if (this.infoendroit.length > 0) {
      this.pop.nativeElement.classList.add('show');

      var array = listadr[0].split(',');
      this.infoendroit.push('Pays :' + array[array.length - 1]);
    }
    var menu = document.getElementById('menuC');
    menu.hidden = true;
  }
  async geodecodeinfo(lat, lag): Promise<Array<string>> {
    var geocoder = new google.maps.Geocoder();
    const point = new google.maps.LatLng(lat, lag);
    var adress: Array<any> = [];
    await geocoder.geocode({ location: point }, (results, status) => {
      if (status == google.maps.GeocoderStatus.OK) {
        var i = 0;
        results.forEach((el) => {
          if (i == 3) return;
          var address = el.formatted_address;
          adress.push(address);
          i++;
        });
      }
    });
    if (adress != null) return adress;
    if (adress == null) {
      return [];
    }
  }
  ChangingDataTodetailvehiculeAndTracingRoute(vehicule: VehiculeMarker) {
    //identifie type of resource (vehicule=="V" || Spi=="U")
    if (vehicule.type == 'V') this.isSPI = false;
    else this.isSPI = true;
    //setdata to detailvehicuel input "inputdd"
    this.inputdd.id = vehicule.data.idVehicule;
    this.inputdd.vrn = vehicule.data.Immatriculation.Vrn;
    this.inputdd.activite = vehicule.data.Conducteur.Activite;
    this.inputdd.positionX = vehicule.data.Position.Lat;
    this.inputdd.positionY = vehicule.data.Position.Lng;
    this.inputdd.vin = vehicule.data.Immatriculation.Vin;
    this.inputdd.NomC = vehicule.data.Conducteur.NomConducteur;
  }
  async LoadingDataAndTracingRoute(type, id) {
    this.showsniper = true;
    var lastpoint = null;
    if (type == 'V') {
      await this.graphservice.getlast5points(id).then((data: any) => {
        lastpoint = this.graphservice.buildAllHisortiqueVehiculeLast5(data);
        this.showsniper = false;
        this.way = [];
        if (lastpoint != null) {
          lastpoint.listHistorique.forEach((el) => {
            this.way.push({
              location: {
                lat: Number(el.latitude),
                lng: Number(el.longitude),
              },
              stopover: false,
            });
          });
          var way = this.way;
          this.way = [];
          if (way.length > 0) this.autoTracingRoute(this.mapp, way, '#32CD32');
        }
      });
    } else lastpoint = await this.service.getlast5PointsUserByid(id);
    this.showsniper = false;
    this.way = [];

    if (lastpoint != null) {
      lastpoint.listHistorique.forEach((el) => {
        this.way.push({
          location: {
            lat: Number(el.latitude),
            lng: Number(el.longitude),
          },
          stopover: false,
        });
      });
      var way = this.way;

      this.way = [];
      this.autoTracingRoute(this.mapp, way, this.inputdd.activite);
    }
  }
  FindListVehiculeAproximite() {
    this.aprox = [];
    this.listVehiculesItem.map((e) => {
      if (e.idVehicule != this.CurrentV.idVehicule) {
        var a = this.distance(
          this.CurrentV.Position.Lat,
          this.CurrentV.Position.Lng,
          e.Position.Lat,
          e.Position.Lng,
          'K'
        );

        if (a < 100) {
          if (e.Immatriculation.Vrn != null) this.aprox.push(e);
        }
      }
    });
  }
  NewRechercheSelectedVehicule(event: any) {
    this.isSPI = false;
    this.closePop();
    if (event == '') {
      this.CurrentV = null;

      this.RechSHOW = false;
    } else {
      // rechercher par VRN vehicule
      this.JsonCulumeActiviteInput = {
        c: '00/00',
        t: '00/00',
        m: '00/00',
        cp: '00/00',
      };
      var vehicule = this.listv.find((x) => x.data.Immatriculation.Vrn == event);
      if (vehicule != null) {
        this.CurrentV = vehicule.data;
        //setdata to detailvehciulecomponent
        this.ChangingDataTodetailvehiculeAndTracingRoute(vehicule);
        this.RechSHOW = true;
        if (!this.liveMode) {
          //charging data and tracing route
          this.LoadingDataAndTracingRoute(vehicule.type, vehicule.id);
        }
        this.FindListVehiculeAproximite();
        // if (this.liveMode) {
        //   this.listv.find(x => x.id == this.CurrentV.idVehicule).marker.setMap(null);
        //   this.SVC(this.CurrentV)
        // }

        return;
      }
      // rechercher par NunConducteur vehicule
      var vehiculeParConducetur = this.listv.find((x) => x.data.Conducteur.numCartConducteur == event);
      if (vehiculeParConducetur != null) {
        this.CurrentV = vehiculeParConducetur.data;
        //setdata to detailvehciulecomponent
        this.ChangingDataTodetailvehiculeAndTracingRoute(vehiculeParConducetur);
        this.RechSHOW = true;
        if (!this.liveMode) {
          //charging data and tracing route
          this.LoadingDataAndTracingRoute(vehiculeParConducetur.type, vehiculeParConducetur.id);
        }
        this.FindListVehiculeAproximite();
        if (this.liveMode) {
          this.mapp.removeObject(this.listv.find((x) => x.id == this.CurrentV.idVehicule));
          // this.SVC(this.CurrentV)
        }
        this.showsniper = false;
        return;
      }
    }
  }
  diffdate(dt1, dt2): number {
    var date1 = new Date(dt1);
    var date2 = new Date(dt2);
    var Diff_temps = date2.getTime() - date1.getTime();
    var Diff_jours = Diff_temps / (1000 * 3600 * 24);
    return Math.round(Diff_jours);
  }
  async filtretourne(id, dd, df) {
    var tournes = await this.service.getTournees(id);
    var tourneefiltre = [];
    tournes.forEach((el) => {
      if (
        new Date(formatDate(el.dateDebut, 'yyyy-MM-dd', 'en_US')) >= new Date(formatDate(dd, 'yyyy-MM-dd', 'en_US'))
      ) {
        tourneefiltre.push(el.enlevementLivraisons);
      }
    });
    return tourneefiltre;
  }
  tourneesMarkerHistorique = [];

  async SVC(event: VehiculeModel) {
    this.tourneesMarkerHistorique.forEach((el) => {
      el.setVisibility(false);
    });
    await this.filtretourne(
      event.idVehicule,
      formatDate(this.datacalendar, 'yyyy-MM-dd', 'en_US'),
      formatDate(this.datecalendarend, 'yyyy-MM-dd', 'en_US')
    ).then((tournees) => {
      tournees.forEach((el) => {
        if (this.routeLinee != null) {
          this.routeLinee.setVisibility(false);
        }
        el.forEach((el) => {
          if (!el.isEnlevement) {
            var icon2 =
              'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.replace('Liv', 'rgb(91, 255, 91)'));
            const customIcon2 = new H.map.Icon(icon2);
            var marker = new H.map.Marker(
              {
                lat: Number(el.livraison.latitude),
                lng: Number(el.livraison.longitude),
              },
              { icon: customIcon2 }
            );
            this.mapp.addObject(marker);

            if (el.livraison.latitude != null)
              //  marker.setPosition({lat:Number(el.livraison.latitude),lng:Number(el.livraison.longitude)});
              this.tourneesMarkerHistorique.push(marker);
          }
          if (el.isEnlevement) {
            var icon2 =
              'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.replace('Exp', 'rgb(60, 119, 255)'));
            const customIcon2 = new H.map.Icon(icon2);
            if (el.expedition.longitude != null) {
              var marker = new H.map.Marker(
                {
                  lat: Number(el.expedition.latitude),
                  lng: Number(el.expedition.longitude),
                },
                { icon: customIcon2 }
              );
              this.mapp.addObject(marker);
            }
            if (el.expedition.latitude != null)
              //  marker.setPosition({lat:Number(el.expedition.latitude),lng:Number(el.expedition.longitude)});
              this.tourneesMarkerHistorique.push(marker);
          }
        });
      });
      // this.tourneesMarkerHistorique.forEach(el=>{
      //   el.setMap(this.mapp)
      // })
    });

    this.clearrrtimer(0);

    this.polylineArray.forEach((el) => {
      this.mapp.removeObject(el);
    });
    this.polylineArray = [];
    this.renderArray.forEach((el) => {
      this.mapp.removeObject(el);
    });
    this.renderArray = [];
    this.stationmarkerArray.forEach((el) => {
      this.mapp.removeObject(el);
    });
    this.stationmarkerArray = [];
    this.closeAlert();
    if (this.mrk != undefined) {
      this.mapp.removeObject(this.mrk);
      // this.mrk.setMap(null)
    }
    if (event == null) {
      this.showsniper = false;
      return;
    }
    var b: any;
    if (event.idVehicule != '00000000-0000-0000-0000-000000000000') this.showsniper = true;

    var type = '';
    this.listv.forEach((el) => {
      if (el.id == event.idVehicule) type = el.type;
    });
    if (type == 'V')
      // await this.service
      //   .getPointsByid(
      //     event.idVehicule,
      //     formatDate(this.datacalendar, 'yyyy-MM-dd', 'en_US'),
      //     formatDate(this.datecalendarend, 'yyyy-MM-dd', 'en_US')
      //   )
      //   .then(async (b) => {

      await this.graphservice
        .getHistorique(
          formatDate(this.datacalendar, 'yyyy-MM-dd', 'en_US') + 'T00:00:00.000Z',
          formatDate(this.datecalendarend, 'yyyy-MM-dd', 'en_US') + 'T23:59:59.000Z',
          event.idVehicule
        )
        .subscribe(async (data: any) => {
          b = this.graphservice.buildAllHisortiqueVehicule(data, event.idVehicule);
          var dt1 = formatDate(this.datacalendar, 'yyyy-MM-dd', 'en_US') + 'T00:00:00.000Z';
          var dt2 = formatDate(this.datecalendarend, 'yyyy-MM-dd', 'en_US') + 'T23:59:59.000Z';

          if (b != undefined) {
            if (this.diffdate(dt1, dt2) <= 1) this.RangeActiviteStyle(b);
          }
          if (b.listHistorique.length == 0 || b == undefined) {
            this.alert.nativeElement.classList.add('show');

            this.showsniper = false;
            return;
          }
          this.listpointsHistorique = b.listHistorique;
          this.countlistpointsHistorique = b.listHistorique.length;
          this.datehistorique = formatDate(
            this.listpointsHistorique[this.countlistpointsHistorique - 1].dateReception,
            'yyyy-MM-dd',
            'en_FR'
          );
          this.timehistorique = formatDate(
            this.listpointsHistorique[this.countlistpointsHistorique - 1].dateReception,
            'HH:mm:ss',
            'en_FR'
          );

          var i = 0;

          this.way = [];
          var stationcompteur = 0;

          var icon1 = '../../../assets/img/depart default.svg';
          const customIcon1 = new H.map.Icon(icon1, {
            //  anchor: { x: 30, y: 40 },
            crossOrigin: true,
            //  size: { w: 12, h: 12 },
          });
          var stationmarker = new H.map.Marker(
            { lat: b.listHistorique[0].latitude, lng: b.listHistorique[0].longitude },
            { icon: customIcon1 }
          );

          this.stationmarkerArray.push(stationmarker);

          this.mapp.addObject(stationmarker);

          var icon2 = '../../../assets/img/arrivé default.svg';
          const customIcon2 = new H.map.Icon(icon2, {
            // anchor: { x: 30, y: 40 },
            crossOrigin: true,
            // size: { w: 12, h: 12 },
          });
          var stationmarkerfin = new H.map.Marker(
            {
              lat: b.listHistorique[this.countlistpointsHistorique - 1].latitude,
              lng: b.listHistorique[this.countlistpointsHistorique - 1].longitude,
            },
            { icon: customIcon2 }
          );
          this.mapp.addObject(stationmarkerfin);

          stationmarkerfin.addEventListener('tap', async () => {
            // var window = await this.Getwindowinfo(event.Immatriculation.Vrn, b);
            //  window.open(this.map, stationmarkerfin);
          });
          this.stationmarkerArray.push(stationmarkerfin);
          this.JsonCulumeActiviteInput = this.mapFn.cumuleAcitivite(b.listHistorique);
          var activite = this.setActivite(b.listHistorique[0].activite);
          for await (var item of b.listHistorique) {
            if (i > 0) {
              // var c = this.distance(
              //   Number(item.latitude),
              //   Number(item.longitude),
              //   Number(b.listHistorique[i - 1].latitude),
              //   Number(b.listHistorique[i - 1].longitude),
              //   'K'
              // );

              // if (c < 0.07) {
              //   stationcompteur++;
              // } else if (i == this.countlistpointsHistorique - 1) {
              //   if (stationcompteur >= 10) {
              //     var icon = '../../../assets/img/flagr.svg';
              //     const customIcon = new H.map.Icon(icon, {
              //       anchor: new H.math.Point(0, 4),
              //       crossOrigin: true,
              //       size: { w: 30, h: 40 },
              //     });
              //     var stationmarker = new H.map.Marker(
              //       { lat: item.latitude, lng: item.longitude },
              //       { icon: customIcon }
              //     );
              //     this.stationmarkerArray.push(stationmarker);
              //     this.mapp.addObject(stationmarker);
              //     stationcompteur = 0;
              //   }
              //   stationcompteur = 0;
              // } else {
              //   if (stationcompteur >= 10) {
              //     var icon = '../../../assets/img/flagr.svg';
              //     const customIcon = new H.map.Icon(icon, {
              //       anchor: { x: 0, y: 40 },
              //       crossOrigin: true,
              //       size: { w: 30, h: 40 },
              //     });
              //     var stationmarker = new H.map.Marker(
              //       { lat: item.latitude, lng: item.longitude },
              //       { icon: customIcon }
              //     );
              //     this.stationmarkerArray.push(stationmarker);
              //     this.mapp.addObject(stationmarker);
              //     stationcompteur = 0;
              //   }
              //   stationcompteur = 0;
              // }

              if (activite != this.setActivite(b.listHistorique[i].activite)) {
                activite = this.setActivite(b.listHistorique[i].activite);
                var url = '';
                var size;

                if (activite == 0 || activite == 1) {
                  url = '../../../assets/img/coupoure default.svg';
                }
                if (activite == 2) {
                  url = '../../../assets/img/coupoure default.svg';
                }
                if (activite == 3) {
                  url = '../../../assets/img/depart default.svg';
                }
                if (activite == 4) {
                  url = '../../../assets/img/travail default.svg';
                }
                // if (activite == 5) {
                //   url = '../../../assets/img/conduit default.svg';
                // }
                if (activite != 5) {
                  const customIcon = new H.map.Icon(url, {
                    //  anchor: new H.math.Point(0, 0),
                    crossOrigin: true,
                    // size: { w: size, h: size }
                  });
                  var stationmarker = new H.map.Marker(
                    { lat: b.listHistorique[i].latitude, lng: b.listHistorique[i].longitude },
                    { icon: customIcon }
                  );
                  this.stationmarkerArray.push(stationmarker);
                  this.mapp.addObject(stationmarker);
                }
              }
            }
            i++;
            this.way.push({
              location: {
                lat: Number(item.latitude),
                lng: Number(item.longitude),
              },
              stopover: false,
            });
          }
          var path = this.way;
          await new Promise((f) => setTimeout(f, 500));
          var customIcon = new H.map.Icon(
            'data:image/svg+xml;charset=UTF-8,' +
              encodeURIComponent(this.replace(this.CurrentV.Immatriculation.Vrn, '#ffffff'))
          );
          // const customIcon = new H.map.Icon(el.marker.getIcon());

          this.mrk = new H.map.Marker(
            {
              lat: 0,
              lng: 0,
            },
            { icon: customIcon }
          );
          // this.mrk = new google.maps.Marker({
          //   visible: true,
          // });
          // this.mrk = new H.map.Marker({lat:0, lng:0});
          this.mapp.addObject(this.mrk);
          this.mrk.setVisibility(true);

          /*  var selectvalue = document.getElementById('Vselect') as HTMLSelectElement;
          var value = selectvalue.options[selectvalue.selectedIndex].text*/
          this.listv.forEach(async (el) => {
            if (el.id == event.idVehicule) {
              this.way = [];

              this.setmarkerhistorique(el, b, event.Immatriculation.Vrn);
            }
          });
          this.autoRefresh(this.mapp, path, this.mrk);
          this.showsniper = false;
        });
    else
      await this.service
        .getPointsUserByid(
          event.idVehicule,
          formatDate(this.datacalendar, 'yyyy-MM-dd', 'en_US'),
          formatDate(this.datecalendarend, 'yyyy-MM-dd', 'en_US')
        )
        .then(async (b) => {
          if (b.listHistorique.length == 0) {
            this.alert.nativeElement.classList.add('show');

            this.showsniper = false;
            return;
          }
          this.listpointsHistorique = b.listHistorique;
          this.countlistpointsHistorique = b.listHistorique.length;
          this.datehistorique = formatDate(
            this.listpointsHistorique[this.countlistpointsHistorique - 1].dateReception,
            'yyyy-MM-dd',
            'en_FR'
          );
          this.timehistorique = formatDate(
            this.listpointsHistorique[this.countlistpointsHistorique - 1].dateReception,
            'HH:mm:ss',
            'en_FR'
          );
          var i = 0;

          this.way = [];
          var stationcompteur = 0;
          // debut fin marker
          var icond = 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.replace('Debut', '#354360'));
          const customIcond = new H.map.Icon(icond, {
            anchor: { x: 15, y: 20 },
            crossOrigin: true,
            size: { w: 30, h: 40 },
          });
          var stationmarkerd = new H.map.Marker(
            { lat: b.listHistorique[0].latitude, lng: b.listHistorique[0].longitude },
            { icon: customIcond }
          );
          this.stationmarkerArray.push(stationmarkerd);
          this.mapp.addObject(stationmarkerd);

          var iconf = 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.replace('Fin', '#354360'));
          const customIconf = new H.map.Icon(iconf, {
            anchor: { x: 15, y: 20 },
            crossOrigin: true,
            size: { w: 30, h: 40 },
          });
          var stationmarkerf = new H.map.Marker(
            {
              lat: b.listHistorique[this.countlistpointsHistorique - 1].latitude,
              lng: b.listHistorique[this.countlistpointsHistorique - 1].longitude,
            },
            { icon: customIconf }
          );
          this.stationmarkerArray.push(stationmarkerf);
          this.mapp.addObject(stationmarkerf);

          stationmarkerf.addEventListener('tap', async () => {
            // var window = await this.Getwindowinfo(event.Immatriculation.Vrn, b);
            // window.open(this.map, stationmarkerf);
          });
          for await (var item of b.listHistorique) {
            if (i > 0) {
              var c = this.distance(
                Number(item.latitude),
                Number(item.longitude),
                Number(b.listHistorique[i - 1].latitude),
                Number(b.listHistorique[i - 1].longitude),
                'K'
              );

              if (c < 0.07) {
                stationcompteur++;
              } else if (i == this.countlistpointsHistorique - 1) {
                if (stationcompteur >= 10) {
                  var icon = 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.replace('Repos', '#FF0000'));
                  const customIcon = new H.map.Icon(icon, {
                    anchor: { x: 15, y: 20 },
                    crossOrigin: true,
                    size: { w: 30, h: 40 },
                  });
                  var stationmarker = new H.map.Marker(
                    { lat: item.latitude, lng: item.longitude },
                    { icon: customIcon }
                  );
                  this.stationmarkerArray.push(stationmarker);
                  this.mapp.addObject(stationmarker);
                  stationcompteur = 0;
                }
                stationcompteur = 0;
              } else {
                if (stationcompteur >= 10) {
                  var icon = 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.replace('Repos', '#FF0000'));
                  const customIcon = new H.map.Icon(icon, {
                    anchor: { x: 15, y: 20 },
                    crossOrigin: true,
                    size: { w: 30, h: 40 },
                  });
                  var stationmarker = new H.map.Marker(
                    { lat: item.latitude, lng: item.longitude },
                    { icon: customIcon }
                  );
                  this.stationmarkerArray.push(stationmarker);
                  this.mapp.addObject(stationmarker);
                  stationcompteur = 0;
                }
                stationcompteur = 0;
              }
            }
            i++;
            this.way.push({
              location: {
                lat: Number(item.latitude),
                lng: Number(item.longitude),
              },
              stopover: false,
            });
          }
          var path = this.way;
          await new Promise((f) => setTimeout(f, 500));

          // this.mrk = new google.maps.Marker({
          //   visible: true,
          // });
          this.mapp.addObject(this.mrk);
          this.mrk.setVisibility(true);
          /*  var selectvalue = document.getElementById('Vselect') as HTMLSelectElement;
          var value = selectvalue.options[selectvalue.selectedIndex].text*/
          this.listv.forEach(async (el) => {
            if (el.id == event.idVehicule) {
              this.way = [];

              this.setmarkerhistorique(el, b, event.Immatriculation.Vrn);
            }
          });
          this.autoRefresh(this.mapp, path, this.mrk);
          this.showsniper = false;
        });
  }
  RangeActiviteStyle(points) {
    var activite = this.setActivite(points.listHistorique[0].activite);
    var randomcolor = '#354360';
    if (activite == 2) {
      randomcolor = '#17a2b8';
    }
    if (activite == 3) {
      randomcolor = '#fd7e14';
    }
    if (activite == 4) {
      randomcolor = '#FF0000';
    }
    if (activite == 5) {
      randomcolor = '#006400';
    }
    var percent = 0;
    var str = '' + randomcolor + ' 0% ';
    var count = 0;
    points.listHistorique.forEach((el) => {
      el.activite = this.setActivite(el.activite);
      if (el.activite == activite) {
        count++;
      } else {
        percent = percent + (count * 100) / points.listHistorique.length;
        str = str + percent + '%, ';
        count = 1;
        activite = el.activite;
        var randomcolor = '#354360';
        if (activite == 2) {
          randomcolor = '#17a2b8';
        }
        if (activite == 3) {
          randomcolor = '#fd7e14';
        }
        if (activite == 4) {
          randomcolor = '#FF0000';
        }
        if (activite == 5) {
          randomcolor = '#006400';
        }
        str = str + '' + randomcolor + ' ' + percent + '% ';
      }
    });
    str = str + ' 100%';
    this.range.nativeElement.style.background = 'linear-gradient(to right,' + str + ')';
  }
  setmarkerhistorique(el, b, value) {
    this.mrk.setGeometry({
      lat: b.listHistorique[b.listHistorique.length - 1].latitude,
      lng: b.listHistorique[b.listHistorique.length - 1].longitude,
    });
    // var customIcon = new H.map.Icon(
    //   'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.replace(el.data.Immatriculation.Vrn, '#ffffff'))
    // );
    // const customIcon = new H.map.Icon(el.marker.getIcon());

    // var marker = new H.map.Marker(
    //   {
    //     lat: b.listHistorique[b.listHistorique.length - 1].latitude,
    //     lng: b.listHistorique[b.listHistorique.length - 1].longitude,
    //   },
    //   { icon: customIcon }
    // );
    // this.map.addObject(marker);
    this.mapp.setZoom(7);
  }
  // async geodecode1(lat, lag): Promise<string> {
  //   var geocoder = new google.maps.Geocoder;
  //   const point = new google.maps.LatLng(lat, lag)
  //   var adress: any;
  //   await geocoder.geocode({ 'location': point }, (results, status) => {
  //     if (status == google.maps.GeocoderStatus.OK) {
  //       if (results[0]) {
  //         var address = (results[1].formatted_address);
  //         adress = address;

  //       }
  //     }
  //   })
  //   if (adress != null)
  //     return adress;
  //   if (adress == null) {
  //     return ''
  //   }
  // }
  // async Getwindowinfo(val, b) {
  //   const contentString =
  //     '<div id="content">' +
  //     '<div id="siteNotice">' +
  //     '</div>' +
  //     '<h1> Info </h1>' +
  //     '<h3 id="firstHeading" class="firstHeading"> VRN:' +
  //     val +
  //     '</h3>' +
  //     '<div id="bodyContent">' +
  //     '<p> <strong>Date :</strong>' +
  //     formatDate(b.listHistorique[b.listHistorique.length - 1].dateReception, 'yyyy-MM-dd HH:mm:ss', 'en_FR') +
  //     '<br/>' +
  //     'Adresse :' +
  //     // (await this.geodecode1(
  //     //   b.listHistorique[b.listHistorique.length - 1].latitude,
  //     //   b.listHistorique[b.listHistorique.length - 1].longitude
  //     // )) +
  //     '</div>' +
  //     '</div>';
  //   var bubble = new H.ui.InfoBubble({
  //     // read custom data
  //     content: contentString,
  //   });
  //   // show info bubble
  //   this.ui.addBubble(bubble);
  //   // const infowindow = new google.maps.InfoWindow({
  //   //   content: contentString,
  //   // });
  //   return bubble;
  // }
  moveMarkerr(marker, latlng) {
    //  let marker = new H.map.Marker({ lat: latlng.lat, lng: latlng.lng });

    marker.setGeometry({ lat: latlng.lat, lng: latlng.lng });
    // map.panTo(latlng);
  }
  autoRefresh(map, pathCoords, marker) {
    var i, route, marker;

    // polyline heremap
    var linestring = new H.geo.LineString();

    pathCoords.forEach((point) => {
      linestring.pushPoint({ lat: point.location.lat, lng: point.location.lng });
      this.moveMarkerr(this.mrk, point.location);
    });

    var polyline = new H.map.Polyline(linestring, {
      style: {
        lineWidth: 4,
      },
    });

    this.routeLinee = new H.map.Group();
    this.routeLinee.addObjects([polyline]);
    this.mapp.addObject(this.routeLinee);
    this.mapp.getViewModel().setLookAtData({ bounds: polyline.getBoundingBox() });

    // this.arrowIcon.path = google.maps.SymbolPath.CIRCLE;
    // route = new google.maps.Polyline({
    //   path: [],
    //   geodesic: true,
    //   strokeColor: '#FF0000',
    //   strokeOpacity: 0,
    //   strokeWeight: 5,
    //   editable: false,
    //   icons: [
    //     {
    //       icon: this.arrowIcon,
    //       offset: '0',
    //       repeat: '20px',
    //     },
    //   ],
    //   map: map,
    // });

    // for (i = 0; i < pathCoords.length; i++) {
    //   setTimeout(
    //     (coords, i) => {
    //       var pos = new google.maps.LatLng(coords.location.lat, coords.location.lng);
    //       route.getPath().push(pos);
    //       // this.moveMarker(map, marker, pos);
    //       if (i == pathCoords.length - 1) {
    //         this.polylineArray.push(route);
    //         var focuspoint = new google.maps.LatLng(
    //           pathCoords[pathCoords.length - 1].location.lat,
    //           pathCoords[pathCoords.length - 1].location.lng
    //         );
    //         map.panTo(focuspoint);
    //         if (this.map.getZoom() < 10) this.map.setZoom(10);
    //       }
    //     },
    //     1 * i,
    //     pathCoords[i],
    //     i
    //   );
    // }
  }

  async NewClickMarker(event: VehiculeModel) {
    if (this.CurrentV == event) {
      this.RechSHOW = !this.RechSHOW;
      // delete polyline
      this.CurrentV = null;
      return;
    }

    this.NewRechercheSelectedVehicule(event.Immatriculation.Vrn);
  }
  closeAlert() {
    this.alert.nativeElement.classList.remove('show');
  }
  //#region annimation et historique
  public pause: boolean = true;
  public activitehistorique: any = '';
  public pauseval = 0;

  playpause() {
    if (this.pause) {
      this.pause = false;
      //  this.iconplayer.nativeElement.value='pause';
      //this.iconplayer.nativeElement.classList.add('fa-pause');
      // this.iconplayer.nativeElement.classList.remove('fa-play');
      this.pauseval = this.range.nativeElement.value;
    } else {
      this.pause = true;
      this.clearrrtimer(this.countplayer);
      return;
    }

    var restpoint = this.listpointsHistorique.slice(this.pauseval);
    for (var i = 0; i < restpoint.length; i++) {
      this.timer[i] = setTimeout(
        (coords, i) => {
          this.moveMarkerr(this.mrk, { lat: Number(coords.latitude), lng: Number(coords.longitude) });
          this.datehistorique = formatDate(restpoint[i].dateReception, 'yyyy-MM-dd', 'en_FR');
          this.timehistorique = formatDate(restpoint[i].dateReception, 'HH:mm:ss', 'en_FR');
          this.range.nativeElement.value = this.pauseval++;
          this.activitehistorique = this.getValactivite(restpoint[i].activite);
          this.countplayer = i - 1;
        },
        50 * i,
        restpoint[i],
        i
      );

      if (i > 0) {
        clearTimeout(this.timer[i - 1]);
        this.timer.splice(i - 1, 1);
      }
    }
  }
  clearrrtimer(val) {
    this.countplayer = val;
    // this.iconplayer.nativeElement.value='play_arrow';
    //this.iconplayer.nativeElement.classList.remove('fa-pause');
    //this.iconplayer.nativeElement.classList.add('fa-play');

    this.timer.forEach((el) => {
      clearTimeout(el);
    });
  }
  tabpoly = [];
  ShowTournees(event) {
    // setTimeout(() => {
    //   this.listv.forEach((el) => {
    //     if (el.id == this.CurrentV.id) el.marker.setMap(this.mapp);
    //   });
    // });
    setTimeout(() => {
      this.listv.forEach((el) => {
        el.marker.setVisibility(false);
      });
    });

    this.TourneesmarkerArray.forEach((el) => {
      // el.setMap(null);
      this.mapp.removeObject(el);
      el.setVisibility(false);
      this.routeLine.setVisibility(false);
      // if (linestring != null) linestring.setVisibility(false);
      if (this.tabpoly != null) {
        this.tabpoly.forEach((element) => {
          element.setVisibility(false);
        });
      }
    });
    // this.ClusterMaker.setMap(null);
    this.TourneesmarkerArray = [];
    // this.directionsRenderer.setMap(null);
    if (event == null && !this.liveMode) {
      this.mapp.addLayer(this.clusteringLayer);

      setTimeout(() => {
        this.listv.forEach((el) => {
          // el.marker.setMap(this.map1);
          this.mapp.addObject(el.marker);
          el.marker.setVisibility(false);
        });
      });
      return;
    }
    var way: Array<any> = [];
    event.enlevementLivraisons.forEach(async (element) => {
      if (element.isEnlevement) {
        way.push({
          location: { lng: element.expedition.longitude, lat: element.expedition.latitude },
          stopover: false,
        });
      } else {
        way.push({
          location: { lng: element.livraison.longitude, lat: element.livraison.latitude },
          stopover: false,
        });
      }
    });
    var origin;
    var destination;
    var linestring;
    var routingService;
    var routingParameters;
    var polyline = null;

    for (let i = 0; i < way.length - 1; i++) {
      origin = way[i].location;
      if (way[i + 1]) destination = way[i + 1].location;
      linestring = new H.geo.LineString();
      routingService = this.platform.getRoutingService();
      routingParameters = {
        mode: 'fastest;car',
        waypoint0: origin.lat + ',' + origin.lng,
        waypoint1: destination.lat + ',' + destination.lng,
        representation: 'display',
      };
      routingService.calculateRoute(routingParameters, (success) => {
        var res = success.response.route[0].shape;
        res.forEach(function (point) {
          var parts = point.split(',');
          linestring.pushPoint({ lat: Number(parts[0]), lng: Number(parts[1]) });
          // linestring.pushPoint({ lat: point.lat, lng: point.lng });
        });
        polyline = new H.map.Polyline(linestring, {
          style: {
            lineWidth: 10,
            strokeColor: 'rgba(0, 128, 255, 0.7)',
            // lineTailCap: 'arrow-tail',
            // lineHeadCap: 'arrow-head',
          },
        });
        var routeArrows = new H.map.Polyline(linestring, {
          style: {
            lineWidth: 10,
            fillColor: 'white',
            strokeColor: 'rgba(255, 255, 255, 1)',
            lineDash: [0, 2],
            // lineTailCap: 'arrow-tail',
            // lineHeadCap: 'arrow-head',
          },
        });
        this.routeLine = new H.map.Group();
        this.routeLine.addObjects([polyline, routeArrows]);
        this.tabpoly.push(this.routeLine);
        this.mapp.addObject(this.routeLine);
        this.mapp.getViewModel().setLookAtData({ bounds: polyline.getBoundingBox() });
      });
      i++;
    }
    this.mapp.removeLayer(this.clusteringLayer);

    // this.calculateAndDisplayRoute(this.directionsService, this.directionsRenderer, origin, destination, null, way, null);

    event.enlevementLivraisons.forEach(async (element) => {
      if (element.isEnlevement) {
        var iconf = 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.replace('Enl', 'rgb(60, 119, 255)'));
        const customIcon1 = new H.map.Icon(iconf);
        var marker1 = new H.map.Marker(
          { lat: element.expedition.latitude, lng: element.expedition.longitude },
          { icon: customIcon1 }
        );

        this.mapp.addObject(marker1);

        this.TourneesmarkerArray.push(marker1);
      } else {
        var iconf = 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.replace('Liv', 'rgb(91, 255, 91)'));
        const customIcon1 = new H.map.Icon(iconf);
        var marker1 = new H.map.Marker(
          { lat: element.livraison.latitude, lng: element.livraison.longitude },
          { icon: customIcon1 }
        );
        this.mapp.addObject(marker1);
        this.TourneesmarkerArray.push(marker1);
      }
    });
  }
  GetVrnFromPlusInfoComponent(vrn) {
    this.closePop();
    this.NewRechercheSelectedVehicule(vrn);
  }
  changelecteur(event) {
    this.pause = true;
    var value = event.target.value;
    this.clearrrtimer(value);
    /*  if (event.target.value == this.countlistpointsHistorique) {
        value--
      }*/
    this.moveMarkerr(this.mrk, {
      lat: this.listpointsHistorique[value].latitude,
      lng: this.listpointsHistorique[value].longitude,
    });
    this.datehistorique = formatDate(this.listpointsHistorique[value].dateReception, 'yyyy-MM-dd', 'en_FR');
    this.timehistorique = formatDate(this.listpointsHistorique[value].dateReception, 'HH:mm:ss', 'en_FR');
  }
  public AppParameters: any;
  private _hubConnection: HubConnection | undefined;
  message = '';
  messages: string[] = [];
  hubConnection: HubConnection;
  disable$ = new BehaviorSubject([]);

  Hubconntection2() {
    let url = this.AppParameters.appURl + '/TrackingHub';
    url.replace('//', '/');
    this._hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(url)
      .configureLogging(signalR.LogLevel.Information)
      .build();

    this._hubConnection.start().catch((err) => console.error('Bus Introuvable : ' + url));
    this._hubConnection.on('ReceiveMessage1', async (data) => {
      var t = JSON.parse(data);
      var mark = this.listv.find((x) => x.id == t.idvehciule);

      if (mark != null) {
        mark.data.Position.Lat = t.lat;
        mark.data.Position.Lng = t.lng;


        this.noisePoint.forEach((elem) => {
          if (elem.a.data.data.id == t.idvehciule) {
            elem.a.data.data.data.Position.Lat = t.lat;
            elem.a.data.data.data.Position.Lng = t.lng;
            this.ff(elem);
            if (this.CurrentV != null && this.CurrentV.idVehicule == t.idvehciule) {
              this.NewRechercheSelectedVehicule(elem.a.data.data.data.Immatriculation.Vrn);
            }
          }
        });
      }

      this.disable$.next(data);
      const received = `Received: ${data}`;
      this.messages.push(received);
    });
  }
  //    ngOnDestroy(): void {
  //     this.map.dispose();
  // }
  getnumactivite(text): number {
    if (text == 'Attente') return 3;
    if (text == 'Coupure') return 2;
    if (text == 'Travail') return 4;
    if (text == 'Conduite') return 5;
    return 1;
  }
  getValactivite(text): string {
    if (text == 3) return 'Attente';
    if (text == 2) return 'Coupure';
    if (text == 4) return 'Travail';
    if (text == 5) return 'Conduite';
    return 'Inconnue';
  }
  setActivite(val): number {
    if (val == 0) return 2;
    if (val == 1) return 3;
    if (val == 2) return 4;
    if (val == 3) return 5;
    return 0;
  }
  filtreactivite(val) {

    if (!this.liveMode) {
      if (val == 0) {
        // if(this.ClusterMaker.getMap()==this.map1){
        //   return
        // }
        // this.listv.forEach(el=>{
        // el.marker.setMap(this.map1);
        this.mapp.removeLayer(this.clusteringLayer);
        this.createCluster(this.listv);
        // })
        // this.ClusterMaker.setMap(this.map1);
        return;
      }
      // this.ClusterMaker.setMap(null);
      var list = [];
      this.listv.forEach((el) => {
        if (el.data.Conducteur.Activite == val) {
          // el.marker.setMap(null);

          list.push(el);
        }
      });
      this.mapp.removeLayer(this.clusteringLayer);

      this.createCluster(list);
    }
  }
  public PolyObjStart: any;
  public PolyObjEnd: any;
  async setcoord(coords: any) {
    if (coords != null) {
      if (this.markerDebut != null) {
        this.markerDebut.setGeometry({ lat: coords.lat, lng: coords.lng });
        this.mapp.addObject(this.markerDebut);
      } else {
        let markerIconDebut = this.createNewMarker('Début');
        const customMarkerIconDebut = new H.map.Icon(markerIconDebut);
        this.markerDebut = new H.map.Marker({ lat: coords.lat, lng: coords.lng }, { icon: customMarkerIconDebut });
        this.mapp.addObject(this.markerDebut);
      }
    }
    if (this.markerFin) {
      if (this.markerDebut != null && this.markerFin != null) this.setPoly(this.markerDebut, this.markerFin);
    }

    return;
  }
  async setcoord2(coords: any) {
    if (coords != null) {
      if (this.markerFin != null) {
        this.markerFin.setGeometry({ lat: coords.lat, lng: coords.lng });
        this.mapp.addObject(this.markerFin);
      } else {
        let markerIconfin = this.createNewMarker('Fin');
        const customMarkerIconFin = new H.map.Icon(markerIconfin);
        this.markerFin = new H.map.Marker({ lat: coords.lat, lng: coords.lng }, { icon: customMarkerIconFin });
        this.mapp.addObject(this.markerFin);
      }
    }
    if (this.markerDebut != null && this.markerFin != null) this.setPoly(this.markerDebut, this.markerFin);
    return;
  }

  setPoly(markerDebut, markerFin) {
    var md = markerDebut.getGeometry();
    var mf = markerFin.getGeometry();
    this.mapp.setZoom(7);
    this.menu.hidden = true;

    if (this.routeLine != null) this.mapp.removeObject(this.routeLine);

    var linestring = new H.geo.LineString();
    var routingService = this.platform.getRoutingService();
    var routingParameters = {
      mode: 'fastest;car',
      waypoint0: md.lat + ',' + md.lng,
      waypoint1: mf.lat + ',' + mf.lng,
      representation: 'display',
    };
    routingService.calculateRoute(routingParameters, (success) => {
      var res = success.response.route[0].shape;


      res.forEach(point=> {
        var parts = point.split(',');
        linestring.pushPoint({ lat: Number(parts[0]), lng: Number(parts[1]) });
      });
      var polyline = new H.map.Polyline(linestring, {
        style: {
          lineWidth: 7,
          strokeColor: 'rgba(0, 128, 255, 0.7)',
        },
      });

      this.routeLine = new H.map.Group();
      this.routeLine.addObjects([polyline]);
      this.mapp.addObject(this.routeLine);
      this.mapp.getViewModel().setLookAtData({ bounds: polyline.getBoundingBox() });
    });
  }
}
