import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, from, Observable, Subject, throwError } from 'rxjs';

import { catchError, filter, groupBy, map, mergeMap, reduce, tap, toArray } from 'rxjs/operators';
import { ConfigService } from '@tmsApp/login/services/environment.service';

import { HubConnection } from '@microsoft/signalr';
import * as signalR from '@microsoft/signalr';
import { group } from 'console';
import { element } from 'protractor';

@Injectable({
  providedIn: 'root',
})
export class mapService {
  private connection: signalR.HubConnection;
  connectionEstablished = new Subject<Boolean>();

  private LivePoints = new BehaviorSubject<newHistory[]>([]);
  LivePoints$ = this.LivePoints.asObservable();
  errorcase:any[]=[]
  public AppParameters: any;

  Message: SignalRObject[] = [];
  Allpoints: VehiculePositionObject[];

  constructor(private http: HttpClient, public configService: ConfigService) {
    this.AppParameters = this.configService.config;
  }

  signalRObservable(): Observable<SignalRObject> {
    const subject: Subject<any> = new Subject<any>();

    let url = this.AppParameters.appURl + 'TrackingHub';
    url.replace('//', '/');
    if (!this.connection) {
      this.connection = new signalR.HubConnectionBuilder()
        .withUrl(url)
        .configureLogging(signalR.LogLevel.Information)
        .build();
      this.connection
        .start()
        .then(() => {
          console.log('Hub connection started');
          this.connectionEstablished.next(true);
        })
        .catch((err) => {return subject.next(this.errorcase)} );

      this.connection.on('ReceiveMessage', (data) => {
        let d = JSON.parse(data);
        // console.log('d ',d)
        if (d.evenements[0].tachygraphe.vRN != null) {
          if (d.evenements[0].geoLocalisation.position != null) {
            //d.evenements = d.evenements.filter(ev=>ev.position!=null)
            this.Message.push(d);
          }
        }
        console.log('this.Message :', this.Message);
        from(this.Message)
          .pipe(
            groupBy((object) => object.vehiculeId),
            mergeMap((group) =>
              group.pipe(
                reduce(
                  (groupMessage, message) => {
                    groupMessage.values.push(message);
                    return groupMessage;
                  },
                  { vehiculeId: group.key, values: [] }
                )
              )
            ),
            toArray()
          )
          .pipe(map((data) => data.sort((a, b) => (a.values.length > b.values.length ? -1 : 1))))
          .subscribe((newData: any) => {
            subject.next(newData);
            this.LivePoints.next([...newData]);
          });
      });
    }

    this.connection.onclose((err?: Error) => {
      if (err) {
        console.log('err', err);
        subject.error(err);
      } else {
        subject.complete();
        this.LivePoints.complete();
      }
    });

    return subject;
  }

  getDatafromSignalRbyId(id: string) {
    return this.LivePoints$.pipe(map((data) => data.filter((points) => points.vehiculeId === id)));
  }

  GetHistoriqueVehiculeId(id: string) {
    return this.Allpoints.filter((point) => point.id === id);
    // pipe(map((data) => data.filter((points) => points.vehiculeId === id)));
  }

  disconnect() {
    if (this.connection) {
      this.connection.stop();
      this.connection = null;
    }
  }

  getAddress(lng, lat) {
    return fetch(
      `https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?f=pjson&langCode=EN&location=${lng},${lat}`
    );
  }

  // addressLookup(req?: any): Observable<NominatimResponse[]> {
  //   let url = `http://nominatim.openstreetmap.org/search?q=${encodeURIComponent(
  //     req
  //   )}&accept-language=he&format=json&polygon=1`;
  //   return this.http
  //     .get(url)
  //     .pipe(
  //       map((data: any[]) => data.map((item: any) => new NominatimResponse(item.lat, item.lon, item.display_name)))
  //     );
  // }

  // getPoints(): Observable<any> {
  //   return this.http
  //     .get<HistoryObject[]>(this.AppParameters.apiUrl + 'api/tra/api/HistoriqueVehicule/GetHistoriqueVehicule')
  //     .pipe(
  //       tap((res) => (this.Allpoints = res)),
  //       map((data: HistoryObject[]) => {
  //         return data.map((d) => {
  //           return {
  //             AlreadyPushed: false,
  //             listHistorique: d.listHistorique,
  //             idVehicule: d.idVehicule,
  //             vehiculeRow: d.vehiculeRow,
  //           };
  //         });
  //       })
  //     );
  // }

  getGetLast5HistoriqueVehicule(id){
    return this.http
      .get<VehiculePositionObject[]>(this.AppParameters.apiUrl + 'api/tra/api/HistoriqueVehicule/GetLast5HistoriqueVehicule', {
        params: {
          IdVehicule: id
        }
      }
        )
  }
  GetLast5PositionByUtilisateurId(id){
    return this.http
      .get<VehiculePositionObject[]>(this.AppParameters.apiUrl + 'api/tra/api/HistoriqueVehicule/GetLast5PositionByUtilisateurId', {
        params: {
          IdUtilisateur: id
        }
      }
        )
  }
  getPoints(): Observable<any> {
    return this.http
      .get<VehiculePositionObject[]>(this.AppParameters.apiUrl + 'api/tra/api/HistoriqueVehicule/GetHistoriqueVehicule')
      .pipe(
        tap((res:any) =>
          res.vehiculeRow.forEach((element) => {
            if (element.vrn && element.vin) {
              let data: SignalRObject = {
                boitierId: null,
                compagnieId: null,
                creationDate: element.dateReception,
                id: element.id,
                ischecked: null,
                organisationId: null,
                ressourceId: null,
                tenantNamespace: null,
                vehiculeId: element.id,
                version: null,
                evenements: [
                  {
                    capteurs: null,
                    geoLocalisation: {
                      compteurKm: null,
                      date: null,
                      hasApresContact: null,
                      isEnMouvement: null,
                      position: {
                        cap: null,
                        latitude: +element.latitude,
                        longitude: +element.latitude,
                        vitesse: null,
                      },
                    },
                    tachygraphe: {
                      compteurKm: null,
                      conducteur: null,
                      date: null,
                      passager: null,
                      vIN: element.vin,
                      vRN: element.vrn,
                    },
                  },
                ],
              };
              this.Message.push(data);
              this.errorcase.push({vehiculeId: data.vehiculeId, values: [data]})
            }
          })
        ),
        tap((res) => (this.Allpoints = res.vehiculeRow)),
        // map((data: VehiculePositionObject[]) => {
        //   console.log(data)
        //   return data.map((d) => {
        //     console.log(d)
        //     if (d.vrn) {
        //       return {
        //         AlreadyPushed: false,
        //         id: d.id,
        //         dateReception: d.dateReception,
        //         latitude: d.latitude,
        //         longitude: d.longitude,
        //         vin: d.vin,
        //         vrn: d.vrn,
        //       };
        //     } else {
        //       return false;
        //     }
        //   });
        // })
      );
  }

  async getALLPoints(): Promise<any> {
    const promise = await this.http
      .get<any>(this.AppParameters.apiUrl + 'api/tra/api/HistoriqueVehicule/GetHistoriqueVehicule')
      .toPromise();
    return promise;
  }


  getPositionFromAdress(adresse): Observable<any> {
    const header = {
      headers: new HttpHeaders().set(`Access-Control-Allow-Origin`, `*`),
    };
    return (
      this.http
        .get<any>(
          `https://maps.googleapis.com/maps/api/geocode/json?address=` +
            adresse +
            `&key=AIzaSyCWP09i8wWlzRwgvDXOhyYWnDOxnozNjTA`,
          header
        )
        // return this.http.get<any>(`http://localhost:3001/delivery/relaypoint` ) ///read
        .pipe(
          map((q) => {
            return q;
          })
        )
    );
  }
}
export interface HistoryObject {
  idVehicule: string;
  listHistorique: PointHistoryObject;
  AlreadyPushed: boolean;
  vehiculeRow: vehiculeRowObject;

}
export interface vehiculeRowObject {
  dateReception: string,
    id: string,
    isVehicule: boolean,
    latitude: string,
    longitude: string,
    numConducteur: string,
    vin: string,
    vrn: string,
}
export interface userSpiRowobject{
  dateReception: string,
  id: string,
  isVehicule: boolean,
  latitude: string,
  longitude: string,
  nom: string,
  prenom:  string,
}
export interface PointHistoryObject {
  conducteur: string;
  conducteurId: string;
  createdBy: string;
  createdById: string;
  createdDate: string;
  dateReception: string;
  id: string;
  latitude: number;
  longitude: number;
  modifiedBy: string;
  modifiedById: string;
  modifiedDate: string;
  vehicule: string;
  vehiculeId: string;
}
export interface SignalRObject {
  compagnieId: string;
  organisationId: string;
  vehiculeId: string;
  ressourceId: string;
  boitierId: string;
  evenements: EvenementsObject[];
  id: string;
  creationDate: string;
  version: string;
  tenantNamespace: string;
  ischecked: boolean;
  position?: PointObject[];
}
export interface EvenementsObject {
  geoLocalisation: GeoLocalisationObject;
  tachygraphe: TachygrapheObject;
  capteurs: string;
}
export interface TachygrapheObject {
  compteurKm: string;
  conducteur: string;
  date: string;
  passager: number;
  vIN: string;
  vRN: string;
}
export interface GeoLocalisationObject {
  date: Date;
  position: PositionObject;
  compteurKm: number;
  hasApresContact: boolean;
  isEnMouvement: string;
}
export interface PositionObject {
  latitude: number;
  longitude: number;
  vitesse: number;
  cap: number;
}

export interface PointObject {
  latitude: number;
  longitude: number;
}

export interface newHistory {
  vehiculeId: string;
  values?: [];
}
export interface VehiculePositionObject {
  dateReception: string;
  id: string;
  latitude: number;
  longitude: number;
  vin: string;
  vrn: string;
  AlreadyPushed?: boolean;

}

export interface addressObject {
  location: {
    x: number;
    y: number;
  };
  address: {
    AddNum: string;
    Addr_type: string;
    Address: string;
    Block: string;
    City: string;
    CountryCode: string;
    District: string;
    LongLabel: string;
    Match_addr: string;
    MetroArea: string;
    Neighborhood: string;
    PlaceName: string;
    Postal: string;
    PostalExt: string;
    Region: string;
    Sector: string;
    ShortLabel: string;
    Subregion: string;
    Territory: string;
  };
}
