import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyMapComponent } from './my-map/my-map.component';

const routes: Routes = [
  {
    path: '',
    component: MyMapComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrackingHereMapRoutingModule { }
