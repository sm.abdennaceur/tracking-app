import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from '../angular-material/angular-material.module';

import { TrackingHereMapRoutingModule } from './tracking-here-map-routing.module';
import { MyMapComponent } from './my-map/my-map.component';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { MenuContextComponent } from '../tracking/menu-context/menu-context.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TrackingModule } from '../tracking/tracking.module';
import { TrackingLeafletMapModule } from '../tracking-leaflet-map/tracking-leaflet-map.module';
import { MatOptionModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
  declarations: [MyMapComponent],
  imports: [
    CommonModule,
    FormsModule,
    TrackingHereMapRoutingModule,
    TrackingModule,
    TrackingLeafletMapModule,
    AngularMaterialModule,
    MatOptionModule,
    MatFormFieldModule,
  ],
})
export class TrackingHereMapModule {}
