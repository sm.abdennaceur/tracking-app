import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  ConducteurRechercherItem,
  InfoVehicule,
  ModelDetailVehicule,
  Personne,
  Tachygraphe,
  VehiculeMarker,
  vehiculeModel,
  VehiculePayload,
  VehiculeRechercherItem,
} from '../../tracking/model/Models';
import { Infotournees, ListInfotournes, mapService, PositionObject } from '../services/map-service';
import { ConfigService } from '@tmsApp/login/services/environment.service';
import { formatDate } from '@angular/common';
import { BehaviorSubject, Subscription } from 'rxjs';
import { HubConnection } from '@microsoft/signalr';
import * as signalR from '@microsoft/signalr';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { antPath } from 'leaflet-ant-path';
import 'leaflet';
import 'leaflet-routing-machine';
import 'leaflet-easybutton';
import 'leaflet-pegman';
import 'leaflet.fullscreen';
import 'esri-leaflet-geocoder/dist/esri-leaflet-geocoder';
import * as esri from 'esri-leaflet';
import * as ELG from 'esri-leaflet-geocoder';
import 'leaflet.animatedmarker/src/AnimatedMarker';
import * as leaflet from 'leaflet';
import { ModalComponent } from '../../tracking/Modal/modal/modal.component';
import { ConfService } from '../../tracking/Services/TrackingServices';
import {
  ConducteurVehicule,
  Immatriculation,
  PositionVehicule,
  VehiculeModel,
} from '../../tracking/model/VehiculeModels';
import 'leaflet-openweathermap';
import 'leaflet.heat/dist/leaflet-heat.js';
import { GQLService } from '../../tracking/Services/graph-ql.service';
import { MapFunctionService } from '../../tracking/Services/ServicesMapGoogle/map-function.service';
declare let L;

@Component({
  template: `
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet-easybutton@2/src/easy-button.css" />
    <div class="detail-more-info p-2 p-sm-3">
      <div
        #pop
        class="alert alert-warning alert-dismissible fade"
        style="z-index: 999999999;position:fixed;right:-0.5%;width: 450px;;top:12%;background-color: #fdfdfd00;color: whitesmoke;border: none;z-index: -1; max-height: 720px;"
        role="alert"
      >
        <app-plus-info [infoendroit]="infoendroit" [aproxinfo]="aproxinfo"></app-plus-info>
      </div>
    </div>
    <div class="spinner-grow spinner-style" *ngIf="showsniper">
      <h6 class="mt-5 ml-3">Loading ...</h6>
      <span class="sr-only">Loading....</span>
    </div>
    <div class="historique-Gbtn-map  d-sm-block full-screen-buttons">
      <div class="d-sm-flex">
        <label class="mr-1"> Live </label>
        <div class="custom-control custom-switch">
          <input type="checkbox" class="custom-control-input" id="customSwitches" (change)="changemode($event)" />
          <label class="custom-control-label" for="customSwitches">Historique</label>
        </div>
      </div>
      <app-switshdate (changeView)="getValueFromcalender($event)" *ngIf="!liveMode"> </app-switshdate>
    </div>
    <div
      id="menuC"
      hidden
      style="z-index: 999999999;position:fixed;width: auto;height: auto;top: 0px;display: flex;"
      class="card-body card-bodybeta"
    >
      <app-menu-context
        [positionX]="RightClickLat"
        [positionY]="RightClickLng"
        (RValue)="setcoord($event)"
        (RValue2)="setcoord2($event)"
        (RValueAddPOI)="openModal($event)"
      >
      </app-menu-context>
    </div>
    <div class="detail-vehicule-container p-2 p-sm-3" style="z-index: 999999999;">
      <app-recherche-input
        [show]="RechSHOW"
        [HideRechercheList]="RechSHOW2"
        [listRech]="recherchlist"
        (Close)="closePop()"
        (RValue)="NewRechercheSelectedVehicule($event)"
      ></app-recherche-input>
      <app-detail-vehicule
        [Vehicule]="inputdd"
        [VehiculesAproxlist]="aprox"
        [isSPI]="isSPI"
        [show]="RechSHOW"
        (RValue)="NewRechercheSelectedVehicule($event)"
        (Close)="closePop()"
        (tours)="OnSelectTournee($event)"
        (GetSelectedVehciuleVRN)="GetVrnFromPlusInfoComponent($event)"
      ></app-detail-vehicule>
    </div>
    <div class="map-screen">
      <div class="map-wrapper">
        <div class="map-border">
          <div id="map"></div>
        </div>
      </div>
    </div>
    <div
      class="slidecontainer"
      style="z-index: 999999999999;position:fixed;left: 17%;bottom: 16.5px;"
      id="sliderange"
      hidden
    >
      <div class="sliderbutton" (click)="playpause()">
        <mat-icon>{{ pause ? 'play_arrow' : 'pause' }}</mat-icon>
      </div>
      <input
        type="range"
        min="0"
        max="{{ countlistpointsHistorique }}"
        value="{{ countlistpointsHistorique }}"
        class="slider"
        id="myRange"
        #range
        (change)="changelecteur($event)"
        (input)="changelecteur($event)"
      />
      <div class="sliderP">
        <p>
          <small
            >{{ datehistorique }}&nbsp;&nbsp;&nbsp;{{ timehistorique }} &nbsp;&nbsp;&nbsp;{{
              activiteHistorique
            }}</small
          >
        </p>
      </div>
    </div>
    <div
      #alert
      class="alert alert-warning alert-dismissible fade"
      style="z-index: 99999999;position:fixed;left: 30%;top:10px;background-color: #354360;color: whitesmoke;"
      role="alert"
    >
      <button type="button" class="close" aria-label="Close" (click)="closeAlert()">
        <span aria-hidden="true">&times;</span>
      </button>
      <strong style="color: whitesmoke;"
        >Pas de traget pour ce vehicule pour le date debut :{{ datacalendarStart }} , date fin :
        {{ datecalendarEnd }}</strong
      >
    </div>
  `,
  styleUrls: ['./leaflet-map-index.component.scss'],
})
export class LeafletMapIndexComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('alert', { static: true }) alert: ElementRef;
  @ViewChild('iconplayer', { static: true }) iconplayer: ElementRef;
  @ViewChild('range', { static: true }) range: ElementRef;
  @ViewChild('pop', { static: true }) pop: ElementRef;
  ////
  messages: string[] = [];
  disable$ = new BehaviorSubject([]);
  public countlistpointsHistorique: any = 0;
  public datehistorique: any = '00-00-0000';
  public timehistorique: any;
  public pause: boolean = true;
  public timer: Array<any> = [];
  public countplayer = 0;
  public RightClickLat: any = 0;
  public RightClickLng: any = 0;
  public listpointsHistorique: any;
  public SelectedVehicule: vehiculeModel;
  public aprox: Array<any> = [];
  public listVehicules: any;
  public listConducteurs: Array<any> = [];
  public RechSHOW: any = false;
  public RechSHOW2: any = 0;
  public recherchlist: Array<any> = [];  
  public currentValue = null;

  public liveMode: any = false;
  public showsniper = true;
  public color: Array<string> = ['#ff2e96', '#6e00c2', '#0000c2', '#00c2c2', '#721d1d', '#00FF00', '#FFFF00'];
  public route = [];

  public activiteHistorique: any = '';

  routingDemo = null;
  thelastWay = [];
  map;
  markerClusterGroup: any;
  routingControl: any = null;
  TourneeRoutingContral: any = null;
  TourneeMarker = [];
  showAddress: boolean = false;
  address: any;

  subscription: Subscription;
  marker;
  public vehiculepayload: VehiculePayload = null;
  path: any = null;
  animatedMarker: any = null;
  Travelways: any[];
  HistoryMarker: any = [];
  datacalendarStart: string;
  datecalendarEnd: string;
  mrkAnimated: any = null;
  private _hubConnection: HubConnection | undefined;
  public aproxinfo: Array<any> = [];
  public infoendroit: Array<string> = [];
  public ob: Array<any> = [];
  public listVehiculesItem: Array<VehiculeModel> = [];
  public services: ConfService = null;
  public listv: VehiculeMarker[] = [];
  public isSPI: any = false;
  public inputdd: ModelDetailVehicule = new ModelDetailVehicule();
  public CurrentV: any;
  public pauseval = 0;
  public PolyObjStart: any;
  public PolyObj: any;
  public PolyOp: any;
  public PolyObjEnd: any;
  public Polymarker: any;
  heatRoute: any = null;
  btnHeatMap: any = null;
  BtnheaMapClick: boolean = false;
  heatMapArray: any[];
  heatMapMarker: any = [];
  AppParameters: any;
  constructor(
    private graphservice: GQLService,
    private mapService: mapService,
    private modalService: NgbModal,
    public configService: ConfigService,
    public service: ConfService,
    private mapFn: MapFunctionService
  ) {    
    this.AppParameters = this.configService.config;
    this.services = service;
  }

  async setcoord(coords: any) {
    if (coords) {
      if (this.PolyObjStart) {
        var newLatLng = new L.LatLng(Number(coords.lat), Number(coords.lng));
        this.PolyObjStart.setLatLng(newLatLng);
      } else {
        this.PolyObjStart = {
          lat: Number(coords.lat),
          lng: Number(coords.lng),
        };

        let IconStart = this.createNewMarker('start', true);
        this.PolyObjStart = L.marker([Number(coords.lat), Number(coords.lng)], { icon: IconStart })
          .addTo(this.map)
          .openPopup();
      }

      if (this.PolyObjEnd) this.setPoly();
    }
  }

  setPoly() {
    var menu = document.getElementById('menuC');
    menu.hidden = true;
    this.route.push(this.PolyObjStart, this.PolyObjEnd);
    this.DrawNewOneWay(this.route);
  }

  DrawNewOneWay(points: any[]) {
    if (this.routingDemo) {
      this.map.removeControl(this.routingDemo);
      this.routingDemo = null;
    }

    if (points.length > 0) {
      this.routingDemo = L.Routing.control({
        createMarker: function () {
          return null;
        },
        waypoints: [L.latLng(points[0]._latlng), L.latLng(points[1]._latlng)],
      }).addTo(this.map);

      this.route = [];
    } else {
      if (this.routingDemo != null) {
        this.map.removeControl(this.routingDemo);
        this.routingDemo = null;
      }
    }
  }

  async setcoord2(coords: any) {
    if (coords) {
      if (this.PolyObjEnd) {
        var newLatLng = new L.LatLng(Number(coords.lat), Number(coords.lng));
        this.PolyObjEnd.setLatLng(newLatLng);
      } else {
        this.PolyObjEnd = {
          lat: Number(coords.lat),
          lng: Number(coords.lng),
        };

        let IconStart = this.createNewMarker('start', true);
        this.PolyObjEnd = L.marker([Number(coords.lat), Number(coords.lng)], { icon: IconStart })
          .addTo(this.map)
          .openPopup();
      }

      if (this.PolyObjStart) this.setPoly();
    }
  }

  openModal(event) {
    const a = this.modalService.open(ModalComponent, { centered: true, size: 'xl' });
    a.componentInstance.lng = event.lng;
    a.componentInstance.lat = event.lat;
  }

  async NewLiveSetMarkerVehiculeToMap(t: VehiculePayload) {
    let type: string = 'V';
    if (t.vehiculeId == '00000000-0000-0000-0000-000000000000') {
      type = 'U';
      let a = false;
      t.vehiculeId == t.ressourceId;
    }
    if (t.evenements[0].geoLocalisation.position == null) {
      return;
    }
    let exist = false;
    this.listv.forEach(async (el) => {
      console.log('el ', el);
      if (el.id == t.vehiculeId) {
        exist = true;
        let dis = this.distance(
          el.marker._latlng.lat,
          el.marker._latlng.lng,
          t.evenements[0].geoLocalisation.position.latitude,
          t.evenements[0].geoLocalisation.position.longitude,
          'K'
        );
        if (dis < 0.02) {
          return;
        }
        var point = {
          lat: t.evenements[t.evenements.length - 1].geoLocalisation.position.latitude,
          lng: t.evenements[t.evenements.length - 1].geoLocalisation.position.longitude,
        };
        el.marker.setLatLng(L.latLng(point));
        if (this.CurrentV != null && this.CurrentV.id == el.id) {
          var type = '';
          this.listv.forEach((el) => {
            if (el.id == el.id) type = el.type;
          });
          if (type == 'V') this.isSPI = false;
          else this.isSPI = true;

          if (!this.liveMode) {
            // if (type == 'V') var lastpoint = await this.service.getlast5PointsByid(el.id);
            // else var lastpoint = await this.service.getlast5PointsUserByid(el.id);
            var lastpoint: any;
            if (type == 'V') {
              this.graphservice.getlast5points(el.id).then((data) => {
                lastpoint = this.graphservice.buildAllHisortiqueVehiculeLast5(data);
              });
            } else {
              lastpoint = await this.service.getlast5PointsUserByid(el.id);
            }

            let way = [];
            lastpoint.listHistorique.forEach((el) => {
              const lat = L.latLng(el.latitude, el.longitude);
              way.push(lat);
            });
            this.autoTracingRoute(way, '');
          }
        }
      }
    });
    //si la vehciule est existe deja change les valeurs du conducteur,activite,mouvement....
    if (exist) {
      var V = this.listVehiculesItem.find((x) => (x.idVehicule = t.vehiculeId));
      var CRecherhcheitem = this.recherchlist.find(
        (x: ConducteurRechercherItem) => (x.num = V.Conducteur.numCartConducteur)
      );
      var VRecherhcheitem = this.recherchlist.find((x: VehiculeRechercherItem) => (x.name = V.Immatriculation.Vrn));
      if (t.evenements[0].tachygraphe != null) {
        if (t.evenements[0].tachygraphe.conducteur != null) {
          if (t.evenements[0].tachygraphe.conducteur.numeroCarte != null) {
            V.Conducteur.numCartConducteur = t.evenements[0].tachygraphe.conducteur.numeroCarte;
            CRecherhcheitem.num = t.evenements[0].tachygraphe.conducteur.numeroCarte;
            if (t.evenements[0].tachygraphe.conducteur.activite != null) {
              V.Conducteur.Activite = t.evenements[0].tachygraphe.conducteur.activite;
              VRecherhcheitem.activite = t.evenements[0].tachygraphe.conducteur.activite;
            }
          }
        }
      }
    }
    if (!exist) {
      // this.SubNewVehicuel(t.vehiculeId);
    }
  }

  async initvehicuel() {
    this.listVehiculesItem = [];
    this.recherchlist = [];
    var a: any = await this.services.getPoints();
    this.showsniper = false;

    //partie vehicule
    await a.vehiculeRow.forEach(async (el) => {
     
      var Vehiculeitem = new VehiculeModel();
      Vehiculeitem.Conducteur = new ConducteurVehicule();
      Vehiculeitem.Position = new PositionVehicule();
      Vehiculeitem.Immatriculation = new Immatriculation();

      if (el.latitude != null) {
        Vehiculeitem.idVehicule = el.id;
        Vehiculeitem.Conducteur.numCartConducteur = el.numConducteur;
        Vehiculeitem.Position.Lat = Number(el.latitude);
        Vehiculeitem.Position.Lng = Number(el.longitude);
        Vehiculeitem.Immatriculation.Vin = el.vin;
        Vehiculeitem.Immatriculation.Vrn = el.vrn;
        Vehiculeitem.Position.DateReception = el.dateReception;
        Vehiculeitem.Conducteur.Activite = this.setActivite(el.lastActvite);
        this.listVehiculesItem.push(Vehiculeitem);
        this.NewSetMarkerVehiculeToMap(Vehiculeitem, 'V');
      }
    });

    //partie SPI or User
    await a.userSpiRow.forEach(async (el) => {
      var Vehiculeitem = new VehiculeModel();
      Vehiculeitem.Conducteur = new ConducteurVehicule();
      Vehiculeitem.Position = new PositionVehicule();
      Vehiculeitem.Immatriculation = new Immatriculation();

      Vehiculeitem.Position.DateReception = el.dateReception;
      Vehiculeitem.Conducteur.Activite = el.lastActvite;
      Vehiculeitem.idVehicule = el.id;
      Vehiculeitem.Position.Lat = Number(el.latitude);
      Vehiculeitem.Position.Lng = Number(el.longitude);
      Vehiculeitem.Immatriculation.Vin = el.nom;
      Vehiculeitem.Immatriculation.Vrn = el.prenom;
      Vehiculeitem.IsVehicule = false;
      this.listVehiculesItem.push(Vehiculeitem);
      this.NewSetMarkerVehiculeToMap(Vehiculeitem, 'U');
    });
  }
  colorr;
  adress = '';
  async NewSetMarkerVehiculeToMap(Vehicule: VehiculeModel, type) {
   
    if (Vehicule.Immatriculation.Vrn == null || '') {
      return;
    }

    if (Vehicule.Conducteur.numCartConducteur != null) {
      var itemC = new ConducteurRechercherItem();
      itemC = {
        num: Vehicule.Conducteur.numCartConducteur,
        type: 'Conducteur',
        name: Vehicule.Conducteur.numCartConducteur,
        activite: Vehicule.Conducteur.Activite,
      };

      this.recherchlist.push(itemC);
    }

    if (Vehicule.Immatriculation.Vrn != null) {
      var item = new VehiculeRechercherItem();
      
      item = {
        name: Vehicule.Immatriculation.Vrn,
        type: 'Vehicule',
        activite: Vehicule.Conducteur.Activite,
        isAct: Vehicule.Position.IsEnMouvement,
        vitesse: Vehicule.Position.Vitesse,
      };

      this.recherchlist.push(item);

    }

    var randomcolor;
    
    if (Vehicule.Conducteur.Activite == 2) {
      randomcolor = '#17a2b8';
    }

    if (Vehicule.Conducteur.Activite == 3) {
      randomcolor = '#fd7e14';
    }

    if (Vehicule.Conducteur.Activite == 4) {
      randomcolor = '#FF0000';
    }

    if (Vehicule.Conducteur.Activite == 5) {
      randomcolor = '#006400';
    }

    var ty = 'V';

    if (Vehicule.Immatriculation.Vrn == 'Krichen') ty = 'U';
    let Icon =
      'data:image/svg+xml;charset=UTF-8,' +
      encodeURIComponent(
        this.mapFn.replaceHoverIcon(Vehicule.Immatriculation.Vrn, randomcolor, new Date(1962, 6, 7), 'H', ty,  this.adress)
      );

    let iconn = L.icon({
      iconUrl: Icon,
      iconSize: [155, 60],
      iconAnchor: [15, 50],
    });

    let marker = L.marker(
      [Vehicule.Position.Lat, Vehicule.Position.Lng],
      { icon: iconn },
      {
        clickable: true,
      }
    ).on('click', (data) => {
      this.showsniper = true;
      this.NewClickMarker(Vehicule, type);
    });

    marker.on('mouseover', (data) => {
      let Icon;
      this.mapService
      .getAddress(Vehicule.Position.Lng, Vehicule.Position.Lat)
      .then((res) =>
        res.json().then((address) => {
          this.adress = address.address.LongLabel;
        })
      );
      if (ty == 'U') {
        Icon =
          'data:image/svg+xml;charset=UTF-8,' +
          encodeURIComponent(
            this.mapFn.replaceHoverIcon(Vehicule.Immatriculation.Vrn, randomcolor, new Date(1962, 6, 7), 'HOV', ty,  this.adress)
          );
      } else {
        Icon =
          'data:image/svg+xml;charset=UTF-8,' +
          encodeURIComponent(
            this.mapFn.replaceHoverIcon(Vehicule.Immatriculation.Vrn, randomcolor, new Date(1962, 6, 7), 'D', ty,  this.adress)
          );
      }

      let iconn = L.icon({
        iconUrl: Icon,
        iconSize: [355, 220],
        iconAnchor: [38, 135],
      });

      marker.setIcon(iconn);
    });

    marker.on('mouseout', (data) => {
      let Icon =
        'data:image/svg+xml;charset=UTF-8,' +
        encodeURIComponent(
          this.replaceHoverIcon(Vehicule.Immatriculation.Vrn, randomcolor, new Date(1962, 6, 7), 'H', ty)
        );
      let iconn = L.icon({
        iconUrl: Icon,
        iconSize: [155, 60],
        iconAnchor: [15, 50],
      });
      marker.setIcon(iconn);
    });

    var vehicule: VehiculeMarker = new VehiculeMarker();
    vehicule.id = Vehicule.idVehicule;
    vehicule.marker = marker;
    vehicule.data = Vehicule;
    vehicule.type = type;
    this.listv.push(vehicule);
    var mark: any = [];

    this.listv.forEach((element) => {
      mark.push(element.marker);
    });

    this.markerClusterGroup.addLayer(marker);
    this.map.addLayer(this.markerClusterGroup);
  }

  async NewClickMarker(event: VehiculeModel, type) {
    if (type == 'V') this.isSPI = false;
    else this.isSPI = true;

    this.closePop();
    this.inputdd.id = event.idVehicule;
    this.inputdd.vrn = event.Immatriculation.Vrn;
    this.inputdd.vin = event.Immatriculation.Vin;
    this.inputdd.activite = event.Conducteur.Activite;
    this.inputdd.positionX = event.Position.Lat;
    this.inputdd.positionY = event.Position.Lng;

    if (this.CurrentV == null) {
      this.CurrentV = event;
      var lastpoint: any;

      var lastpoint: any;
      if (type == 'V') {
        this.graphservice.getlast5points(event.idVehicule).then((data) => {
          lastpoint = this.graphservice.buildAllHisortiqueVehiculeLast5(data);
        });
      } else {
        lastpoint = await this.service.getlast5PointsUserByid(event.idVehicule);
      }

      let way = [];
      lastpoint.listHistorique.forEach((el) => {
        const lat = L.latLng(el.latitude, el.longitude);
        way.push(lat);
      });
      this.autoTracingRoute(way, this.inputdd.activite);
      this.RechSHOW = true;
      await new Promise((f) => setTimeout(f, 250));
      if (this.CurrentV != null) {
        this.aprox = [];
        this.listVehiculesItem.map((e) => {
          if (e.idVehicule != this.CurrentV.idVehicule) {
            var a = this.distance(
              this.CurrentV.Position.Lat,
              this.CurrentV.Position.Lng,
              e.Position.Lat,
              e.Position.Lng,
              'K'
            );

            if (a < 100) {
              this.aprox.push(e);
            }
          }
        });
      }
    } else if (this.CurrentV != event) {
      this.CurrentV = event;
      /*
      if (type == 'V') var lastpoint = await this.service.getlast5PointsByid(event.idVehicule);
      else var lastpoint = await this.service.getlast5PointsUserByid(event.idVehicule);
      */
      event.idVehicule;

      let way = [];
      lastpoint.listHistorique.forEach((el) => {
        const lat = L.latLng(el.latitude, el.longitude);
        way.push(lat);
      });
      this.autoTracingRoute(way, this.inputdd.activite);
      await new Promise((f) => setTimeout(f, 850));
      if (this.CurrentV != null) {
        this.aprox = [];
        this.listVehiculesItem.map((e) => {
          if (e.idVehicule != this.CurrentV.idVehicule) {
            var a = this.distance(
              this.CurrentV.Position.Lat,
              this.CurrentV.Position.Lng,
              e.Position.Lat,
              e.Position.Lng,
              'K'
            );
            if (a < 100) {
              this.aprox.push(e);
            }
          }
        });
      }
    } else {
      this.RechSHOW = false;
      this.showsniper = false;
      this.CurrentV = null;
    }
  }
  autoTracingRoute(ways, act) {
    if (this.colorr == '') this.colorr = '#354360';
    console.log('act', act);

    if (this.showsniper) {
      this.showsniper = false;
    }
    if (this.routingControl != null) {
      this.map.removeControl(this.routingControl);
      this.routingControl = null;
    }
    if (act == 0) {
      this.colorr = '#354360';
    }
    if (act == 2) {
      this.colorr = '#17a2b8';
    }
    if (act == 3) {
      this.colorr = '#fd7e14';
    }
    if (act == 4) {
      this.colorr = '#FF0000';
    }
    if (act == 5) {
      this.colorr = '#006400';
    }
    console.log('this.colorr', this.colorr);

    this.routingControl = L.polyline(ways, { className: 'my_polyline', color: '#51ce9d' }).addTo(this.map);
    this.map.fitBounds(this.routingControl.getBounds());
  }
  closePop() {
    this.pop.nativeElement.classList.remove('show');
    this.pop.nativeElement.style.zIndex = -1;
  }
  NewRechercheSelectedVehicule(event: any) {
    this.showsniper = true;
    this.isSPI = false;
    this.closePop();
    if (event == '') {
      this.CurrentV = null;
      this.RechSHOW = false;
    } else {
      this.RechSHOW = true;
      this.listVehiculesItem.map(async (e) => {
        if (e.Immatriculation.Vrn == event) {
          this.CurrentV = e;
          this.inputdd.id = e.idVehicule;
          var type = '';
          if (this.liveMode) {
            this.listv.forEach((el) => {
              if (e.idVehicule == el.id) type = el.type;
            });
            if (type == 'V') this.isSPI = false;
            else this.isSPI = true;
            /*
            if (type == 'V') var lastpoint = await this.service.getlast5PointsByid(e.idVehicule);
            else var lastpoint = await this.service.getlast5PointsUserByid(e.idVehicule);

            */

            var lastpoint: any;
            if (type == 'V') {
              this.graphservice.getlast5points(e.idVehicule).then((data) => {
                lastpoint = this.graphservice.buildAllHisortiqueVehiculeLast5(data);
              });
            } else {
              lastpoint = await this.service.getlast5PointsUserByid(e.idVehicule);
            }
            let way = [];
            lastpoint.listHistorique.forEach((el) => {
              const lat = L.latLng(el.latitude, el.longitude);
              way.push(lat);
            });
            this.autoTracingRoute(way, this.inputdd.activite);
          }

          this.inputdd.vrn = e.Immatriculation.Vrn;
          this.inputdd.activite = e.Conducteur.Activite;
          this.inputdd.positionX = e.Position.Lat;
          this.inputdd.positionY = e.Position.Lng;
          this.inputdd.vin = e.Immatriculation.Vin;
          this.inputdd.NomC = e.Conducteur.NomConducteur;
          return;
        }
      });
      this.listVehiculesItem.map(async (e) => {
        if (e.Conducteur.numCartConducteur != null && e.Conducteur.numCartConducteur == event) {
          this.CurrentV = e;
          this.inputdd.id = e.idVehicule;
          var type = '';
          this.listv.forEach((el) => {
            if (e.idVehicule == el.id) type = el.type;
          });
          if (type == 'V') this.isSPI = false;
          else this.isSPI = true;
          if (!this.liveMode) {
            /*
            if (type == 'V') var lastpoint = await this.service.getlast5PointsByid(e.idVehicule);
            else var lastpoint = await this.service.getlast5PointsUserByid(e.idVehicule);
            */

            var lastpoint: any;
            if (type == 'V') {
              this.graphservice.getlast5points(e.idVehicule).then((data) => {
                lastpoint = this.graphservice.buildAllHisortiqueVehiculeLast5(data);
              });
            } else {
              lastpoint = await this.service.getlast5PointsUserByid(e.idVehicule);
            }

            let way = [];
            lastpoint.listHistorique.forEach((el) => {
              const lat = L.latLng(el.latitude, el.longitude);
              way.push(lat);
            });

            this.autoTracingRoute(way, this.inputdd.activite);
          }

          this.inputdd.vrn = e.Immatriculation.Vrn;
          this.inputdd.activite = e.Conducteur.Activite;
          this.inputdd.positionX = e.Position.Lat;
          this.inputdd.positionY = e.Position.Lng;
          this.inputdd.vin = e.Immatriculation.Vin;
          this.inputdd.NomC = e.Conducteur.NomConducteur;
          return;
        }
      });
    }
    if (this.CurrentV != null) {
      var type = '';
      this.listv.forEach((el) => {
        if (this.CurrentV.idVehicule == el.id) {
          type = el.type;
        }
      });
      if (type == 'V') this.isSPI = false;
      else this.isSPI = true;
      this.aprox = [];
      this.listVehiculesItem.map((e) => {
        if (e.idVehicule != this.CurrentV.idVehicule) {
          var a = this.distance(
            this.CurrentV.Position.Lat,
            this.CurrentV.Position.Lng,
            e.Position.Lat,
            e.Position.Lng,
            'K'
          );
          if (a < 100) {
            if (e.Immatriculation.Vrn != null) this.aprox.push(e);
          }
        }
      });
    }
    if (!this.liveMode) {
      this.CurrentV = this.listVehiculesItem.find((x) => x.Immatriculation.Vrn === event);
      this.getHistoryVehichle(this.CurrentV.idVehicule, this.datacalendarStart, this.datecalendarEnd);
    }
  }
  ngOnInit(): void {
    this.markerClusterGroup = L.markerClusterGroup({ removeOutsideVisibleBounds: true });
    this.changemode('', true);

    navigator.geolocation.getCurrentPosition(
      (data) => {
        this.createMap(data.coords.latitude, data.coords.longitude);
      },
      (error) => {
        console.log(error);
      }
    );
    this.Hubconntection2();
  }

  Hubconntection2() {
    let url = this.AppParameters.appURl + '/TrackingHub';
    url.replace('//', '/');
    this._hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(url)
      .configureLogging(signalR.LogLevel.Information)
      .build();

    this._hubConnection.start().catch((err) => console.error('Bus Introuvable : ' + url));
    this._hubConnection.on('ReceiveMessage1', async (data) => {
      var t = JSON.parse(data);
      var mark = this.listv.find((x) => x.id == t.idvehciule);
      var lat = t.lat;
      var lng = t.lng;
      var newLatLng = new L.LatLng(lat, lng);
      if (mark != null) mark.marker.setLatLng(newLatLng);
      this.LiveSetMarkerVehiculeToMap(t);
      console.log(t);
      this.disable$.next(data);
      const received = `Received: ${data}`;
      this.messages.push(received);
    });
  }

  async LiveSetMarkerVehiculeToMap(t) {
    //#region Partie Personne

    var map = null;
    var type: string = 'V';
    if (this.liveMode) map = this.map;

    //#endregion

    //#region Partie Vehicules

    if (t.lat == null) {
      return;
    }

    var exist = false;
    var marker: any = null;

    // comparaison de des dernier point et mouvement de marker
    this.listv.forEach(async (el) => {
      if (el.id == t.idvehciule) {
        exist = true;
        var newLatLng = new L.LatLng(t.lat, t.lng);
        el.marker.setLatLng(newLatLng);
        marker = el.marker;
      }
    });

    //si la vehciule est existe deja change les valeurs du conducteur,activite,mouvement....
    if (exist) {
      var V = this.listVehiculesItem.find((x) => x.idVehicule == t.idvehciule);
      V.Conducteur.Activite = this.setActivite(this.getnumactivite(t.activite));

      this.recherchlist.find((x) => x.name == V.Immatriculation.Vrn).activite = this.setActivite(
        this.getnumactivite(t.activite)
      );

      this.UpdateMarkerIcon(marker, V);
    }

    // si une nouvelle vehicule
    if (!exist) {
      return;
    }

    //#endregion
  }

  getnumactivite(text): number {
    if (text == 'Attente') return 3;
    if (text == 'Coupure') return 2;
    if (text == 'Travail') return 4;
    if (text == 'Conduite') return 5;
    return 1;
  }

  UpdateMarkerIcon(marker, Vehicule) {
    var randomcolor = '#354360';

    if (Vehicule.Conducteur.Activite == 2) {
      randomcolor = '#17a2b8';
    }

    if (Vehicule.Conducteur.Activite == 3) {
      randomcolor = '#fd7e14';
    }

    if (Vehicule.Conducteur.Activite == 4) {
      randomcolor = '#FF0000';
    }

    if (Vehicule.Conducteur.Activite == 5) {
      randomcolor = '#006400';
    }

    var ty = 'V';
    if (Vehicule.Immatriculation.Vrn == 'Krichen') ty = 'U';

    let Icon =
      'data:image/svg+xml;charset=UTF-8,' +
      encodeURIComponent(
        this.replaceHoverIcon(Vehicule.Immatriculation.Vrn, randomcolor, new Date(1962, 6, 7), 'H', ty)
      );

    let iconn = L.icon({
      iconUrl: Icon,
      iconSize: [155, 60],
      iconAnchor: [15, 50],
    });

    let tempMarker = L.marker([Vehicule.Position.Lat, Vehicule.Position.Lng], { icon: iconn }, { clickable: true });

    marker.setIcon(iconn);
  }

  ngAfterViewInit(): void {}

  createMap(latitude, longitude) {
    const basemaps = {
      Map: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
      }),
      Satellite: L.tileLayer(
        'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
        {
          maxZoom: 19,
          attribution:
            '&copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community',
        }
      ),
    };
    let temp = L.OWM.temperature({
      showLegend: true,
      legendPosition: 'bottomright',
      opacity: 0.5,
      appId: 'c477e6a9fb7b3bbea69d074f28652e8b',
    });
    let city = L.OWM.current({
      intervall: 15,
      lang: 'fr',
    });
    this.map = L.map('map', {
      layers: [basemaps.Map],
      center: [latitude, longitude],
      zoom: 10,
      zoomControl: false,
    });
    let OwmMap = { 'OSM Standard': basemaps.Map };
    let overlayMaps = { Température: temp };
    L.control.layers(basemaps, overlayMaps, { position: 'bottomleft' }).addTo(this.map);
    // let pegmanControl = new L.Control.Pegman({
    //   position: 'bottomleft',
    //   theme: 'leaflet-pegman-v3-small',
    // });
    // pegmanControl.addTo(this.map);
    L.control.zoom({ position: 'bottomleft' }).addTo(this.map);

    L.control
      .fullscreen({
        position: 'bottomleft',
        title: 'Show me the fullscreen !',
        titleCancel: 'Exit fullscreen mode',
        content:
          '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" width="1.04em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1056 1024"><rect x="0" y="0" width="1056" height="1024" fill="none" stroke="none" /><path d="M702 383l275-273l-1 155q0 9 7 16.5t16 7.5h17q10-1 16.5-6.5t7.5-14.5V37l-.5-.5l-.5-.5l2-12q0-10-6-16q-7-7-16-7l-12 1h-1L777 1q-5 0-9.5 2T760 8t-5 7.5t-2 9.5v16q1 11 9 17.5t18 6.5h151L657 338q-10 9-10 22.5t10 22.5q17 18 39 5q3-3 6-5zM355 641L80 914l1-155q0-9-7-16.5T58 735H40q-4 1-8.5 2.5t-8 4t-5 6.5t-2.5 8v231l.5.5l.5.5l-2 12q0 5 1.5 9t4.5 7q7 7 16 7l12-1h1l230 1q10 0 17-7t7-17v-16q-1-11-9-17.5t-18-6.5H126l274-273q5-5 7.5-10.5t2.5-12t-2.5-12.5t-7.5-10q-6-6-14-8.5t-16.5 0T355 641zm685 346V756q-1-9-7.5-14.5T1016 735h-17q-9 0-16 7t-7 17l1 155l-275-273q-6-6-14.5-8.5t-16.5 0t-14 8.5q-10 9-10 22.5t10 22.5l274 273H780q-7 0-12.5 2.5T758 970t-5 13v16q0 7 3.5 12.5t8.5 8.5t12 3l229-1h1l12 1q9 0 16-7q6-6 6-16l-2-12h1v-1zM126 65h151q10 0 18-6.5t9-17.5V25q0-10-7-17t-17-7L50 2h-1L37 1q-9 0-16 7q-3 3-4.5 7T15 24l2 12h-.5l-.5 1v231q1 9 7.5 14.5T40 289h18q6 0 11.5-3.5t8.5-9t3-11.5l-1-155l275 273q9 10 22.5 10t23-9.5t9.5-23t-10-22.5z" fill="currentColor"/></svg>',
        forceSeparateButton: true,
        forcePseudoFullscreen: true,
        fullscreenElement: false,
      })
      .addTo(this.map);

    L.easyButton(
      '<span style="font-size: 2.5em;">&target;</span>',
      function (btn, map) {
        navigator.geolocation.getCurrentPosition((data) => {
          map.setView([data.coords.latitude, data.coords.longitude], 13);
          L.marker([data.coords.latitude, data.coords.longitude]).addTo(map).openPopup();
        });
      },
      { position: 'bottomleft' }
    ).addTo(this.map);
    this.map.on('contextmenu', (e) => {
      this.closePop();
      var width = Math.max(
        document.body.scrollWidth,
        document.documentElement.scrollWidth,
        document.body.offsetWidth,
        document.documentElement.offsetWidth,
        document.documentElement.clientWidth
      );
      var hieght = Math.max(
        document.body.scrollHeight,
        document.documentElement.scrollHeight,
        document.body.offsetHeight,
        document.documentElement.offsetHeight,
        document.documentElement.clientHeight
      );
      this.RightClickLat = e.latlng.lat;
      this.RightClickLng = e.latlng.lng;
      var menu = document.getElementById('menuC');
      menu.hidden = false;
      menu.style.top = e.layerPoint.y + 'px';
      menu.style.left = e.layerPoint.x + 'px';
      if (e.layerPoint.x > width - 250) {
        menu.style.left = width - 250 + 'px';
      }
      if (e.layerPoint.y > hieght - 170) {
        menu.style.top = hieght - 170 + 'px';
      }
    });

    this.map.on('click', (e) => {
      var menu = document.getElementById('menuC');
      menu.hidden = true;
      this.closePop();
    });
  }

  infocoords(e) {
    this.pop.nativeElement.classList.add('show');
    this.mapService
      .getAddress(e.lng, e.lat)
      .then((res) => res.json())
      .then(async (address) => {
        let Country = await this.mapService.getCountryInfo(address.address.CountryCode);
        this.getListOfVehiculePoximite(e.lat, e.lng);
        //this.closePop()
        this.pop.nativeElement.style.zIndex = 0;
        this.RechSHOW = false;
        this.infoendroit = [];
        this.infoendroit.push('List des adresses :');
        this.infoendroit.push(e.lat.toFixed(6) + ',' + e.lng.toFixed(6));
        this.infoendroit.push('');
        this.infoendroit.push(address.address.Match_addr);
        this.infoendroit.push('Pays :' + Country);
      });
  }

  GetVrnFromPlusInfoComponent(vrn) {
    this.closePop();
    this.NewRechercheSelectedVehicule(vrn);
  }

  DrawOneWay(points: any[]) {
    if (points.length > 0) {
      this.thelastWay = points;
      let ways = [];
      let IconStart = this.createNewMarker('start', true);
      let markerStart = L.marker([points[0].lat, points[0].lng], { icon: IconStart }).addTo(this.map).openPopup();
      let IconEnd = this.createNewMarker('end', true);
      let markerEnd = L.marker([points[1].lat, points[1].lng], { icon: IconEnd }).addTo(this.map).openPopup();

      points.forEach((P) => {
        const lat = L.latLng(P.lat, P.lng);
        ways.push(lat);
      });

      this.routingDemo = L.Routing.control({
        waypoints: ways,
        createMarker: (i, wp, n) => {
          switch (i) {
            case 0:
              return markerStart;
            case n - 1:
              return markerEnd;
          }
        },
      }).addTo(this.map);
      this.route = [];
    } else {
      if (this.routingDemo != null) {
        this.map.removeControl(this.routingDemo);
        this.routingDemo = null;
      }
    }
  }

  changemode(event, showlive?: boolean) {
    let sliderange = document.getElementById('sliderange');

    showlive ? (this.liveMode = showlive) : (this.liveMode = !event.target.checked);
    this.markerClusterGroup.clearLayers();

    if (this.mrkAnimated != null) {
      this.map.removeLayer(this.mrkAnimated);
      this.mrkAnimated = null;
    }

    if (this.routingControl != null) {
      this.map.removeControl(this.routingControl);
      this.routingControl = null;
    }

    if (this.path != null) {
      this.map.removeLayer(this.path);
      this.path = null;
    }

    if (this.HistoryMarker.length > 0) {
      this.HistoryMarker.forEach((marker) => {
        this.map.removeLayer(marker);
      });
    }

    if (this.HistoryMarker.length > 0) {
      this.HistoryMarker.forEach((marker) => {
        this.map.removeLayer(marker);
      });
    }
    if (this.TourneeRoutingContral != null) {
      this.map.removeControl(this.TourneeRoutingContral);
      this.TourneeRoutingContral = null;
    }

    if (this.TourneeMarker.length > 0) {
      this.TourneeMarker.forEach((marker) => {
        this.map.removeLayer(marker);
      });
      this.TourneeMarker = [];
    }

    if (this.heatMapMarker.length > 0) {
      this.heatMapMarker.forEach((Marker) => {
        this.map.removeLayer(Marker);
      });
      this.heatMapMarker = [];
    }

    if (this.heatRoute !== null) {
      this.map.removeControl(this.heatRoute);
      this.heatRoute = null;
    }

    if (this.liveMode) {
      sliderange.hidden = true;

      if (this.btnHeatMap !== null) {
        this.map.removeControl(this.btnHeatMap);
        this.btnHeatMap = null;
      }

      this.initvehicuel();
    } else {
      this.btnHeatMap = L.easyButton(
        '<span style="font-size: 2.5em;"><img src="../../assets/img/thermometerIcon.svg"></span>',
        (btn, map) => {
          this.BtnheaMapClick = !this.BtnheaMapClick;
          this.drowHeatMap(this.CurrentV);
        },
        { position: 'bottomleft' }
      ).addTo(this.map);
      sliderange.hidden = false;

      this.mapService.disconnect();
    }
  }

  OnSelectTournee(event) {
    if (event === null) {
      this.changemode('', true);
    } else {
      this.markerClusterGroup.clearLayers();

      if (this.TourneeRoutingContral != null) {
        this.map.removeControl(this.TourneeRoutingContral);
        this.TourneeRoutingContral = null;
      }

      if (this.TourneeMarker.length > 0) {
        this.TourneeMarker.forEach((marker) => {
          this.map.removeLayer(marker);
        });
        this.TourneeMarker = [];
      }

      let ways = [];

      event.enlevementLivraisons.forEach(async (element) => {
        if (element.isEnlevement) {
          if (element.expedition.latitude !== null && element.expedition.longitude !== null) {
            let Icon = this.createNewMarker('Enl', true, 'rgb(60, 119, 255)');
            let marker = L.marker([element.expedition.latitude, element.expedition.longitude], { icon: Icon }).addTo(
              this.map
            );
            const lat = L.latLng(element.expedition.latitude, element.expedition.longitude);
            ways.push(lat);
            this.TourneeMarker.push(marker);
          }
        } else {
          if (element.livraison.latitude !== null && element.livraison.longitude !== null) {
            let Icon = this.createNewMarker('Liv', true, 'rgb(91, 255, 91)');
            let marker = L.marker([element.livraison.latitude, element.livraison.longitude], { icon: Icon }).addTo(
              this.map
            );
            const lat = L.latLng(element.livraison.latitude, element.livraison.longitude);

            this.TourneeMarker.push(marker);
            ways.push(lat);
          }
        }
      });

      if (ways.length > 0) {
        this.TourneeRoutingContral = L.Routing.control({
          router: L.Routing.osrmv1({
            serviceUrl: `https://router.project-osrm.org/route/v1/`,
          }),
          waypoints: ways,
          plan: L.Routing.plan(ways, {
            createMarker: function (i, wp, n) {
              // switch (i) {
              //   case 0:
              //     return false;
              //   case n - 1:
              //     return marker;
              //   default:
              //     return false;
              // }
            },
          }),
          lineOptions: {
            styles: [
              { color: 'black', opacity: 0.15, weight: 9 },
              { color: 'white', opacity: 0.8, weight: 6 },
              { color: 'red', opacity: 1, weight: 2, dashArray: '7,12' },
            ],
            missingRouteStyles: [
              { color: 'black', opacity: 0.5, weight: 7 },
              { color: 'white', opacity: 0.6, weight: 4 },
              { color: 'gray', opacity: 0.8, weight: 2, dashArray: '7,12' },
            ],
            routeWhileDragging: true,
          },
          show: true,
          addWaypoints: false,
          autoRoute: true,
          routeWhileDragging: false,
          draggableWaypoints: false,
          useZoomParameter: false,
          showAlternatives: false,
        }).addTo(this.map);
      }
    }
  }

  replace(mat: any, color: any): any {
    var template = [
      '<svg xmlns="http://www.w3.org/2000/svg" width="200.389" height="40.842" viewBox="0 0 152.389 35"><g id="Group_17668" data-name="Group 17668" transform="translate(66.408 0.161)"><g id="Group_17448" data-name="Group 17448" transform="translate(-875.391 -442)"><g id="Rectangle_4" data-name="Rectangle 4" transform="translate(878.371 442)" fill="#fff" stroke="#354360" stroke-width="1"><rect width="83" height="15" rx="7.5" stroke="none"/><rect x="0.5" y="0.5" width="82" height="14" rx="7" fill="none"/></g><text id="AC123AB" transform="translate(915.371 453)" fill="#354360" font-size="9" font-family="Avenir-Medium, Avenir" font-weight="500"><tspan x="0" y="0">{{matricule}}</tspan></text><g id="Group_17584" data-name="Group 17584" transform="translate(-1958.619 3021.504)"><path id="Path_12471" data-name="Path 12471" d="M1355.795,2084.344h2.033a.4.4,0,0,0,.371-.546l-.36-.918a.408.408,0,0,0-.337-.255l-1.673-.139a.393.393,0,0,0-.43.395v1.069A.407.407,0,0,0,1355.795,2084.344Z" transform="translate(1510.592 -4656.992)" fill="none"/><path id="Path_12472" data-name="Path 12472" d="M1277.876,2073.552h.023v-.023a1.731,1.731,0,0,1,3.461,0v.023h4.82a1.731,1.731,0,0,1,3.461-.035.809.809,0,0,0,.662-.79l-.012-2.218a.809.809,0,0,0-.058-.291l-.894-2.23a.822.822,0,0,0-.674-.5l-2.439-.186a.794.794,0,0,0-.859.79v4h-.743v-5.1a.79.79,0,0,0-.79-.79h-5.946a.79.79,0,0,0-.79.79v5.761A.776.776,0,0,0,1277.876,2073.552Zm8.316-5.064a.394.394,0,0,1,.43-.4l1.672.139a.393.393,0,0,1,.337.256l.36.918a.4.4,0,0,1-.372.546h-2.033a.392.392,0,0,1-.395-.4Z" transform="translate(1579.8 -4642.6)" fill="#354360"/><path id="Path_12473" data-name="Path 12473" d="M1288.029,2120a.725.725,0,0,0,.151-.011,1,1,0,0,0,.267-.058,1.3,1.3,0,0,0,.36-.174,1.412,1.412,0,0,0,.616-1.127v-.023a1.363,1.363,0,0,0-.314-.883.747.747,0,0,0-.093-.1l-.012-.011a.756.756,0,0,0-.139-.116,1.573,1.573,0,0,0-.3-.174.414.414,0,0,0-.128-.046,1.822,1.822,0,0,0-.267-.058.621.621,0,0,0-.151-.011,1.247,1.247,0,0,0-.546.116,1.731,1.731,0,0,0-.348.209,1.553,1.553,0,0,0-.2.2,1.265,1.265,0,0,0-.256.476,1.819,1.819,0,0,0-.058.267.614.614,0,0,0-.012.151v.023A1.438,1.438,0,0,0,1288.029,2120Z" transform="translate(1571.401 -4687.676)" fill="#354360"/><path id="Path_12474" data-name="Path 12474" d="M1360.655,2119.96a2.059,2.059,0,0,0,.174-.36,1.807,1.807,0,0,0,.058-.267.61.61,0,0,0,.012-.151v-.035a.321.321,0,0,0-.012-.1.992.992,0,0,0-.058-.267,1.3,1.3,0,0,0-.174-.36,1.4,1.4,0,0,0-2.555.767v.023a1.438,1.438,0,0,0,.406.987,1.387,1.387,0,0,0,.987.406A1.464,1.464,0,0,0,1360.655,2119.96Z" transform="translate(1508.206 -4688.206)" fill="#354360"/></g></g><path id="Path_11171" data-name="Path 11171" d="M2719.464,1517.2c.242.512.508,1.024.774,1.512.145.243.266.488.411.731s.29.488.435.707.29.464.436.683c.29.463.58.878.894,1.317.217.317.435.609.677.9.145.2.29.39.435.561.024.024.024.049.048.049.121.17.266.317.387.488.145.17.29.342.411.512.1.122.218.244.314.366.024.049.073.073.1.122.121.146.266.293.387.439s.242.268.363.39.217.244.338.366a3.829,3.829,0,0,0,.314.317c.628.658,1.015,1.024,1.015,1.024a33.305,33.305,0,0,0,2.538-2.707c.363-.415.725-.878,1.112-1.366,2.683-3.414,5.85-8.584,5.85-13.876a9.658,9.658,0,0,0-.29-2.39,8.752,8.752,0,0,0-.459-1.341,5.26,5.26,0,0,0-.411-.829c-.072-.146-.145-.268-.217-.39a9.347,9.347,0,0,0-.8-1.122,10.153,10.153,0,0,0-1.644-1.585,6.734,6.734,0,0,0-.749-.512,10.539,10.539,0,0,0-1.668-.8c-.29-.1-.58-.2-.894-.268a8.864,8.864,0,0,0-1.4-.244c-.315-.024-.653-.049-.967-.049s-.653.025-.967.049a3.218,3.218,0,0,1-.484.049c-.315.049-.628.122-.919.2a8.539,8.539,0,0,0-1.329.464,5.147,5.147,0,0,0-.822.415,9.915,9.915,0,0,0-2.514,1.975c-.193.22-.387.463-.58.707-.1.122-.169.244-.266.366a8.358,8.358,0,0,0-.677,1.195,9.117,9.117,0,0,0-.362.854l-.146.439a10.064,10.064,0,0,0-.435,2.853,16.824,16.824,0,0,0,1.16,5.853A10.692,10.692,0,0,0,2719.464,1517.2Zm7.663-11.072a3.634,3.634,0,1,1-3.6,3.633A3.627,3.627,0,0,1,2727.127,1506.126Z" transform="translate(-2717.699 -1499.526)" fill="{{Color}}" stroke="#354360" stroke-width="1"/><g id="Rectangle_6101" data-name="Rectangle 6101" transform="translate(-66.408 -0.161)" fill="#fff" stroke="#707070" stroke-width="1" opacity="0.002"> <rect width="77" height="35" stroke="none"/><rect x="0.5" y="0.5" width="76" height="34" fill="none"/></g>   </g></svg>',
    ].join('\n');

    var templatePersonne = [
      `<svg id="Component_41_58" data-name="Component 41 – 58" xmlns="http://www.w3.org/2000/svg" width="200.389" height="40.842" viewBox="0 0 154.389 28.842">
      <g id="Group_17668" data-name="Group 17668" transform="translate(68.408)">
        <g id="Group_17448" data-name="Group 17448" transform="translate(-875.391 -442)">
          <g id="Rectangle_4" data-name="Rectangle 4" transform="translate(878.371 442)" fill="#fff" stroke="#354360" stroke-width="1">
            <rect width="83" height="15" rx="7.5" stroke="none"/>
            <rect x="0.5" y="0.5" width="82" height="14" rx="7" fill="none"/>
          </g>
          <text id="CA123AB" transform="translate(913.371 453.249)" fill="#354360" font-size="9" font-family="Avenir-Medium, Avenir" font-weight="500"><tspan x="0" y="0">{{matricule}}</tspan></text>
          <g id="Group_18890" data-name="Group 18890" transform="translate(897.507 445.249)">
            <path id="Path_14320" data-name="Path 14320" d="M2738.878,777.9a.842.842,0,0,1-.078.072A.842.842,0,0,0,2738.878,777.9Z" transform="translate(-2734.543 -773.165)" fill="#354360"/>
            <path id="Path_14321" data-name="Path 14321" d="M2736.44,780.6c-.013.007-.027.02-.04.026A.132.132,0,0,1,2736.44,780.6Z" transform="translate(-2732.298 -775.689)" fill="#354360"/>
            <path id="Path_14322" data-name="Path 14322" d="M2692.7,771.1a1.986,1.986,0,0,0,.15.235C2692.8,771.257,2692.746,771.179,2692.7,771.1Z" transform="translate(-2691.402 -766.807)" fill="#354360"/>
            <path id="Path_14323" data-name="Path 14323" d="M2695,774.7a1.934,1.934,0,0,0,1.559.783,1.978,1.978,0,0,0,1.011-.28,1.951,1.951,0,0,1-2.571-.5Z" transform="translate(-2693.552 -770.174)" fill="#354360"/>
            <path id="Path_14324" data-name="Path 14324" d="M2691.485,768.67a1.4,1.4,0,0,1-.084-.17C2691.427,768.552,2691.453,768.611,2691.485,768.67Z" transform="translate(-2690.188 -764.376)" fill="#354360"/>
            <path id="Path_14325" data-name="Path 14325" d="M2747.253,778.184h4.794a3.183,3.183,0,0,0-6.047-1.39A3.4,3.4,0,0,1,2747.253,778.184Z" transform="translate(-2741.346 -770.463)" fill="#354360"/>
            <path id="Path_14326" data-name="Path 14326" d="M2744.4,741.4a2.072,2.072,0,0,1,.163.339A2.072,2.072,0,0,0,2744.4,741.4Z" transform="translate(-2739.783 -739.036)" fill="#354360"/>
            <path id="Path_14327" data-name="Path 14327" d="M2744.563,767.1a2.069,2.069,0,0,1-.163.339A2.069,2.069,0,0,0,2744.563,767.1Z" transform="translate(-2739.783 -763.067)" fill="#354360"/>
            <path id="Path_14328" data-name="Path 14328" d="M2734.531,781l-.131.091Z" transform="translate(-2730.427 -776.063)" fill="#354360"/>
            <path id="Path_14329" data-name="Path 14329" d="M2737.118,779l-.117.1A.788.788,0,0,0,2737.118,779Z" transform="translate(-2732.86 -774.193)" fill="#354360"/>
            <path id="Path_14330" data-name="Path 14330" d="M2740.124,775.8c-.039.046-.079.091-.124.137A.862.862,0,0,0,2740.124,775.8Z" transform="translate(-2735.666 -771.201)" fill="#354360"/>
            <path id="Path_14331" data-name="Path 14331" d="M2742.011,773.5a1.265,1.265,0,0,1-.111.15A1.265,1.265,0,0,0,2742.011,773.5Z" transform="translate(-2737.444 -769.051)" fill="#354360"/>
            <path id="Path_14332" data-name="Path 14332" d="M2678.437,801.646a3.022,3.022,0,0,0-2.629-1.546,3.012,3.012,0,0,0-3.007,3.007h6.021A3,3,0,0,0,2678.437,801.646Z" transform="translate(-2672.801 -793.935)" fill="#354360"/>
            <circle id="Ellipse_306" data-name="Ellipse 306" cx="1.944" cy="1.944" r="1.944" transform="translate(5.574 0)" fill="#354360"/>
            <circle id="Ellipse_307" data-name="Ellipse 307" cx="1.944" cy="1.944" r="1.944" transform="translate(1.063 1.431)" fill="#354360"/>
          </g>
        </g>
        <path id="Path_11171" data-name="Path 11171" d="M2719.464,1517.2c.242.512.508,1.024.774,1.512.145.243.266.488.411.731s.29.488.435.707.29.464.436.683c.29.463.58.878.894,1.317.217.317.435.609.677.9.145.2.29.39.435.561.024.024.024.049.048.049.121.17.266.317.387.488.145.17.29.342.411.512.1.122.218.244.314.366.024.049.073.073.1.122.121.146.266.293.387.439s.242.268.363.39.217.244.338.366a3.829,3.829,0,0,0,.314.317c.628.658,1.015,1.024,1.015,1.024a33.305,33.305,0,0,0,2.538-2.707c.363-.415.725-.878,1.112-1.366,2.683-3.414,5.85-8.584,5.85-13.876a9.658,9.658,0,0,0-.29-2.39,8.752,8.752,0,0,0-.459-1.341,5.26,5.26,0,0,0-.411-.829c-.072-.146-.145-.268-.217-.39a9.347,9.347,0,0,0-.8-1.122,10.153,10.153,0,0,0-1.644-1.585,6.734,6.734,0,0,0-.749-.512,10.539,10.539,0,0,0-1.668-.8c-.29-.1-.58-.2-.894-.268a8.864,8.864,0,0,0-1.4-.244c-.315-.024-.653-.049-.967-.049s-.653.025-.967.049a3.218,3.218,0,0,1-.484.049c-.315.049-.628.122-.919.2a8.539,8.539,0,0,0-1.329.464,5.147,5.147,0,0,0-.822.415,9.915,9.915,0,0,0-2.514,1.975c-.193.22-.387.463-.58.707-.1.122-.169.244-.266.366a8.358,8.358,0,0,0-.677,1.195,9.117,9.117,0,0,0-.362.854l-.146.439a10.064,10.064,0,0,0-.435,2.853,16.824,16.824,0,0,0,1.16,5.853A10.692,10.692,0,0,0,2719.464,1517.2Zm7.663-11.072a3.634,3.634,0,1,1-3.6,3.633A3.627,3.627,0,0,1,2727.127,1506.126Z" transform="translate(-2717.699 -1499.526)" fill="{{Color}}" stroke="#354360" stroke-width="1"/>
      </g>
      <g id="Rectangle_6356" data-name="Rectangle 6356" transform="translate(0 0.249)" fill="#fff" stroke="#707070" stroke-width="1" opacity="0">
        <rect width="78" height="12" stroke="none"/>
        <rect x="0.5" y="0.5" width="77" height="11" fill="none"/>
      </g>
    </svg>
    `,
    ].join('\n');

    if (mat == 'Krichen') {
      var svg = templatePersonne.replace('{{matricule}}', mat);
      svg = svg.replace('{{Color}}', '#FFFFFF');
      return svg;
    }

    var svg = template.replace('{{matricule}}', mat);
    svg = svg.replace('{{Color}}', color);

    return svg;
  }

  replaceHoverIcon(mat, color, date, Hover, type) {
    if (date != '') date = formatDate(date, 'yyyy-MM-dd HH:mm', 'en_US');
    var hovervehicule = [
      `
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="252.5" height="179.187" viewBox="0 0 252.5 179.187">
<defs>
  <filter id="_Container_l_Color" x="24.5" y="0" width="228" height="179.187" filterUnits="userSpaceOnUse">
    <feOffset dy="3" input="SourceAlpha"/>
    <feGaussianBlur stdDeviation="3" result="blur"/>
    <feFlood flood-opacity="0.161"/>
    <feComposite operator="in" in2="blur"/>
    <feComposite in="SourceGraphic"/>
  </filter>
  <clipPath id="clip-path">
    <path id="Rectangle_6359" data-name="Rectangle 6359" d="M7,0H203a7,7,0,0,1,7,7V79a0,0,0,0,1,0,0H0a0,0,0,0,1,0,0V7A7,7,0,0,1,7,0Z" transform="translate(-19.625 -61.05)" fill="#fff" stroke="#707070" stroke-width="1"/>
  </clipPath>
</defs>
<g id="Component_446_18" data-name="Component 446 – 18" transform="translate(0.5 5.481)">
  <g id="Group_18987" data-name="Group 18987" transform="translate(41.721 41.5)">
    <g transform="matrix(1, 0, 0, 1, -42.22, -46.98)" filter="url(#_Container_l_Color)">
      <path id="_Container_l_Color-2" data-name="🔲🎨 Container l Color" d="M5.6,0H204.4c3.093,0,5.6,3.1,5.6,6.933V154.254c0,3.829-2.507,6.933-5.6,6.933H5.6c-3.093,0-5.6-3.1-5.6-6.933V6.933C0,3.1,2.507,0,5.6,0Z" transform="translate(33.5 6)" fill="rgba(0,0,0,0.7)"/>
    </g>
    <text id="_Label" data-name="✏️ Label" transform="translate(76.9 61.856)" fill="rgba(255,255,255,0.87)" font-size="16" font-family="Avenir-Heavy, Avenir" font-weight="800" letter-spacing="0.033em"><tspan x="-75.563" y="0">Véhicule: </tspan><tspan y="0" font-family="Avenir-Medium, Avenir" font-weight="500">{{matricule}}</tspan></text>
    <text id="_Label-2" data-name="✏️ Label" transform="translate(33.9 104.5)" fill="rgba(255,255,255,0.87)" font-size="12" font-family="Avenir-Medium, Avenir" font-weight="500" letter-spacing="0.033em"><tspan x="-33.156" y="0">{{date}}</tspan></text>
    <text id="_Label-3" data-name="✏️ Label" transform="translate(0.9 85.5)" fill="rgba(255,255,255,0.87)" font-size="12" font-family="Avenir-Medium, Avenir" font-weight="500" letter-spacing="0.033em"><tspan x="0" y="0">37800, France</tspan></text>
    <g id="Mask_Group_15204" data-name="Mask Group 15204" transform="translate(10.904 19.55)" clip-path="url(#clip-path)">
      <image id="Image_70" data-name="Image 70" width="267.453" height="149.774" transform="translate(-77.078 -77.05)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACoCAIAAADVSURYAAAMYWlDQ1BJQ0MgUHJvZmlsZQAASImVlwdYk0kTgPcrqSS0QASkhN5E6QSQEkKLICBVEJWQBBJKDAlBxY4ep+DZRRQreiqi6OkJyFkQsXsods9yWFA5OQ8LNlT+DQnoeX95/nme/fbN7OzszGS/sgDodPBlsjxUF4B8aaE8PiKENT41jUXqBAjAAQWgwIcvUMg4cXHRAMpg/3d5cx1aQ7niovL1z/H/KvpCkUIAAJIOOVOoEORDbgYALxHI5IUAEEOh3npqoUzFYsgGchgg5JkqzlbzMhVnqnnbgE1iPBdyIwBkGp8vzwZAuxXqWUWCbOhH+xFkV6lQIgVAxwByoEDMF0JOhDwiP3+KiudCdoD2Msg7IbMzv/KZ/Tf/mUP++fzsIVbnNSDkUIlClsef/n+W5n9Lfp5ycA072GhieWS8Kn9Yw5u5U6JUTIPcLc2MiVXVGvI7iVBddwBQqlgZmaS2R00FCi6sH2BCdhXyQ6Mgm0IOl+bFRGv0mVmScB5kuFvQaZJCXqJm7kKRIixB43O9fEp87CBnybkczdw6vnxgXZV9qzI3iaPxf1Ms4g36f10sTkyBTAUAoxZJkmMga0M2UOQmRKltMKtiMTdm0EaujFfFbwOZLZJGhKj9Y+lZ8vB4jb0sXzGYL1YqlvBiNFxZKE6MVNcH2yXgD8RvBLleJOUkDfoRKcZHD+YiFIWGqXPH2kTSJE2+2D1ZYUi8Zm6PLC9OY4+TRXkRKr0VZBNFUYJmLj66EG5OtX88WlYYl6iOE8/I4Y+JU8eDF4FowAWhgAWUsGWCKSAHSNq6G7rhL/VIOOADOcgGIuCi0QzOSBkYkcJrAigGf0ISAcXQvJCBUREogvpPQ1r11QVkDYwWDczIBY8h54MokAd/KwdmSYdWSwaPoEbyj9UFMNY82FRj/9RxoCZao1EO+mXpDFoSw4ihxEhiONERN8EDcX88Gl6DYXPH2bjvYLRf7AmPCe2EB4RrhA7CrcmSEvk3sYwFHdB/uCbjzK8zxu2gTy88BA+A3qFnnImbABfcE67DwYPgyl5Qy9XErcqd9W/yHMrgq5pr7CiuFJQyjBJMcfh2praTtteQF1VFv66POtbMoapyh0a+XZ/7VZ2FsI/61hJbiB3ATmPHsbPYYawBsLBjWCN2ATui4qE99GhgDw2uFj8QTy70I/nHenzNmqpKKlxrXbtcP2rGQKFoWqHqBuNOkU2XS7LFhSwOfAuIWDypYOQIlruruxsAqneK+jH1ijnwrkCY577oCpoB8C2DyuwvOr41AIceA8B480Vn/RLeHvBZf+SSQCkvUutw1YUAnwY68I4yBubAGjjAjNyBN/AHwSAMjAGxIBGkgkmwzmK4n+VgKpgJ5oFSUA6WgdVgHdgEtoKdYA/YDxrAYXAcnALnwSVwDdyG+6cTPAM94A3oQxCEhNARBmKMWCC2iDPijrCRQCQMiUbikVQkA8lGpIgSmYnMR8qRFcg6ZAtSg/yEHEKOI2eRduQWch/pQl4iH1AMpaEGqBlqh45C2SgHjUIT0YloNlqAFqML0CVoJVqN7kbr0ePoefQa2oE+Q3sxgGlhTMwSc8HYGBeLxdKwLEyOzcbKsAqsGqvDmuA/fQXrwLqx9zgRZ+As3AXu4Ug8CRfgBfhsfDG+Dt+J1+Ot+BX8Pt6DfybQCaYEZ4IfgUcYT8gmTCWUEioI2wkHCSfh3dRJeEMkEplEe6IPvBtTiTnEGcTFxA3EvcRmYjvxIbGXRCIZk5xJAaRYEp9USColrSXtJh0jXSZ1kt6RtcgWZHdyODmNLCWXkCvIu8hHyZfJT8h9FF2KLcWPEksRUqZTllK2UZooFymdlD6qHtWeGkBNpOZQ51ErqXXUk9Q71FdaWlpWWr5a47QkWnO1KrX2aZ3Ruq/1nqZPc6Jxaek0JW0JbQetmXaL9opOp9vRg+lp9EL6EnoN/QT9Hv2dNkN7pDZPW6g9R7tKu177svZzHYqOrQ5HZ5JOsU6FzgGdizrduhRdO12uLl93tm6V7iHdG7q9egw9N71YvXy9xXq79M7qPdUn6dvph+kL9Rfob9U/of+QgTGsGVyGgDGfsY1xktFpQDSwN+AZ5BiUG+wxaDPoMdQ39DRMNpxmWGV4xLCDiTHtmDxmHnMpcz/zOvPDMLNhnGGiYYuG1Q27POyt0XCjYCORUZnRXqNrRh+MWcZhxrnGy40bjO+a4CZOJuNMpppsNDlp0j3cYLj/cMHwsuH7h/9mipo6mcabzjDdanrBtNfM3CzCTGa21uyEWbc50zzYPMd8lflR8y4LhkWghcRilcUxiz9YhiwOK49VyWpl9ViaWkZaKi23WLZZ9lnZWyVZlVjttbprTbVmW2dZr7Juse6xsbAZazPTptbmN1uKLdtWbLvG9rTtWzt7uxS77+0a7J7aG9nz7Ivta+3vONAdghwKHKodrjoSHdmOuY4bHC85oU5eTmKnKqeLzqizt7PEeYNz+wjCCN8R0hHVI2640Fw4LkUutS73RzJHRo8sGdkw8vkom1Fpo5aPOj3qs6uXa57rNtfbbvpuY9xK3JrcXro7uQvcq9yvetA9wj3meDR6vPB09hR5bvS86cXwGuv1vVeL1ydvH2+5d513l4+NT4bPep8bbAN2HHsx+4wvwTfEd47vYd/3ft5+hX77/f7yd/HP9d/l/3S0/WjR6G2jHwZYBfADtgR0BLICMwI3B3YEWQbxg6qDHgRbBwuDtwc/4Thycji7Oc9DXEPkIQdD3nL9uLO4zaFYaERoWWhbmH5YUti6sHvhVuHZ4bXhPRFeETMimiMJkVGRyyNv8Mx4Al4Nr2eMz5hZY1qjaFEJUeuiHkQ7Rcujm8aiY8eMXTn2ToxtjDSmIRbE8mJXxt6Ns48riPtlHHFc3LiqcY/j3eJnxp9OYCRMTtiV8CYxJHFp4u0khyRlUkuyTnJ6ck3y25TQlBUpHeNHjZ81/nyqSaoktTGNlJactj2td0LYhNUTOtO90kvTr0+0nzht4tlJJpPyJh2ZrDOZP/lABiEjJWNXxkd+LL+a35vJy1yf2SPgCtYIngmDhauEXaIA0QrRk6yArBVZT7MDsldmd4mDxBXibglXsk7yIicyZ1PO29zY3B25/XkpeXvzyfkZ+Yek+tJcaesU8ynTprTLnGWlso4Cv4LVBT3yKPl2BaKYqGgsNIAf7xeUDsrvlPeLAouqit5NTZ56YJreNOm0C9Odpi+a/qQ4vPjHGfgMwYyWmZYz5828P4sza8tsZHbm7JY51nMWzOmcGzF35zzqvNx5v5a4lqwoeT0/ZX7TArMFcxc8/C7iu9pS7VJ56Y3v/b/ftBBfKFnYtshj0dpFn8uEZefKXcsryj8uFiw+94PbD5U/9C/JWtK21HvpxmXEZdJl15cHLd+5Qm9F8YqHK8eurF/FWlW26vXqyavPVnhWbFpDXaNc01EZXdm41mbtsrUf14nXXasKqdq73nT9ovVvNwg3XN4YvLFuk9mm8k0fNks239wSsaW+2q66Yitxa9HWx9uSt53+kf1jzXaT7eXbP+2Q7ujYGb+ztcanpmaX6a6ltWitsrZrd/ruS3tC9zTWudRt2cvcW74P7FPu++OnjJ+u74/a33KAfaDuZ9uf1x9kHCyrR+qn1/c0iBs6GlMb2w+NOdTS5N908JeRv+w4bHm46ojhkaVHqUcXHO0/Vnyst1nW3H08+/jDlsktt0+MP3G1dVxr28mok2dOhZ86cZpz+tiZgDOHz/qdPXSOfa7hvPf5+gteFw7+6vXrwTbvtvqLPhcbL/leamof3X70ctDl41dCr5y6yrt6/lrMtfbrSddv3ki/0XFTePPprbxbL34r+q3v9tw7hDtld3XvVtwzvVf9u+Pvezu8O47cD71/4UHCg9sPBQ+fPVI8+ti54DH9ccUTiyc1T92fHu4K77r0x4Q/Op/JnvV1l/6p9+f65w7Pf/4r+K8LPeN7Ol/IX/S/XPzK+NWO156vW3rjeu+9yX/T97bsnfG7ne/Z709/SPnwpG/qR9LHyk+On5o+R32+05/f3y/jy/kDnwIYbGhWFgAvdwBAT4XfDpfgMWGC+sw3IIj6nDpA4D+x+lw4IN4A7AgGIGkuANHwG2UjbLaQabBXfaonBgPUw2OoaUSR5eGu9kWDJx7Cu/7+V2YAkJoA+CTv7+/b0N//CZ5RsVsANBeoz5oqIcKzwWZtFZ1tm7sFfCPqc+hXOX7bA1UEnuDb/l9nToigcRAFuQAAADhlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAABLKADAAQAAAABAAAAqAAAAAB0kmUqAABAAElEQVR4AezdWZClx3Ug5rtW3bq1L11dvVd3oxsrAS4iSAojibIjrKGlmZBshyZiZIcerImRNA92+MEvfvSDHxxhT9ghT8yD5QjZYYU0psJWaDSjkSWaIgkCILEQILE2utH7Uvt6t6pb/vI/1T8uq6sbDbApQSIShez8T548efLkOZknl/+/xeef/3a3293e3t7Z2SkUCuUslEqldru9tbXVarUkZPX19dXrdfHAwMDq6urS0lKz2axUKoC1Wk1CVn9/P1Kbm5voDA4OAnY6HWijo6Nra2tXr1594403IE9NTR0+fBgd6Wq1qmpF1FIsFlUedSElCwQFWWKPEgKgXNVhUkUzMzOLi4tKeWw0GihAwN7Q0JAYPyMjIyAbGxuqm56eVpca8bOysoLDiYkJrCqu7CuvvILVJ554QsPViEJUFzRBFFQEvlaPjY2hoHVEhCbiKioUSuXKQGEnMbYnoLkHkh6LXVUXCkny9xky/PvEvRcamcgOar3xvco8uLyovTd+UO26G4/67m5Z4HnteeID5bOv0HqBOamotzdrDycVykFTI6gYqkDtaFsoHwQaTB1pOdWHIyGXVUQMKEGJEZEFQSJMNFoSVQwPD8/OztJU5ied7CmzLrkS2GJaVBm1nGlZAmrooC+LYYBEQZYgqIh54JmU8aksthkGM5ucnFREAo6CURYEKQyzopxbVRsjWBfeYtxZWFiQeOihhyQghwViIzHdbrM6VasRBXJAE07W8A9hTrd74iMUuV30k3//TkigEuYXcdhMxDSVNlMyeknDINC55eXlGPjD5CifEBNaWAtjIBZpaOCMDREBTVp74MCBMD/WwiQgwwxVRkcWM5AIM6PrwRWCiqMGE9AjnFB6bIQhQYAcnEcuO2EeakGWCcllaYImsElE1AUfQbng5kBAbAtGivX1deYHB5MCoBrxD0eWSjUKJGoPrtAxWQ5WavvPbMWusp+ETyRwpwR2pzsZocGZySTbo7XhuTEeCkrhaLxZ5datWzSS2lFxuqtgqCPFBQ9TFCtLdykxnxBlqs+EBNTC0pTySG3DnJBi7ewHQbUjHixJB034YZORG8VD+0HCHpRSF1JCEMczI4z5jfFwXPnS8CFgTxWCBDrQGGfUBYKTa9euXbx40XTK40UBMAQCh0AgC4ACIvjJpvGM4ifRJxL4MBJIXiXlDhVXUEKgppSP5Qgx8NNRiiuL2cCneWE80jQ4p8AGlBUzyFBTWapgDAIlRsR8wmKpMjQh7EcplAFRVosiMMWASImVZRgUHcEgDg2OdEySrE5ZzEAGR5nDqUVzc3MKWvuJb9y4YRwZHx+HAFmQUDWaJmcFtRo/bC+IaDt70/YjR44gBUcwu1rWcl+xpBbF0cEquFXt8kpyiT8Jn0jg/iVQ/s3f/Kc0SYHQJypFC+m3mHKbuHJLoOVyaTbFpdARYNJgqh+mQh1Dpz3at+DRyQ2fUBwUwjiRlWCZ1B2OIAECBzPBgBhXaIJE2WiYKiTAAdExTQVEjJNYBDInONhQBbLgSOGBXTFIowBkFKKxakdKLEul7FbbGZ5JO1gKBNSUUkQCcUMGZBCJ4GdjY1M+WXrcE6IVe4DZ4+6EvF/WjxEWnf5jrOCepKP23vie6A8g8+7y35/4X6d8yv/kn/wGDYsQ7ET1FIux0VoqDsJCQATTgvYIgGEqEh6ppuJUGQ69ZBuAlNV0wZKREmShFqXgMw8TC5woHvagrBA0wTGGLEhOXBb7h4waCmFd0uAqwgCauFXkypUr8/PzqpNlSlcRo/KoCdGoYC8YiDlWLs4RV4W2s2RwCKghDmgWleBjh0A0ChBv+MTJysrq4NCQx2xZyLr2/GU5H4+ITP4GGYnae+MfNzN06UNV8dcpn0rMALkR0kK80jm6BWgewD29pJEUHbI1FW0TcAlNiCK0E7LZj9XRdY/UFxHqLgFuejExUuvcs0UZHbWE3lNuCZCwPaqvirwW9hbU5MqKgmzg7bff5jHKwqHi8BHEquqgScNUY7AHyBmGoyGaKYuVSkDAvLaIWSCeGTC7PX36tCzIiqvi5s2bgCDWlgLIoUOHuKZ5vdmI8aH6+hPkTyRQKP/2b/8W5SOJUHf6FIGuG+zpMYWGQDXFcMAFCWi0MBSaNnsEFzM51NiMsopIANJUqswIxaG1CLJVbiEK5i6mC8KQJBTPa5RWRGBpVnRUX0Ip1qJS+NevX2cVEizk/PnzWFLktddes63CRMHha4K60JQrYIzLirGDBw/Kev3113EePBtQlJIGh2bG46B6VJ2YKw5BRYwZzywczwgqHo9jY+O8WnLi0fsz2ZjLs0PHdjxm00+aHsFvz5N/MzMSsWvRjy+ESPNaJCJEjdISvfGD4qS3luBBLIT25jVGInjIi9yZe/9c9RLZQ6c3K68x50duOgGjTKHonqWFwAglpo4MIyyKOcGkeXRRGoIEBES0UwUeg4g0TPCwKwlw06A4CCIirSIToES0VkUgAjqCBLISWIoslUrDZ1TosKUvfOELykqcPXtWLc8++6z928997nM2V2AyVJh4UAsDhhDMmw+1iB2CHz16FFocP3gMynLVJYTRskaGx/hNiajJdYQo15wJASdYxQaSjut3tpIXDRINjFEJTrTxk/gnSgJ0IEwjEvljrxB2D+sZDFSB6oSW0zZ44IpRI3CzgfEeDjjDoM20MJRPESHZTWY50tSU2okh0FRzSIaSHEUTl8Coogilp/oqEgDVpUYhSKkr4ysxhiVmI7ClYEOuXUrUmEdw+/TTT3OYTVaMU+2CWpiQBDaOHz/Oz0QB54GmOvOh0USuKsBRZpNKsS5ADLM6WfBBMEYI5mTMwJQrqBqdKGtAMs7Axz8REZL/Pa6vpxPOLCQBZtOgKJnuJ+HvtgToTK6uWupxT3t3pyNQeBHon0cxqwBJepQZm0fazxRRARFAYIqFZH+ZBcr1KEaBguZrSIpIiVmLOMwgKlUKBDIg4sqCQwYJdvEAgg22Cjl4CExonFv+JBt4+OGHzbRR6tKlS+ZGJsQXZUKM0DTIeFjgqVOnlEJQpWI2jKUTJ06wUsSZH/owJVgmTGMElkD4typFEBumTcWxBE1F6ChLINq7U0zmp2AEzCMIYf/gBP8TO9xfNH+noDSZJvTqc2/zyr/1W79JmSKE9gRquH8U0SNVE+TSSHAQaUWonUSQUwe1i0daS/MUCSMMu4oscOpLieELslBDB7WoN6wo0hA8wkGHAbBnaTYjgONE4EwCmvpAbJPYOOGIsky1QDNJWtSJTcUOJ6BBxgmDZ0tsT7341C5AlaoITRBtQQEwEOArLgZx8fWRRx7BDxz4WgQHBfgYtokMDiIgqDrmraA29sq9J/03MxmGhHvY+LEk81ryRFQTj73xg6p+T0U52Q8LzwveZ+Ie9PNm7knklMv/7J/9dqiamPbIoEkRU2tKJkiEYgVO2J44Am0Dp7jqEMK0opRHc5QsWsieI01ZWQ6dpqAow1FcSIWz4uoSpLEBJ2MhncjZ12FjqmNdSCmrLks1/iTDYBWxYxkjhYIww3iUUtwjwzOhoXzu3DlLRzMnr1LsYjdSmqNGZE2h4EwXxBzokVUbOFAGUbW2qC74B4k1ZxRvtnbdTnSCeVYqoQjidwl7/ZO7oD1IcIj3QVLcj1ZeS54IrHjsjfcr/VFgeyrKSXxYeF7wPhP3pi83ECLeQ3N3dzT0Xh7VCe2hcLQnHDO5NJiaMid6DC30D8UoKKaU4HCCgoRHpcxOzifeeecdFkLdqTIEBwPKwhHDCQuMIkyUNgOiGQTVJeBHrBZ2JaFIoMEEZIcCg0QZ2wxMLUixHxaLMnzUwq4AscSNxJKAskahqYgGSnhkhIojpWAYvCKmYiMChLBMaUyyQBRUgY20BN0xathVTpzyBohEg5wmGlWk7/gDjD/c/bWGfbXhgXOQ15Inoop47I0fVNV7KsrJflh4XvA+E3ejT0P2ZO15RD/dmPFPZNBCGhZ6FmaABAi9p53ShnNwqknFxPAVpH8CBHQiHTF8GmzG4/VZSrGQfJ3GMKSVRY2uh4WgJrAQQJDgPoAIQmZCclWtLDgI3mQhzjyYE+PBlblOE2BiGyae4YCwIhBkWa+pzyYNi33vvfeUgsx+BNxqiCJhloonGZXLKrWwRBwPGOayOp+0noSAWwRlKZgaXkiLQAiKYBIyKxUAkbojfDQjtGhX8H7iOyq8DSC928kf4795LXkiKovH3vhBMbGnopzsPeH3I8kPwDEf7dsjAe+NM2tLipLjp2suQtKeLEhjl6ZKiKmjICdwQk0jl1oDmgRosCK7KphhelSKdsIEp5ESlD6qyBddHqkmOnIVicCuPEbZYCZMVFrt6uLKsmcBTuzK8EUZFU4AzYfeWpSFOOMUzFqWhayL/YCrJWwGwSeffPLYsWNMRRU44e6yHKQsIBWXBmc/6lXKI4uVqzrIshA0YapXLiKAjLxYTtf9OLlaoS1GifX1TQ5pTJ5sGxyme34pb2enPhA3bLBzn6HrLcQ0p95PnEimrrwzqDoH4l86Yg3J4T9KQjP3LR61RFbwEPHd8PclAhh08lbkibvh34lwm8J2eqtTq5M8CWVb3AMpZZBSguwUe+GBn8WpL6yNsnTCDArRR5Z0+8ITn7d7ML2jhBsBA4nS7UBF8qbKlQ4x0bPwAyGCiEEgU0rFWVQQjCwFTQXwJQKByuZWDRgBciTUEhMIstKKKCsgIrDnPERFNJslYECCbSCCFEPl9zK8MA+5AoKMH5q0tyJgslhA9gbNhKYJUVZ16kVEQnUMRsCzQQREvcpyqj0CCh5ZKbbFMIfru7upwYxWRO0SUYUiQT8Q2q0PpfS3kfWfcD/x39Duq9YlDj/u4Q55xuiG7Vy2d0J6cx9EOp0T5upCKSOgnMPDQsSRiCUWbRagCaHrJhkqqHhYL2QQuWySztE8CUFCFuKMSm5ULc4DZLlJ8W8HWYqYXkLLVeGR7YlZhY3KCxcuuDeDskc02Q8j4aAqoixDYgbSEDxiD4TV8SdNj48++uiZM2eQQta0pnaMwVE5NHaL4Jtvvona6dOnT548CU2ayUHQQAUlYuBQhYnX3TwFo6WYVKkiIHiTABdHi9QluPmuio9PwNvHh5mfEE7S3VGaFK2lHxLRDdRFItJiODRGgqpRKRYYqpZmqGxRp6ygFILQpKMUTGXZj4TcmGHgKAVBiKrzNAQQxZmiKuh3hLBMcNrMqRNoPzqQQZgZg+E6QgA0y6HPSGSZ6+AAKsKZPH/+PFIwXXlhugryQpkKJhkhePATbbStGqwqGxBoAmMDVxfe2CSTJhw1Yt4Mib5caGoXS4PEKCDOIZosbfcqavwJj3NN+AmRA+XJW5rWTvEcZiAdxkZvcrlEljIS1E5MmUyJzAAySCDTNgEa3RKilJj60lQbG4yQ+pofmEdu5BAgx6OyaEZZlMM2qHKodUytYVrwGTayiLMcQJYQCz++KJthEphhmQiyf7kqslXL6hRkLYqAoAA5DFWLlFIdIaCvoLSEVWg0wTGjgphB1qyIT0QghGTC7OtDqXXRIhTkaggIZlCTBpFGJARlI1ji4xPI5IEw86DoPBBmPuZEkrrkAa9kJ+SKFY/goZe5iVImIX+ExgCiOIWjbaFwIFScyiLIVsUMRhZVVkSukCcizbwpKKUXJFQRcAqNMuSAIAKTRfFFXYJBFjIcufZmmDpLY/bul2EAUJa90JiWw39mllhS0ATIbtUCrgohSGFAQXFYFymBQ0PKBixDUlZAX5CLH0EV4vBCg1UEQVg7HPyHbEOAsrYTyb+Dobdb76d5HxY/ZHs/lD+eOL38v2+E1CJUnK7kRqgBAQyIx7AfOBGUkoBD48MwQGI+CYLUke7maNRObuioUnkIIugHWQk1KkjXg12qDxlQAgXaj4i5i3I7KmBIZjOPDJ7ZcCNtnCpuaABXqUecY+Oll1767Gc/69RERRDE2IbAblFmmaqWYEtMVO0ONuDY9VGdI37V8WmtJCGgJoDjJ9Iw5aKAZ8QjNhwI0SL8R3NgqlTV5kRZH59AyB8fZn5COElzC4WIIJ2rSG51AckfqXWM9IqEntEwqk9lYdJIcOolRHfmBKkpInJJFpD+SUTt4jw47oMDOWoJz03MhMCpvrrwoGqqz1qeeeYZd0e/853vmADZBkxVm5rgqMLkBg1QWkKNKCglEQSx7RHbYotDpVSNGY+qwwOyly9fZpxIyVU7+tJGAa0wFQdvIKqQhX7M4YojixpWQSDIBYSvoCDhESefhJ9wCSR9pSJ0kSBCjSSoCP0IRRGDUCYB0FQgwfDMNgrSRVONecYmR4hSFmOjfwKCVI0WCspCdnIATndzqwvLzB+ZCkyP4EjRfgF7ZiqTjLnO1GTWZSQgOHFTFLJSGMAqRbfjAsfcpSB8Me8Rq5BZ0VNPPYUO60IfWfjoiLXLI6C0hmBbjBPLSM6qG+FMET+qgHbkyBFVEB18Cc3RKE1DdrPZkoYZUoWPiCCB5xCRRwkQOIWd/deEGhXId4sDIUfDyd0wPxQcV/vi5xXtmwu4h5+gk5fKE9H2O4nQkDuBHwhBdg/D0XH3KBj4t0uF0DylVvfGOYV9gXnu/SSCwt3og6d9BUgaI4SZBeRu1EPzIIcraKFFy+kfA0u6lgVlETFpiGPPAz7lFqIKcS/9YAAkb3DiJisS3q84v1/GsJFVKWoqfeyxx+yFKms+/O53v8tCpL/4xS/qDCaBIOvCiT5mbJjhUjJFRZhimF/Uzh6QZcxxrhCP6sKGSYzVqQsRZWUhODs7iwFFWCDrUq+7cvC30sHv7nkpTqKle9oL/pFDxm0qHWxH/JGp/W0vqPkhimhIPH6sGpWztIfVXiZ3P2sLRFEERkSH8pK9qJGmeVSQ2tFOOkdlxULsHHqkrFQZBSoroCZABmQVYYdi1KIWWb3peFQEED/Mz8QC03RE0VkU+hZ1KLBDs7E5h73FmpB5GCOgmfpchYnmYMkRHyJRhPnhn8FgBilFUFCp3MBhcrIAOZb5jK0uZDETB/2QBVwpqBQGMCZIV/uT5YMLgUY4OAnp3X8cwrkDf1fnEM+z7oKZ53+8Eg+K2zvp3Am5s+WBk2NK9EryTvwHBclr3Jfg7pv18pgf1AhhNvsWoE90lBHSy0jTY66aNF1MBnc7sMCwVTFSFDSMUEX0EnK0X425IKSDjaDhUUG5gBZsilBrloCOLDHbE3IKvGITlE0XzARNtoQNxmbeBhHkms0UYWxYMjSwLoEJQQZntDGxKwVfvbIMMQzSQGBKNH/iPw0wmY+NQkY4+RFqabR2v77jMYxQAv+ktK88PxowmiwO+Xw0In8HSoUENCRP3K1ROUIk8se74T8oeG9Fd6t61whlC5SJ3jCAe3BgGqFVMa0pQr0UYQ8SVFMMiIIYkVA+ys1+FBFAAg4SOB7zBAYgRHEqDidHs/RizOqCr6KoCzI0aRbFeGJnBTUBKXTYj1mR/UBgPOZJFNgqThhzlIoBhTFrmpGFtSiFAgT02RWIWODNqkVjcYVOcIgfFECwAThYSR6sguIIEO4tUgh3BgzcCcxEBZ5aFwh5Yj/kjyPsLu360KzmdO5M3I3WHszs8f0J4G6lHgg8qs4Z2EMzHSLTIfoUpmJyYGN7kHofQ2UhS5hAqBedNr243BzGQ/9QyOuj2dDQp7t0FFyNgUCBgjJgpCUiBBxBBhAqzgbYBmphGCwKEZSvXLkCmXV5s956z6MsNiaX7XE7zXvMA524/oIHpBgYBEEV6MNE3NRnDsQAZtSijQKB2AfCPytVi7oUEcNBClllcSUBGamp6YO5BSIVhqoIlqJRDyRGOXgIbh8Izb+NREIOEd+b/xzn/ovcm+B95u6pV6mA5MoPkua9MCcPoV73tkO2RK0pdG5dsTZjD5SYutNvmodmIKhSFsMLzfaoluCgl5s8raBcISzWoywEzYSMhxkwiR/84AexJasuQJjQJGg/NEBMqj32LdmVhRwePCJryIDPJITAZ6im0Oeee85Q4tKZVqAgVhc69nuML9qlLDPDTMAZcAwrmgMCH1k4WgpHrnT0k8cQbDzeZ6zUfpjAuzNhnguS15UDP7aJu7Tro/C7h1Q83lsUgZMXzBMfpfqPVGbfGpN3R28M5GEboZ1asi82ZPbASnllNE9MOymxcwKb+K5Nnzx5kv6ZNMChIcJiFQktDLKAtF8c8soT0SjmhAKFFqI6EPhsDE3zFYNRhCdpCkrEt9uMp90sLa00bly/ev7Cuc8tfdpLJ2cfehiH7GDEEq5SWXKgl31jirWwTzRRUIXRBE3hK1/5iirw6ajDEpeBaYWYQQqynFXgR+3hsppytRcObkMgREeSTBAahsEjoZmR9nhn6MZtfUVuG13YbtGbMemNwR8KO16oKaaPJRpOk/RuyzASWn1n6JbS1j/iCFoJFG+/VBG1dVOR3Vp2jyzvcmRyJ+W7QFBDSRw15Dzlidvt/KHyu5X/EOz9h5xmUE4xHchqoUgOk3bjDELs++AjFphZLmZQyFi6Lf/3a/sxpDJuMbDbdolQ/qiqbCMDyHRBz6iQwZ7GUDvAzBCSv2oaYaWKMQN6Kc5tlbYpe+rUKYpIoU1T5iiqCV8paEwlLFwaWWVRRlNBHKApgAQ3Eh5lZeAEF2RhKTxDJqQiBE1rDKnaV3b+vbBwa2TQnc3B9ZXV8+fPHzww9elPf3plcamvv88HCdPnzorldqdNEZkUZ1VBDKCpIlugLtywOkcdzBKQiRpccAvI5LROixingcYlOMwohZk4lAdn6toFLi2rPlgvVcsMaGt7q9luNvzX9smL1kB9oOjleoajWQWGZN3Mr/W3vVM023sdLXuTLcsp7XSL291yd9v6uLyzXd7pVgpi3kR3m2kmA81slO6VCt1SsSsuFrKslNgqpbR4q9TtWDWXbYxF/nZxZ7u0s62uItKl7Z1SShSyv2LZm3UIm8PfX9BGR/T2UfRUHmMnezvSjWKfnOsfqPURqx7Why7JbnWa1UppoK7TSb2RaX3yvLZ0R9FenbtTvPQdjhNIpUxyVaradshqy7pQdKLlSwX5e3e9b1EmxrJxSxzpXQh9yewqk6cS2dWILM7fTkI5L0UTdFxoWsTRtByStzSH74Hkj6GrHqNsTjZHSBzuFyr0FRJWaBUVZGMeKSJrgY8cWkFFIownKotcceQ6HDfjmZrYHmuMOYQqM0iqSe5UHE3IQUdWFNwT44GNMV10YCoLU0G6jhQGmB9DYhsMst1pVgoOJHm8m9sb7Ko4e/wo59OLxhOnx0zO127cgnzs2InDB2cQLJRLXjTEPyLaiz4+HWYgZQLki8YYYTwyu9oINaawN4OR6tDRQMyYAPm0DBIzSOEQt1qRrLpS9kZnuW/XdyVGCKpAnwR6Wxp9AUJL2Acjyj67FqKOaSHS4qIJkHlEoOBpNqPCO96/yNQNisfMzhLBRC2pq//5/TTUe6alrEx5p5Bm5wShlSltEtklzJChpiLBCbz7CpovaLuWGngtJkgsfX81BFIumIlL7cSOfSuWmZqT1hf0nvyMPrYk+Ci2jqPVqVLCCbJJu1JRoEwaPxyjkwajTIa7sacMkvBT+nYcOAnXSLcn9vgAQmImC5HIH++HdLpIZW6hKFSNUkagr8SqPFo5OVk0Cb6ErJBUrkxhbx71AN2lxyDMKaQZk6p0EEzCvUtQKugHJwqC4JB5O6+nzcwDG/QbQjLLlotjAGlvSe7JkyfNY3hQl5jpOpQXqzcpSim9y7tkkFhexoMpjmkdPXrU3TRONSKmPivAgKtaK5gryvQFPgRsqFda1QgK0RbAhGZCy+6yJYayVygwjw40DMMUMJbHEiay5CsmWSctDBeJbZjcBAgg2E5dUUxW19cxb5n1EmZIMc1emTiBk2OWyu2GbVNiomNCKWi8OFPRjHQJhVQi8ZAsQIVZXT9M4Talu/6rORqY+MwuAOlxr6wYlTjtA/V+I28mgbSdDo2ULDVy4RQxlRkk0U1NpW9tdX04OUkkRJQYAxTdrfosN3ASisdozp3wu1F4sPCo98PSTNe16G5uh7SNHAkFOSITC4QSCeILUapGawVZUWWaZ7ILmWEzKKATqskYaCE7B4QvKHg3RiGri4OHjkRUpKDiDEDBMG/zmBoHhwZ0Nv3GFUtjS4hrixiysmZFE5qxFlpWb3JBtVeMZsz20LSaunA4mShMtbz77rvQEGRsVErDNQGOFikl1jpMqhdcLOyKrrvdXE3TLCKxcgYnZNWBqBe+EO0CMUX5VGmmZtlgT5NCNty5lN7pltlG6oOkjN1CJVvVpYJIAJFlyvDQM5Wkp+SAlrsML9msOIEytCySRoDdgSa3DhGzZUw3tyfHVOKDAg1JvGiRoTDEQjLEZa3uHCdrbwwDyQ51rn6ErEhSs7QsEHY1TQqFrGW7Q1U4k7t878dKaEjE8vNEnu6F7EfggcF6K4p0L+TOanpz09fQqIjDtJg0zAyUmLBCIlDJUSAdAZAyAQYcaVmZ1FLCI0GbRpBCQSBuxJUierlKISIBLi1xZ2BRSEGg1nKjRqUclNNpym0JBwHBmB6Zou5kGDi3L6v7dao0y1G1giCZkqS5aGV9jWmZWIOBnB+NCv5Nm7Ozs9KYN1tCM/GqOuiAa6AYTRANx4+AT2TxAF6rD1io1vuTqokrRchuuKdvgiStp+s9f9leSXIvk4XlIXO6kpjSYq+YVnrEBZhZidmM45hsKJlhSgFnpRlzAmdkdkdGpqVkJU1yiV5YohSs5NVmQHQUz4oRuMz4y7nJE1kl+dPthA7S8zGoSnMIyEfXbDbSB/gsFy2ZMznzJtKNBVffIaiUAAk51MkjG6ZZWkNugvEqaUrmWfjndm0/9G+uQpGIODXntmr1wnPgD5F40A+9Nd6bdi8/6folcRAfBeJ6KWmAJ4UgAVWrPEoQWZiELECBNEOgIGyGQMldQhaaAngg0F1aKB3AXg6iojxWu9xAC7JJfTN1R4Qdmqn4pa6wsMOh4brlmdcqVCpkLzls+r2WrBcT28oiJYE3rubVG9dNpOMTE2rRXkXQpDFibTfpMUJA5hpGKGHtpwr4mmZ2RRAyxjAJE2OZ5u1eFoVvgQOuFgiqUEoCqyGHEJo4KGQrnuQXZnrG6UwGkEwjcxQVpIWpCgJJupWEtKUr0qovTY9KSqcN06xHzHgMMcVopHRCqmzTeDWUwRhhUmxF/EXvwOHO6qZUFHmFUq/db0ifSMJm6lYGZmWuyRju60/fofTdcxssWb/bVE9bawYsomCqctPotb7rYpCYgiSpPdJ6BAMo806LNHF/G0xdEHxGojdOxbPcPTj3264Pj3dnRQGJvr43PYqbFMtmAzmSQihN6KWSocGhynJDUrk+qSBC1AFO0K50w+S5QUacqIlVABRwBo1VSO/LGRWP0TEQVC0oHtOaOdZMqxbLTsTbnaHB/rTdSgNAlpZWTF9O9pwfWOmZxBgtau4LyGO6Wucdi4MzM1SB1YWfiU/Gw0RVpDmAKLMiwxMe+KgQTK14xrC0BPkYCxBRJDQGY9hAv7mZ3qhgoMriUxFoMbL0tjdpPhNL/yXdF7JZK7MHFpcZS7KWQrJAONs0MtkKU+xaIgIlU82MlbsKM8GjmDhzb02d/k02nUGyjVH7rYglM9OzqJTQEtJyNFl7guyaasbTD0X7m4LetLniYNhQSCbkY8u6r5guAxq/CH9mJq3JVYdZMkkJLKRGJxvjcBrLFeEvKCuTSEkYLzBB+mppM+9uIRQ9Yjh7Ense70bkxwHPq74f4mlJQzTKaDxhhbXQaapDvkhAACQRj6mbbofUZ1mIahSn8dSO9GHG8ok2m7KSUmZn3EFKDFml+/KnI9UVvYWOXgy9t/uKCMthjczDHW77mW+8+YP5G9ecq9NywSpDEfamUm62WjCDoL04CcUznHSHE0usHVBL8cx0MawsoOrYoZZBxqotUxD8KOIxhhWY5sygjx+UZSkrgbjiMY4AhqESYDQWQm+gNZm9pFWZcwi/50TQ2VTH5WRsLIuZJktJ1pEWh6TmSCMRw6HiKQ5f04yYVoVU/P1YsXBp0vyWrNFflO5WYh0oRjlNg/4CN5no/Qc6wpy1lxiTI7DV6u/6oJZfKb/13nvnbdIcO3ZEhwpqIJn6mtEqLQhJMuuatNonk4Fa+pmtZKqZc0F0ZC5wOO7NjLKBcGdiD/zedB5IbvDQy0nWQfvQznHkpV/qpSIUiEopIE8MEkgBIR3ioHkUMcRHfRVhtIB0FIIiNhhNQdZs5hzUeHEcPPi2N2yQxIYK4aoVvoLBX28sS2/JQg0FMfzgIRjTYYL+Zo0g0wcPjAw5j6i5sAbCVcSnVcfjjz+uUnYyMZ6OFvzeGTpM9+ixY4lsdlKCOFKYx7BtGFsycSQIbjThGggSEJQVovmKSKhFljmfHEBwGKxqLK5UpLHgAkuGzMg1jTRYrLYToxhNWxiD9eG1lZVuu3FgdMSpS6nVqLSaw+ViqblZ7xZqxdJAsTzQ2SpuNtpLy+2F5WE58/OXX3+71t6+ce69xq2FYqM9f+Hixq2Fwmarr9MtbrSbi8udlfVqe3uoVKk5l9jxsZxCucLgfZGxb7BSaK4s+FjdaF/FT5+n4bDVsGB1gtDdau9sOempaDvXQEOwStpaRw7RzFxVlNOKxmZ6SXqzsa4X2B7T0i4J/aj3dMGwM9xB58PJyScNB7rGWJ8nX1xcGBlJ/sXc3LzNs7HRcaOnUcd7OMhyVQ2Pa2urI6O7N+/VfmcgVUBxniD/PAT83jHkeyBEbhDUIolcb5XKy0pkrX5/gRZoEQfbUUtvkbzevcNMb4EcqTeBrvowBIiihKA/9JMOM1/ZxqCLAplCBtRtcqUVgamUWsB7yebpcF2CJmQdLK0IhebZUoi8XjawuDQ/NTZirDUx6uMbN9JN0fA8abxSiht9TXgSwS2b1IC8FRijAcYIA4fqbFCZEukKfHWpVHPYZ4ws2EY2M88DCgposmoNjGlTEdXhU3FA+Kw0hENowYNHrIohtFuNje7OwbGRvnJha92vGW/WCjv9Ng1XNjsb65sN+6xpuvBOVQuQR1oqVuv9c0sLaqk88igFXV1c8wvI/bW6Pc7lpZWb7a4fR5TGG4fDoDo4VGtsbTqhbxFGoT04VOferS8tDhQrfQU7AiNO7JY3mtXhIX+rzfbY5Eytnk50oyMMbVg14pAY/jVHd4T0tIgJ1/qqXJK19RXHrdp77Zqvkrd9zYsUH3300YnJMZqACLvinmj4Mz/9M3hDCoT0iNdIpU+hiVEgQx6NhEqFXDHuTCjeC9zz2Jv1YNN7KorHHLjnMareA8yRIzcZoQYLkQhoPEZ6T5z1bvgtu0ZImkLoH62l0PrJo5hkqVrMaWGEgAKaoaMSGFJdxHvq6n1kWgrCjCGAZsSjrmWqeoteMktD76lTD2EjOh4D9Ca7WZ3OyhUB0f9BRxaDMRyYA4MTxDEmhM3QFZQZHgjb0ygFQx3RVxYbwTyVDQ/c50qpmuaAIEIyyqYpxcfzNzb41h4JBBGcLNyaOz4zs7k0X+mrDBR2bl678q3vPHfz3Lliu1VLDqg24ssVgKoLANW+WqGvstVf7vaVJ4eGbl26sD484mjStbTxianVjU1Lq0Kp2tream85RixZrQ70VTqrC+3W5naxs9lpNtob67Zrt7fXbs0VN9trCyscjk63tLq1ffqpJ554+guVWv/N69cOnTixurrMPPBpvWdKPHfubXM7QfkFdE3DuRMoF6i0xXWg0bFhSzvD3/p6WkrQEKk052+3awN96FgoX7hw4fLli7Leu3jeWp2XRD769MqVS9g077FkTU3ex8gg5lVho9U51Eg1/U7mPQJ+5EZ8D7QHlZVXtG+9eW5e3f1A0v5eFIiEOBI5lT0JikWBwgBUQAsFqgZCZWXpGIoLwiTMIWQdfATlKB40c7iE3HikwXKDBzFqgWym8qgXaQYbEyh9ozm1cPO6hYcsRsggIfumodoNzMEVTHosF321swcjLVaxjefgByaFoBmQWaZcqkPnpOmTgjDhe4RJP8x7CCoLX9WA6oUT1gUCxwyGW0Q0AZosaBJKRQJ8sL9vZ6NxdPb41sL8H//h//nsv/uzcrtxaGioZnHIVk1o6eYW37TSbRRbhUq7XLjVWl/YWH3ksSecrjU2R8xag6Nj1s4zM1OOxqsD9ebW9vLK+mJ63bnZ4ZF2twudJkI1CZ6wabKx2Vpe3VltlOcWh0cmdvpr2+3OQKcLYaXRPHrk6OLKyssvvzw7O2s9zHHQm1phAIo2Jn4ynSENmzEGB1tlgNxLQauHhwdhbnc7sSrxSAeyfkkvlJFJcgS20ueIGOrly5dPnjytU7gBiITfXuuvE1eE0ArF94TgIXJznDyxB/mBP9673j25as8ZyxO9LH3omRCV3pBXQKZ6yyP90z0kSGtptgSdo6CkRsoCHLYaiguIWsTgIVmJO0MsqEw+skIz1LK6tnz88MylS+/pex4jg/nUpz519uwjOjtGVhVJG69jMcZ49Lc3oHQ2ZKQ8UgtWZ9c0TAvPKIelgWMYh7hldYCQGRKaYXJhYCAaizeamkyrlk5ZlIVMJnJV5DppfyVtpSbdbTQ9yh0bGjnufcWrV37w4neuv/XmYHdrrN4/VNzmmi7cWOuvlgfxOzzqq4yjY+MDw2PFen+jr/zVf/3HlWLni1/60ujE5BpSvixe7etW+jo7ha1ipzY8cMhCuDExv7i4vrjUWeXwbne3HMM1XeW0NhuwL9ZX22jzmbuDXMJqcWOnYC+FrQvmwOu35p577tkXX/wOf9KHQjgXeGaQZEjO2qJFeko7yNA71N7lch7Im4B28+b15eV0RdFdXAIxZLFMLVWvRbpFoIJ6inCsAgjfJjbHdW7u5o0bN1WUkU1jB7VBBAJJqvEeIRA+EO0eFD5UVl7RvvXeCdyDr64cktf7oWdC2iZQwaBFaqnrshsq4MQdQyblM/6ZW/IqA1MciTBCuR4jDv6UQlzI6nl/TcgdYiTBt75XPCjbB9f9hm3dZnqjH/pY/wk0Hj+KqEJCkGsYdmxiihZAcIgs6/IzMmKVsqLgASaaYmgSAsywOmkFKRkNk+aBi8NQTV9eH1YR9hiwBIXDMH1CSrvIitOFpbf9rtNbb5/40hf/9A/+4Pm/+trBsaGf/vSnhis74/3VYwf4m3UH14WKy5ea4PpZt73dbe103QN8YnZ20ulmq73kB4mZfX3QN6qGJ6aSIzgwVCmV67UBn98aHay3DxxYX17ZWF/d2FhdXbnVaW82tzsV7ril8tpqc3V9brOzWSovV4tDdp2t92xzDQ+99Y1vatepU6eMR7/7u79LvD/7sz+Lf/LUUvJM40h2C4+geJLaEsOTn3tUStmbt67/5df+XxsEn/3sp8+cOWPifPbZK46OdCK5Kc6YUXDB3ngqYSluZYu4R9T4zYAqIr1SZf8jCmihANAikT+C3Bng3wn8aJC8on3r3ZMbVeyLmdde/o3f+M/xF4F+3E7elWMTAiFSJl0iICStFGDYBiAD0D30Ui7VpLthEtBwE4G6eww+IhFxzDy3sZIfKA0NPoKpV7IL0+rS8fxELypw3NCHZrVP45eXVwzDCnKTSsX0huHa2u6k57ft0eGO4hYm3hAxakgwY0pAb4zHqAXncDQEgplToBmKqwhxMzNWFZfWZHBpHDpGDzFGjILiYqVUCk3IdHfj2WefffWF5586duKpk7P//jNf/MwjZ4+Mj0yPDNUrO8315dWFW5tLC+tL80tzN+x2vHvp/Fvnz7177lzz1uJk30B3dWPlxq2b713qbjTLWzvbG81+i8BiqV6xLix2G+3GylpzZbWx2Rg5NFMYrvWN1quDA5VqyY7zTqu9sbjSXF4v+QWp/oGtasUV2/LUeGtoYKnTandLD509y5Zee+01vmJ4N8YLzTfoaKDGarJW6C/plZVlQE2jGDaDyNw6kPUSjt7WUjYM7jI9+TzzzDMI0jJzsb30W7dukqW0rWwx+aPMie1stfv6nXgN2TJ1/y2UZE+cK0+oR8R7cHofc/xe4I+S3lPvHvq9/PSm1RiPvfhponByxBri/ChPO2JK6R+OIZM+SZnAMlqM1nifjssp8MbGO9ZjExPuWKb3DKX1jalDMEZTyiie8yTRy0o8xhAYcLGySgnshCpLADJFbFDupvF0Pb28y+H19Qm/QfrNb37z+9//vhXd7ImT9YFBDBiYeZdshkLYlbBfhIjPUeFnYX7+lt9m8gsZ3t/t73eA4fc+5xcWvCpPmXxO2AyIAW4YzUBK1cnMsl9EpVVUh9HiBEFqhw1vEx87fEQTcAstKWtnq1ouGuCRmKhW+kqFhRtX5i69tzw/13zj1enG+vf//N/0NVuFrVZlZ7uy3dpurrc3VjrN9YL3gLbZRMdGS9rs769Z7w1U+y+//eaRQ4cvX7x04NBhs1+ruTF/tTkwMTHvrY7FSs3vgk9PD41NEMituVtXFheOl7a7tWqagoaGR4ZGffBjo3BlY2FzrXh9q7vlDEDDtlhOs3Go0jc2NV0eqF++coU9/Pqv/7pW/9mf/ZnzG04+wbIfPuQjjzxy9uxZpkUaDI+ETGscVTZDtPrEocuTn/q0mRMCma+uGi6dx/gKwfb58xfMhJThoYfOGPXQTyNUe9vAhxS7ZctG5sxxHQG5fuPq6Jg9s6Q5fytC6HbEGM4TeboXkreo/J/9+q8bEKma49qk2R3rt/TCmT1Fm9fkCO5UIp3yGrWYTalMM1wQIzzD4vAIt3Oor2bfrD43P3ft2k1wv7G5bZW/ssYM9Q/FRZ9GCqHEKhIQFLASWSB0136DV/IUSO+UtVsq94eax34Ldgs8mxla5xdw+/uG6kM2fxbmFhYXvLcxxjX2WcO5uXm73ngTu5QGgiwL0fcqsq20vrE+f/PW62+88d67530F8bGHHzkwfWBkcChx6WKCGxvuqcAspO0/8z4wnds1Y/aQ7aASikEhMZytBpko+jCvX7tRr9XPX7gg04fiBvqrVq7VrfZEtdSdv3H9pRcuP/v1+Re+XXr3jcObKzOba9de+t7G5atOV3yKY3Nufn1+IU1i643mRnur5WTPAs6WpKvi/TVXA7s7jmQMegcPTQ9PjfWPDQ1NTwwfOlCbGO2fHLFX2z86stHZnl9dmzw4U6rW3nj7zdGR9H2A/r56qVs1M7mFXquNTh6YWW02VtubIwcny8P1tXaDT3v46AmHDiMTk9RhZXmV3E8cn3368087a33rzbe169DMYQPzyy+9/Mbrb3oTcHRkVA9NjE86Omm3tkiVKMyNhjxXKuxJLS+zTCeRGBh878KlxcUVPpORZWbmEEeGuGJ2dX3dvFeppo+V6PPDhw/ZzGKNIyPD2TmqN7NdSzCepWFYvNNNN751A6g/upDHKXHbvcpV/N6J0MA7cWj+HuBtiEGc0u79y9B3gXk6T+T4d0JkpetgNd6IdXl2XShNE5ltpAWYQ3PTFzPIdkGBk7XY/Y4bTt47dTjQ3i5QL5PBzvbB6UM25BC0TjO20VGqb31CNUk880923VFkKHE0Ujqr8HZULKAgC5yKGyn1KzXCD4hBN+ZDxmyUHeivnTnz8FudN7IvjrYN0vrR3dGnnvoM+oZSIeiai4zBSIUpOhvodrZQmBpP7+ziUI0uQtIDfAYEkPemxnBfFQw2sISsUVzzsWF+ENDHnlDxy9i16vTRmarXpuZvDBSKs9MHxmp9i++ee+v5Z1/75tfXL10YaDWGCt2haqVerc1UBtOct5VtXPWlC3H91nV96cqrtx/TYrSWPMBqf3awUSnbrXQav+J0XPUGNZ+WGhutjo4dODa7uLq+ttG6dO3m5nrzxvVbU1MHfvU//tX3br2nwxqrG+n9XQdDpb6B4Yna4NDhs2fn1+Y/97nPrG2sL2+aqvq3Gi1j0KUL7xm3tIic7TYTwuzsbMz5v/d7v/fCCy84YCCKP/qjP+Ju/PRP/7RrfelcYXiYji45t51bjO6+cP4i4NioOXnLaScPdHLiwOTU+MMPPyQmPY2CEGqgj/SvA0UviBp+rIX1QtnWcCVdhCwVk7+WSTdJSe2kAvLjDrjqrWLPY2/Wj5iuTIyN6nJWwsE0pLkalczF/Yb1DedT3kETc0oZGTGkCSjbbyAI4iNfjn6MEHwwOmq6oMf6j8g8hpTR97cr7jRYuUO1Y+ceqSRZupTal/4MebYOYSrIg0JHb4HY3OPepPq3uzYe1C4AbmylUz73Yzzyl77+9a9//vOff+KJJ1gOqzB9UVRpNqMejKGcCGY/6KkK2mNTNBwkCmd4DpvXCt2sae1Gesue7wuugZlnm9Z1+GXAyAKiJqgrOWD91SWK1N89MHuo6xy+ucolGOsvrVw49wf/4nfmLryzfu3KSLEwMj7M/a3126jsm50+WKNyycSSlAS2Z/InojTmO9h0gGASaDW9OLlVKC8tLHuj0K0We/zVgZrX1ks7fcWt8uVzl/oGhsoWB52dEbdi2ttLV270VcpD9cENb/m76qGfijutTpptGt329OFDWy8WfbfDfkx1oLi22b50/p3BwzMOPAw2+lvr9S/Baqm+eOedd37xF3/Ros54x0ElSdL4/d//fTuozNLeqe4GTCaX/VTWyZMnU+dmYteP4DF4oSwBjqy+0x0hdhCPZChAUDtq3sGSsDlFJSEgog2MnJTQ/BFV/97Fk0pmIRL5471LfbTcitdaaRU1ih12rbUtUS/VrZrCulSv8bYpU6+kzcOa67YD2zWnxGzI0JVul/WVk7Ow3ebsOWzlRLXaDTE5JovLQtKB7JQCQbUQpXqF3kZCsBrRB0Rs9xJXEvpGhwWRND5kzq1HuczArSxEnn76aQO24dk+G22gE2EhiutjCHgU049QFJWyNHWhY39AEfyo16PGoqwiOO6pKKg3lA01EoNgCWXqghn0kcU5OOezWu+riisF32s4Pj5WW9/ovHfh+a9+deXN108Njxz8zGdHbSN523Wg34KvubYyaRBM41KBNFs2ZFaazNqbduwmvZJrnDKRJecjDX9bhf5GYaDUP2gniiaW24XVpbWttVanVD545GitqGv6V9226WyPj0w48l6zJ1VubhTarr4NDJqkK8m3KVaqO85mqlMz0/Ory+bVJT+x0S1tpvVY0fyWLoC20tsPxE5cBhpBkzWTNf7yL/8y8/ve97534cIFTT5//rztaDufTNEH74xr7MoRsbKIQCBJIrJmRgqCKjSWtEPChkhkpXVZ9HV0kDSFrJQdsaZNMhaoCwAV1wUeAf8aQq9yqs5jxv4Drrn8j/6T/2hzgz+/5tCn027po+TCDwwsew+FNrR2/3hg0m6699d4btYqyUU1L4lNTfRBmsWZncyO7hKT2Mba+sLCnN2E/np/X60vfWGFb1t2UThNue6IeDPO3oD1nkdZ5XQxxEG2H40woHJll1fXVmyUGQDWN7yy0G+TwiO1tH5Io2FfxVpi7saNQ4dm7AoYJaamJsEXFubPnHlodNRG6JZ9RXvocmdmDqJw44ZTrJUrV644RSRQfZ926G7eDNPSzfpYSOqe9TTjYMyhVXQCPl2EQBsYKg+KHYopnAmTwhHW6vqqG2etW3PM71B9uHr9xtoPXr/53Zf+vbOPff7E7Jmpg1M+fOONhrQF09xY9TbPmmto1+ZuXXXEtri0sLq22tnaJBeHPcXSVqlKUtt9Azv9A4X+erc2Uhqb2a6PdvoGGqXyZrnaqvTZSez21Zo7hYW19aGJycXVtes3b7pDMzA02Cl2y/VqMX32xf3w5PYyZ5fZquVSu7lu6bWwOO934daabccyXL0pI2Ch2OykXSV2oqXkIGg4COdCghyMWTwImzSkwR1lrkRHjNLsU0GmyOqEsEPyJC5BpyiCVLas2+E9X7t63WrDKnF0ZGxh3qy4QUvcL7UvavPW3h4JWyigHzMzlsgZBYn9TeFu8P2x7wrFc+RFIn+8G/m78nPXGt7PqEyO+iFbNuM3FYx3rv93Nlas2H2Lwc1+fGi/9bChkRtjAejORx8f1frJ0GT6rLQr5MF9ZyEUUV9tbK7JIru6q8vGYDs7GeNGL0FjxAQKGB0slta7goTtSq5dTH16F6Zx1NpSIjTDRK0j9bTc/mrfl770pRdffJFdxZLv8uXL586doyI0ABobowcoiNE3bLuJiRT2rHOM69B0MK5oT5CF5jEGXVN8QsjGXfAQmwQ9UBa+sgKlRFCR9eWlzfXl0tZ2pbs9OTFZsP3zjW8uvfFG30aD6V9PrvWm3ZtCtc83OdqE4cNGbsMUq92+aqGWLb99uapcdUe0Vax0drZdf2m0OhvtRjM5B53GTmm9e3OltbW8vrqwsrq0sbbaLficiws7VFL8H375Z2emD3E/Rhfn7a60thrTxw6MT09wMvkxvg5e2Oputlsrm+uD1fLM4aNvvfXG1IGDqQO9FT08wkR1rddG9VE0NnpKSymHloZlskM9Qv4+UOQ0yJToB+fCR4VGYrLYIWRdRvLwWaA+Wt9YfeyxRy059a9+N8BFN5Ger4oY7JSiRaxdWVn0QV84RFJc7SH5ZMM9ewrv6/KDTqk6SEYi4rsZ4Y9SeeX/+sM/rA/WRobHuJFDg640GITGvCy7srzmda+tTrdjo7zRJL7NDZrQTkN+9sU0/itfgpi8JlbxbS9XonzmiEJvdRrra4C2N2aPnzDWsxcjqTZQI8sBMZm6HU2s0gRKuISeJti+PoMl5wNp3Un2cn0A1EYBc9K1svxZiZqZdSTtP/fuO6bEowePcF+vXrvigrJp+H//P37vV37lV+ykPzr1CAp4pEYSipw//563nOy5nzp1CgNMGjN4k4ZjWmNg0gJ8CiSkmS67CApI1tDUGy6osrb4QtsoB3fg0Mh0dbs7Wa0MFQvf/PM/ee5P/s3A2rJxzu/RtBvOtS3ShquDwwPeb6j0deu1txauddKXLHY6Wzs+zLa6ubLBL9xqO2N3A4Y9WBHyisWdwrY7zm6sUA2Ldlu3Ptm/k7n8NgqNKbpg/JGHusUyR/etueuvfe/V9eWFYzMTT3/+c48//imXvH11bnCgzs7WmhtapO3eL2w1k3Lb/h7AMwG4wbOexikyASY6ZLU6EiSgmaREAmFCvHprAetwdvj888/bkyMTtuQIZ3Z29uTJk0gZGTmo+pr/n8mKj2rnycuZ7lQx5v7x8UUnGY4i/F6r8y1eiDWBRYlaFLcVH26IgS+GYMzonR9F7z+wbJgctEjkjx9Y8CMgVDZY22rhxuWr3E0dbh3n1lXfQG1ybNzeoEWGlYRNM3BjpEUF2yvXHHeVbBSQDd9SPh/Vf8srS5zHzcbGDdfqN9aOHz1+9rFHBtJnKJTjqrnh7yuA6aTcL61Iu74pNg2lbaE0K5S5/KvL6ca9nmZyZM0GqIKRUr/qRZZpvNQB+oMt6VGqoKsMBxr/5JNP6mxzmrWH4gZU0yNSYSrG2udfeO4H33/TRo6bpUYTxKEZLCRQQF+NqsMAsjqeTKiRXQVzqUqpIFLS2NAODFBlmApSDmmedxHv9tRL5cvXr/zx1762NH/jp06f5Clc29wcHT+E/DuL63MXLzXstxRK3kS84psbRb7WVtqL6Ox00jDjLXjTYT/n2CS5Xerb6Rvs9rO9klVit+w7hdYAXkxKC8XOTnptgg0v37z+xS984exnP7s8P7+5ura50Xjj4vnW8uL85fMHhganBkccI3nnof/QobT+dX282Uq3rbt+sLFRKVXtw/kw4+bMSmE07WYRLEvTKSF/EgbUTHEYgJIQiI7c9AiheZHlp37qp4yVfivylVdeOX36tCN+95BIW0ccPXrUTKisPdvMGdLzyQNCRB/xbLk/DFgV3Fr1kqdYvRucdiuK7Fcx1QKfOqldR3wE1YAkugAAQABJREFUdf94FqlsdVq6MK3o7KCk21Fb62sr3bXlTQv8ZmN9NZ318ZCGRoYn0vXFES+EjUyMH5o+NFivFbe6y2vL9g2bA9yp0sBgra/OK2u6C/f2m2+8+doPbAOcOHnSxoWZ1C5r+uBqMrz0BTBKsGWR0u4Uy6X+gbQPJH1z4QZOkvpnLygQGW1ISxHb38vLRE/jV7OrG0sLCxaxy07AJyYgw3Qowvx0p+63ZccOmR8dojcCOtIQPve5zzE8BgmorF6nDeyHTdIn1hXGD04DRifGAZ0lGu/hY0AwB1I7ni11oUCCEQF9KuVsUn0OT/uHB/7n//V/GXj49Pgjx7/5ygvV7taVixfbm4VTJ49+5tNPN1ab/+qP/9RA1l8fbVgs1/q7xf7VrZ2WD8r09RdLfTxVWyzpk6AWBdkhbSG9cWslvlUqM8Bi2hqyuUgjbZCmY5fKww+fOXZo5tq8d7tGFxeXvMv0j//TX3vi5Oxr/9/Xzx47fHz6IBGtr6501pbfee3SxNSUi5n2jw4dOszr07OdghdESgfHJ32WhwQYAFGIyVBBohNrIDMAFDwSCByQSHuUMFz+fBa+9rWvERRTYZDkrFPYISFbLBislGJXELQDNf1C2urSv8xPjxA7kerZ5aXdTtEFAh6CMQnV3X/I8YP5iKN4nu7NyvEjkT/S4N5Ke+BJJr0U4lGjAggzr0gWRcrppLvL6Nr14eWBRwyS3kdPuyAmKHTK3a20zWjjYX6x0Hz3nLwE9S2j7BtnM40ZVwR5ib73rKoD05OPPfKInjswOcFU7HBVa16Y6yaZZh8IJkdzEdWhVq3N5mLbzas0uR08MH3l0iV9Ay2UXqx7dK1ZTqU6KSYftestFAy9IAL2tDP5wLOzOjuzl67e1QTdad1ihOayPvH4px566CydQBaHtARX4OwKfXJRF3tmWojzA1F2YCCLoNBHiuNnWsYtZGwAoiNG0Kn6xNDwhXfPr9Qr//Af/eOJ8YH/4X/8765WSy+8cPHhs+NPf/bpqYmD51c2Xrnw7ubYeKU8MM/Fb2/1s8NSZaNSbHYNOi7GpHaV+2yzZtdYXXD2YeP6sKHKd6Pq3IbyTjrMcALv3QwnnwlSdPKZdo4Ne4WyDx43N9YGJyZH60NPnH2k0Fh/57Xv6/fDR2a8BN09MrO4tOIUwF0MR991x5KVtOVtxehCqS/1ptf9e0Kv6vSAPyD5a7/2a64u+cECL2QY8vSp/iJYqx77ZJaUgCzQ/BlDIZFSJ2JEF7JO0ddW3zqI9fJjiRpct5K8cRnCB3CwX3a0JW9RnoC7J2u/0h8A66V2J8F70Oedc55snZmlIs52xAuWCu3kitoM1COczmS69u47I+kaUaHjy5d8s82N5cWFWzeuv/tO/ZWXXoRMDwmLjGzXeFV7fXX50cc/5UYDNWVXXnYhO2oyOT66tpKuFyKVHMKRsemptBmDpnNLnyxxaYvSq1JB255Kes+N9F0wRnnLRq2VuuNH03HNm1ODOtKIbmzljvKbKBxUpsi09KUx5datG0aiL3/5yybJ6ekZXg0+Y8iHwBNE27iADg6pCKBWCODpOznZzEyOOFQWq3bk4xGTEkJiuLWzurI8Mzrhndzx6cPffP7rfRPTlQOHR08tLvcN/KtvPbdwZWV4tF7upPcHncyOHzk6XGmNTbibPpgu+TVaLsbgu2+g7tpKusAd57Rp29hY6CPcxebqpjuinWqp01d1uG5xmDIdqOmUZb9k3t9cWyqsNRYuXn3I67nd4rN/9Y0nTs3OHJz0srvff3v3rTdOnD49Oj52c37FmvvqzfmlhcXtTnqznhtUHh48diDNmYIW5XEG+HCRQwujFWkbPR3hOmY0XCJx/sKqBfnEhDe/00Gf82CLU7ZnSHcIr1sNQOPjEx4pjP53ZGJY1EG6khESfthqKM/98xTN6cXvhfSme3HuP51TiET+GBT2PO4h63OySdZiJ8RszVVQiw1D/LbFSDbXiZEwFWzt8B8dJ/hcZ9m2pGNfGzZmS+k0WNcHzaaczPnFuSuXitY4i/NL9i1r9f/7wIF0x9IcZRrh0VNr5kGgtJauW4FzVpuDG66KqYSWQxP0h6qVMm0yMKR06u2VfYkZGFmFlbVVsxxq0mZjXcgOOTMmxqDDfmwMWJAgZdHSbnVoAzMLfvBg7DDWKi5BEsYRCVXrePWqfSO7Cxq5uALHpybQA1UQDp4BjRd8gULD+eFYZ3vnd/75//SX3/rLv/8rv3Tt+uJqY8crdBNTR6emT056ha+5kz6Rv97uq5VbO4XhKWvXaTSdV1jm0TOLbPXoDGOjASUNeNlH2LxaP9I/UC7aYbHsNHZyWL2/xGkteGfC9thw1fcHllzmbq6sT1hC1OpTE5M4dCSTbvIMVBeW5g4eO7LR2ZqYnp5bWdvc6txaXtrptA5OjB6cOjA1fSD0I5QmYhCJmKMi935iwxmzIWom9Ku/+qu649VXX33zzTe9g2+GNPwZ4OyNaazGkTbfXkyG5CxLrIvluqoYHaEfCVxBXaBTLMjvh409OL0tiqwc4jHSvZA9xXOcO+EB2UNhD6k9uTmRCmvJH3oTWc/bZ1FwN6TcndLi3JwXTev9A343YMiysNSlMobj1eVFnzoY6Kv5RQEr70E/vTDetcHjdRWf4XrjB9/XDaRJd004JKiTGNWJY0f1blLrdmvR8ZrNU6Iwc7YbXodhD3zLwSG3Ey0SCvqvfTn9eD370Rkeb83deOzxx231GAt82mZtbcVG+cLCnNlpYWEGHCYlNg3qOC/a2J3l+NkiV7s+NgToWgywJb0e0x24NJb0tDfbeE1WjbRBETEjV7VYc7Lx+/0Dq0xFSrXBUmd95cj0+Fsvv/Lo8YeqjcJocejJ2ccdetaq9dmjsyxndXH5zbcuWhKVa+VmabPYV+wfSF+qZlxpVbCV3swwOtidoZfq4rabCDktFddH7Yf66JrINMmb91lfcfp0KRfGKjJ9TCkNCs2GnSIJ71q8/carb7z2xkCt8NCZw5MHpj719OcOHTvWqQwMlqtTx48t+4ayj83UKiZkE7STRCF1dU/cqxj3mVa1SQz/6BAsedpBNQdaExo0SfXa1RvuuJ1/9z2mqJet93ikrrYZjNbX0ov2EocODVAJciZ8A6VEGrWzc8v7ZCNHi+Z4vDORA/OsvNSdibvh5PBI5I9BYV9gTjzdyhM0L+JISFM1chQohMdMFdLtB46PoYiLOJD9Zq4tQ3tXDtbNb2zPPqoxurnVcRzrgJEquWmVbj0MDZsq7YtyLudv3OQC0TV7Ng+fOXPg4EEnEOXaAAfUrcFGp5V2YrOPbRE31WRI+lLsUWNYAiD2pMVf/epXGbbFhskNk6ZB5iHmzNBj85tStssD2RJlfMxyL31x3TiNLGrRUrG0lqqa0shVsD/bQsANypkw0mVRQUHLSAlFjBSKhFPa7nr3vdvpbr363Gulys6xY0fsbB2aOnzwyJFrN65/79XvNxYb9jL9EMvKuoONqp3n1bmbt5ZXRlfdjyN7k5xjQ0u17f7agGcQ1hWGrgrXIxRJDguvxULQX5oSkwFucikLpXQGuLFWMM1a51fLC6vLt1aWfvmXfvG/+C//6cHp8eXFue+88tLixnK1NV6p1nYs1cdHmoVtc2BzY0XZW0tzM0dPEA45CJGIGCsBvM+YTIQkw+y+ruLoGNd4K44WJcyKTjXYpAs30o4cdVmsCHQoX4Z3wxMJS0bE4KtqdHQEmd8nG3vQelsUWQGRvjOxp+wHPu6hkD/eraKcYMX2ZshXV0pEkE3Rs7HWyklOsooUStWRwRE+hg2PUHqw8N8orpAU5fa9E6Q8MoNE2bf3HNZZZdraS95u8dZi8vTOv/2OT1bPTB88PnvCroxryhYq1jn2CWYOTGPDt6uXHFhnxwBYSvdvUq9WhwaHRg8fcSnk5KlTRla3qCxC1BgmwbfBHiM0AOOQtShrTFW7ffOLFy97RIZzaxg2fMDBKuvV90GEdWGPEhiDmaAmhEwlYGqpIqgli7z9lbo06vgUfH/3xe89/zu/8y8WllaKY2ajkfOXL7557rwDhieeePILX/jC669//y/+8s+9AOHE4ObCYv3g4drQgXZxYL2TzukFSwPjkd8iYmk8TONewBMPpZ3OdpNhJuPMcvgdqYRfOPJuYG1gy0doLDYL3YGpkf6JIbfgFtZWWGOj02aQ7Z32zPHDZcPOUP2Go/7tkiHh5urKY48/vLbdqI8PuezLE+lVmj2alOvN/STIP5YJRB3i0h1GcDSJ11rx5MmTLPCtt95y1UYPClwkt/BtcbNDQZepSI8oGPpGCElEHynsacuexyC5L/B+assLRiJ/3FN2X3jl8qWrGiloXh4rab6IBqc2a7VpjpIVu63+jvMEppUOqZqt9camJaQTRZD5pfR6WLrlmJRx2xYnfbVdbovEm34+0eDeqTcQvOJkrzwTtG1An75esma7evWyV6LcQf38l366r5jex+U9WhfZKGXznEmvSnl3cHEx+WDSLuS4WcHkXOKZnJj4+S9/+eGzZ90tZorayTBWV1b0orW/6dtrSEbZq16Tmz70mc98xvCspUwI5vXrV63pLFltG2Vr1UGz6Pz8LQhUx9f6KE3aKeEX+LXdbOozI2w2GkePHLF+tWfKVgmHpnBSa/1DV6+8+43nvlXk3U1PHTl54p13L6YjhP7+sZHRs2dO/4Nf+vu/+Ev/gW8svfTKiy4Kjs9Mzp55tD6SvYnvAl9608r1pfRzabXagKSQ9sTSVe52+tp1sTswOsgU2WdyT5OTWjUf6ikd4S5O0cvz7ZYPKPqBM9cmVlcW++263Lz6nXOvugx3+Oihk4+cddlPQWrdLfZxdm0IuYy33mwtrG5UOt2hEy7fxO555pTGlkFSpR/aMt2jW3c+EjgPk7ERI2s0jBrOdKs2kphHWQZBanb69GmPOs6pj9UER9SwaN8LnFIZFmNDQRXo6BcJzKdJ4sMEkgz0SOSP+wKT2O8y8+8peJ8s7Kl0D30bM2mcpZE6RsWyjegkdf3GHPHRZo9MSyyt5VMH0nyiTJzyUYX0WcnBus5yf5n35QIqa3Y0PHPkMBapjrKr6yvNts9O+riwpcyOo32K1nIX1EZoqVAf4gfuNL2zsLzxp3/6J94ummK0WXxgyk7qmEuk7hnaFLp549aNm9fHxyaefOpTgwPpOoXXZ8bGJrA6OTb52MOPNdYbNuLaDb95MLi5tmlXQN9rjnfKaeql996z9e34+PTpU9rCvJ0mP/ftZ2XNzs6aFXWzURnDIBzOuYX5Jz/11IlTpzls3nrgbHsPuLG+URusu+HiKH+4PigG5yW693hgauJ733h5/cbqqy/84L/6b/7r5Vbj0tzlZ37uZ6rF8t/7/BcOTEy2NtMtudJ286HZYxvb7ZJrLIM1LfJGla1QUvVBap/+9tOCvAj+gx1ULyK725acUlOkw4n+PuOOQ33rR0eEbnn79jxDSj/FWK1bnHPZTYWO7N1wHdrpjG4uz3QaBwf7d8q1i+9eOGhBfWSgtbxWbrstlD7Plq5DrTurd5406RXE9uZGeWDQyQdl8JVFx/PcFr3sakUoXDICu3i3tW9XtW8/5v/yHokd3/oI0HDJ0iRCtoQvwaEAoVeCazc8Un6pbRtdIPBZ+K46CzIE+DlxWoo4NQRXhSy16DvDK+Qc7YcSt8/lgvOcf3Sg7QF6zt72SQT22E/6xvJ+ISe460nkWJnx36Z/GysztJyMT181abAZyZETuWtMts8XPyEQ54eWb1i19E97pF7udH86fbbZqqNbcFemk35dZ2uglr4NY0ZIJsoB9BaNj6Okn5z0Q5XKpSO4VrqAmuwc5tJSeqOMBKWJks0YGuvDg4vLS25OXLlyxaP1gJHSgGqzZGQ0nTe4yW1qso2KiP0gOBZ4Jkz2YuilTzxebzDqD8tOFqj29G57tcqSMaCuo4dnTpw45gADvvccH3vsEcd+KF+5cunVV1/hf5o/9b3RF5xYvMHs5iaWNCt8XTF/GEuGLWlCE9KopC2WvKubb3z31f/tX/7LGxsrV95+fbPVePpLn3/o5Cnf+XTXZXvTDyryzAtjoyPlViP2Xo4dOeQEOH25zDngzg7OzYEIMsKqE1qXGYr96StMXJLSzvryur2T9J2Kgs/N+6ffFq2XI7SuuFXwXsn60oqfpCv5ppNea2/1tRqtuVuFatF7T+4E1It9k8Pjw2MHFtbtfGw77Sh0ti0mUfd2heVsmvnSj5P6jdKkKQaD9JEqsGxKTBvot+dEHhCvh1rl+gYtD6G7JCMhBs8TOU5vwvRIpPwUK0aLHVOiwfQv/uIvnPVzXG1rc50IPIjodxwjG+YXFqhzo5Zesj9iOloR8QMnnvNmv4GJGDTTBoBWZcOJ7x35eKHZn7Uk4xHjwx1Mg5HxLJOp4cc5nmGs3OBd2Zdcce0rfTuCSvikLHeR84mItyqC+2gJe6C49h9ZHTrmWGV948WmHhEnY/Xl2+VVXULQjc3m3K15zgk3xonTzEHvSxy23rOQePV7rxkO7Mfo4vSWji+XjXmzfvfLF1Z3ius2G7CqYGPqNddZeSrO7VEpY0M5mS4XOus/yLgyBn/729924YY7ZG/Sl9GmporWKogzUXFs7YYSGHdBqEIaehzuN5r/8Cu/8LV/+6//wS/8wn/7z//76dHRIz4xNlRvNnzKt0nX2aHP4mqdzSryrhRKy3OL3ngwFxGzmSxJYKvLcYNjCjIjM1kj2ZbFYTrKI94a3ServmK16I4D59UqYbujvVVfl9ho+iC3655s3lsqra3t6QOHfPfDF9uMfteX1m/eWBg/1hpxHc4HFFtbfY45Olsm21qpbAw2SibDux10WdZrEd+G3t+/ypBMVny3wJ7HPWT0hSbD11O6VW+Sv7HVXprTKR+80SNuOx0/fpycYUKLudFQrtd0h340QO8h+5EfezkPIndC7of4/ZRKEzpauNcGYhI8xpyuPO0E0eXiUDsWJJ1lpRcrqQw99pX3zKgMmSSz+0Ujl40VZ+NRPGVktwHTNtewi4/pxlNSuY7DbidiacjlZwEihSuBuFVhicaKbKPRMxOjjWxzI1IesWolaVzg2OgMgfNpQGUtcADtiyKIc3SMrG+fe0dfoqa4shoSrWaQliIx/XoDQ1p1Cj7++KfcX0E2WVgWghobNpRokUdEBAlCay0tf+2P/x8D21/9xb97+flvP/7Fp900v3jh/KOPP5aubrrO4ie7B9Pr5IJ5zURyeHzalkqh4jJ2+oyik8YhLxv6lJNJKXsBxfUVgjNKpF+6LnQnBse0R3U+6WTLKWm6ebhc7C+V+7rFVnvL2ZGvTXX4NWnyKT782Kc3Wt2FK77Nvd1s8ei8Pzzg82uupTK/evZGWcUS1LtLxYL1pKqypWiSkP/TP/lzShFbes7ivZ5XyvugEGXvxCI9Wy8kQDIkzQmKY2GLQ33BNXXzhqfqoOKpp54yW+oUpHSrXjYT6g6B5iRdfHAh5zZPPDja71NKbowKNF5Cd2bqkSZ6goiKk7pkATDDdIWXu5nWkEqFAYgZJz+Fj8AT5dnKpaOCI0BThYLhM1Qq3mpJ9+KZPWuJWiCn7cDsjqJZkvkpiCYLhYOosigYF1kXKeshFghC7q4ZsupY+DFXOCgzKr6o/pPgScZBMGT6FZasrvPnz8Nh1cZd9J955hm7rMpCQzCWK3rdQtTErlKssl4DdgQQAsEhbQjpYdsHyV5+4QWHiTcuXR4ddJ2nb2ig5gru+NTk4px75/YWbFqtpmNNV8A7jKpcM0ptd+oOJ9Z9kPuWT42cOHvchYe6S6SMMM4Dsz0YfcN5vLWQvh+hFemOqa1rtuglFd80TV5kx1tb/kl7kO4OeCfdl8gbzWI7ff6023FpvMHJ6LT5tGtpD3u7MOAYt1wl67otaRuy9oSy+z9alMaALHxkI1SaiIg6wp7H2+Ddf8MCdbemGRn1vn4XG0xdbWN1OpEpcoJIXsIC3r4OZ0fT9ALfR9BHwh7KH+0Rt1HwzsSHIpgX31OqVzK+gbW7ic8SZBA+AxBTKTESYYQepWXZ2ACheZRVg8WBT9cVB6fZkGV5zMrufg6Eu+QaZiiQUplxmjnT+7vSXt8UrItskNr2IVaM6RL4qIWuh0FyRRibPkOco2rZZiFh2SDXuGg5wVBlMRhzJn7A9aIRVKnvvvQinLB/dmvg1Eakku9XKJgDtcWOOc7jnvfbb5/b2OR+l5SNnkaWEqBpJz36WxyCUooeqPSFt99UBePi2RrWGQqD89rI0MT4UDYL1YfH57Nf5x6uj0Eb8JWlwcFr6bO5yydGRx86ddpW7ZVLl7miycjS5moSQrqhZhXqmNTZpJfzsxW1AdJ9UXOiN/m3fJ+lxU/zc6jb6R3t1oZVQbvcv9hscBhuLq74IFx1fMJXMNY5Pu1t18D7qjVLaNsy5lJvo+mz29NeUh7/p3+yf/fo0Ac+ZgX3wbobnHhj4E7Lk+ztFjIx3oHrFJZph8aShBHavBGbHolal+kIp4swKQyjRWSfWrMJZl/4vYHB7d14vnfZ+8+1/rf7xi2yBPfn5DftwpkT0x0yp+1pr6Vrl8U5nx1y4mAVHEgv2fu4E1ui7vYGav2FwZn0+W0igEAvqcQG/c3u4GoDeLKHbPdFrlLo2pP3R3YKAhrTbRZ6VdWYnsgmP7nE5p3xshyTp9kJHRPX0aPHT5xIvwFM6cVxMmHIxI+6MgtPH37WVWL2gz4vVId9+ctf1gRzBxxbL4rwPAVEpFlj9DciNgb0q7l0eWX10qUrJklGa3uAMUNGM4aYiJUiKDxjyGRoux/3fsJieXWj3Nc/+9CZAzOH/Nqlv0vzCzevXqsMDdZaowNbheH6kAMFXzvc3Fx548LbXkC6unhz7NDU2xfPGRfSVJQ+N2MtmIYzdUk88eSTpf5KyXcJ+Jy+d+GTiK4F7JSNBC4Xuol59OCk71nPzd3waRkvz15bXXnrxZe32o3l7s7swYmDZx5ebTW/9fx3Tz32uI9sEHJ9aNB7Z5Mjg346gni1XQgFup1+HxLw+4mVwXBOSpE9j3uI6H9DGxzC1DtGMdYIqF+kdb2xlQrxXH7u536OZep0Xo+jRYOmWbHXFPdQ/siPvcwjEo+pFz5M2EMkL9pLJ7WcAmkVRZehVwSdoeUgJBJyCT2DxhV09Z9QpGPEQhc+OhJ0heAUiclBKV4RTFkkyN6YDSDKJkzFZTFUuYY6BSWsBJwNQohHFSGFJZja49FAyEsECbeQ5cBUr8NG5hHT1MmTJ3UhszF8MiRlTdTKeinZtGkBZ0LjjvJnsGFApfFiaD5Rgx9Ojo2BjL2ubRgHkkZlBHlEGJOO7YEYbrQ9eMOnm//emj965vR7V66m47hS0V2il1/63rde+O7E1IEXv/vyt7/xrDtrByemHz19xoYttiePTHEdtaJ/oj514iBr7/btnHnsDFmRU/pyj1d0s6AWb5o9//Wvpfe+RobdNrCsVPuQD+9X+84cO2ZEnJ6YODl74vLQ4JUL76YNNWe53vT7mZ/9+S//PXvauqB/sO73Xlrfeo4zXLe49LaGt+ZbzclS3YxT7vPJnnwu1Bu7geQFD2GeeTqMdddkbyPn/0ILVYtElApIjpMndCjuUhsLaX+BbKkKoK4hJY8KMk44NJNBfuUrX3HCxALpg/djHDXZ09bX+jen+aMkglsUIpE//ig071a2fOQgVaCW6V2JzBLTR1xYe9W3v3yyKL2Oa+3HbNhS+oy0z7yyot6sdDLPUU07qNnXX9KaruIx26xq6QczT7rpUnOpI02n/rLVp09QWnAZw20ygFssebTVR8v9vp/PoZfW1rzAwFHEHreZWdoKssL0/jVP0q/YecXWXdASAzM16b+wEApNaTHBsFkUBAzjHND3RfWlZSUbY4FyA0FZJsfbOXLkiD5mnyw826Mbx4lvjut7WYDoIMu81YUIsapIWi3s4dVXXl6cn3vx5ZcfefJJZyaunp6/dOmLzzxj6vYjwr7Vybs8duTozOSB+es3G6vrLsK/+t4blxau3Vqd71a7jU7D5HTx6oXLVy/Oz3k12r3zi9evXJq/eWPTwNTctI806mRvbqFq+7rVHCh0x6t9DmSOsee1jRF+zEZz6dq1ZaPF1atWpF43fPPNd84+/vhDj551iruyudE3OLDe7Pi54+MPPeRqnyXohUvv1frcp003eNZbjXahfGR2ljQcQNEL+0lJ/t6eyqYyUzIB3o7TzHy3QCDCnbl3U0R9pINIMvpCTJ6BTOboGGqjE2NQNuCyTI4JB8dACd/Gmy8yuibOA2KuRlUjMhxssGQQ1IKfnGwOAe8FSqsrIBHnDUEN5E46MesoFSEv6zHSkdjNZio9Ic1aMSOlMTX7vQEQqMahnFBPomRv7bZsica8548TyCwtDpMEeYiE5o4LE/KWvS8oMzvw0NRkoFno5SlvgLZ7F86UhQdFdIyEghIKigVdhTdwhsEYTp8+jX9ARMxvxG1ONihauxsXIXiEyathM2M+VTs5aXSJYAOGvdkBN/9Y+nNBsaYKdelj9mkL6uix4ziJTlXKGGxKdP8YAyqFz5jRV0qRm3O33jn/7syxIwTS2vBzmZXp+tC//YOvvn7unXShxtdEt3YOTkzV/3/y7vQ50uRMDHsBVahCoVC4r+5G39M9N2d4H8trSa0sybIsrcKOsBwKyR8coQj7j/EHOzZCPiIsOcL+IMrrWO1ytVzurrgkl1wOyZmes3v6RuO+CqgqFFBAFfx73kQXwZ5ucrmirA/OQL+dlW++eT5XPvnkk4XB5toa776rD273T1ecULIcDyPQ2LSwIZ/zBPv4YKzyaP9sojj/7H3OuduZ/trO4RZxc5+JUenSpeGRUXvuQ8f5tVt3ifjVsI6dPF8ZWXr/ww9vvHvn/tKnPvdZJ69pbTr5vuKQFejQw3feyQ0OhZ9FlLE4sLOzOTHuQrKj7d2d0sw50613CSwT0PxqEtgpcPmVouo6qTX7rPczgUqvqPQTMpggU2+yrAmJqSYd1lkr8rIBEuAnPDRZqKpJlzOVkD5/apm9xF6kV2kv0nuVIr2fvQx/jUih0YwjRSAYJJVic5geIBYGVghPKY6+3BmHmCSiDFkIZEFCegHMCtpgmDCHrRhLSy5b+KcIv3c9w3x4HgVQtLMuzjQ9yET8BeGJP9sUkb9DCguxxJRI9BPqWWaSUnjB4WxRHtA5OjJ++dJVhvlwCXrAEytAYolpwNM4ydQLX5oJ02AyxGnSZJucnjZ/cCYgLXOyeOnSJdkwOhKOiUQFoJZidU0zNFU2kUSnxeGezKkLiaIpHIe0Mvn2H/zey9eu8b6wvbgk/wTvqY3mK5Mzi2srfDZpCWtYV7sMHB3PjE2wSIlT0nn+kw1l/yCfFs4RUrQ4BV/bIuaahjjBSw1WcjuoJXihfJyrXJqsTo4e5DoD5Vgj5eif20dVHvIbcRivu13PudVoa+f2g/srrcb0tWujM+OUNNT57HtiqzEXTr2ID+cvXzFEml3bOCTfjlbK7j/UEYn6iNSapuAI5uAxZ8iikZal/HW2KHz4C4KSVZ2esqWKUv7HlZ4wFmMLnGROFBwAEFUgnpWCMTE7oAX9haskIzjp5xMFni42xZ+VcvrtXz3PR796VkrhC1/4gne6hL0k8g8BzCUIe8o3mQtZw+GoDaE1U+qwUUlKlDjVFofb4vx/CJbd40O2ac5PGFdF+UrJzHGazTi0j+tKNIjpmSLi3djLOgnGV8wr7TGaGuknoBEglXQQQxBFPcwBBJPN3i4kNPraj1NBOehEsDQrUBTuycM0W8n6axnJZNFeoqJ++7d/G9tUPi6H3WkhJLQJiTKkrVGfJBZN+BEUpUkaoCLjJgIIvvzFL06VCx++/dabf/nGzsJDppitoU3Oiwl1w/U9W3c4z4iLO8OtS99MH59/9f1W3Slndm3kfkiInsFD2/qvjk1AQgPpqLUt9WBaFgj9+XrNHUqtwjYtSq5wVCz3dSXS0pQP2uXd2tjo5G6rSTC4OjVrlPKTY8PPXRwbG6Z3ts3oPgsMtb5zdG7uzPL6Zn1sYntjc2eLALt0bnbqeLza2meAGreRhSgYtFKnIzxd4XgyS7+e/1SaYCBFej9T6X6KpGcvkvKbZUEiMDDd4MqZqYRycA9USBHpfdv7/KMpvVenI+LPCk+06lnZfml6AeUAQCg0yJM7QTbwAsdP/Zgu1KyoXk7wF/zzsYlphinBymCLcbGFbosM0PMoqkDBrUB9bDid9ukWwjtwFsx3tp6KNZUydxrN8mDsj6tFqzzFwRO4N5SomsINsUSSJyziSgPCBG/cD/N8TYJvNJ9y+iSxR8hDgSa+uLwEx+h8SaZyYnrKgXugULr80JUFI5RWoxLY9O3WHQ4O18DalqpQFwQ2aLqmEEHJXkkfGsg3NlZ++Cd/1N6qveYAU9X53X2H5GNU+7uZVxhllTbX1/Z26sPFgdpe/8TI2QH2KwT7sI0JshbmvAzT8De2C/xgiRx3mOAqBMkarwy2QtLfH6wM5jvF7kG9MjoxVql29g7aa8tuD3YhaGlu9sWXX7gykB88O9MaKjbsW7Ad3G/ToPKZsba6PR/3GV9z7cTa+mb+45+sFDVjlOdinM9uinlPSEgNnoKBFaT/B+WEqQqdFEnP0xD4uAEnnNDggwRTLwLNPM2IKdBygGFSRKSgjyLE0UTBnygk69FJgenV6ZTTtT8R/2g5T2T4lX4WLIQQde3WHz3XYhVosW48tSBDFP57bS4zL0bPXdbCyWEYzg6CEw7ZBstgTiGxTY8k2x2mcIGTRkSBqQpDhikq4fQfLmj2leFtQkIQ4Cv4gJ5Z+2E1+F5aleGE0mGjBqMXgvYnTu5bOPa5z33OWymW6dgjHFOsbQ5KbQY4vkUBCJ8SFQ7f/sW/+Be/+7u/CwltKiJM1pNe4YErq2v4sTGBh+QcH4qoVI8S8isHEkrUVJxkslqeKOUvXL86M1iZOB6oL63Ojo8ftvYm56bclOu2iL29+uH2Op9W/LrMHnaoegrh+Cp2iPzHdCh75riyy5aIIV7E/mA2NUav1dxg3k1gHZ8sMzbbP9zrtvq5J5ioTg6VWIzuO+ux12Zf2jmim93bPTgsbO/VZycnHK3o7rsBZrZm2clLpT2Vo26pL//chUtczTZ2txjUh/etUgCAToWZnGWpyXuGyfJTweOvnZjAOuFeeiZo8UyvepH0U8NkM+yaCk5MR6IXZjPBsHQzmL6S+YlCeqWJ9OK9PL2U7OXTH73MvcjT8/0VUrUzBhwCWMsloEdOdCmJix8tQZWZNpXEEuajeic/+bQcVtMwzXYCsLbNavXFuWZ+e4d0ZM8wvNZBdZmzee1WquHTLpUvwtMkHISVOK1ky0vNKhbDwhtSWaE4/oLF4T1at7GxSZFGR0rdgsgZdyRDyWk5Bz9JmDYGE6LC3jSp8szMxR4A57ImT9XWgcqHV6YNutpbt1IS8MZETXF6lWLtCpdfCRDbkEE/4yMx0S8kAB7qV7G/72tf+sKHf/KH23cfugyiMjpZzfeVd7Z2NjcaW6vDY0OjHISWXY0ycpsHqr3DQY5Uj5g7hrgXy1+L6ZDPg5oNDOUdarImI97zhmFwiPkQ1SesYxoHuc3aSqFTdnqqMjZVypd297ZzA7mw7a4O7vR3drDggaHWQeP4MA/3jg+GaP1N2BGjXJ3d2Wnu7PdTAXS6Q8zkiuX1Wp2PKsohcGA6DIimaI+4kCLZ0+MEKySn+TuZxXjz7xvU1cNDZaWqU6Gnm5FSetQQaJkRswNU0qJDHMiBSPOlQJEnWvbRknsZTr/qJT4r8itlflYhSAUfj1ZQ/DWEg6MM2siiIOx0u2NtlgKAg36Qgf5GCrzVf0/cFESSyhKMOuZmjDiqd4yQittAyAxMDYfgE1COhUoR/BREmCavLm5YTLLnMI7sOTzhCd3qgwf34YPyMUMHM+xKe7oF6PbtW5R72eIxytcibAmb/d73/hxyWtf5SsOgItRy/Oo3vvwbFOH3792r7ezMnZkRr/EYbUk5UnnhpeeBIE8riqWsdEzRRra9FiZi7NF1IUPd6DE+z5N3NuuxkUXwlugFk+rGdo0XXcquvAXdTq1/r8nQ5ZWLZ+/cuzXIj1mhfzRv52BwjY/BvdxIta+GRGF5YR9hmPq7IWVE0HnkLluWGWSYYXloBAMVh0ecr+getKiS24M8X5URoI7TZCz+Dur7au/YTisPHg4NNuq1MxPTJUjtDsLxMQuD2maN7o2CGcC6D4NBqSos0/ngGquU7Bo5TMjyW4s0xJQBsgRnQahRiuyZsE7W/4ghEUGYZtjhnrkGJ9qTuJ8naQhhRYjFEwx4m/qSnr2fqRdPvEqJz3o+kdlPU/aszL80ncVMuDOh18TNQJXZZlzPIMaXupdKt4rTk0zMjpOH+kxO0+WEPwlPLHzAiZyGA4MCuFR/fPxyVRpHUjPBMkhn4Zipo0P75SpBIjb3BbWoS353zFy9dBGcZcd53fBMv8xwBF3Oux+BYhGlLpYof3KDysgdLC3fq44U9/a3J8anzMr25vr9B7dzfYcbm8ulwcJxPTPIOeRtZXhxccFdDxaka1ubbMVHxysjk0H4aZpm5yY8KS1tbcc+gVvdi6VCOFXdX1y6PzE+HZhgt6EQxw7a2pDNpV7bOhXnC1vznXfSi1bnkDfBqYlzS0e30BZ+P56fndpYulcdnx3c6OsW7DLw0bhyYWp+ujK62Nzp1MNjTAigEfQ+JPaM8dhkT/rheBEoqiL/FXjfOR7qGxocsNguNvePttbqlfEDnrMadRcRHOV27e/tlM6Ujpt7R5XClNNYLQtOHmwqa49c0fE8fHRcMJdvHBWOl7dWQOfgQN6h4vFqxYUIBq7baI3kSy6y2mrsWqKcOT//YGHR+NO/RduiOUlkcfIx2sok3cT/+4cExKefyvwFkI0cy4AsGnYRMJzaQKbmq4GLhmZrz1anOCiVmJGUaL/8Jzrf7EPkJn2YyklPKanq3s+PRtJX6SkzAD6d0os/K72XQaSAiusAc+JWiIrkSdKmMQ/pjnCUtZmmM8BEOwSbdiy3M54TnFBW0ikAFTcifkLCsbFQEEtk+Og2hUQj0ufalJgGtAfw6C28yoCMVT8/RQ4ZcDYTdL90MDBYTm4RFBwugFUBJ+kurCSPc7ykDbCDe7R4Hz7ohXprtV07LrLhsUhGRjWo2mMDLiAlFvy0tSXylkbogpAxsGjTnbt3soR4pH5lXStMQtY4GTzFPZR1L16e7U61zpw5p+TY+4uKGCjAUhehdpbWV2t7DUqVPiy80G0e7nIqce/RnYPcvpvk+bgfqlRrB/vsqt2IRsrE/XRHjRoHHtLTz9joERKEZE+go+fBoNzK0Q7g4dqC4KgMy0Zj7zzmfs6ufs0MTVMolbp2Zs4OTbYbXDkNWeQ9fLDIoeJsZSRHkCGw7dePjtvNRj9p25ZkZn164EJg6tZKcbA91CGBILiCmUygYL2AVAmeuLI2mMBfY9D9bHZPGEuMxv8PQiEw77HVPZEQ4kErdJ8PTCMA/wLiM6g1PiLETiArAvTNBT4m4qdIMJJsVw+VIl5KJIj2Ow1Acg1cxfGC67LD8pa63PDicvFdqGSEQPUQTLOruRSiYQAPBICDTOpzZMmVne4GOcKrUHELQrWYNksyVVh1yilimceXf6pUeezCYa/CLaz2XUlNsnuMhFAudefK1UviUXkmLUuMFiH0R1xu72SbFiy/LQKd/whXqMZAn6xamVKBfk9xTvALOxvH6yt9DhSViq3d1lJtZ2J0aL+v6wpsKsqt9l5+ZMyxjoWDg5FyySAPw6ZsYDNup8aoVmDF4qlrvaeIvA5IODFIA6OpHd7XBgb79tq5QejMbe9wixawfdjlJ71SaR1RNdc6heFwqJfPTUxNbrsqeyC8UTr+ub28UKkOk9KPj9pOYXAXABLMtdGO2un6nTLsxv0c4jDWuSfpQkxrRs9OCERK/XU/07T+0lKzxqQmRd74Kojt0/+yHNkI60H6k5TRlHj1Hy/ECQAUO3z89tE3AEOMNTApgWbGuIGC3uEeIpJjEQg95JGIG/iJU9mr6/XCGyKAPBTsLlsPm+6MSSojQ/i4yCHfgOsh5UKVx+WrmJxZ5vWCtRpxH8+x/EzbdHKqWqWJUkIVhWiA9Z6UgMiOC7rDYM2qNShCBr6B8pkpggwRuof9JTc9BHA/ESB5dDIjvepKb5F5DEQ/UAQ8aGS0cvZc3OWkCktTNSoc7Arul92t1wrtvcH6erW9V+lnjufbuFGoPDHabLgOuYLCYUTdYnG1Xu9W8m2nAftz+0cQHXFgJ4O14HPxJB20DsN/YYpnYnIIyzKVciUH3zuO5DKioTzp57SyS4Lh3sDW/Op+vdU9Lh5XrDPHhobJEo4GoU1vvfs2tZi7NeyjWW429lt0ZlwHXLpw3pSESWGxwF0IM4FHYWGyXxoMXVfxmLOMrpyGBU1KzBDUxihlsBvj1YPm3vT/tSJZQTEvIp7K6KU8tbz0tpet9/Opmf8/SPz3aQDdKJCjIRww35gi35+AP2ODIbMpmiznfxgoDun4Yzc9vgE0gjnI0o8BJXyAUQA0g2DY5aVz36QaMm7KaZMfIoSpKSwnR5EbQyCFykG9QuBKyJK9D4S3ZFK+QQTunghzX78txFLIqxnTcx5QpV5B2lKpRXTEBDwhFVRMzNBb7Qi+riirotgaOQlemXkP25qS5E9B3OfSw3Wgy97DsEyT+RDY5qHbK2RFHlKoY0X5fPhrEyrddv/Sw717d+rL67mjoRE+nopDuebh+oOds1dnmlvNobHJeq25vtm0TK012vYMjwYCngkcEC89E+LFDeRxWVMmsT9+Fju5IScQDV1h6IhdjGNf1cqeHYeuM4P9W6X+5lTVTYY7udz7jx7YpbUWeP/O3XypuLaxOVAqX3/xFdcyTY6PfP1zv5Uo2tzUNN/mEidGx8wxamKc+VMeGSoe9h0jcuzrzUUMUhCJYMXZdGSocoKJXv7aQprrVJzhFTmd8kQ1vVcp8vinWXviD/wEAck+TzSj98zknThPLvQyZL+y2h8n9t5KCKj49YY4T5igmTtDOLafD5YiUTV6FSFNAW1GeOHrjo8FwBkgs0hsFMn4XIEmyiegk12+DPFdMIo4JRo7idndFRJDWgp5L86kQw2f2+bxYRpxkVA3Z8cLgyA79dYH98IwRbrPYxlGeCI1sfLKroiQCVHQGA2wRYEQKF9OulCUgiiV7eE5+og3ho8WuhUMV1/ogcKPZzwNf4cggPMYhNhat13pOHu4VbFojEZqLcMgnfNP0E6nT9WiXuyZG5puuMjgYubg8lDZ/nEgbMAsp9dVIsHwiHMO1b3adrG/vLy1unOUGygN1ZodJt1mPkn8yhSUmSLZ+EQ0jUx6QkK3UdjOO66UGnbxB/qLg2XbNTyl1rvtpQP3RQISHuNrC3jz+i4C1+weffpjn/+bf/f6xUtXzpy/8O3vfOfhwqPLmxv6hh5tlwb55orjHmGKsKefFMiu9y2MVA5yKEofuzopmkGDl4hYjxOmpkZ7T5qcGv7XfKbSUjfFRXpD8dQSe29TpPfzqZl/UeIJBv6iLH+Vd3/9BhhbOlz0j1s6yGMdJw7isxIt4cBEYEjwj4wrkk2IdMEdgz0mOp0Up2GnwkqMUEo1PDoyCusyuo60hs4PZmNmAafBVRH2PhZbcBkCQ5UMQ2LQlWyng7LENmBiic5vSIRXif2ShbE44rA1IRzH/TQW31NO0lbDObuU/rw1j07n4KYZIYlpBUDUOXBZ27XIt8GP4w8HDs9G4U03ADKTyQFdt8MllDZrTEZT4rQX9PDkZBg2igs6oB8ilc7AzuoSo5NDoubgYCzWBkuN5uaZyy8iLYel3MZBfrHB7VxlFb2rjrjSw+0cj7WjJxiYDf7P1H3qMgXpOdDJnS2Hw7/+obwdQPRhYL/T2NnNDeStNlf3D7f7cy1rt1buSx+b+tgnP3P5zJkXP/FqYWjQoqB1eLS+u31n4YFLcF/9+OsuOkf9xqvVdQ6INzdVsbax3hngQb/MxJULOfctc5uHQ6Mlao+5ET0hEwboPwAfzApPPc2q+uWPNFbpmdoWk/yMvygukYzeM3Xp53jgExSl9zNFTj745S37VXKEO/cAoQEnRwruaY11nLsNAgp1TVBrIJuZSL8hbQZ4J62JlQOoLvZxDwjuiYK0lAIADQZlFVSqQMdeiNIykMpwL7Y9EnZls6yfvAHyM4OFaIwqKCBO/LjAATk1TEQJhF7ooSVumUwFBofKloKemqp8ihOjzhLd3Q80+4I+4d5aoJxeePx5YFfsbGbLV2+DXoQIBjcypSQbMkzBUaswTghqJY9PUvs9xXV0cnyssVjc7Mu5LY2VTak8fGthmdvA2ysbzePcxtrWzUfbh/nc3lazXB3arO91C+SBYK3K7D1FekEt4ulpB2/1sO7Wcss1SIiSUK5ubyo4N1DNFaZGps9M2jmoHPV9+rNf/uzslXkXQrR2V9bW5y6eH5+bbrtmDVWoVg66R7vbO7y43qbm4p+/2F8Z4WlXgwfrXAJYbQbxYzUXl2wxn0BsyAQ/a1Iv9uuL6H7qY4r0fv6CGtKI9TI88bOX/muNJFT8tRaJE1K7I/NQi5BoxR9wRaLMU/pTVARL9ATWhSP0Pgj/2FgZdOIhhgyga47P7QeCQkwGIwK6ikpaNZk4MWm1w0MuuRFDy746YSCKVVGGLSG+gkMlMxwjhdoKt1epugCHLEBsHIAGJBOVQ8EDW5i82Ja2p0+qRE1UkZWJGY8lRREsSqoTrJvKlGULc1R+clNOLFSSJqlBgXFLZisOkmq8Z/S804HtShDRTmdppetv4swKEYyARIVoW+Ngv17I70+M/vTNnZ8srT0/dy7XeuTM7p/+8Eere8xecvuOw5dyBwP9R4XQynQ5vXYHaOzzszEIdyF6aKxstNqSkZjq0jY1ipOe3ZW9h5AdHdYPWlxdMPO7eP05a8OLL1w9LOZ4d+Oo9fLYzHC9fW7qYrnv+Oz4yObdxrZrQ/v63V74lb/1W0wY/vyHPywc9o1XhiYrwy0GgK5ze/jAVU3t/sJQoZ/WlHa1afFKO9A5tAfrDPvU2PjM1PTq0srkzLR0Ch+nJVEiBhok+NR+o6SRnoKx9TYtzv3UO/1CoyUaMcPVC+mnuRYRMkiI5xOR06/EfZ5SnsgZAonVSn+Okzt32GoPOmOkzX4y4za5Eg21zVV28Wsbq4xV/EwgGqCeQbUBF0mt0v7UGJVKlyibRHEdlw0ASDndo9Sq023T2tTgFEntT/GwwGKna+iCzMe5XEsgWJd2qWJTATvKvg6+JGLdlYpWiuphgttyReCMNVXW2uAS2nRyqDfnLIEGRzegsSFgKGbRdNQOJhMvUPNQV8awuv2y3gj1HO1oFky0NsVwQ4Yss8XWya6Jn0bB+k3tFj9CQokMS8PJRaxiuoY7+Z+1304DnHwTxkD7GR9Gn5QfkwSiHNzzVGwmb2seR+7RX5kMtDza7ys/LT5VZPBMiSDdW0O4tVuffvGlsYfL3//2Ox+uL1YKuSF7MRBuuNgpDBwxZ6FG5kaYtbStl77C3trOmblZRakFUvHhc3Z+mFErhm1AGO3ZfikMhU8nP9kSMCaq2r2olC6Pjai0yhucnhEbCn1V3mf6yofb7dbBbn7vMFc5fLi8eP7VK1S8DxZX+Kt7sL7qyBRt887a9mixPFYu/8anP22nZaveGJ+dpW61jZkfHnOU+LhcBMiscOikPFn3OWLjbhlm8uZFuyiiLSrMhTZYNhsQ7ddCAUAYKz8NjjWCScnGOSQjUOEp87NCGur0lEckxdMzpfS+7SX2cprIIV6YD22kxtyhnl6hqLBGwzMtWjhk2dy0GKKCOjBlciWwMaGJlMigZL3Ql16NWc9CV+e8GcWBn3onf2qM/BlYRv7TX0lPP1O2Zz0L7ojsFPGB8LZ05Fr0bMmXyYFHJ+rEjDYpLguxQSWiOO3BBUVUz6BFRLq4cqgq/YSKgY15B17xxmAmEoG9aXCcIkODgF1TpsN+pn7qWw/Z5M/6EG+MqPTU86B1mZW54SPx+ZzeJ7QpWYASAQehHjIoCQAs77DyKMxMTJRGqWTi8CMNkL1qzD+oA3+eBUudzKm/o8V2wAPIQmuTEQsNwGxBlfYLXgkqNA69thEmRiem1ream812ZZadynCX+dERia6MXNmCNVJa5PjgoVsmNPk4f37+yuFua7AQtub93aM8ibBQOTd7QUWp+9Foi5xASRYAuZlzZ1xXXmu3XAUgj4snOGllRuCONRYAN//ix67h3Bko/p3Pf+n6/MXhvj5KmvGp6d3j7vb62lFfwVeloeH54TG7iw/v3Htld+/M2fP8Su229+8sPHLiHsVi47653xieHC865F0ZYp8E+a35IRtWpu+oAWmCDKVJtHNG21ibC0MhrtmmyTPRJtTQJ36Stgygcgyan2my0tNP2VJ6evULnumVunrf9ooKYHSRB8eNtGUH7twL9gDGQI5NGOPtr767s7T4CMo5NMN1NTixFNJgT+hKVtLsRF5TRTH+AYeKVgx9eCjoUu1yqjplSJl7Xcg+iQ+fFU7njPMg0XB6Qr7VD3k+gwMBfPQ0ys+yemZDFmvDQDPlqtgrbFK7/PRJklrBSdafeC89TEmKg9Ank+yCNPFCZqS8DiQsJM1q7DEKCV2Yy0BdOZWjcNUFA8i2KPyUHvsY2SJKBhuJY6Nx7xII8MxQOrzmQHIp2YIT+7Nlx77DiESTwynVSQjHlcpPQZFgRRsEjTHKqTo2ONmYB8dUhWfKgxWYg16zvTJzbr8dah9/51vfeffdW2emZ8emZ+liaTlIyqPlMdVRFVvlhj9CoELOJ9UXWuuLqw5tKFBLSOMffvgh2RuCqVddWqUWT9VJ2djaHHaPqo1ELWs2tpZX6ysbxe5xudMdHRhY23zw+qe/Wtg7eG5mlmWE2wemrp4fqxTvrrr8aaMyNzt/5TKrQRuA4zyVF8I8bXiv9WB59c7CPbc1/dZnPv3Ci69Qip577vJ+92izXl9aW33wYGFteeVrX/6KoSQI28ywvuDE0ZVVIWQehqNByAnBtFA7zRGCYhb01ywYMSMjD7Yjmy5IT0F/U0QecU9BSu8p8ovTTwrK/su+ckx0t7m3M9Edsd3G/wkrRFupBy3L5mKj2dA2otzaepy3ZjuFTBLFfZgAEvzogtaiF1ouMQ2+xmsGvAWYQfTdAZjxSdXKk8BDxE/ZBAX2EtNPKb8gKP1neKUgWX0G/uKWwSg1279KSBg/DWiceMrqiJxcQ7s31lfQTFstnfThdH0EAa2SU0V2mRxY9S3ESEBGZXhUPIRbHRtmGZxhNkrQX2Z06lIjUPSJ0TnByTAWixQ/jVSr6ByWK2jjOLw8aZqJTLDC5gT5kVCacdBola+0MzD5+BiUoMqKkhkAUeHCMwKr68GZR1sgBt9r7dsDSMQIndIelWaMHSmJ9bNniqjIWiu80x+Vcjv7k0y+i0ObK5vVUWuo+W5n9ZDCSUBxj9i7tPs6B4x2jczO8R4XE+trS0SRuC617tztfn03nNageWbiiLjhLqZM7FUnoXF9Y7nJjKFU/PRnP3Pl8sXLZ+fPjU0eb+8uvPX2T/7k26/OznHIXew72mxsLO+ulw6m3WC8tb07MjF14doLZP833nx7c3n1bHX8tedeWHTbRm2n5objRjO80I2N3r5/z4rZoroyPnqNJDxYunfvwa0PbkKvCrvzlTVGCW++/c5rr7+uMdZULs/a2a455AVGbQsZHQA6Pz/vMAqYjl5kSJhmR1yiYRB54plg74n09DPlfOJVyu9VCult3EYcxr2WVJYVxGbjByp6YoYAAEAASURBVJo4wmgsLXH4GGROC60T/V+rbbr+5MKlK/QSPvcqZ6fJleH1eu8oBnCyOI+FBr0JNfQOlbj7BCTHiVZtEPTRE+wFiD9mjCnFqyfQQUUpyPk4yiI4A80ozAGy0M9DkowFJaVMZJUf7GasL5OVFa3KrO4YAXzUE9OLRVe2d9c+PNHB2HzDw6LsjNggFjLIjBcmWpLNUVQQiVkeY6QzQmoYGhXCYvaVRNkwUOgqyACdrLa5PoSEPqzExTKxMtzasnrWYMtC62Yn1EOFIIPPoRxtQVIYJAMD6cqHk9oJJ1F3bcuaENUx5uYyQCQFYyq/QoCdPOYmrSV8gn250s3ti+srq6gNI7Kt+vrG4mJ5ezfbCtSWbDPC7U4xylQqsQFQLFUQ5KWFR7/5N7+mrj/69h+PT45iMvQJJGHisSdBhT4znN7njp3GGu4MzQ8Mzs6f/fv/4O+Xq+V6bYdt+lCzPT1a+Sf/4LfzreaN9976X/7X/+nVL39mZHbmx2+9udnc32gGEm7Wdv7dD3/w/T//89xB7uPXX9jfbX7w0xuuFnjts5+oTk810DRS3P4Bo2fHYZrrbQxuZCxujAJzX/3SV6w/FxcW9+qNje0alzz4uWXhQWOP8zv2g7IZikePHok4k4nAGRPDbkhFzFQswLJTtgFsj4ORT9E04wbZDKZnipyOn37Vi6d5SU9IyFCJO2Orj43NFQIzyoY9I7MrVrSry1gZAg64EHyz2tirj46Po7PaoIVp0hFf6IqZgzFzqqJUl6kHWjylVIaCiMusa0Jqv45I0UEgIb90KUL29S95xCa7rEAd0wsa32beSWSyeOHcKg3H6W7GglX+VEdGPlQcl8MYeo1Wqc6EBOh7olTBqRmEnGwaAJ21Uvmh4ncbUVZuIAa8I3vibcGKspB9DkrpZNQVSw7fpt5GsZnc6KdECJ+wyE+fige+dfvYW6eilK9J0AaNNEAhNRX6hqvh4a/GtUPmKRgGMjpLVaBwYA7E+NxILi8v97FDz/Qu+INEhQhmQzwakM2NCAo6VKoN5AYvv/BceXz80rUXrh4c/ejNt+/ce3hm5kyr3nD9J8EjLV59a2bQvCbz6r72i68//6nf+MTLL7x44/aNlcWlkcGxxt4uEIeruI3bRP2FS4J8v7OZGkMsGHP+odznjGLzoD44Pvj5T338T//l//VnP3gDEo7MVK+//nxz6LB5uDNXPnewZSv4cGdl7eba4tvvv8+qdXZmcv78xYXllfsby6bnk6XB6bHJ5Tvv33jv/ecvXIY/sX6jVCyXN3e2h5zhHxx0oefwIPdu4Q7HT0NUHR3hhnhqdNzY8iFijpC2TGYLdQiHb0DLEU3zZdy8oso2VgY2zVSCEE9BisxmKkFrijzr2YPoXgaRFOCv5jGz293e2txa55GIQX8yvXAV+drKEvEqFoEO8QyVrGb7B4p3797NhJ0QzUCOxigfwCjQTxNtpgRj7gn4664Yf7xQMunijysPMNNBKUrQa2+FXmufiPiql8JMxF9sFRiIPNkv3+Gl1lZzagfkgWlQIfsLC/7WQdwhUbBm1NzYDQ/UDzSg1MiFKGJDEZb4M1UCRqUyPCa1VbPpcTyD32W6eVtxsQ0Ry9JuocOgXM2HdK26Q30CESCRvHqVlROYFvAYSpfgEdoChcL+i11o6DkJk5lKgHDl8iTYG7Yvgatx/ddhmAeZie2tHWLGpYtXLl6IdYtw7949ACQoVrMBTQbrVt6QO0+0tRzmnYNEix7CaEsLeVBKQa812CC0Dg73c31f/9t/e3RysuEmmaOjra2NhXt3LUkdjVQUJXQsoLuHOUbVxxlBpSBtHlx64do3v/0tsuLE3MzNe3cq/aPuB9WdWPyG0lpXu0fh+IIB0dH1s7NcPr72wrXL0+MXLl9cn666nn46V1x88dr+e7emnYdsbo6PVv+P7/5xAytd7d9cadS5txga2DHWbEQrFbTn/sL9merYf/kP/wso97HXX+8W+v/1N//NN77xeyPlgcpI5ez5C9Nnz86dv8CB+fmz55jNXHj+RVMyNGD0uEo4/PDWBwbTXODOd+7cGQ6qVR6enbrsyjcnRZzArJyBe+Qj2y1sJwgsnBu8/c5bHCj7CvCgpCmIJ/iO2Uts8JR5KjFRojcBtVl6Al6I9PhnxixCeRWgKrOJcH3JrQ9vjlTH+JKW02kBE4TOUmyCCjZcCIdhZQO2uLTKvRZNJ+2UIwPEoGwjKj8/fwF1bXX2GwN1d2HS1NmAy1X6drZtQsWxA3AWrQLPlAWEQQZkNNcBvXH8qIvTZp0BG9oUOZ/1dMQtrmPRHKb3KYT0EyEf1mG+9TzBHwZfVIpqzw7pQqPQKckJPaiVmLXtuzT0yObEieqCDOsmPapzHQbccmoQMUA9mRajDYdV4XNH2lhZkoI4OuWROtfGgi1KHdXD62MpCDMVYt01XHVAPFxjUDFjaMbUd2pk4VFvNnhJGa5OQD03hN1Z/zBX6A4Nlob4GAv75Pzw0GBfS8n9D+8vIHtAhKNhIJjRivKVS1cRY7Vokqaigs16WJ9OjI2Mj7PYHoao25njbWiO7IxW44KKne1dDAEYtffDUGFiZu7ew9Xf/YNvXjs//7lPfOJT16/9rddeLzfq791+uHmASOSJ5kRSxNY4UO2jW3uNvcHhkR+98ZPnX331W9/7wRvvvuf0PgocLcZ2GCcR6WMKBjh8Ohw4Hp2pbB9YSO5+/LlLX772nNMWd3/ygx9+eLNdbx0trA/25XcerWnvztLWD36yeVDJXchXZy9cPtreKFf4uOljBzM2Mbq8uHTrzs0H/fn1jZXPfeaz4V6ov+/K3IUbP32vv1J8VKs9Wqv137zp9DIHF5XpM0Gnu8cuVH7p+rXJ0eGLF+Ynp8aQpHKJV5HjseEK1nzzvbduvPVDk3V+/uJujdJmGA2lJZkcn0JfrRLNV2V48Pnnn8cu4KQLXsi0IMKOtEuXXGRgSekVFgokktUhlUFzp3b27Fk7k+7M447W6LHrN2uOC8JhsIQEM9EjLaCMhVxg14Vzs42dlqXP/t4BzNlY37JdTLgjoJGmdo/qhpRUsd88xHOc0SQ/UYIXaD75GTnwX3fQhWLt49HoQs7A7g82Z0bHUYPN7Q2m1VMT4zOTEwj81qZr7DZo2jiUdeufk3cuBXSs0z60t27RRIDs0EJyIOVMEJbAlgXHgGS0txYjttbgQKE6Epenwl/9B3mHDCEfb0ECx16A1Vk45kcu062E4EddevI87i4+WkoMJDjDYFwEH+ia2GjsbwG8YMC4qq+cxyBgKVyeIIT41XGfisOYM/S0J5wRMlC3ap5aUBc/oT2vNopBiMgdSoiUbkj5YWYDoyWT4I4LXLqTW3EzCy3BNCja0qQ6Mu02bvhjth48eKgEJNxEwkmQkaF6OWOD9O+K7exsu8qhHvemwLTYTSUmRIEM3W2a2c/AYPXO0MUlLIfdgePCXHV0LrMZvViuPHf1ymvzF2+/f2egaxvHqBsKGhcLWgoxfMRhfNYvhb52X2trzy0xYbnkvp3Y+mM4FJZ0Tj5z65NzlxlfpN3OOAHUCeeFpR/9/jeXvv2n1eGh7dzB9PTkd995c7pTYlgAbSbyg2Mz556b71t13movbhqrTIzVjw7Wmttn5mZefv2VkeHKX/z59378wx/dffBgYnz0+WtX6evPT08j2iOF4j/9Z/81H3kfvH97dW1zb/+ILdT9O/cmpqfvvXdz5d79fH93c8MNikHd3SRw7fLzRu9jr7107tyZydkJhGx7yzUBa/192zZWGV3cuPEObzrVqnVm49Gjh9muXYh8rn/TbHBi6DDdM2dm0WjOgCzUubG0anj33XcvXrjARcrtWx+Qfd3cnJleMSQcYU5Cow4elIOe8nerEGeiST2jpXLyEI/1NY9i6y8RVtOH8bFyiH2ezGjZt0i8ha1Fk2Zk63YCSqfe2YXVFGkgGXhoyeLDpft378lf241jOuBkceFReoW7KBnkJ8gR0SpBBGexiLAyUjiopFlmhRL8I1Y07Sg88EPosiscBuKFfkbzJp10YXELzEkcVExaGaCd/QVwYxKVMkc6sZiGq45A9CHnwOqYc72ZKLDTt2cBsheqTvQs67mPqFZC1YHzhSsZFCDsOaPcCNCnaw+aMz7u0uKsaKxJXf9OxA0RI+OcnTBxsIUZJqUhcnD1HXK0U+0WLxg5NhX2aH3H9WYLKjPaqe+1rGpxZkJ6uR03HJEi4UmxdGCyTaSR0iRIBW4Mq1WQKjLZKYbeWGcrw0EKShd6mwm4Sg72lbq0bWRkXHf3sntsbMMo0zbrzsZG+TDft7y25P7vP/veh3/8ndc++9mVOw/Gh0a3txsF9uGs1CBGuFqKp4EYyh0Pse3bO95fWOcxbbyZC5Ao5GuNHZpXLXFNfSHnMsPYHkIWGvdWJqZHzgxNvjIycw4h67QH9vbR6PoHNy/NPzfW1+Xqm0vJ6qWpsU5fs3P8lc988sL89aWdzQfrK7vtpoWZQbY+dtcjukOZSLrZbzbGhga/+JlP1VeXS/19f+/5T2CBFw9Lgx8bps52tQZbJB4Dbrz/7jsfvGtnZHK08lv/yd+wWKUpvX3rw62bG3/27/5kfGJ4anYC8G1u7Z6fvzR/7vLM3PzoKCev05XqULtN2c0JLYGIZBQ7/nD79u27RlIfqdYIkGgfUdBGxoULl+gnl5ZW8M/q4NB777wTy4T+voWlxfJw5dz5izubJ7aKhgfZdQoZHLkZB1/bb9b2mnxD5xVifkGgkEDRnKrXVJpx0y2OaqrRsiisxShSs9MFplVpxt2RVNTW2/puk0JBnNDpXhbW8g5kAgP4r0zlg3bOwYivivcOiZcOJPTl45/4JGDDC5w7VzU+mTG8/StXrqhI3wOYtzYtiDUAk8GKUHrUP44P0T97beozYRtU6AkFSR/BwNO6TH6M01OcnmB6ei5k6PBv6/4Weyx0M8EMyeUQqeAAjvtLUH82iJnNNKpA7VFC3d2mpxaVQZ443JhPFjaeLosGp7Gt3iX0krwha7wEvnQcJeWIYI+xuCTeA+wcbmSNqTCyAEISpfp33EcOHhp2pVEQTjNnjPQ/OYnSeQO3tLRklajBfsojxFJzMJbjENWEGWumW0DKrqddTPNaKIwWS6ExC/aYzTSRsb74qFRvvHRufnjmLIx9/wc/unnnbstMm1rr9VBaQSVmVeGugEervKNMnMscdOsrW+xmyx3HNcJuFmLz+4sVBo8/LuY5UetSKRy7n/Cw1rh989Z3FlevDZUt4er5A65/B/bqIwPsRXI7eYSn2Xe4f35ieqpaKXQOcod+ttFVjkWAy9LyKhAvDxRn5862azV89v7tD8c55i6XXr92pcoAMejF8ecuXJ6YnFzb2mY+OjY1/XBl+fOvXL9x6+U//Yvvf//HPyGpkjGsgWGyDcel5Q4MxNkg4Qc3PySn/+jHbzUbvBwfnz17nhhis+PipXkcb21tw8DSLV/KArjExGKZir0sLgIDcyEij7iLbfJzJMHxqZnpcBZeKrKtcxUX7aWNJsNb26pRB3BNMjsTJ9pG+UAYm9ytb7s3mq6IuOiidVvfahEPkwxCmlsMeKojFMZ9VzbVYufJ/KpXNrMJCFUNkYyVhgEVKQKAceoFPITuJTb3+3AzJUA2z5RBG3zipxIgrdGgc8Tx3EvCfy2vg337yqGMzN/68L0MA2UkCRun4An+/JSO3UUsS/RUuGcsBTO9EXIM/vFCYhQsBhjWLlat/TWK+JA0aZAGuYAy7mFskpkBKDMpctEAomZwRAofb2PtGgWTG1WsRkGf0Rv4FsiqQEIo/hrrX0KnXPbwgoP4L/Ic9w2Pjx7sObRANLXmdLOTxsGyDvf7yIGztm4+KQx0uN9lG2SMhof5g29bBYUAc3yMSll+YICefho+ebQpcXIRrgezU8XJiaWrAZw9CAekumEOOPLY2yvUdp33b/lkuDAwNjm69GBhdbex188vWmmpfbDtFvixEWcWyM2DSOlRf9EufV//ASs25GyAQWmMFo/A+TYxxsa7e6/7jor5/aKVccwIk/ZBWYP4HNcP9qmMRmbmhnnR4bBpe3X7eKdePJ4/Y28y16w1upWQXhqHoQ0+HOh/8Oi9Uf587Ov0xeYnHMgPV466/fcfPuIIKLZCBwp1zuA2VtcbO5OV6tFB/uZP3typGZ9WbWvDaeGLly9Vxsdnzs1Pzp/70uuvjELsQmF+9mzORBdK9w6aLOHaj9iBb/NdevXa1WvXryIezT3SF11I65Of/OSNt9+0orv7Z/erI6xZA7SiV/m8xZ4IfDMLcAb4QmnCs7W6kYdpgOHR0oonWhA+Y/hTn52xJUkN5OxytTLs5mOSrAEfG6qwKDi2GCu5G2tnxk7OzBz/QNQklENYhSsQMk4VW8pulfbc3N2O8h+FYGnuICeN+vnz50T8RJGlm2igoiUiwl49fMHgAxBEniDT4QEz9gugJUQKxGFVH26H/CDOtZeXF1qtOhav8XGWIJZUocDh+yHDt/jEtli42VWEzykRZUocKcPmYJEJS/FnowY/4XfYuoTPEXX7SsbYM8hyhk1JYG1mshRScT9z51BsamW22ZgKjJ1DOmIFeuOZgioER/I84Z5NNP8s8+J/a79up+gwXXDdEEehLllVIoE4GHwn3EvbKo+6LbnyA6QmN2EQPXSuvF86cM8Ms/LCAJLJDFqTfIQhIH6G2/SjhZ4xrGGDHgsJWIocnpmehp8aIPP2zi4OiVUZJYlmhRgj/5kzYazsVbvRPNrdIt1YXJHZh0dHty1Vc22LsQOuHJEU14k7qk4Go7VCZ/JMBByNP8xMqo7DMXIRJgYDCV1ZOL5SM47pwH0nbqvzf+toOFfij67W3pviKa/bnhsbHq/mOxPDub6DzcZap3BYGapYX64t1rc69VcuncPl1inejkIThuG8dOnq2PjR5tqGqs0LOjDEdMnlGVYR/bmFtRCbHe2gD7aU7tRbW8v3br3742svv7y6+uCgv/Th0lrhqOOy7geP1n7y1o/Gxon6lGSGXcf6+GK+fv05rlHsgO7u7j94sIjpjYyOnp0/U6ttUFcsPHiYoNakwwEjLAAZWjfC5wcf3GGL8vnPfx5fggx8acsMgk0N5OQxxHzhk2NcUzmKSWap7W4e99c2NqeGR8Yr1fHJMccgCa6+AngZ4thJip0G7M5QqnR0FMGdSiAnRUgSXCYu7tFmk4lIm1l+hmyBmQ6Oo7biAJJZLy2guQYhwtpa4CdY0kKDqTRP4TFIu6envra+LEUbIBD0gZygBdgYMpzDExbQsRo+go9TaXw6E1tCBZJ148ThinzWliwELFZgcnBKa1zImqlwcEkiDH2R1hg7LfPK9QqGzIZW3W5p3OqjOqo5ulyjhND3DRbDrAlDtB3ROQwbXwHkWiMyFAkpOOQ2O9oZdVErNoUBwv8i/Lc68s/eRrfZqDH5xqv78iPki+CtPhsoBM/1SZFIzMlDmY4mdD+dI1RNkUhGIniWB0bQoHjCQMxNMJTS9QJx0SPtzJBzaCxboLpaUzCazha0DrYRb1zXOMYmf38fl4xDlANdfnnZwucOQEploF3M7R/vFzoaw+suiI9dmYynxoIYLPaTHvidRFuYlTb3+o4oKali6KN01NweFy27Q3WQ50VqqN8Ro8NWrlPg/IWM327t1xwJPXCNff2wSWl5XLATMTBVzdMSTFeHbU+sr7iqcZvhaCtffO31Txiadquzcu+2nTRL8Vajsbmy6DTHO3duTZy7Qgo9ajX2jw6qc6OlqlE+qHYK926/O7G30y1VH95ZaBz2b61soBVj1ckB3oja9aFh2qygX7S+c+fO2GwaG529c4cnnc7c2TNXalcINayr6A3xOYOfzCTkB6ykNs/Lly/Tx2xuhuP6anXk4cOFra3ttbW4d9n0GWRQ7kPTND42im068OJuucnRETfFXjxzBtdxA8/D3Vp1CsVUHlgecF0tzDGPvlJOhiGGOMS6FAASlhji4kBpdCwct/fnr9FzcuIspWmBt2unKfyCLDx6KE5dAxJMCFjlTHN2dsZPNAIwUOqm5qnLhKaA9V26PK9qUHd41AoWUkjWoFyDb2tPyHqQkMO/HvqmlvkpoAeKA52Ky5A1DGHVrUpPGdJbT1/pNGUy8s2rE4tt3Y6iSHBYqxVan5UQaBLhY6JNRLXfRudOBEMeNF35ZMAolmhbii2HQEUkWiOzrSE/5cR70ZKs9mCGvlKR5mmm2ojE2aSSJPNMZ2z7aNVQYdi+ECoLnSJ75oemWY91iE/gEjboK+IQrENl7RYmJFQLLFWCdSmBhPNFIqts+htbmNkSH7nBxPCW5ZUVvZibO3v10uXt1t7y8tperZ3X0aMOx4G1gcMmXKNfzR2VuGPqHlvNtimKyLV86sCvfb7qK9g4Rk8x1X+4z9bOAQQCZOiwY3sQdhc92/1FM9oZHKKSohLUGFYu5cE867uHm42JiZkBBso7zf215qS1x05ndKTk7iX0j0wAorg+3KWDmRjP9RevX7/+3k/e6HN0gA/SuSnYMz89w3T7u28v/Lf/3T+9dv1ibW1p0PZqo7S6sNBq1njUuH/nZmF4qloeHj87R9rlvvLlF176yzf/pN2pW/nPzk2CSLqTB/cXt2tNHjNu3ro3OjJ59erVj33sFWqzXYUwFtvY0uyFhQUsxSzQuDBww1hQtLt373KwjnOaEVfrKO0b3/h/rKakCEb74cOHZtzlEjfefItUuru1ebTfrm3Uzk5Ncm31/HPXKlYHFKRTE2xxzYgpM4+qAyrmzrcSM64YyzpxtBujoyzx19kNFWtAcrZNkMAAZk4NTAIVmcE8LQc4gYqAZdNlBusrRFTJSDDinzG6IEYTk5DTiFpAxq7GFsMBZ655Xrc8oKPN3ESArsdK2m7h0qWzUjVUs06QJ0Oq4eFLKT21yVMeGZjR4wz6Yluej22LLPCt6WDRchPz8JWQOmyffWaaHjKwhTFqYXSwOhx7O1iRjiEBilJY9gyOlylajkrkynzsn8Z2KrS145PvG62GjB47166mje0IyygSK2ZVmJyKsbbjRJRWrGxGU/BTyZaF7neJSrXjuHNmekqGNKyBpdkl6eiLXugjmMD6tAQceOXJ2tqFRXBcp2CpciwJPFG+aE86ziNzxUIiv7Sy2Nc+oCIiTIyMjlmPj4xPDLQ2rlw8v+VaVVqE9mFfMzc2WByfmX7U4Te0M9Y/2K2Qrahh8q2jNl+R12dfPNjZyu0OsFls7zXdg9w3WFnaqtX5ChkpVYdHoGjV7mfbBb913HF/k/1ErlDJ1e6vVI/dOTV27JLVnYPyQa65flC8mqdR5AuYymxjYWH22gv0cv/7v/w/zRw4OGzu0k4BrFLpAj7gQiqz8m+/+Ycz1XLh6GAkT28WWgt8JvwJV0dbudLS2voLV168s7L9d/7hP7rz6I7OUjDuNHbai22Y89abby8sLBF0yoOh2QIV77z/3uuvf8xGOVoAsq9evEx9+9zV6zibWTPg4ErEfJmC3/zq1//Vv/pXNHk296j9XXj6aGnx/KXLFErYNROCvfou2vP1r3+divPf3Hj/6rm5f/Lf/7PZ8cn/+xv/+p2335o9d3Zha+1v/b2/a+rAFSHUTXYmkSgL1ZVvY4M1FEhQnaoJotAP3MEf+UmkPVCEY/Ij05DDdAvxKpbbITlD6cuXzyvPt5SR9KXM3BVJolpfX719+7aqJyen3UW0tLLE6BTpZKljZaRYUEdooqnKNla82RcLH8aWG2GlSJhDjGMvK+wwGeskmAZ/AFSAqGYuAXFiREZQkGHuzIS36acCfaiyWNGFXifUO8Lp/mBcSoh1bIRsKReR/vXtHbpBBUK/zLzEt1EC31MxiGWtjx1/aBmDEoqckA4MojmOcbecIsV1c2ihCskD+KSWa5jm0eagjrhyVPnY4gxq4XXWeInVkxzSWhyTJAW1dveASMJSDVGae22Vli7SYTWCbvLhD45h727nuNpqb2ztFHOV2l6LpQVZe4SshulWSPrjz40PTh4VxoplYmpf66g0PHa8093YrK+vL1M7NLuHG8XC2erITLH4sStXKR8LrpQh4uYHhvoLjzS02L9Lkuwc7Xba24cHW12Y21ehdCkdD42U9tp99eZ+Yb9dPhx4/sVX39tcb+W6C8ur+dkZgEbfOjU2cf7ihcWV5a3a5tkJexUsLkLLSMH9k7fe2lqlHx5u97XH+gb7W50j7tkobVmvhgFG306rNludnJm/9GD7feJeYbDY7ranz8y9+OLLq2sPN3i1WVijOd/cqkOAqenxpcXNobLBcEbZaY+64WU2rQcrTuwfhjIZZwN/UsQhpwEU+epXv/o7v/M7EIZo+q1vfQvtKFeq4ME+7d21u6aDdHhmZtpEYJ6XL51FJt94441RN3/lOvPzZ8sjw3/3C5/68te+CvHMI6Qyz7oIMeBbJtjhWqHPhI3seNTIXMEPajsAaYmAZDNwo2aLvRyABGRsxOUY2lhSFXFCx76xBnuerf0AIWCnronJ8cmpCU3SC8Cj/TQI8qxv5HhzhwvwFvhJ1BIR6pxLly75HMaAUoumWPbbgsiMY0KaFjIU8ogQ6GA3IpZ7zne0EZUwOAg+po3B3GGeh91eoGlYPb3KqE6wl3qdgyYLguBFKSQGRRrWAZiTVXKyokVpDE1oRcMMCP31oa4GstHxCGGJltm8G1Mj5fILbvwgYVZIWKtlxt5KO15ZfhS/bFBa2buIIT6kQu2v79bCBCk7tIKgJOxCF2WLCrJLnfQRmZSoO/IgBzpuiM19VFQIa2/DHUiPcqEcQXFi6CrH+bLtBzlcA7rjMt3u9kYtNzqw66bA/d0JsnRlYrg4zKBWM2weXLjw3Mj8zFG+tHG0X+seuGhCg8ZyfTP9A6s//en6w8UZ4JUvZA73i1bU+7nD4nDVngEXaNvt4lb5uOSQKc5YcksaA5t8dXp4KFce6gy89KWvvLG1OdTfmXvpysDk2OHKmu0zCrGJ5u6NG29evHTuxcvP3XnvzaXNFdaBlemxZm1reWP7K1/6zZmRs5WBwcbmxsqjO60dTHZn72gXpeStdHltvV12XHlxaG3l/trmH3z731578fn9w25ttzU+NkNyGRmdJIi6Bs6dzsxdC3ZZdvdYxvz+N/8N05vyUHGoxCRutFgITgKjgK9hh4EYCGxE9W7ceIcW5NVXX7NoNIyGnZDBJolM+/7ND2AzEP/0pz9px+I7f/bvWOqwnnj3g/d5/rd2GeNOcnerMEYB1x4rj27XQnKBcgxZMBh7EhmoIsn7bn82Wao2a2PjI7gWWGLUaKJBDlApDQ439+qANgSx4JN0IRnAx1xbTsf5LI33NkO5k0vytJlQDfnVqy5A0m6PAVQVWUlJIbuCGfTozNlZCIm0ZZCTLzBQVg25FlrpdgZMNgz48yQf08RYB4Lp2DMkP4DIpHeJDYY4ohcnk1AIqk6FxhqI/iRLT+u9DA1CfA2rq5OQoRT8ip2WgGl5hFS7FN4voWIYN9DJhEYwWgctncSXNYy5rJ0cSMiVc8cj5EI+aTPdYRKAgydDdW04M4vSi8NhrWrTFsomRU3JwCKwKduQMPfiPjRYqY/ag5RkpLpN6Q8bYSB+r+lWrMCC9Br044RJh+WQWRHsPFgl2une2j8gUFCguNhlIJc/qLcq+SLfGC5wCeE859D66OWJsU996jcYiJ17/noz37dQW3eSaHp8hIqd06WjjfXd9Q12dwxztuk2h4v1/k49d9R0TM71OOyLnCDj1KN7PGqiuscvffyT5NXmdmtro37oTpFbH2w6XTU3QRiiDtna26fms09KX7Wyuvj3//PfRnsBn3Vw+6h9fmaCb/xttwIf9X/w1vvVQvlwv+4mqRJv3KMD1dHxgeEygr9Q2+4fqsyeny+NVGz0bDW3X3jlJaeE7t25P1AkvNjWtLAdrI5YHRRrhfBRwsz7c1/4vMktD7HnapFB3BPFHhBBtyZMnBA5E4GKtKPf//73QfBXvvKV733ve+bFri+nX0qz6212ZDMFtBJmB8BaC1CWOD7A0GNnq7m0ti1ttbH1yide/sLYF89fOAMacTYGNKtrS9NTs+Q7oAN2sFZxjJFyu/5o23RnCEmaCtWD9b4naI0JzRQHJC/TDYUcuuPZaHl5Ea+DYwkd0GtvwT84AUVgGW23qeHQxsrKEp/Lmp0gSrFQ1+qJYgmUgj48QNVYc1KEBPoFCTdOsVXoAtdqxr4d0HQUUuH4WxAD+jR90ETwLXPEsyexTPUZh4G09KvEAFop2pSwURKyTvlQFJKZrTiOFUjIBPQxEtKz8vMCpLNNevXFeR9DI2MI0IfdgyaVYwdDo0UkIFDkQC3LkgL5tKjxQduU6ZtA+WyvMkhDYKa/SPPeFBovYxc/UTkcshBe5wyrdImeSgAWjghWWRSFiydybpjZxnI0vC2GFBCbOrGvE6vXjGp22BkMsHY6O7O9WLPxRD96NDjM5taqnixjo2u50+QTjXcBJq0jQ/nF3YOBc+e6U5ONfVeydPtpO2i/WFLljtb6uw+60C9Xa9Z3GRsMF1YCznNNRifsb11MyJd2f277oGO1d9zIfeNf/j+Uogf7OZrv0eLQH92/X5mfOtcZ/tKnPu3M4Idbu3vdzZ2dWnl3e/pg7sLFs2uPlozV7NxM30GrxP57bMTVUfeWVs9UJsI8IMdHIyt8V1+QMZtcFHM0SyFEzzYwOrKwulwYGZ0anfjEpz/VqO3+5Q/ewDe2txqWjfsHVEUMqQjRdjFDH4iWsVkrWaTZKTg4ImUcT4YgZ5oSJxQHPMYa8Lz22mvf+MY3TNC5c+dgIyh3ctIexq1bt8CNkR8eHWE7trDwEHt0gh6FevH569euXGVhODnGR8d067h98dKFxaUFvbMbQbDCcu1PAmMAEeQSzQzwoImI2TOPAMxUBgwGS4g/iXBDM7K3MgckI8R7+02b7B/e/dBaV7GWNjLIBn7Co3wcXwzYA3IwIAFegkal6SCdjU4Rd+/du6fLAM8zkLA6HHwALAoB74EtEfRcBYDPPQdQ3niBUcNkNZvKlTllUJAImqdQGTwVIhEoax9vwn4+DtnaL1PLKi2rTgfDMTCeiOHZkD53Zhbvw/2oBVndeIaNW66vvuOqPXZBoUDG2+CxFafxYnlk48D2CsUiQqIT2Wj2nT9/QfMpd537TSG1k0hryDQyDXGwzdg/DQsYcURLX7Rcg6PxNtB5eYT82akWArJscZVuSAdREUHBIJKqqDmkxH1Gw4f5kut5b27scNzNiK6D3E0yJRnI7WysbBwG2eq4c8rW+9rO2k/ePx4uHv3ojZ0MnoBskS6kc1h16Hl56dH2btHOJGtQO6S7te2jTrlUgeexQVo8tv9V788f9HFOknO1ef9YtevWwmZ7aMQJiol3Pngnv1u790Hjp+984KaX5Z2tQ/fCAEbqkGL+xptv7G5srbvBG/FutuhagHuhMrplM/po16WFiIyDEWMjQzS1bSq9UnGPEDE+ybi2buu8UXv50qXnXn0JRFv07rfch5Nz/HVkdIJOIewxCnlsENYA3N/7vd+zOTExOeriSsqG2lpjZDg00obdIAMDcRCVxt9gEvAQxKlMR7q4tILCYYcMx6anZxstq/dNZiH429n5+W1+i5cXyQNIGMmkvt9orxyUh8uLi840lu0fAAYrPHPXaNgur1o4gQcyZMBcAA94sbKIPQxBNs0AGDEUhQI0Sws86T4AFaCo2XKwN8w8cGlI5SskBlylryCYlnsrxSscW7awfMocBRKypABQmOUQz8ho3IgM8OQsuE82QDeD3GhdEPZ4MT1Byg9Fi6f0NEyykZdAm59GUOm9PKqPUnDRzDtGzjksU3GcI69nyIb6+CyQMf0sn7iZ+BlaZu9ya3EilnmJ2+3D0pKgnD3B2AhbdUfajo6rOCHGYpWHNIZCCTUiVBnh2OGJRamED957T2uyRXjgiWCkaLtcAK/xQmqGnqZgjFI3vRIxjgbdmoSjJcQk2ha0IXaB48LdUvhcS0ioVJvU3oJIANHOtfPTowcTQ7eWVgoDIyxwP3n9lc9/7rM3333z5sIjm6J0iYccTh04Ptvo373Vz2z2gLVN+ImyGX98uE+yKxwfnp0eHZmebLVbJXDNcX2bEJ0rt47yW/tIU9tBnAHb/UeDR8dDNoBgemXcNpZTUS466xz0F6h8SoNDdlU6ebvTa20XCuQaXOq09p577ordbz4ePDd3auRnStrV2u4QLzuDpe2DFv6NpI8WjyYOiXN1rrcGDsNbF2Pn5c3NNSrU9tFOvTZ/5sydWzcbdSvzbmsv3vPYAYZBz9zU7Eh1yqo9CwdjU8O0FLdv321utSfHJ8LcOOeKRBchx5kVQTYDayIMPg5jwYj8ffzjn1xa/oOYlMyiBdQO7g0uLi5wbzNaHaY0oKDKtlttiDdtQg12ymxKOxuHo7UqRct2bQORBCNnz8xjUy1aKtRfYRYJ2SmKAPl87sMPb6kaITCP6kKFtSohEiQUQJcMGiYRJ8Rsq90KGCNOaXb21qm9kkkEFWwKdEchgMdXJCkGW1Z2/qTDbYRJXQCIYOXbFArbO7GsTEhoFBJSeSerz7JXKFgEcSmHsU1P+grLvVTuYOYbZ4SHhQyHH5csTzCZTLgN+VPn5e8FxCUD3ExGlC9+2hTjb5aEncm4qmNeGWrQMOT25F2afBp8noV3OAhmukRfXHHhRah0bXeXqX6HGQNYsZ49c9HWP5HVREBX1sYIuj23bOJ7EnJUnIJBTTMhgqQdHfa7Xt4+m3sWo2FZa0nJsRYOYUEvw/UdrhIjGxsVIfQKewP75erY8Mxa7eBuodPkyGJwbPjqi8/Xdtcb3SNuT9gwHJcKh655OPRVh1NHw8YKj/LdrrDdQFuIB8et1d1tFePIxcGci2Y2dhuMYhkATBaGtMGROI5BcU5kJZqdL9UcZTp2VmhEG0IxOzG23Nqqjo0urW0iBFu1xvDESKveWl1cmh6fODs7t3z/Pg0Ea+/DFk1468Hbb7F817XicNGhGwKQI/UO7B7sx6UglezsuSO8FOgka+Lq+urG/NyZ/r7SyvISmAMbiYhggHbPmFbMnZsHamg/ZXOpXFhdflTMF89cP3v7/fuNXf6XfNE/MzNFOhVXhX1pJJ9cZkwePXxgmmjB1VQulFt13kDq6Pvo0Fhnv8uc/KCQn5ubtU+IztK4UPwBi+FRnucC68AKBsiyBQTChCCe/oUxd6w7xE2xGgUtD0WgKg1nFsC/QGJlPOUCdK90AV3QF5kdEV5aXaEjdUJCYQg3tHYrPL/J7vMgQAAJT7W42RK5YWRnP4YaWPlZ1daBkDAWUlJUpEy43fff/OPf1iwBevSeIqqP1CfDcebD4ySzl+krEQ1K5Xr6GQCaqT2CvkaIofFfLz9S8fP5g2vFaX7uOQMJQzGjQXA0kNBhYiqtjIJFJzFJhMFuCiqRIUmUnWrInvLjjLFajPNaP/c0mll74nG6yyrupZ+OIAN+preBruldtuT0uW4+DhGPNvCmkVl4c11nCG0sSdfY8fGx3//93//BD36A4lpNxPKiGb7AaJrcT7q2vEoP94//q3+ETXG2BR4y3U+QLT215rX+9tQYN75oQzsO00LGGCb2RWjcX/7lG9/9/vdgNDW1a085buAkBqAf7fMh1z9YGQI6TK3whC9+8Qt/42987Vt/9G9HR4avX71C6EfRVYT0ELGcReHISAPoSL7zne+YVgXS+6GoOoL5sDalt3do8FOf+fTLL7/6ox/8WNUmg30JHywOfMqmXuIcUTNTMdAvsFCAYlSd5ZnpuVu3bjuX9OrHXnYm85VXXqb0h3K/8zv/fHyEHUjRiFn8/Kd/5z/Tqj/85p8sL9cuXrg6PjF65dqVt995c2npEQuQr371izOzE2/85V8sLT+cm3M2rd8K++rVy8yvMCiADveMM6wzY8AMMBtqMq9+gXuvtE35uPHE2Gi2XxdLp1AuZNtRTqttrK0xHIeB0Z24Eje2NFmAWCRTgmRaCfgevFY8bBSd3MGcWFAwpcQ0YkMjdBZMyROSQzlVBIZrz3H4ZT0BJSDzm1/7m70fIgkuRaJNTwnmgq4lQDHl7OWXovSoIMNe06D1Aqadivkr5ifUZaQrMZ+Tp5SkPjaOgXyPn9SzjJDgwker0JKU+MTTaEr5aGN6+PVE/tMdTHnSUwefyOknhDfB5LE01nKmcfCE/O3214nDN27c4GII+4IX8HCozB/u3vjUKDvJz37x09b6JpsGX/t9fjooH1I7B4AdSTfOKaQ8jEXcUG8/YGntkZRiuUhGdqsHsyr7PQL1EE3hfrP74O69d966wfSYnmBknPPR0OArnMFkZbCyvbZjJQJGuWd7660b5DF7j67tosigwhrEV/oG4tRp+2hxYcnSfeHRfbZQzt8BcpIIvMLcACuJkewDuwwGAYWbCIf03HvRqIewd/35537j858fHbNOa5eH8ufOTF27es6yP1yR8FGQz918/52QxY6toR3Kqduc21ovtnbW6YOPOcKqre43ll944cLXvvrJYrmwvrro9uPLF+etJxHjNC+6bAxNRB/LvqSLiW7Gvrwm2b30Kn5m8hUkDQNJsgVGGlo+MlNhz8F614y6AolhHUGKAsbu4lgF5VNyCjA5bPnDiCX8GusvJNeAmN9KZTA/iHKGFJxU/Jpm6oKzxD5naicAC62gH08ApZ8ZH0/ZTj9DaO593PtQBNykbnumdBgYFZAis/BEFQmAPHv5QSpyH3kz2T2wEc1BYUKhHM+eNJ9kehgYZQatiHF/onzIEEU9IzyR2U8teWpebUot9DZF0hMyi/gq4ZunuDmTjTBnPpKkgZalPoJm+Y2qAmlDUCezSG52ezzhh8rCRHz3u9+ViO3AgVR45DlFQXFL1zlBwqjrcUjlK5nePOkYgxBkAYHGUSGM0gTdRIvtDbz//vvr62s0JR9++CFBzHLaKxEWoDmXxxUCgBYXF0PNcNhZeLSA16aQ/qd+wvQo5W/evIm4kHEgMMajRcDagaZO9+DGjXfSJxMT1ZGRqleEbZsRKpqcGDt/bp7Jy+EBd3Y8Ox5j3dyCPtqmVgjLYmD75k9/yOLEGYbZmeL8hQFK6rPzbhmZ7+bO8kZ35bkzCjt7fgofc38K0ysS71DlkIl9cYBT2VBvGhYzhjQkSDDspkeTjINJMTv4JDLkeA1f/8SrfpcTCKGbCVnLQWQYJVTsVgstLtss7VCEkNJSOempQBHYqEa1qE7tfpoBA2gkpQsxAaeCT3qh75//zv+QfqhIJD1FoFAv06lIhjOZerOXOX2iSimplvQqpZOeezlPR3rNkpiC/EIG9DqpMQrz5uSJjYtn7yOXxmZPZyik+5lh76n2J473uOyf/Z8BSvxMn/Q+TNX8LN/j2BMZetmMj7jSdDw99aiHhEEgsyBiymXQL/OhVfBTkJLyEAIlyuutKQfTkBCZTNibShbPcAqe59wCEwuzU6FHPbKt76JivVSnHji9DmzAgzhwlEyQsQqanJzAbwn/UBcZwQnViyWOVEZYmO83DyCzPWXKblIT8q+R9tmi2IBtC4XYWANtytQwYpdEtF8vdBM5tkXmp7EBhWpMBDri3e54dcKTlZ9rw7udg7HxKkWje85nZqYf3LuvWDU6BENG0AbSxtr64oXLZ/FSqLzPrKyv26jXRsaqULG2u9Wo75Qrg3EI/PhoeKi8td0YKs9ZbKfhMkGmKQXHl7TfUGu28c+G3dnuUFGm4YI0GgaUrB70wsE3FsN0cg4FKkHXzIL0Rn07DKEzQPRTEBfkeQwyJ1igZIFokyIKV5Fn+pk+TJ+EG/wUU5BIevaK+0gkaEBwqiyc/sSgx5ssiKSaRGI74ecxJP003L3MWWEnnxgm32bMTXM16eRpfLJVpT7H2/SMirKb1URSsb26lJOKfeKZ+vtEZj9jAp4WDFbWnniXIqd/mlE/eyOrvWYX7MKoVEXvE/WaA1Ol48r00yuNBJrALv0kTTlKB0SwzTR5Su4F+Q2IE1r+S4lpUsWlKBOsw17fWoNpLaAhi1qaKlxjqGEyQGFty/FR4GpmNRTmFmm6NCyUH8Oj9oGUJjO/AQqxWPc5oFSOvX4lM2T2Nppz7FBf2baB/nqbIXZsw770wjVtI3tLlF/DDJQygwz1x6XLVm6TU19x3YWdcZv4uasX4OSFC7O+pQWRNjEWJ4NiVyOODIAiC866nRC7wX05Aq2zHwqhvOmbm52gL1vb3CBmukSDyfVBO1w/J7RJg6Mxg5XQvuojQI0V91DYhdpCcFpQuhvh8C8RQwm6qBFtdVjVhfSQxwMprdqloQphu9kYjIP4mXhiDJmSiftOH9UiGBPTmhG1IVVYG8eIPeYeEQkGkrPJKT29CvV9ivWeKaIUkY8GQnxCQsV5m54i6jZqKfgZ/cmC9I8WIsVwP84ewuTj7MoJVaoEiYCt9+y154kCaU/lEZ5oj8JT+hPPDBB/lv+jHz6RPxXbq7QXMfTplYoMurig7eaeSKMWQYrSdC19JeIrwU9AGfmzCTNzfuqguRDxYa+zTzTGz+GRKoEgFXj6qTRCrNqVBo69gt7ycxynXhG1aWpWJ70BZYnznH2UIj5Eo00H6MQUlZBEDCVoiU9CNnX59l54EB1UgPzZjZHBcuMensN6Y5eKksQzGp4Nw5zlwoV5bznYUjXBHP2JkwTbm9tbGztbdYtGvWTO7YThLlOXvR3bJxMTo9WhfNhdhV91hImrOpDeZlbpkBLKgrppvkbC7crQIPdLY8NOjzgBVsFs3TigRpY4/fm6E6wxyHbJzEBAAQiMK5PFbBhQoMS4ESa6bcqkbJZipoQQMsxRbOL3a63lN+NVAOm3eTG8xNfRkQpaIBioFJJcY8p0VjD+nhIFZWYjH4WnSALL+J0FiZH+v/3P/+NJ7HG+9FNZKfLEM4Yvyyk9RdJTxSoI0Mv4ss8FicDqiZzppw6kzKlZqfU+KQ5k5wyzWk8DGere+9mLyBXr6iycbkxKeerTt09tfyrzo5+kzKdrlMdPTRXpVSoiQMJI16VMStGpgKSMB6KmCVdTim9THhRUiqHwuQx2cns/Tzem1zwrmayqeKQS0k/fyoPJmHvpaTqG3BaTNUdRmUYtrTKi+RCe+gETS2gvAwcDMWO8kjlvlvkaD/2XI6Z6FWqbQNewndD9zOgx+7CvsRsKxnnOYEYqXkGVTDuSTkUz3Vq1ux3WUdkGFd/VF+cvzc2c2bGi3Fq7/+A22ZKeE/65/5ixYmZ1HU7uXJdFkLff9vGPf1y9CIpyjC3o1yQSrPGEFU4D6qkMMF/tWzs1Fgt2rIwGURl5jCZnIUkHob7MmHP0i1XGPiKyD+nE8bA4txPc3ijEzNrRhYR+J+eORozKxqlP7VRpCj4TUUOPqRhccc0TRBiCeyvxdEhNSk8VPVMc9e501l489Ta97eUR0RQdk01lnn7qjyaipX5+NL8REnzimTJEKx3WdeLqMfNMkfTU2d5PkV6Q/6mcOQ2Nkp8Ip9vsVe+nAp/ImX4+kaGXrVd+Sjl5OsULB7KFR4ICvRNRiMlI0y8nSCU3Agt2IbBOioGS6FSRnGiqJxDXAK9U1HtKkdPbNHQiaQBF1AtA5QyRj1OsbPDRZY5TEpqJGyuzioxicvReRGN1JehMcixMG+HTO/PvoCjFCrErnC35PG3PClZNBDkMDTI4Hzc7N81wWUeIlLb7fILvKdluFoTUmJkLPFZMY2J4SbvB40s4t2bEzEjpj//4Dw8P6u2ZCXvvuuv0oMNes1NTaDR9leftW3d9qJssdihZeFw66LbzWBpi0e00d2O9ramezstfOD+9vVfLkDBAK9LjQG/kCbOy8HfmVzrdyzt4q7bjJOQEemg1L0+GgQG3PtD7jF3rfVw2hOXa9NrZazDHdqDuNGzob5opJQimRu+MYQIAKUbMU7EpiGsYFPVV+vaZihn5PlpTSvnZx1n1qaAk1vYy9CIoUcrw1K9OGnEK4Um1mTjaK+AkknD1yVSWbjR3j9eoT7592m9D87TkZ6YZwae+S+Nz+ikbuUXwQUqX0os8qxxT8tTyJT513JLGpfe2l6dXSKqxVy9YfPxKu9C7rDtxDjNpj5/sHf82KSghVgbRnaCSVBQQz8IMsz17fv78+fPwF5hynWP833zzJ5BQXeRGQgDkCcwv9C0sLHDu5FwSGmEEoNP+rt3CwfEJTmGKO7XNu/c+WF91fNyZp9b29hZ2TZnkaJL86ALFpBt1qhUu2OI2CHKq9mCACA3eCCSUqS5NisJLrPqKnJEfOGnE1ikOo8f2IHKgLZ5h5BEh9JaPhx2wDRAWjIzGy0xc9a2zlyiGVMUSBFQaKjda+lw49bXrllhRWj6kbGhoD9PkT1RSmVqr2BiNjPdG4bqRWXT6MIXg2Sn2BD6k9CcST0/5E68eF/iU/7VJqvwi6Zl+evZSHpd80piPlpIK+Ui6ZIU/nYl9JHMkPKOcp+b9RYm9clLk5GcmPTz91TMKe9zxZ7x+jIq9bOYyZU0pvfTe9z/XnnDGo4hY5GT6ZvGUsadt+rmhs8vcy9ErMEUAcWIp8IEIDZGiXCcDC33OjYOqROCBDUaiDYILHXxigUrPCTmZXg6V5rh4ZmsBEHlaZD9w6dIl7+s72265wS2sUaHP0dGOlkADpwW4QbGlKb/jPqpQNcR49OihBmiJgz3Wae12eDyIdhYChRhT4YA6DBfoBQ/sNHJJEFcJDHAGzbGB0pLcDv246g2/LixtCnxPhzU/vDSqsC9WDMDWb04EWMfHSIZmNGSJLBgT/weidjpWs7os7tuEdQnxEsKjCNINjmessK2xM06YxjZpzCLude8poohI/Uh6Rk2fkn4yCtknP/fIZlnhepKeqcwgsychRdJTR6Q+lTn08j/+zv/2V39FJDRMp77/5dHUoI/m66WnyMnzMZKfToxmZr36aCG/NCVNyulszoqkn0/MVy/Pz1cdmhhz+PN/kTdj2L2PTkdifB4Xko25TVpnuLKTRAA7DrBOToI/4M6jXrGvuLq03Gow4rZ1wYsOeXLfZPvjiZD3RSZsErlBd74aA0g2ww3ni7YaQ5XSzPQ470y0HCPLI7XJk/N4MESriyXnKgZXVhatbHSWRpaTk62dFfyHaeqjR48OOJTtDEEDIK4xnvjVyMR47CsH/oQloXsJvIQJfIXi5Cxv8EVSb5ddYvQyLsRNbJCVHJkzwNuB1W62mZ7hTOChQcw0UqEqZTqpb44BxUUP4fAlISFP7WnQjB5kk54QVV+C+WbbhsqRbug8+8s/U3w+kxOenpY0K4roTU806xRb66U/8VX8zJDwKenPSHrcE3Ul6Ok9DVYv/rO3sXP6q1TxqyLhM5oZE/i4qT2QPSEsmpNePZHhWUU9NT2NdnqV4ulpaiWeTun9fKK67CfC7H1inqefyWdPKv7nnj9XSDbLaXSNG2AiXFkK4mziciatGyYAu4hsWkV5kY0wQZa2drjR2IMtDN84evCT3gVLtPGoSqgCQLe2+8cnYhV67fpLe826DHT6m2ubNDrcDbtKabe2Tn2KGU5OT5ED3W6NHJw9d26vtXvUPeBmAEOkV6bCLfSP2DzZrNXZkkED1vtBaA6PD51zYBbPbU/p4LjtEhjeH50rCIeNlm9u7uKQHicPT+q58DLBXTSOQjuhO4HMtDERImY16C5xGCXE22zpDqN0B2r5VsAVBYOTAtYtM74niCgZvfAhZtgb92cukHqczQdyp6fICaA9TjmV/gxUyJI1SM70zAoJobnXiNMReUxQxm+feMr1RIoSEOnTX//yuGH65Zn+Cjmydj6JbFqjm702nc7zrCJ7A/jUDOnt6TyGLVP4RUVm4/GrbBRiorKMg2Q/AAAxFklEQVQmZWu5TPLM7P6yD35WfpJBelYw8aI3FzE4ySLE5/Aqfmb5gamhs7vN7JM2jRYWiPMxzeGkXGHwi29AAiZSYckURmCM4MaqY7Wtnbu3740MO01nt7D08NEiEx08ZGx80oa7IxHNpRZPvDCCx4PK/1vbvT5ZdRwJAhc0NOYNzUsCZCHrEaPZiPXq81jj56xf829NzKeNifU45g9b7wfPCFkYjIR5CWhAgBDaX1bem11d95ymkbVFU7cqKysrq07myXofN5yePPeDS6+Z6bl8+U9Xr/3JjM6TZ5s23Nm2evDwkXc2TrCoD10F5F4Myw6+WPyVpZCDtlSboCMMRw6csLeas2kfG/YXeF+0Cz9tSDDvsw/DDnk7RASdoj194ruiNoXeoyQhG9Gq0Vc/4oqgqIY3lylhp3pY1mhtH2O0OzzGq+2QA13SMU77JhfXdDAGnwIIStW41lX4lF4p0WF2urctDUbbRyd6KZT5OJcPdetNOcATfQCKzinVnJnCbnIw+Pnglz3S7Jfu5M+QGahuRefK3cLYHqoG2Q6OIXtCkuDCLzVoaX3SHJ05eBVXCItATERFMYiDpF/IfYkNx06tlZ5Ca/jEXGasZwGMfoAbQtDP4vKlzGSRKsJFwthkD90A7sTxja+e3QIhi9Cth6dVZDvZARn/esNGuf+0jHHRcffzF+1EeeK7ayyLAdiaTdXPHj+JXT5xQs0VIW5zO7nx1qX3Nk6d/8E77/31sz+Zv7mz586LzftfQnv8qMn38w/+24fmxXVNLRxSOcoWw7zHvoBsQjS+k+SAiot57ZBlKJnGz/9yLU64PnrshHb06F2N5Xj33q8eP3AP2pcuj6M36FgajK8vaVkfC4xuLdWOl4thtcb2snJfDhU0DYON1Cg2MIWfz+VjIh7pqCuycLRbTuSIYmTZ8vEbl4hlPDOnD6L1M5yBigZydPiDTz6XgXqMAVpxiTNkWcECUA7Sk8aqicZKnphNfRUX9F/F4XkSvehkYOEvUXsgmOgcnWWOl/xWdqQ42K0xF3rYZ87U9JUcF6FHDyLdbttqO5EYE1roS6kifOZL6RuFIUze+OfOvW7NxbQf8XIigcBRPOyRUZLHVhBZ3wtqd3g+3XjjDR08G7MfwbeAxWrFEYQXJzdO2yoA89Ztd9c/MVJk0Nb3H/279z90lT3RV5Z+73XnCR89YDZ9cwVT5i5sdDPYc3USJl2Nd/mPl+08oCk+lWd+9pDLtlyIeuiAAZ9OoTvZ2Tm7vXFvyOgYKp8RdaDEvI1dQY5reTHoKsdJ/HhwsfZrWisbBNyqkg10mAHJ15Bm0SZeAYmTzyWYaU4XNIxhW7fQMtqNKsKnnMuH8traP//217GvNV6Y+h8LH0TzaPjet0Ibk0Px+KHmPBGMiLU5I78Ijb54q0SkxORb+PBcZRu/y3BCWoUxEXRGV1I4JAT9LGN3flyDGBx5k+zKV2eYgbvdb22FStJJmqEcJe/5SHBbgYHzjHo8k/AEZq2r7i3AFAbvDFz6TcWjLmHyFOYxtjryMcNmEZ2WGnmCveYva9QW0bdqF31QOOpMv5OHEIzobMau6EMOLPrGuLNZpKl1rgjgocOH7ty+YyTlc6Zm7/UEccJKOmtL1q0sBsW9r/kaxM1bt6mwQ5DHYyPbAfJgqEX3qOvm/U1iZJkAcmsxt+g80+9TtEvvNk6cPuyWwbfe/u8//B+uDza2uncvvmMXN6M9eRZ3HfmMjk/s7V+/9NYlCmxFkn10CpzlPnLMBKqh7Bkfm7dHz3fy9IwPHli3HcfihhuB1M1MrIstVc0HutoNRmrfXjQmTcPgm5AJ7myXpEJqT6/iVnDIKmje5enTs2dcs+yYtN6pYqBHQ/IsM5pSMlR2aE2T6a/HgGJt7b5dGZ6N+4+Y7N/+829ac1OOaOv0g4VWZIOQEvDmhxRI9Mdt98MQb4cgEsqmAtHWXqVRiebbj5mQEIFQRNTiOic2UMFAq3/BfrwCMBBzyMESUQktiL8mWtt876ccHQ9+lNSoDD4yIKt0Eh8cHTghUl5yLVz4eGrUoh6NRngy7tJVltUACkkqkyIaLzztE/0jhaTvpZkQfoYLImMMfeKVF60XEzXUNci2cF4DvqTTqLWBYEtVqXgjhizJQAmpU2yBct2Jp+RVb47EBaNe1cq9c+8Oe2VXHVnzCUFZGbnI4qtJ6wfipR4fEfr6+tXrjx88duuxzyWyUIyVv4OusmRGqWAcvA3RNX9he4xtDi5LcFWTNQXL6Yq3rcWl+m+/8/bF8xfOX3jjwP51M6qfXL782V8+0xHVW7bObnx43Gcozp1x0QdhcZvEzZufe2g20O3RF419jpbwXeFr3vKFy358rtxok8qqpf3x8eUvW9qsXT5xya/7L++6m4e+m64hU9euXHvsypEvbRmwFGJLqaPZOtIusIp+rZ0ybuMw0eKT9u63ObFxzKDya1/nsSby9LHWC7k3uvRRd1e2RZvQibU9//Efv9eCW8+4SY9o9W6FEyF8D73dkraKX0QyaYHQtjkxbFsUWoJoQnpkYcoFnsI9JBV+Gpb0B5w+Wt3sHig8Z3ko7YCZUV0OgaFQ0eJnMtcqcI7hpL+KX5AsqIqb4z8RVvmscoekoj8Eqh0GfKqlD0ZTTHWeP3/eNKk+pzkGcI4ps1fm6tWrJj/ViKFoPdU2zdheW8tS9m7evW/ukMKZgTl//g27bVgUm1tsQA0bHTuvvOiiJ9dOltroHHtQ0IxiXCTCRe/L5QRh1ryQFG2njqKvX7vmsus3Lpx3L6ghO2q2mZny8e1ZC4CPH94LJbc3xnpBbGf1yU6nB+PCkGdfO3UR5RqlMlm5CcETNs60NcJec6l0OKx5bE8L3iQwgIpYMNaOEQM6maX/GUXHYNVxwvV7jzddiESmHfa19OaF5lw+KXz25bMwOLEx9bW13/72N0gPTxokH/YKnNUKg7ACX1BYtvXiN9637W+yiAFZNG1a+qupCRmEY2e0udTdw7Om8IdyC757UpOYc3QKvhqYpFPAgc+MSh3ghT8ZqFyVStrIKfkDIV6UUIASgkOO7lnbuu0cFqAotokQJzU0qm0iaZf8hC7ohxkUWZAwx0GaobQr76LjZ63f8BIQmgV5Ez9kS48nzGCM4Mzj2EDtggvXB7uk0YGsfadPnfZmOPf6OW8HiTSG9bQYsmlwGQsD9n66cGzT1EtYUh1ER0t00NzksN/2Gl8osefW0UbdW9fi2P1ng5o7b3yO0tvkkDUJd96w8GZ3jTDdYsr4+yKWXQrG2+2g8r0v7rlT8iEj6qiKuwX0z9m4p1+5y/Qh821oa88OExbnEX01KD4V6ougYfFbH3CphBp0eNiaLx/ANrhMTQlX8RN59NnAUMIJSztitjj1m7ZHmepJRulb8tQAs95kEd8a2BeNSDXLtya4m4xZSl9WH56kMPCZUZgDfI5O4Q9ZaKAs1ImOwdHx4xhGaAmhliYeILCBbAKLRT9LCSkVp3cKx6oe2ySj/qFJF8bTt5AcD6K1h9xP2i7CQ0FxQb8pc+PWIIs+U0WFMEZxStiHqXWuLT0wp4d9Xvj4cdr4+hvnz545e9KHE52Nfu21GHBuPnCdervf8BtswZRkjGcr7J61Q06qWuvANZutOxz3ca87/xF933jvxNAt9DdroGNkTtdQ0zVtaqI7iancHM90xzvCbE/cq41/81cO2i22sqpvW1CLsTuK3ivRRVAfY8Lf/ObXUuuRDIEh6lE2Q7/VGVtBiLLKpRnUEKE68T5b+AlZ9aUHzpQrGRJIB2sZfIXfYniqkFmYArK4xBCtl9Rsnu0JSWE7bKfYKp+rkNX8q3xWuUPSat4esoqsl0Wv4IS9aFtDYwPKyZNUiIzCp3IMIBUjuPSQSzi2tRW/GdI4okHgXcViWvNwfCTDPu+nLMlfb970AV/jVpLtwiZ6SmD0+dyb1g7ZohFLBQoKR/EsDLAm7PCLUKG2buAOHTeDnDx46IhFyHMGha7jP+ECSB9QihVzKoVTA2Rf/nBBARvnCua1dYvmcfmAj0vVnyrSOr6JH3t7wF27KG+cfv6eb0SzFBGmck3f4vyQM9Dty3pucDS8jRdQPK+Y0bFsY84gcOIbDhYVDRPjU1fap6mjxZ7sjuYDyMecvjz1VLbgnSWUugUv1O2BULM0hhha4mcgWnLKTUOX+iZHZownMUMhqc6lJs9TJe8ES2pFU0Ar75RhJY0IrsB2AgxtW2xXYMjcc9jz2YdlqejOdHrMzEKj2CVS4VVPwSAwJqyPLmUuf6kgTDgxbNu3L5UTMkwIdAaQFDihoOhEE4jZs9jgavXfVOhDn0Oj5MaAOnz6rgJRaM7JkT20Qug4bLSd5k4Nxm2iofAxY3T46M07NrK6acK0UNyXS/3OnD517vU3ZHQBIc13fOmu6xBv+0C6L3jrTxqauefCt2JPm4U1IPSgWEWdYUXQQ2sVDv76sNzt23du3nTzzue3XY7u5p34QqFFD71YMzh6mPHZCh1PHQS9aONXJSLlGBdL6SyGoaxK6QNQS6NRkDYRZXS8b5slVLmo5rKiGSi/BZjUeA0k5io+nMGhGKoIut0SxhNbJvWBIXtFS3oyUNFC+BsDVZc5OkO5r6qEr8pwvQSHpt6ZPakDn1XuAH9pfbOgyk4ZVLnpVxxcJHPNssWtYXQMMn2jCdgWJYi0ThFsZmpv4ggbitnDrcenu2cHqcuUBIyZjLSIAam+d9+ndB/Ed3lZEEponyc1NTtN8ARiFSbGUkQfO2ymN4HctM54jlqwLviIa3N0L0XcS2UFfv2AnW5nz5wzs0o9dJcfbj5yjsmdNF8+8YHyA2fOnrv09g/eefc9/9+69Lawvu3JjVNHjh6zoGLKlKW9fefurdt3fHhLxkf0N5oALzG9Y4rXJjtHTKzB3Lx5x6f0pPriideU/VNx+jImqNuFme7LMl1sfWZ93fiS3fS35/e//1029/CwS8i2wU1rx9A8Xurb4MuJnCTV+60PGoABv55uj1xoA1AUfmXJQEVXkXeAJBurCCX0q0lVUF9uStUq8hyE/M0lTcLxU6z2TVfAIRfekj3wDMz5mXGuvqu5kiCjpwqkgmujrE0toEf60UcfEX2zLJSQitJVHMKkhFeuXPn444/TVELWIYRmBykLo3unIDzw82yReQ5dNaJlPYA9VAknMO1T/fu/j3u+LZPEuIsixgXJ4ZibmLGkwRaMrLbFAaU4FH/k6Amlm8CBw4ZaKrQFxuwoxbQ00DiMLaB2Bdy+7R7jB1eu3cCSox76k6rj9QEnGVMRRNQLsn1tzka6o+3PV//kIGI6zEBWNbX2/TP4RsEJUf34kJMLSFm+1qlUfa8kjRkfbo/NA251Dj3i9vz7v//vRWh7j3H6Ie150TabLjJnxspe8pGBRdS7q7khtc/bhwutBwqrarR9m2rLd3BGC21bocuXAhwI6Wdgjn7RGQJkLiEDfTSLVJ9UZQ10XjWqjrL0lDOacOGhasXMAK9yCyED3xWf7777buuAxXRom7ow0R8rCsTUIyO7ly9fJr4UDJC6ElpJOpAEVEa5HHHipJrkaPWKWRCCDocC6FVevHjhvffeP3PmtAUJB4W/emIX63O32UWW2EJgGcGkDuGOLXUmOK0FUEJKq47kH45FcrteLW0qjmY5BuEnmnDP3qtXPzM9k69ImsOw256ODt5C42MyKVQfY+jIRSHpUmxeexgX7EcRlvr27vXtlwaPC4XZPBS8tuzMcQlWfOJgucMGz14odtbZZR4r5DlgnlPCxmU9wWXAYdDgfloJEykzbmVvSjgAt1KXhOt3Lim4b06LZLtkVP0nyy060CCkL1DwKnHngHITITNW9oHgKnxnsi9NfSn9yXoNQNFibAgU/ZdysjOC9vn+979v8RBBcunRhBHYv58B5NNDQPbwz3/+s6hrrIyyqBazwE/2PEC80UM+xz6YXLT9zVyIrWc0FoRAX7x48QeX3nZPHPNEb11hSs08fcXp1yGCJrXVTaTeMdDy4JAyZIyFvudHjx2hfm17alxHwPAulGqTGY8N3E4JQ8NkvlPMOWHeuFcUEDLBWw7FYtKlmYM2GRp37aSuxrqoF426m+/16vny8YM1d3L6PkycL4leupJRSd2mR1Ff64S//vWvMiSSgd4fH4CNZgFaGLcxdSoee1pwnHlit0YUk9FJP5iacp5uutYWMaIUyKc4x7/UpJSB9At5qpAJGPzKkoH0e2o9QhU6QetbgXriRaCvEeBkoT2HcAY6k1mK/u4DxBcyMaUMJAxZ8krgaAKJFM4+XioeHmiAVQazigSDpjCaHhKzBYLFpWyElMXd41YM2udGHj98/MXd6KbGJ6Jcb73fzYhH9fYW+4fs+/HhJJ3SuOItxqLxbUuizgtLFRdrU+awmGQo+q86sYSDRfrm1OmzBw/rH8YFpPQSG/Zn37t//78uf3z9s8983MwV93fvfQHoO21Ms6u57HSLeU0VdfebMath3aFDPsxsVnbffhdeeQcE0CfiTm2c3nxwH8O4aAfKSH6sLuLM+6lN0LT9T7/73fS9o5pv4km4TiIke8ISJnI+6fSjHeOFMb6MK3WCfvA7CV6c6kAtHSQBvipnhsmiMykx05+uV2M1kQe/8Af67a24kOyqkUDCByKiWfoqvPKuJoEMhfZ0+koVZpVSgaI/BOb4nGSjL3dAUBD51oXTL83P7tE3yqO47E+mfjJ0n376qVGiQw4oaNXkh1LA5xgH8ASimS4gcUHPN/aSmW5FhHq/7nTTmVPvXHrbeE9BCFiar54ha4MmakmQFJMQaDagIaTrSNPE+bEzgIFqV9xjMgUp8/IZNGca8WwnEOK2laqdXamnNy7k/BROvHeMBxt9lGLKN95E+6JbLnv0V33j+94tw8UvfMH4kW+R2KhKFOj+N2ddkcyOeCt883ztV7/6ZVUeIeGKZni7v7VYvx2+ELJqOwEIfPt0Ykdus4Hpb0UhbP+jge0dlWS2+SqW8So36t0ciN/yMwA5MTNQ0VKqTC0/KVR0CKzSTyEeiIsmZMguOgdfxUzIKj8FKVIZqGhmrGgGKtcQ+K6UkNi1fmCcNqQAxlRkkVLRTOMiqfnghFPQ735x12yiRQe2JG0XfWAWYvIzukjtJRwmMeYUKUocdCATRNxtrodcRrrH6v5nn1839DKVSgMM36ytU7SoYPuuho6iKAWhWYCOD+rasrpthnKv+bGYxnFhML6jB2sLeFsHdNbedaMHv+e+8BMnjnti9ny/9967H3zwd2++eREpKxNXrlz95oWPF/i0OTKxEwgb3g72l2rt+HR0rNS7bsuiiwu71plY281NtFrbt/oZ6y5tr41VfJ8/cBjF/K053OiO9k+9HtWMcOykhPjgZCw/4nP9y6kCZDYLNpXSrhLslC1fpT23GS5I8tAzk2QLYSjlpfCB/pwQV7kDffjRGiturtyB20JDIJMyUNFEGIAwK+MQqIwDn3PROXxSyAxSsLRUqXiUUHGycBkgr3COucPJ7rbnzx2fZ9YojyhLAj+NJ34rl4xMF3ViJm1PgxNzoS98wtXaHNHfvHPn9r27X1Awu0gtBlB5xsknCPI9Ww1uzgYkgHGSIOjTf47a+zN+pe5qDUIncaUi+tgYwxLe1CjN4DvvvGOq1tq9d02s9i1suckdK42vHT7ic9zmUTfv3dd3tYXN8NJd+usWOc3B6KnaNrBx6owPfX3voN3dB0zfmtswJeQlFZZQXbPpM9D74yPZvljfp6pbH60wJSz6CRyihblInVHCfJzyZoNqIAG+p1sEM1DRYikD6cfDmHKVazWxknr6nhnMHpLRKnSgMwcv4gN+wYciEq2vEYgotKGIjFZ9B4LJ/1DoDtGBeGHqqhEpwsr0mdvgKxGEEEuioqLUT3aOUOvUEb+cwmkTNzHpTcFsFvNshTGmNk2aiKUP4jUL9s0eNsaWGmbNJjXGigQj75y7W4Xvb9637aaN9xykcHcLGxVzJHFfeNxWGAsnDE5rgbg7jgyxijF34yurFiSbEW52MhZBTBn5Mwi0Bo+mZ9yGeY3ZAwdOnzltE45zF44m7T+wZrHhyFEDwqMKEj3kvtTDhxzgt5nGlvQv7n7RVix94FiqEycHdWGzN6tLioGcMYkBlZrXE1qNVnNnIHY4xHA4xoW9rxsBok9g93v6UptoRIMm/Qy09h2obkURaT2SLUiGNIUAOh5q+Tk6neQfMDNmoKIj3V3EJ+kXccwUwg6l7JA0yUJPsy8C8kAqowUcopPEvwWw6A95DY2YDlLOYhD7a9euQWAb0/R5WGlhsgo6oKK2WRPGK9euOtZk3GUfp8kbQ8Oo2fM1txya1DRmiSNue/ewdV61YUWPuUImPieWfSXv4OfP4xY2t2hvfvLp9c9vnPE14BPHPvjgPSdh6Z1DunDb8DCuDG6yFwIZ+keCovdK6W2vjjtabao2iovJFp80M7cK6NN68TIIC2mzS4ykzKi4N2C/l0V8Z4awIx79Wd8o90mUJ19ZzwyicZtTTE1pkH1H1n3IUakOCq8fcl7rQNRK4T5bc/z4k+cuhlx7/HR9z7/92/8amjWjc42eixuYw5Z3SvkxYaRuK3CPoej38lH08RQtmz4FXu72rlwZ6OlsJbVLREoVwYts4fT0AV0gW0l9INF6yM7hOfxkYCgUKc8yCQ5JqwwnGsmbZGCu3EROakWzAlKHpEnigHP0e1J9XsrWRyus50ZtyCLN9OxiL3XdeuxjY4ePONrgCzD/+fF/uWHJCh+I3TOW3+22pACmMUgU62HolzSHdmN1EaQ/sUzfbBfbaKLD5u2NjWMX37jokJQLvy11PIwJnc04Q9wmaTCTqoVzE6U0JcZl/XcF9677OtqjB5teAY4duRjKqnt9h/DosRNMtbwKVjV+0Hnh7maHmMLyA2JVX0CV+Vevxu2MFjocWrJ+Yhom9tc42e8lolQ29+vni6nFaruXB+Kof0yo6GaG1Wq+eJwcjHjMwFB2fqbGjZfNtaeYXabQOt2ObNb2ilpYy2a8Fn3jgRNvpQES0RlzlxKzpN/s8dLaZ9IqqURehX87iFIQTP9bU5jMuAOfQ9WG6CS1/3/AP/zhDz/84Q8tIdIWnU8FsWZ8Lxdm58HXL8jo+++/7/bEP/7xj5988olpFttWzIVaXiPHDq3TFtaJGsvlFaY6fBIvwFk5zOhXX8VtEfE/pu6+pnDU0mqkT8ScOmG176QzSEqxrggB5WfPn8ZWMnOk7bD7vTu3XGfRgmE+sefDexqZgeP7yNreZz6aFmMfLi4+7F7ikLlsQ/fiMMtWCPHPv3XrNgpU1dLo5zeuqwhVPHnyOPzP7jrvddsGHZZTFbi1X/7yfyaVwVfPAdJHUxRKICoAZy4pCQ5kB2TZdRP6gl4WRi/mqaDtkr6xwMto7iq9r3KfYahgJYFXlr7Wc/geeeV9pcBkO7wShUnkOT7n4HQv5VjXlCLRDX1Cgog4+0AWCR81oEuAXEzoty0yUkUpmyjiBlGaIlusSWzooQA0wHKhIuH2mJDVbTSBaSun1UWbAkwy+DOki1HUcmKRdCNjeeP5a66foH6WFvYbNpmwZZDtXA2jotfXZCssLH1K6+fUoNX5Zv+VF5rcHP79eo+4hMZ4GGOqbJ7GqoxlDHCvgCtXPrVpwfvE7h/vnfv3zC7dl3Htn/7pF6q66iafxA5AFBScCBmoaAIhCKTf0ym0DLyi6KE3rYQDWSUmJDrR34Ur+gOx1QoOCAs2lm01hz9Hf6BW0cRPakVT4FXpVN6inIFXhTN0bIJZUGKqX8oMosCJSiKvjBL5I5pGRy7VJ9aEm8XAMJcKSVetUmTGZCNTSb+MsqTLMM1kLE3MNJ2NboguI4KmOn0X1XoFZL65TdvbUFOc3qOVdmUgCz926MSkjHxf57sD23jgslyjOecmsrieK+HEwYNS1FGlbPGxdoqOl4CmkGQz7LlzZxXtNAYmgpNY5z+09otf/DzJDX7Wec6HLCn9DOAykTNQ0aIwZOmRhQt/zhK2Jh54FF3MpTZ2ok+Lkj8B/YZGNfw+EL3lKVcMFMM7B+bw0Z7MmPDMVXkF1GsSH3CKzZcrVRZUbFRZc6UM8Mq4S/iAVlF0yBh51fWiUWkfCD31A1RrpoyIpxZRKtYPDlF24yiRlSQ7IlQtHz3K0CAQcQ7NhFdqK9rtvjEHw6xZrjChQz6clac5DzddVmoGJdYkI8ndGEZ6sX0nDn8Y3PnzykK/jeB8QCJOGsU8ThOkpRTtcfC+ypVRXSgzZrDU6Hi3xIyOKP7Z+UuX3rpw/uLGxinqqV5M9NGjR/TSabsWMGPKRIcSVsP97YFVISua+XTTB+yFo881Z6ey5rJvdySY0AcMzfQz4IFV0T391nWtlK1Az88WdD7U0++x8NlHV8PbmOkmbAbMOX52gPfVRy2jc/hDcRWd47+IF2YG5uAkkl6RdzJqjKRvJmxsllIrryifWkLIQtkEBoTgokms+ZG9zXPAVBFtno6qyMJPR+IF+O03FFUMpgkLzRC7tffuuX3zzmNH/2Ld4cs806jbq6xG1jawsIR51RcIlwpWvoAS+ckEfgK97QLFP6ciqoaDfLlIAmHqaShODE3ffPP7F9+8oIKOO7pk3D2R9qWqYDD77ZQQl9ku/AwkJMPlR9rSYVow/SVsoYp9Xj3xSu0DatVHl+HQwHxLNfJbxjBfYL2fz7Ft3Vnm/ht+texk7hk+A7evZkXn8OfoTxZa1ASGdq5C5zIO8Dl+hgc35FqNEkciKBdBZxx0CMkoJUSf1OGK4JrJINahMUszQm/tBSepYVBi8TAGeBBCo5rIIcghIgqYSRlodPbr3sXmmG/2tp2khMJielzRyPqCUMIHXgn3Nymkbqe7ZKyex4Khq/HD5oVuM4lGknJGf5feAXFKbW0bhms5T5O1bhzFTA+G9X69brxBsMcSGhxSdQRtmjNIRssNNCrIPOogKKWhP1r7+c9/ttqCO0CyLSBkYIiuwpNUq8I2DRwyVnSH7ugUV6GBMTXbXF+KRkngwCcmpui8MqzoDzmTh+mipxrN8x0o9HknkyaBc/xUw07mWgXO8dPXq8+1A32ySNMg0zEOZcMzfU5ZiLckwLRFBJQZ1AUlxzSTfYDGB/e0CH2WggdEKLPsyQNIumKJMITeOMrUbq13g3D0+nwfvB1113G0luDtsBlTtg9N3pw8ecKUi+k6B/zaYgSF91fDhCjZf82bDpMZ7X1J7YMcccAfnMNP1lEVhBk9g8NWx9hDo4TTp8/YHHc4brs5uOdf//VfqgJ9AF3RJJd+RuceUiFXIHP1dCpJYI5OXJ0z5ardtyfGzQiTq/tz9F2jicJqvaqO2+lve3H0SdncSUredKJEJIGDT1ZACtgHGnj08E8KQVOOK6ygRM1ARbOdJQ3wuXaAWSxlIP0sSK50gAJ8mlBZenzwigqkg0nBBvyMEmIrE2+99ZYoAdVcSkwVzSrQQ9oLLor4//m/f6At9gNoBxAOP3AElMVHkBPW/ePsTUseiv84o4H/uBHDdb/hEJdKbkzHbJw+deLEMQsJZ86eZrsMD1kzi5OeryEhynhLy4cInl1RigJzBy2ZTBxAqShiqXwQNEEQgaam6KkUyihQAgjcrCWUMzHyoQpXIOFzfqL1yNko8PvAXPa57mg2wUouEqlpFvK9kjoBqO7owGfPcJ9tptyFBMtVTi5hTzcDg1804WS4ApXUB5TrYYHk26fClSsDFYVZ4T4p+ekpD+GsYFWznjtIAXviQ/bCH+DFTBIpUjSqemvykl26QUDzpSZXiGiO7STv3Xv23Fk+keVS6OFwBBpNEBm5VN1QVHvBOBdKmbSL8/i+VrMMx2nfWKiPtev4ja0m7YYY49LU/NikZtqG5hw9esyWVHtknj6LUauCshG+fGy3dwxicZWqhWFhLCXnwukam3GiAnvwy8VsTly0E1ma/9raz37206H5hmg1X8KH6ICMKkj6GSgOomXaOzW5KZyBwisqodytN5qa2PttUhQv2/5iqL7Q2GKgAgMnO0ezHeRNB1kguGnvywz3flFLYOEXfAigU4onqcJD9iFaZAue/AzEK7qaSoAA0xUaakWwgBlI+DJH/ILzk07iFFBURXRKLV0QAz00oyaYxBpOigeCKcQgJNgd+FSCkxEaBQZP4wMfQuYC5ITtuMmFwZC1tuG7Re2W1j31jOxZozEWDEMO4NgL7ivBDx893nxIH59Sl0AwTDRK1G2M3zj/C+4jNnYUPH2y6HPSwGbZYg0Dz6Jcci6a/KivtU86mJpI/bws2pgzrlEO/sLNK2FPKBBby2YgW3aXvmaKglYcdicpfBslnCQ0CVwo4GKiXx0nsXYDjOfdOaSSmooCZ7j3CzeBhZP4ldoHSvEAKzxkr2ifcTfhvtwMF6SPTlahpw+hMiY8oz28J2hcBI1Zo4rgVrENCzNjKlW+tfkZsHZA1qGZYMwBJFXMvl+KFtGHwIUyrO2nlEs2UmQz1q76bNoNTdcUNLXXcXc2zCQNmtYw3E7vnzEj/o0V495e6wjrMQ40r2ODXHzKYfmaCHXuHJpqxCXnrQYxNOUySZgegtNJWdlDNlEt1n7605/AmHSVWWpQarQmMQuYOIUpMAhrYc4pYUyzzLjKuz2w3dYNpm+MUj+5F7qYdDTjdoK7imUdV/P28EwdcCqagcSfLLIUT2qFh+wVrXZOgkW2ApNFDMhwQNIV5RQ1wEkKCVxmit8kUsgDhClj1syC0kNLgnxhCqaULEhFOLlEKZgHKAvzQXvd+2TilNTq02bvFFoqIcOiREaPjDOG7VyiTg/bR99UJbZqtxmXmHcBWSDs2WtqJIZoENrRIruxY7fNA3dzxFKkUaSFCa2i/dddiOoaqHamIspqKxZZO9Fe8VL9EhKzqTagsqpWTMK1vnEoarOK1PrZjkoox9CCVWQjt1tPG0y6yfwLQ66k7W4SeQegEqdSiUj8VWoFppB3guEuk/uqgSQ8yfZ+0aoSM1B0CqECpXggFR6yV5RMVMY+sAP9RCuEDPR0EC9XaD1xYQhD0mS0gNSJdMpFeRgf69dknb7RohD0ZtASWZjQeyfTuhyGyQKNabIFhyGVq6qAJsq+5WJAmYZKEYtRV7NXHkzhCyfnrVUXWhpTlk1Pcoepq0StH+g66vPmiyXJ4QGe7JjsHSY1XTrwZAwPvgqlXAU1dhb8+lGjNINhCX/ykx9nypyfFIvuHFrClSqQfgaE02FFIH2BOTreUd+Jmykit4EvnkFymMXNVXCGzpayQVjFSUjvV6UKOQNz5cIvxevDQ/aKFp2EFLzK3TlQ2YlRhlHoHfgOFGQpB01YXn5myUD6BIDYCUcPsk3/WpOgirQIcpiatmYIByaVc1BQOwDK0gT3Octp9zPM7MeG7rWJExSM+2iuhUFiH7IWesPT/WT9bASNCYFAikP8FINKrz/xpWFTNdg1ECKVgaKoWGp3krj1Tx/hIc47tO5jG0+GPuOKj2ctg9usKR+QS57jrbFvscdVkoKQ1irNiOZO9IDMnqII3OayNatNBRI+6ctV+BBEMVRZMpD+3EN1HQanwQR242vdScyw+isUgqO4qyrfZNHygdVqVPUd6pWpA1C0appJc2irGQfIq5Y7ZK9o0mk12lKAl3KVuYoHgQz3GYX7aJXYBzToYuVuCU06YhkwI5lyQ15zW2YaN4pEiyyfk3kBfU5TNbSJeeRnuSgI0FiyJAwN8ocffvj555+TdXQ4yCn6cYuMkz1tkTA2L8Z8NUluXdOv49NI+ZYJFfKfe/rka4cjTAIReF9iChWMD/Qd9W3TLx/a5Hn9xvNjh4+cfePsm+cvrG8cfvzIt2LsKm9vkBffPPvKgoctB+aKQpUIVAp8J/aLquOcU3oykFULxvR1P/rxR5Rx9Q/j+MkXQ/rBnhNI7SxJEsoGT3KUXsHlovbNLR9K/CYf6SeFHpLhaKRIy9ba8mMjLxJurIEXvf/GjEdikOvFElkCkjjlC2z/22MnlVdkq3w8HhnbTSfx5cqAx2DBQwxSOXDwEha2c6IVtHiLer02tPBbuDV/K2uZFCUsi05qkI1QAj/6A07MxDvaHSrPSIJXPkokKYVJY4q2hoh3RB8my61xYoAvgLfoRq3tdae1qTwl5qNM4gpqOMFJYzgYSM6J4LIKUoPDRcaoUrh8HHjAUrZua/h4iPEs29PkswKROwS9HfSRswmBLJG6IBvX8sqIEPk1wxGfPmr8J3vmPyye37p9y6cIXZJ9cmNDjSRhss4vKMVUZTNEZlkcon3iuJMPv/DNpVh9x6jtqAyarTh0SscWET1Zz9Mg9ImPE3r7puq10/r5WB2Ed/+3psN2a72ooGcfvUubbfYguO5t8PDR0xs37ly5+pdTp87Z9/bEbro8ePyNCRs8xjqki38R8qXUtrZIHZwcXmNLpWtFbaRB4sk3l5tyot3dWvOPP/5I2qrTmAnMQEXb81w+KNCGxkd9lcgOkHxIOyAMSUk/JSB9CNFeKRPJSfM1ZXC15H+g4yMiLUfo2UKQWkXJfcMMKHjzw9NoWUoWlDUN+kuX4R6yTBl/B5yMmiBrQhvv+JT1jI6Zl3GcJT8AKHDw0y8KiZvwZDuRy0+E9Asho1UxNCfx+7wRdpg7XlwLbAAciiXZbMfs3Xh9iNaAKulU6YZ5hJNJNFsju9VzVtEaPZMDDqJxyuIxQzqxTCIi7GpO2EBwB5Tjgq4YdRGbU0Ju2ad+xpCmW6jB4rE21qIZZcZna095uWQmtCnmS5/zzbzGLVKOQVG2uJL4+e2bt3RMXdxkn7e8TRV9cGKfQxGaQEe39Y4Z8qcGqe1ERrwrV10+5eavvbw7mo1Vfj2khOA7W1wxhbObQLX+gIzaAMlo4WegogMyePIzR6faWsYkMoc5UN5ldI6xnbPLVRkrsHOW1dQho2jfFENUdhAu6fThnjL4bton0QpZoCfShzOp96WK6qYa7NGrGzdu8EmnWzBAUsO9IqkiNI+vdRdid3jrhz7Tfc3tptGVPXb0+o3r9BMa3Tv4vdgrBy3VuNjw1gLPaA7nUFZNaMqtJBBRaErMRoBmz6eJ2Qf377ml//TpDa8AO+LgmLCFyxLKktWJudXY+Lp/0RGv4lsAewVY++gfpy1hYYyB7c2LueQv/RF5Po7R+cSJlKKfGSt7BTJPcDOjxokQPc7mWjSaHrr/2dzkIeGJzH9VSzjwU3QE+qQM82PjYnMRbl39ivZ5t8Jet82BNOYXllAYmABxAlKzRhlO5PIFelc4EWgNEI3S6BdaQipaAUxvJWXLNz9pjq2J5rIdFgh+Grc0R6ANtw4I24pJFY0YXevUUGKslTOoitZQwiBVa0BKePrMGR+I19fNHZueHUxlknjUIKdrs+MRlIsSJgPZ7PSQwnOUXFhjypg4othzvyKtdgyCs5jhndA+Fm71sHU0UYnPcVPy6KS6TM3cj07tqsNSlhg5fvTRP2QlBz9ZrLauAIkVnkwdKLw0WjT7wCTlKhFN4fT7wGquVciilBDRcC363Suhx7YoaPtPV+giIXmghJ4ubkVTf4Q9mCWH26mINcxMhZnI6Wf2Pgk8+RFoWcPPAAjMwUVSQ+i9IW+fJGycl5Ag2AQjWzaBi1bu8qCfhUbe5dMUIPe6cLg17alBWBvO/kx3jcJMlaNREGDC0VNVIq3QVpSEqiBivHXy1EnfkYHfziXea+p0MFPhc6jVwW5RNPlb+tCMIQa4RA785hJon2u+C+zewYPjEW6W0fH0DbaoEP4WExRtYsEXadYP6N5OOpTThRIuw9t+i4Nt0O4hJWfpDzi7ieYDWMWcLbd/vy5UaEsb0dktP00Js1wkI9fyRdWAo9h4T2E1XZaiQXsmM1wQDyGJDz4KCamAaJBtuxDRFCQQ/HxgQ/at6FJ5QBTKJT8CjVhIgYBUcH6GE7l8gXKFABLhxiZqibAaqIwZ8L4XWKDlM6oogg1pUfNFhkUsy63SiXgyTKNSMfgMmo8hgVvN1+2ETFHTLmU7g2R9+TSNDTx6/KjTSdYwdBS1BDW2ziCVNrbycaovET6XHKGfDkSgpcTRfqm4ouepdVmWqSHAuOE0vqC4H7P3793ThXbdPYU0/cR0u49YJtNITbRi6WPVYakcJfxRsrJLH48wi2jlwuIruco4BLTCABmiSgFJv5JW+QGp1D7gzS1vpkIRWDZ74o9i86pKSHT64io8ybZUjy0fPARPJXnzmCvjGJhXwsyeBVWDAFZTLGu9eIIwEznYWIar1XpkCEVk4MebIyEQtF36hTO2poSYR108viodRB9PD42qCFAAdIS1zJ3btzWpgKhmAYdAtXLTabJdzLNxVvNMSOqaXrhwwacgaKx5SNqyLKvVfXnFCWoc4lyGk3PIMmZSFlpoVCrssTWJ9eiyBsNPHd7dvHvntjOKdqF5jMGPuVBz7xHKeAt1nleGeqVb+4cfvdwS4gBzC38ZSHYBE57Rv93XHJNElKIKmZSBivYMBDcdt6ukKGEhQGyt30vYKDavqoSEfrXQgjTuFhwCCum8eMzYUJ3UIuGMVq4+4OUKk8vsKEDmiya8Twr6jZ9ESL8yFtnMkhQYrx4tkcuvLBXolZAOyLt4SA1jbM146SzaJwutovGZDoVskNYM8RV7/VJngvmEPnUvu5dUKwaNbYaDNAeTe/facYYJUVpkS5o5G751RXrbOMqGKvZDADiFcsWSAO2ChI6yMCZVcRhwzyn2rHToArOxSjl00MUcp9oV/U57xMeGjRjtCw+D2zqiTSmR3OZkXNb4xf8DkcHY+CkvTVYAAAAASUVORK5CYII="/>
    </g>
    <path id="Path_18520" data-name="Path 18520" d="M0,0H210" transform="translate(-8.721 37.213)" fill="none" stroke="{{Color}}" stroke-width="1"/>
  </g>
  <g id="Group_19035" data-name="Group 19035" transform="translate(-653.779 -464.075)">
    <path id="Path_18570" data-name="Path 18570" d="M2.452,21.278c.3.633.635,1.267.967,1.871.181.3.334.6.514.9s.363.6.544.876.363.573.545.845c.363.573.725,1.086,1.118,1.63.271.392.544.754.846,1.114.181.246.363.482.544.694.029.03.029.06.059.06.152.211.334.392.485.6.18.211.361.424.513.633.126.152.273.3.393.453.03.061.092.09.126.151.151.181.332.363.484.543s.3.331.453.482.272.3.423.453a4.822,4.822,0,0,0,.392.392c.786.814,1.269,1.267,1.269,1.267A41.5,41.5,0,0,0,15.3,30.9c.455-.513.907-1.086,1.39-1.689C20.045,24.986,24,18.589,24,12.041a11.781,11.781,0,0,0-.363-2.956,10.735,10.735,0,0,0-.573-1.659A6.567,6.567,0,0,0,22.555,6.4c-.091-.181-.181-.331-.272-.483a11.642,11.642,0,0,0-1-1.388,12.693,12.693,0,0,0-2.055-1.961,8.215,8.215,0,0,0-.937-.633A13.23,13.23,0,0,0,16.205.944,10.074,10.074,0,0,0,15.088.613a11.248,11.248,0,0,0-1.751-.3C12.943.281,12.521.25,12.129.25s-.816.031-1.209.061a4.258,4.258,0,0,1-.6.061,11.13,11.13,0,0,0-1.15.246A10.848,10.848,0,0,0,7.5,1.193a6.523,6.523,0,0,0-1.028.513A12.389,12.389,0,0,0,3.333,4.15c-.242.272-.484.572-.725.875-.125.151-.21.3-.332.452a10.32,10.32,0,0,0-.846,1.479A10.984,10.984,0,0,0,.977,8.013l-.183.543a12.316,12.316,0,0,0-.544,3.53A20.636,20.636,0,0,0,1.7,19.328a13.115,13.115,0,0,0,.752,1.95" transform="translate(653.529 533.58)" fill="{{Color}}" stroke="{{Color}}" stroke-width="1"/>
    <g id="Group_19035-2" data-name="Group 19035" transform="translate(656.549 544.216)">
      <path id="Path_18516" data-name="Path 18516" d="M21.155,13.32l-.963-2.414a.887.887,0,0,0-.725-.54l-2.627-.2a.856.856,0,0,0-.926.855v4.323h-.8V9.826a.853.853,0,0,0-.85-.855H3.364a.854.854,0,0,0-.851.855V16.06a.854.854,0,0,0,.851.855H5.015V16.89a1.864,1.864,0,1,1,3.727,0v.025h.713V16.89a1.864,1.864,0,1,1,3.727,0v.025h3.631a1.864,1.864,0,0,1,3.727-.038.876.876,0,0,0,.713-.855l-.013-2.4c-.024-.087-.049-.2-.087-.3m-1.739-.3H17.228a.423.423,0,0,1-.425-.427V11.434a.425.425,0,0,1,.463-.427l1.8.151a.422.422,0,0,1,.363.276l.388.993a.431.431,0,0,1-.4.591" transform="translate(-2.513 -8.971)" fill="#fff"/>
      <path id="Path_18517" data-name="Path 18517" d="M8.683,15.9a1.584,1.584,0,0,0,.053.269,1.382,1.382,0,0,0,.911.916,1.586,1.586,0,0,0,.268.053,1.1,1.1,0,0,0,.416-.027.792.792,0,0,0,.254-.081,1.378,1.378,0,0,0,.844-1.251v-.027a2.048,2.048,0,0,0-.027-.283,1.4,1.4,0,0,0-1.22-1.1,1.372,1.372,0,0,0-.829.176.39.39,0,0,0-.081.053,1.354,1.354,0,0,0-.509.619c-.013.041-.028.081-.041.134a1.456,1.456,0,0,0-.066.418v.027a.258.258,0,0,1,.027.108" transform="translate(-1.253 -7.834)" fill="#fff"/>
      <path id="Path_18518" data-name="Path 18518" d="M4.991,15.9a1.584,1.584,0,0,0,.053.269,1.382,1.382,0,0,0,.911.916,1.586,1.586,0,0,0,.268.053,1.516,1.516,0,0,0,.55-.053c.04-.013.08-.027.134-.041a1.419,1.419,0,0,0,.857-1.264v-.027a1.38,1.38,0,0,0-1.379-1.387,1.41,1.41,0,0,0-1.286.861c-.013.041-.027.081-.04.136a1.4,1.4,0,0,0-.067.417V15.9" transform="translate(-2.005 -7.833)" fill="#fff"/>
      <path id="Path_18519" data-name="Path 18519" d="M17.534,15.766a1.4,1.4,0,0,0-.4-.942l-.013-.013a1.294,1.294,0,0,0-.269-.2.694.694,0,0,0-.107-.067,1.428,1.428,0,0,0-.321-.108,2.019,2.019,0,0,0-.282-.027,1.294,1.294,0,0,0-.588.134.284.284,0,0,0-.107.067,1.417,1.417,0,0,0-.683,1.184v.027a1.374,1.374,0,0,0,1.379,1.373,1.376,1.376,0,0,0,1.38-1.387v-.013a.041.041,0,0,0,.013-.027" transform="translate(0 -7.824)" fill="#fff"/>
    </g>
  </g>
</g>
</svg>`,
    ].join('\n');
    var templatePersonne = [
      `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="140.25" height="34.593" viewBox="0 0 140.25 34.593">
    <defs>
      <clipPath id="clip-path">
        <rect id="Rectangle_6389" data-name="Rectangle 6389" width="17.094" height="21.033" fill="none"/>
      </clipPath>
      <clipPath id="clip-path-2">
        <rect id="Rectangle_6387" data-name="Rectangle 6387" width="17.094" height="21.034" fill="none"/>
      </clipPath>
      <clipPath id="clip-path-3">
        <rect id="Rectangle_6386" data-name="Rectangle 6386" width="17.094" height="17.094" fill="none"/>
      </clipPath>
    </defs>
    <g id="Component_446_16" data-name="Component 446 – 16" transform="translate(0.25 0.25)">
      <rect id="Rectangle_6360" data-name="Rectangle 6360" width="111" height="30" rx="2" transform="translate(29 1.949)" fill="#323c35" opacity="0.895"/>
      <text id="_10_12_2021_11.00_am" data-name="10/12/2021 11.00 am" transform="translate(34.855 26.801)" fill="#fff" font-size="10" font-family="Avenir-Medium, Avenir" font-weight="500"><tspan x="0" y="0">{{date}}</tspan></text>
      <text id="Frontino_Sorel" data-name="Frontino Sorel" transform="translate(34.855 14.098)" fill="#fff" font-size="10" font-family="Avenir-Medium, Avenir" font-weight="500"><tspan x="0" y="0">{{matricule}}</tspan></text>
      <g id="Group_19033" data-name="Group 19033" transform="translate(-653.779 -533.83)">
        <path id="Path_18570" data-name="Path 18570" d="M2.452,21.278c.3.633.635,1.267.967,1.871.181.3.334.6.514.9s.363.6.544.876.363.573.545.845c.363.573.725,1.086,1.118,1.63.271.392.544.754.846,1.114.181.246.363.482.544.694.029.03.029.06.059.06.152.211.334.392.485.6.18.211.361.424.513.633.126.152.273.3.393.453.03.061.092.09.126.151.151.181.332.363.484.543s.3.331.453.482.272.3.423.453a4.822,4.822,0,0,0,.392.392c.786.814,1.269,1.267,1.269,1.267A41.5,41.5,0,0,0,15.3,30.9c.455-.513.907-1.086,1.39-1.689C20.045,24.986,24,18.589,24,12.041a11.781,11.781,0,0,0-.363-2.956,10.735,10.735,0,0,0-.573-1.659A6.567,6.567,0,0,0,22.555,6.4c-.091-.181-.181-.331-.272-.483a11.642,11.642,0,0,0-1-1.388,12.693,12.693,0,0,0-2.055-1.961,8.215,8.215,0,0,0-.937-.633A13.23,13.23,0,0,0,16.205.944,10.074,10.074,0,0,0,15.088.613a11.248,11.248,0,0,0-1.751-.3C12.943.281,12.521.25,12.129.25s-.816.031-1.209.061a4.258,4.258,0,0,1-.6.061,11.13,11.13,0,0,0-1.15.246A10.848,10.848,0,0,0,7.5,1.193a6.523,6.523,0,0,0-1.028.513A12.389,12.389,0,0,0,3.333,4.15c-.242.272-.484.572-.725.875-.125.151-.21.3-.332.452a10.32,10.32,0,0,0-.846,1.479A10.984,10.984,0,0,0,.977,8.013l-.183.543a12.316,12.316,0,0,0-.544,3.53A20.636,20.636,0,0,0,1.7,19.328a13.115,13.115,0,0,0,.752,1.95" transform="translate(653.529 533.58)" fill="#fa254b" stroke="#25272b" stroke-width="0.5"/>
        <g id="Group_19037" data-name="Group 19037" transform="translate(658.233 538.38)">
          <g id="Group_19044" data-name="Group 19044" clip-path="url(#clip-path)">
            <g id="Group_19041" data-name="Group 19041" transform="translate(0 -0.001)">
              <g id="Group_19040" data-name="Group 19040" clip-path="url(#clip-path-2)">
                <g id="Group_19039" data-name="Group 19039" transform="translate(0 2.343)" opacity="0">
                  <g id="Group_19038" data-name="Group 19038">
                    <g id="Group_19037-2" data-name="Group 19037" clip-path="url(#clip-path-3)">
                      <rect id="Rectangle_6385" data-name="Rectangle 6385" width="17.094" height="17.094" transform="translate(0 0)" fill="#354360"/>
                    </g>
                  </g>
                </g>
              </g>
            </g>
            <path id="Path_18571" data-name="Path 18571" d="M10.824,10.606h0Z" transform="translate(2.392 2.344)" fill="#020203"/>
            <g id="Group_19043" data-name="Group 19043" transform="translate(0 -0.001)">
              <g id="Group_19042" data-name="Group 19042" clip-path="url(#clip-path-2)">
                <path id="Path_18572" data-name="Path 18572" d="M4.672,3.569a2.182,2.182,0,0,0,1.712.784,1.955,1.955,0,0,0,1.6-.784,2.316,2.316,0,0,0,.377-.639A2.188,2.188,0,0,0,8.5,2.176,2.151,2.151,0,0,0,8.008.784,1.4,1.4,0,0,0,7.718.493a2.212,2.212,0,0,0-2.785,0,1.4,1.4,0,0,0-.291.291,2.151,2.151,0,0,0-.493,1.392,2.188,2.188,0,0,0,.145.755,2.04,2.04,0,0,0,.377.639" transform="translate(0.917 0.001)" fill="#fff"/>
                <path id="Path_18573" data-name="Path 18573" d="M12.694,11.627c.154-3.063-1.049-6.77-4.361-7.289a3.056,3.056,0,0,0-1.249-.254A2.957,2.957,0,0,0,5.7,4.4L5.7,4.4a3.568,3.568,0,0,0-1.023.381,4.994,4.994,0,0,0-.523.319,5.839,5.839,0,0,0-.871.812,3.194,3.194,0,0,0-.377.493,4.411,4.411,0,0,0-.435.813,9.806,9.806,0,0,0-.764,4.859V12.1a1.5,1.5,0,0,0,.084.479,1.354,1.354,0,0,0,.22.405,1.233,1.233,0,0,0,1,.5,1.109,1.109,0,0,0,.93-.5,1.534,1.534,0,0,0,.22-.405A1.5,1.5,0,0,0,4.24,12.1a1.554,1.554,0,0,0-.021-.252,8.372,8.372,0,0,1,.571-3.9v4.2l-.232,6.84A1.254,1.254,0,0,0,5.827,20.13,1.07,1.07,0,0,0,6.9,19L7.1,11.184,7.36,19A1.169,1.169,0,0,0,9.7,18.92L9.4,9.958h0c-.027-.761-.028-1.546-.028-2.305.059.087.087.175.145.261a3.925,3.925,0,0,1,.319.812c.059.175.088.377.145.581a8.834,8.834,0,0,1,.145,1.479v.291c0,.121,0,.253-.013.382a1.481,1.481,0,0,0-.046.354,1.416,1.416,0,0,0,.088.479,1.322,1.322,0,0,0,.228.4,1.3,1.3,0,0,0,1.039.5,1.167,1.167,0,0,0,.968-.5,1.479,1.479,0,0,0,.228-.4,1.447,1.447,0,0,0,.088-.479,1.563,1.563,0,0,0-.015-.186" transform="translate(0.367 0.903)" fill="#fff"/>
              </g>
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
  
  `,
    ].join('\n');
    var newtemp = [
      `<svg xmlns="http://www.w3.org/2000/svg" width="140.25" height="34.593" viewBox="0 0 140.25 34.593">
    <g id="Component_446_18" data-name="Component 446 – 18" transform="translate(0.25 0.25)">
      <rect id="Rectangle_6360" data-name="Rectangle 6360" width="111" height="30" rx="2" transform="translate(29)" fill="#323c35" opacity="0.895"/>
      <text id="_10_12_2021_11.00_am" data-name="10/12/2021 11.00 am" transform="translate(34.855 24.851)" fill="#fff" font-size="10" font-family="Avenir-Medium, Avenir" font-weight="500"><tspan x="0" y="0">{{date}}</tspan></text>
      <text id="AC123AB" transform="translate(34.855 12.149)" fill="#fff" font-size="10" font-family="Avenir-Medium, Avenir" font-weight="500"><tspan x="0" y="0">{{matricule}}</tspan></text>
      <path id="Path_18574" data-name="Path 18574" d="M2.452,21.278c.3.633.635,1.267.967,1.871.181.3.334.6.514.9s.363.6.544.876.363.573.545.845c.363.573.725,1.086,1.118,1.63.271.392.544.754.846,1.114.181.246.363.482.544.694.029.03.029.06.059.06.152.211.334.392.485.6.18.211.361.424.513.633.126.152.273.3.393.453.03.061.092.09.126.151.151.181.332.363.484.543s.3.331.453.482.272.3.423.453a4.822,4.822,0,0,0,.392.392c.786.814,1.269,1.267,1.269,1.267A41.5,41.5,0,0,0,15.3,30.9c.455-.513.907-1.086,1.39-1.689C20.045,24.986,24,18.589,24,12.041a11.781,11.781,0,0,0-.363-2.956,10.735,10.735,0,0,0-.573-1.659A6.567,6.567,0,0,0,22.555,6.4c-.091-.181-.181-.331-.272-.483a11.642,11.642,0,0,0-1-1.388,12.693,12.693,0,0,0-2.055-1.961,8.215,8.215,0,0,0-.937-.633A13.23,13.23,0,0,0,16.205.944,10.074,10.074,0,0,0,15.088.613a11.248,11.248,0,0,0-1.751-.3C12.943.281,12.521.25,12.129.25s-.816.031-1.209.061a4.258,4.258,0,0,1-.6.061,11.13,11.13,0,0,0-1.15.246A10.848,10.848,0,0,0,7.5,1.193a6.523,6.523,0,0,0-1.028.513A12.389,12.389,0,0,0,3.333,4.15c-.242.272-.484.572-.725.875-.125.151-.21.3-.332.452a10.32,10.32,0,0,0-.846,1.479A10.984,10.984,0,0,0,.977,8.013l-.183.543a12.316,12.316,0,0,0-.544,3.53A20.636,20.636,0,0,0,1.7,19.328a13.115,13.115,0,0,0,.752,1.95" transform="translate(-0.25 -0.25)" fill="{{Color}}" stroke="#25272b" stroke-width="0.5"/>
      <g id="Group_19046" data-name="Group 19046" transform="translate(2.769 10.387)">
        <path id="Path_18516" data-name="Path 18516" d="M21.155,13.32l-.963-2.414a.887.887,0,0,0-.725-.54l-2.627-.2a.856.856,0,0,0-.926.855v4.323h-.8V9.826a.853.853,0,0,0-.85-.855H3.364a.854.854,0,0,0-.851.855V16.06a.854.854,0,0,0,.851.855H5.015V16.89a1.864,1.864,0,1,1,3.727,0v.025h.713V16.89a1.864,1.864,0,1,1,3.727,0v.025h3.631a1.864,1.864,0,0,1,3.727-.038.876.876,0,0,0,.713-.855l-.013-2.4c-.024-.087-.049-.2-.087-.3m-1.739-.3H17.228a.423.423,0,0,1-.425-.427V11.434a.425.425,0,0,1,.463-.427l1.8.151a.422.422,0,0,1,.363.276l.388.993a.431.431,0,0,1-.4.591" transform="translate(-2.513 -8.971)" fill="#fff"/>
        <path id="Path_18517" data-name="Path 18517" d="M8.683,15.9a1.584,1.584,0,0,0,.053.269,1.382,1.382,0,0,0,.911.916,1.586,1.586,0,0,0,.268.053,1.1,1.1,0,0,0,.416-.027.792.792,0,0,0,.254-.081,1.378,1.378,0,0,0,.844-1.251v-.027a2.048,2.048,0,0,0-.027-.283,1.4,1.4,0,0,0-1.22-1.1,1.372,1.372,0,0,0-.829.176.39.39,0,0,0-.081.053,1.354,1.354,0,0,0-.509.619c-.013.041-.028.081-.041.134a1.456,1.456,0,0,0-.066.418v.027a.258.258,0,0,1,.027.108" transform="translate(-1.253 -7.834)" fill="#fff"/>
        <path id="Path_18518" data-name="Path 18518" d="M4.991,15.9a1.584,1.584,0,0,0,.053.269,1.382,1.382,0,0,0,.911.916,1.586,1.586,0,0,0,.268.053,1.516,1.516,0,0,0,.55-.053c.04-.013.08-.027.134-.041a1.419,1.419,0,0,0,.857-1.264v-.027a1.38,1.38,0,0,0-1.379-1.387,1.41,1.41,0,0,0-1.286.861c-.013.041-.027.081-.04.136a1.4,1.4,0,0,0-.067.417V15.9" transform="translate(-2.005 -7.833)" fill="#fff"/>
        <path id="Path_18519" data-name="Path 18519" d="M17.534,15.766a1.4,1.4,0,0,0-.4-.942l-.013-.013a1.294,1.294,0,0,0-.269-.2.694.694,0,0,0-.107-.067,1.428,1.428,0,0,0-.321-.108,2.019,2.019,0,0,0-.282-.027,1.294,1.294,0,0,0-.588.134.284.284,0,0,0-.107.067,1.417,1.417,0,0,0-.683,1.184v.027a1.374,1.374,0,0,0,1.379,1.373,1.376,1.376,0,0,0,1.38-1.387v-.013a.041.041,0,0,0,.013-.027" transform="translate(0 -7.824)" fill="#fff"/>
      </g>
    </g>
  </svg>`,
    ].join('\n');

    if (Hover == 'H' && type == 'V') {
      var svg = newtemp.replace('{{matricule}}', mat);
      svg = svg.replace('{{Color}}', color);
      svg = svg.replace('{{date}}', date);
      return svg;
    }

    /*
    if (Hover == 'HOV' && type == 'U') {
      var svg = templatePersonneHover.replace('{{matricule}}', mat);
      svg = svg.replace('{{Color}}', color);
      svg = svg.replace('{{date}}', date);
      return svg;
    }
    */

    if (Hover == 'H' && type == 'U') {
      var svg = templatePersonne.replace('{{matricule}}', mat);
      // svg = svg.replace('{{Color}}', color);
      svg = svg.replace('{{date}}', date);
      return svg;
    }

    var svg = hovervehicule.replace('{{matricule}}', mat);
    svg = svg.replace('{{Color}}', color);
    svg = svg.replace('{{date}}', date);

    return svg;
  }

  createNewMarker(mat: any, markerType?: boolean, color?: string): any {
    let ColorRandom = '';

    if (color) {
      ColorRandom = color;
    } else {
      let randomIndex = Math.floor(Math.random() * this.color.length);
      ColorRandom = this.color[randomIndex];
    }

    let template;

    if (!markerType) {
      template = `<?xml version="1.0" encoding="UTF-8"?>
      <svg xmlns="http://www.w3.org/2000/svg" id="Component_41_58" data-name="Component 41 – 58" width="154.389" height="28.842" viewBox="0 0 154.389 28.842">
        <g id="Group_17668" data-name="Group 17668" transform="translate(68.408)">
          <g id="Group_17448" data-name="Group 17448" transform="translate(-875.391 -442)">
            <g id="Rectangle_4" data-name="Rectangle 4" transform="translate(878.371 442)" fill="#fff" stroke="#354360" stroke-width="1">
              <rect width="83" height="15" rx="7.5" stroke="none"></rect>
              <rect x="0.5" y="0.5" width="82" height="14" rx="7" fill="none"></rect>
            </g>
            <text id="CA123AB" transform="translate(913.371 453.249)" fill="#354360" font-size="9" font-family="Avenir-Medium, Avenir" font-weight="500">
              <tspan x="0" y="0">${mat}</tspan>
            </text>
            <g id="Group_18890" data-name="Group 18890" transform="translate(897.507 445.249)">
              <path id="Path_14320" data-name="Path 14320" d="M2738.878,777.9a.842.842,0,0,1-.078.072A.842.842,0,0,0,2738.878,777.9Z" transform="translate(-2734.543 -773.165)" fill="#354360"></path>
              <path id="Path_14321" data-name="Path 14321" d="M2736.44,780.6c-.013.007-.027.02-.04.026A.132.132,0,0,1,2736.44,780.6Z" transform="translate(-2732.298 -775.689)" fill="#354360"></path>
              <path id="Path_14322" data-name="Path 14322" d="M2692.7,771.1a1.986,1.986,0,0,0,.15.235C2692.8,771.257,2692.746,771.179,2692.7,771.1Z" transform="translate(-2691.402 -766.807)" fill="#354360"></path>
              <path id="Path_14323" data-name="Path 14323" d="M2695,774.7a1.934,1.934,0,0,0,1.559.783,1.978,1.978,0,0,0,1.011-.28,1.951,1.951,0,0,1-2.571-.5Z" transform="translate(-2693.552 -770.174)" fill="#354360"></path>
              <path id="Path_14324" data-name="Path 14324" d="M2691.485,768.67a1.4,1.4,0,0,1-.084-.17C2691.427,768.552,2691.453,768.611,2691.485,768.67Z" transform="translate(-2690.188 -764.376)" fill="#354360"></path>
              <path id="Path_14325" data-name="Path 14325" d="M2747.253,778.184h4.794a3.183,3.183,0,0,0-6.047-1.39A3.4,3.4,0,0,1,2747.253,778.184Z" transform="translate(-2741.346 -770.463)" fill="#354360"></path>
              <path id="Path_14326" data-name="Path 14326" d="M2744.4,741.4a2.072,2.072,0,0,1,.163.339A2.072,2.072,0,0,0,2744.4,741.4Z" transform="translate(-2739.783 -739.036)" fill="#354360"></path>
              <path id="Path_14327" data-name="Path 14327" d="M2744.563,767.1a2.069,2.069,0,0,1-.163.339A2.069,2.069,0,0,0,2744.563,767.1Z" transform="translate(-2739.783 -763.067)" fill="#354360"></path>
              <path id="Path_14328" data-name="Path 14328" d="M2734.531,781l-.131.091Z" transform="translate(-2730.427 -776.063)" fill="#354360"></path>
              <path id="Path_14329" data-name="Path 14329" d="M2737.118,779l-.117.1A.788.788,0,0,0,2737.118,779Z" transform="translate(-2732.86 -774.193)" fill="#354360"></path>
              <path id="Path_14330" data-name="Path 14330" d="M2740.124,775.8c-.039.046-.079.091-.124.137A.862.862,0,0,0,2740.124,775.8Z" transform="translate(-2735.666 -771.201)" fill="#354360"></path>
              <path id="Path_14331" data-name="Path 14331" d="M2742.011,773.5a1.265,1.265,0,0,1-.111.15A1.265,1.265,0,0,0,2742.011,773.5Z" transform="translate(-2737.444 -769.051)" fill="#354360"></path>
              <path id="Path_14332" data-name="Path 14332" d="M2678.437,801.646a3.022,3.022,0,0,0-2.629-1.546,3.012,3.012,0,0,0-3.007,3.007h6.021A3,3,0,0,0,2678.437,801.646Z" transform="translate(-2672.801 -793.935)" fill="#354360"></path>
              <circle id="Ellipse_306" data-name="Ellipse 306" cx="1.944" cy="1.944" r="1.944" transform="translate(5.574 0)" fill="#354360"></circle>
              <circle id="Ellipse_307" data-name="Ellipse 307" cx="1.944" cy="1.944" r="1.944" transform="translate(1.063 1.431)" fill="#354360"></circle>
            </g>
          </g>
          <path id="Path_11171" data-name="Path 11171" d="M2719.464,1517.2c.242.512.508,1.024.774,1.512.145.243.266.488.411.731s.29.488.435.707.29.464.436.683c.29.463.58.878.894,1.317.217.317.435.609.677.9.145.2.29.39.435.561.024.024.024.049.048.049.121.17.266.317.387.488.145.17.29.342.411.512.1.122.218.244.314.366.024.049.073.073.1.122.121.146.266.293.387.439s.242.268.363.39.217.244.338.366a3.829,3.829,0,0,0,.314.317c.628.658,1.015,1.024,1.015,1.024a33.305,33.305,0,0,0,2.538-2.707c.363-.415.725-.878,1.112-1.366,2.683-3.414,5.85-8.584,5.85-13.876a9.658,9.658,0,0,0-.29-2.39,8.752,8.752,0,0,0-.459-1.341,5.26,5.26,0,0,0-.411-.829c-.072-.146-.145-.268-.217-.39a9.347,9.347,0,0,0-.8-1.122,10.153,10.153,0,0,0-1.644-1.585,6.734,6.734,0,0,0-.749-.512,10.539,10.539,0,0,0-1.668-.8c-.29-.1-.58-.2-.894-.268a8.864,8.864,0,0,0-1.4-.244c-.315-.024-.653-.049-.967-.049s-.653.025-.967.049a3.218,3.218,0,0,1-.484.049c-.315.049-.628.122-.919.2a8.539,8.539,0,0,0-1.329.464,5.147,5.147,0,0,0-.822.415,9.915,9.915,0,0,0-2.514,1.975c-.193.22-.387.463-.58.707-.1.122-.169.244-.266.366a8.358,8.358,0,0,0-.677,1.195,9.117,9.117,0,0,0-.362.854l-.146.439a10.064,10.064,0,0,0-.435,2.853,16.824,16.824,0,0,0,1.16,5.853A10.692,10.692,0,0,0,2719.464,1517.2Zm7.663-11.072a3.634,3.634,0,1,1-3.6,3.633A3.627,3.627,0,0,1,2727.127,1506.126Z" transform="translate(-2717.699 -1499.526)" fill="${ColorRandom}" stroke="#354360" stroke-width="1"></path>
        </g>
        <g id="Rectangle_6356" data-name="Rectangle 6356" transform="translate(0 0.249)" fill="#fff" stroke="#707070" stroke-width="1" opacity="0">
          <rect width="78" height="12" stroke="none"></rect>
          <rect x="0.5" y="0.5" width="77" height="11" fill="none"></rect>
        </g>
      </svg>
      `;
    } else {
      template = `<?xml version="1.0" encoding="UTF-8"?>
      <svg xmlns="http://www.w3.org/2000/svg" width="75.389" height="35" viewBox="0 0 152.389 35">
        <g id="Group_17668" data-name="Group 17668" transform="translate(66.408 0.161)">
          <g id="Group_17448" data-name="Group 17448" transform="translate(-875.391 -442)">
            <g id="Rectangle_4" data-name="Rectangle 4" transform="translate(878.371 442)" fill="#fff" stroke="#354360" stroke-width="1">
              <rect width="83" height="15" rx="7.5" stroke="none"></rect>
              <rect x="0.5" y="0.5" width="82" height="14" rx="7" fill="none"></rect>
            </g>
            <text id="AC123AB" transform="translate(915.371 453)" fill="#354360" font-size="9" font-family="Avenir-Medium, Avenir" font-weight="500">
              <tspan x="0" y="0">${mat}</tspan>
            </text>
            <g id="Group_17584" data-name="Group 17584" transform="translate(-1958.619 3021.504)">
              <path id="Path_12471" data-name="Path 12471" d="M1355.795,2084.344h2.033a.4.4,0,0,0,.371-.546l-.36-.918a.408.408,0,0,0-.337-.255l-1.673-.139a.393.393,0,0,0-.43.395v1.069A.407.407,0,0,0,1355.795,2084.344Z" transform="translate(1510.592 -4656.992)" fill="none"></path>
              <path id="Path_12472" data-name="Path 12472" d="M1277.876,2073.552h.023v-.023a1.731,1.731,0,0,1,3.461,0v.023h4.82a1.731,1.731,0,0,1,3.461-.035.809.809,0,0,0,.662-.79l-.012-2.218a.809.809,0,0,0-.058-.291l-.894-2.23a.822.822,0,0,0-.674-.5l-2.439-.186a.794.794,0,0,0-.859.79v4h-.743v-5.1a.79.79,0,0,0-.79-.79h-5.946a.79.79,0,0,0-.79.79v5.761A.776.776,0,0,0,1277.876,2073.552Zm8.316-5.064a.394.394,0,0,1,.43-.4l1.672.139a.393.393,0,0,1,.337.256l.36.918a.4.4,0,0,1-.372.546h-2.033a.392.392,0,0,1-.395-.4Z" transform="translate(1579.8 -4642.6)" fill="#354360"></path>
              <path id="Path_12473" data-name="Path 12473" d="M1288.029,2120a.725.725,0,0,0,.151-.011,1,1,0,0,0,.267-.058,1.3,1.3,0,0,0,.36-.174,1.412,1.412,0,0,0,.616-1.127v-.023a1.363,1.363,0,0,0-.314-.883.747.747,0,0,0-.093-.1l-.012-.011a.756.756,0,0,0-.139-.116,1.573,1.573,0,0,0-.3-.174.414.414,0,0,0-.128-.046,1.822,1.822,0,0,0-.267-.058.621.621,0,0,0-.151-.011,1.247,1.247,0,0,0-.546.116,1.731,1.731,0,0,0-.348.209,1.553,1.553,0,0,0-.2.2,1.265,1.265,0,0,0-.256.476,1.819,1.819,0,0,0-.058.267.614.614,0,0,0-.012.151v.023A1.438,1.438,0,0,0,1288.029,2120Z" transform="translate(1571.401 -4687.676)" fill="#354360"></path>
              <path id="Path_12474" data-name="Path 12474" d="M1360.655,2119.96a2.059,2.059,0,0,0,.174-.36,1.807,1.807,0,0,0,.058-.267.61.61,0,0,0,.012-.151v-.035a.321.321,0,0,0-.012-.1.992.992,0,0,0-.058-.267,1.3,1.3,0,0,0-.174-.36,1.4,1.4,0,0,0-2.555.767v.023a1.438,1.438,0,0,0,.406.987,1.387,1.387,0,0,0,.987.406A1.464,1.464,0,0,0,1360.655,2119.96Z" transform="translate(1508.206 -4688.206)" fill="#354360"></path>
            </g>
          </g>
          <path id="Path_11171" data-name="Path 11171" d="M2719.464,1517.2c.242.512.508,1.024.774,1.512.145.243.266.488.411.731s.29.488.435.707.29.464.436.683c.29.463.58.878.894,1.317.217.317.435.609.677.9.145.2.29.39.435.561.024.024.024.049.048.049.121.17.266.317.387.488.145.17.29.342.411.512.1.122.218.244.314.366.024.049.073.073.1.122.121.146.266.293.387.439s.242.268.363.39.217.244.338.366a3.829,3.829,0,0,0,.314.317c.628.658,1.015,1.024,1.015,1.024a33.305,33.305,0,0,0,2.538-2.707c.363-.415.725-.878,1.112-1.366,2.683-3.414,5.85-8.584,5.85-13.876a9.658,9.658,0,0,0-.29-2.39,8.752,8.752,0,0,0-.459-1.341,5.26,5.26,0,0,0-.411-.829c-.072-.146-.145-.268-.217-.39a9.347,9.347,0,0,0-.8-1.122,10.153,10.153,0,0,0-1.644-1.585,6.734,6.734,0,0,0-.749-.512,10.539,10.539,0,0,0-1.668-.8c-.29-.1-.58-.2-.894-.268a8.864,8.864,0,0,0-1.4-.244c-.315-.024-.653-.049-.967-.049s-.653.025-.967.049a3.218,3.218,0,0,1-.484.049c-.315.049-.628.122-.919.2a8.539,8.539,0,0,0-1.329.464,5.147,5.147,0,0,0-.822.415,9.915,9.915,0,0,0-2.514,1.975c-.193.22-.387.463-.58.707-.1.122-.169.244-.266.366a8.358,8.358,0,0,0-.677,1.195,9.117,9.117,0,0,0-.362.854l-.146.439a10.064,10.064,0,0,0-.435,2.853,16.824,16.824,0,0,0,1.16,5.853A10.692,10.692,0,0,0,2719.464,1517.2Zm7.663-11.072a3.634,3.634,0,1,1-3.6,3.633A3.627,3.627,0,0,1,2727.127,1506.126Z" transform="translate(-2717.699 -1499.526)" fill="${ColorRandom}" stroke="#354360" stroke-width="1"></path>
          <g id="Rectangle_6101" data-name="Rectangle 6101" transform="translate(-66.408 -0.161)" fill="#fff" stroke="#707070" stroke-width="1" opacity="0.002">
            <rect width="77" height="35" stroke="none"></rect>
            <rect x="0.5" y="0.5" width="76" height="34" fill="none"></rect>
          </g>
        </g>
      </svg>`;
    }

    let myIconUrl = 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(template);

    return L.icon({
      iconUrl: myIconUrl,
      iconSize: [150, 50],
    });
  }

  ClickOnSearch($event) {
    this.showAddress = false;
  }

  getValueFromcalender($event) {
   
    this.alert.nativeElement.classList.remove('show');
    this.datacalendarStart = formatDate($event.dateRange.startDate, 'yyyy-MM-dd', 'en_US');
    this.datecalendarEnd = formatDate($event.dateRange.endDate, 'yyyy-MM-dd', 'en_US');

    if ($event.view == 'day') {
      this.datecalendarEnd = this.datacalendarStart;
    }

    if (this.CurrentV) {
      this.getHistoryVehichle(this.CurrentV.idVehicule, this.datacalendarStart, this.datecalendarEnd);
    }
  }

  getHistoryVehichle(vehiculeId, datacalendarStart, datecalendarEnd) {
   
    if (this.showsniper) {
      this.showsniper = false;
    }

    this.map.removeLayer(this.markerClusterGroup);

    if (this.routingControl != null) {
      this.map.removeControl(this.routingControl);
      this.routingControl = null;
    }

    if (this.HistoryMarker.length > 0) {
      this.HistoryMarker.forEach((marker) => {
        this.map.removeLayer(marker);
      });
    }

    if (this.path != null) {
      this.map.removeLayer(this.path);
      this.path = null;
    }

    if (this.mrkAnimated != null) {
      this.map.removeLayer(this.mrkAnimated);
      this.mrkAnimated = null;
    }

    this.graphservice
      .getHistorique(datacalendarStart + 'T00:00:00.000Z', datecalendarEnd + 'T23:59:59.000Z', vehiculeId)
      .subscribe(async (data: any) => {
        this.RechSHOW = true;
        this.showsniper = false;

        let result = this.graphservice.buildAllHisortiqueVehicule(data, vehiculeId);

        if (result && result.listHistorique.length > 0) {
          this.closeAlert();
          this.drawHistoryTrajectory(result.listHistorique, this.CurrentV.Immatriculation.Vrn);

          var dt1 = formatDate(this.datacalendarStart, 'yyyy-MM-dd', 'en_US') + 'T00:00:00.000Z';
          var dt2 = formatDate(this.datecalendarEnd, 'yyyy-MM-dd', 'en_US') + 'T23:59:59.000Z';

          if (this.diffdate(dt1, dt2) <= 1) this.RangeActiviteStyle(result);
        } else {
          this.alert.nativeElement.classList.add('show');
        }
      });
  }

  diffdate(dt1, dt2): number {
    
    var date1 = new Date(dt1);

    var date2 = new Date(dt2);

    var Diff_temps = date2.getTime() - date1.getTime();

    var Diff_jours = Diff_temps / (1000 * 3600 * 24);

    return Math.round(Diff_jours);

  }

  drawHistoryTrajectory(listHistorique, vrn) {
   
    if (this.mrkAnimated != null) {
      this.map.removeLayer(this.mrkAnimated);
      this.mrkAnimated = null;
    }

    this.listpointsHistorique = listHistorique;
    this.countlistpointsHistorique = listHistorique.length;

    let StartIcon = L.icon({
      iconUrl: `../assets/img/depart default.svg`,
      iconSize: [30, 40],
      //iconAnchor: [30, 40],
    });

    let EndIcon = L.icon({
      iconUrl: `../assets/img/arrivé default.svg`,
      iconSize: [30, 40],
      //iconAnchor: [30, 40],
    });

    let StartMarker = L.marker([listHistorique[0].latitude, listHistorique[0].longitude], { icon: StartIcon }).addTo(
      this.map
    );

    let EndMarker = L.marker(
      [listHistorique[listHistorique.length - 1].latitude, listHistorique[listHistorique.length - 1].longitude],
      { icon: EndIcon }
    ).addTo(this.map);

    let Icon = this.createNewMarker(vrn, true);

    this.mrkAnimated = L.marker(
      [listHistorique[listHistorique.length - 1].latitude, listHistorique[listHistorique.length - 1].longitude],
      { icon: Icon }
    )
      .addTo(this.map)
      .openPopup();

    let stationcompteurObject = [];
    let stationArray = [];

    for (let index = 1; index < listHistorique.length - 1; index++) {
      let element = listHistorique[index];

      let ReposIcon = L.icon({
        iconUrl: this.getIconUrl(this.setActivite(element.activite)),
        //shadowUrl: `../assets/img/flaga.svg`,
        // iconSize: [30, 40],
        // iconAnchor: [0, 40],
      });

      let Reposmarker = L.marker([element.latitude, element.longitude], { icon: ReposIcon }).addTo(this.map);
      /*.bindPopup(
            `Station de : ${formatDate(element.timeStart, 'MM/dd HH:mm', 'en_FR')} : a : ${formatDate(
              element.timeEnd,
              'MM/dd HH:mm ',
              'en_FR'
            )}`,
            { maxWidth: 500 }
          );*/

      this.HistoryMarker.push(Reposmarker);

      /*
      const element = listHistorique[index];
      const elementPlusUn = listHistorique[index + 1];

      let distance = this.distance(
        element.latitude,
        element.longitude,
        elementPlusUn.latitude,
        elementPlusUn.longitude,
        'k'
      );

      if (distance < 0.07) {
        stationcompteurObject.push({
          index: index,
          latitude: element.latitude,
          longitude: element.longitude,
          timeStart: element.dateReception,
          timeEnd: elementPlusUn.dateReception,
          activite: element.activite
        });
      }
      */
    }

    /*
    let stationcompteur = 0;

    for (let index = 0; index < stationcompteurObject.length - 1; index++) {
      const element = stationcompteurObject[index];
      const elementPlusUn = stationcompteurObject[index + 1];

      if (elementPlusUn.index - element.index > 1 && stationcompteur > 2) {
        element.timeEnd = elementPlusUn.timeEnd;
        stationArray.push(element);
        stationcompteur = 0;
      } else {
        stationcompteur++;
      }
    }

    if (stationArray.length > 0) {
      for (let index = 0; index < stationArray.length; index++) {
        const element = stationArray[index];

        let ReposIcon = L.icon({
          iconUrl: this.getIconUrl(element.activite),
          //shadowUrl: `../assets/img/flaga.svg`,
          iconSize: [30, 40],
          iconAnchor: [0, 40],
        });

        /*
        let ReposIcon = L.icon({
          iconUrl: `../assets/img/flagr.svg`,
          //shadowUrl: `../assets/img/flaga.svg`,
          iconSize: [30, 40],
          iconAnchor: [0, 40],
        });
        

        //this.createNewMarker('Repos', true, '#FF0000');
        let Reposmarker = L.marker([element.latitude, element.longitude], { icon: ReposIcon })
          .addTo(this.map)
          .bindPopup(
            `Station de : ${formatDate(element.timeStart, 'MM/dd HH:mm', 'en_FR')} : a : ${formatDate(
              element.timeEnd,
              'MM/dd HH:mm ',
              'en_FR'
            )}`,
            { maxWidth: 500 }
          );
        this.HistoryMarker.push(Reposmarker);
      }
    }

    */

    this.HistoryMarker.push(StartMarker, EndMarker);
    this.Travelways = [];
    this.heatMapArray = [];

    listHistorique.forEach((Points) => {
      if (Points.latitude && Points.longitude) {
        let HeatArry = [];
        const lat = L.latLng(Points.latitude, Points.longitude);
        HeatArry.push(+Points.latitude, +Points.longitude, this.getRandomArbitrary().toFixed(2));
        this.heatMapArray.push(HeatArry);
        this.Travelways.push(lat);
      }
    });

    this.path = antPath([this.Travelways], { color: '#573f3f', weight: 5, opacity: 0.6 });
    this.map.addLayer(this.path);
    this.map.fitBounds(this.path.getBounds());
  }

  getIconUrl(activiteNum: any) {
    let activite = this.setActivite(activiteNum);
    let url = '';

    if (activite == 1 || activite == 0) {
      url = '../../assets/img/coupure default.svg';
    }

    if (activite == 2) {
      url = '../../assets/img/coupure default.svg';
    }

    if (activite == 3) {
      url = '../../assets/img/depart default.svg';
    }

    if (activite == 4) {
      url = '../../assets/img/travail default.svg';
    }

    if (activite == 5) {
      url = '../../assets/img/conduit default.svg';
    }

    return url;
  }

  distance(lat1, lon1, lat2, lon2, unit) {
    if (lat1 == lat2 && lon1 == lon2) {
      return 0;
    } else {
      var radlat1 = (Math.PI * lat1) / 180;
      var radlat2 = (Math.PI * lat2) / 180;
      var theta = lon1 - lon2;
      var radtheta = (Math.PI * theta) / 180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);

      if (dist > 1) {
        dist = 1;
      }

      dist = Math.acos(dist);
      dist = (dist * 180) / Math.PI;
      dist = dist * 60 * 1.1515;

      if (unit == 'K') {
        dist = dist * 1.609344;
      }

      if (unit == 'N') {
        dist = dist * 0.8684;
      }

      return dist;
    }
  }

  closeAlert() {
    this.alert.nativeElement.classList.remove('show');
  }

  setActivite(val): number {
    if (val == 0) return 2;

    if (val == 1) return 3;

    if (val == 2) return 4;

    if (val == 3) return 5;

    return 0;
  }

  getListOfVehiculePoximite(lat, lng) {
    this.aproxinfo = [];
    this.listVehiculesItem.forEach((Veh) => {
      // if(Veh.Immatriculation.Vrn==null)
      // return
      if (Veh.Immatriculation.Vrn !== ('' || null)) {
        let distance = this.distance(lat, lng, Veh.Position.Lat, Veh.Position.Lng, 'K');
        if (distance > 0 && distance < 50) {
          this.aproxinfo.push(Veh);
        }
      }
    });
  }

  playpause() {
    if (this.pause) {
      this.pause = false;
      this.pauseval = this.range.nativeElement.value;
    } else {
      this.pause = true;
      this.clearrrtimer(this.countplayer);
      return;
    }

    var restpoint = this.listpointsHistorique.slice(this.pauseval);

    for (var i = 0; i < restpoint.length; i++) {
      this.timer[i] = setTimeout(
        (coords, i) => {
          this.moveMarker(this.map, this.mrkAnimated, L.latLng(Number(coords.latitude), Number(coords.longitude)));
          this.datehistorique = formatDate(restpoint[i].dateReception, 'yyyy-MM-dd', 'en_FR');
          this.timehistorique = formatDate(restpoint[i].dateReception, 'HH:mm:ss', 'en_FR');
          this.range.nativeElement.value = this.pauseval++;
          this.activiteHistorique = this.getValactivite(restpoint[i].activite);
          this.countplayer = i - 1;
        },
        50 * i,
        restpoint[i],
        i
      );

      if (i > 0) {
        clearTimeout(this.timer[i - 1]);
        this.timer.splice(i - 1, 1);
      }
    }
  }

  RangeActiviteStyle(points) {
    var activite = this.setActivite(points.listHistorique[0].activite);
    var randomcolor = '#354360';
    if (activite == 2) {
      randomcolor = '#17a2b8';
    }
    if (activite == 3) {
      randomcolor = '#fd7e14';
    }
    if (activite == 4) {
      randomcolor = '#FF0000';
    }
    if (activite == 5) {
      randomcolor = '#006400';
    }
    var percent = 0;
    var str = '' + randomcolor + ' 0% ';
    var count = 0;
    points.listHistorique.forEach((el) => {
      el.activite = this.setActivite(el.activite);
      if (el.activite == activite) {
        count++;
      } else {
        percent = percent + (count * 100) / points.listHistorique.length;
        str = str + percent + '%, ';
        count = 1;
        activite = el.activite;
        var randomcolor = '#354360';
        if (activite == 2) {
          randomcolor = '#17a2b8';
        }
        if (activite == 3) {
          randomcolor = '#fd7e14';
        }
        if (activite == 4) {
          randomcolor = '#FF0000';
        }
        if (activite == 5) {
          randomcolor = '#006400';
        }
        str = str + '' + randomcolor + ' ' + percent + '% ';
      }
    });
    str = str + ' 100%';
    this.range.nativeElement.style.background = 'linear-gradient(to right,' + str + ')';
  }

  getValactivite(text): string {
    if (text == 3) return 'Attente';

    if (text == 2) return 'Coupure';

    if (text == 4) return 'Travail';

    if (text == 5) return 'Conduite';

    return 'Inconnue';
  }

  moveMarker(map, marker, latlng) {
    marker.setLatLng(latlng);
    map.panTo(latlng);
  }

  clearrrtimer(val) {
    this.countplayer = val;
    this.timer.forEach((el) => {
      clearTimeout(el);
    });
  }

  changelecteur(event) {
    this.pause = true;
    var value = event.target.value;
    this.clearrrtimer(value);
    if (event.target.value == this.countlistpointsHistorique) {
      value--;
    }
    this.moveMarker(
      this.map,
      this.mrkAnimated,
      L.latLng(this.listpointsHistorique[value].latitude, this.listpointsHistorique[value].longitude)
    );
    this.datehistorique = formatDate(this.listpointsHistorique[value].dateReception, 'yyyy-MM-dd', 'en_FR');
    this.timehistorique = formatDate(this.listpointsHistorique[value].dateReception, 'HH:mm:ss', 'en_FR');
  }

  ngOnDestroy(): void {
    if (this._hubConnection) {
      this._hubConnection.stop();
      this._hubConnection = null;
    }

    this.graphservice.killpross();
  }

  drowHeatMap(CurrentV) {
    if (this.showsniper) {
      this.showsniper = false;
    }
    if (this.heatMapMarker.length > 0) {
      this.heatMapMarker.forEach((Marker) => {
        this.map.removeLayer(Marker);
      });
      this.heatMapMarker = [];
    }
    if (this.heatRoute !== null) {
      this.map.removeControl(this.heatRoute);
      this.heatRoute = null;
    }
    if (this.BtnheaMapClick) {
      this.heatMapArray.forEach((element) => {
        const Position = L.latLng(element[0], element[1]);
        let Marker = L.marker(Position, { opacity: 0 }) // hide points
          .bindTooltip(`temperature: ${element[2]} °C`, { keepInView: true })
          .addTo(this.map);
        this.heatMapMarker.push(Marker);
      });
      this.map.removeLayer(this.path);
      this.heatRoute = L.heatLayer(
        this.heatMapArray,
        { radius: 10 },
        { gradient: { 0.4: 'blue', 0.65: 'lime', 1: 'red' } }
      ).addTo(this.map);
    } else {
      this.map.addLayer(this.path);
    }
  }

  getRandomArbitrary() {
    return Math.random() * (100 - 0) + 0;
  }
}
