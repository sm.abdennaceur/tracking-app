import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { plusInfoObject } from '../services/map-service';
import { ModalComponent } from '../../tracking/Modal/modal/modal.component';

@Component({
  selector: 'app-more-info',
  templateUrl: './more-info.component.html',
  styleUrls: ['./more-info.component.scss'],
})
export class MoreInfoComponent implements OnInit, OnChanges {
  @Input() address: plusInfoObject;
  @Input() showAddress :boolean;
  @Output() ClickOnSearch = new EventEmitter<boolean>();

  constructor(private modalService: NgbModal) {}
  ngOnChanges(changes: SimpleChanges): void {
    // if(changes.address.currentValue){
    //   this.show=true
    // }
    //
  }

  ngOnInit(): void {}

  open(lat,lng) {
    this.showAddress = false
    const ModalPOI = this.modalService.open(ModalComponent, { centered: true, size: 'xl' });
    ModalPOI.componentInstance.lng = lat;
    ModalPOI.componentInstance.lat = lng;
    ModalPOI.componentInstance.mapType ='Leaflet'
    ModalPOI.dismissed.subscribe(result=>{
      this.showAddress = true
    })
  }

  closePop() {
    this.ClickOnSearch.emit(false)
    //this.showAddress = false;
  }
}
