import { Component, EventEmitter, HostListener, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../../tracking/Modal/modal/modal.component';
import {  vehiculeModel } from '../../tracking/model/Models';
import { ConfService } from '../../tracking/Services/TrackingServices';
import { mapService } from '../services/map-service';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
})
export class SearchInputComponent implements OnInit {
  @Input() Vehicule: vehiculeModel = new vehiculeModel();
  @Input() Vehiculeslist: Array<any>;
  @Input() listRech: Array<any>;
  @Input() Conducteurlist: Array<any>;
  @Output() RValue = new EventEmitter<any>();
  @Output() ClickOnSearch = new EventEmitter<boolean>();
  @Output() tours = new EventEmitter<any>();
  @Input() show: any = false;
  @Input() show2: any = -1;

  public showlist: any = false;
  public showlist2: any = false;
  public ResRech: Array<any> = [];
  public ResRech1: Array<any> = [];
  public conducteur: any = '';
  public list = [];
  public keyword = 'name';
  public vehiculepayload: any;

  public adr: Array<any> = [];
  public adrApro: Array<any> = [];
  constructor(
    private confService: ConfService,
    config: NgbModalConfig,
    private modalService: NgbModal,
    private mapService: mapService
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  Search(val) {
    if (val.target.value == '') {
      this.showlist = true;
      this.ResRech1 = [];
      return;
    }
    this.ResRech1 = [];
    if(this.Vehiculeslist){
      this.ResRech1 = this.Vehiculeslist.filter(
      (x: any) =>  x.values[0].evenements[0].tachygraphe.vRN.toLocaleLowerCase().indexOf(val.target.value.toLocaleLowerCase()) > -1
    );
    }


    document.getElementById('listg').classList.add('list-group');
    this.showlist = false;
  }

  Rechercher(val) {
    this.ResRech = [];
    switch (val) {
      case 1:
        this.ResRech = this.Vehiculeslist;
        this.showlist = false;
        document.getElementById('listgroup54').classList.add('list-group');
        document.getElementById('listgroup54').focus();
        break;
      default:
        break;
    }

    // if (val == 1) {
    //   this.ResRech = [];
    //   this.listRech.forEach((el) => {
    //     if (el.type == 'Vehicule') {
    //       this.ResRech.push(el);
    //     }
    //   });
    //   this.showlist = false;
    //   // this.showlist2 = true;
    //   document.getElementById('listgroup54').classList.add('list-group');
    //   document.getElementById('listgroup54').focus();
    // }
    // if (val == 0) {
    //   this.ResRech = [];
    //   this.listRech.forEach((el) => {
    //     if (el.type == 'Conducteur') {
    //       this.ResRech.push(el);
    //     }
    //   });
    //   this.showlist = false;
    //   // this.showlist2 = true;
    //   document.getElementById('listgroup54').classList.add('list-group');
    // }
    // if (val == 3) {
    //   this.ResRech = [];
    //   this.showlist = false;
    // }
    // if (val == 2) {
    //   this.ResRech = [];
    //   this.listRech.forEach((el) => {
    //     if (el.activite == 2) {
    //       this.ResRech.push(el);
    //     }
    //   });
    //   this.showlist = false;
    //   //this.showlist2 = true;
    //   document.getElementById('listgroup54').classList.add('list-group');
    // }
    // if (val == 5) {
    //   this.ResRech = [];
    //   this.listRech.forEach((el) => {
    //     if (el.activite == 5) {
    //       this.ResRech.push(el);
    //     }
    //   });
    //   this.showlist = false;
    //   //this.showlist2 = true;
    //   document.getElementById('listgroup54').classList.add('list-group');
    // }
    // if (val == 4) {
    //   this.ResRech = [];
    //   this.listRech.forEach((el) => {
    //     if (el.activite == 4) {
    //       this.ResRech.push(el);
    //     }
    //   });
    //   this.showlist = false;
    //   //this.showlist2 = true;
    //   document.getElementById('listgroup54').classList.add('list-group');
    // }
    // if (val == 3) {
    //   this.ResRech = [];
    //   this.listRech.forEach((el) => {
    //     if (el.activite == 3) {
    //       this.ResRech.push(el);
    //     }
    //   });
    //   this.showlist = false;
    //   this.showlist2 = true;
    // }
  }

  async Showlist1(res, event) {
    event.target.value = '';
    this.ResRech = [];

    this.showlist2 = false;
    await new Promise((f) => setTimeout(f, 200));
    this.showlist = false;
    this.ResRech1 = [];
  }

  async Showlist(res, event) {
    this.ClickOnSearch.emit(true);
    event.target.value = '';
    this.ResRech = [];
    if (res) {
      this.showlist = true;
    } else {
      await new Promise((f) => setTimeout(f, 200));
      this.showlist = false;
    }
  }

  Onchange(value: any) {
    if (value == '') {
      this.show = false;
    } else {
      this.show = true;
    }
    this.RValue.emit(value);
  }

  vide(event: any) {
    event.target.value = '';
  }


  ngOnChanges(changes: SimpleChanges) {
  }


  public adress: any = '';


  clickBody() {
    if (this.showlist) this.showlist = false;
    this.showlist2 = false;
  }
  async ngOnInit() {}

  selectEvent(vehiculeId, item) {
    this.RValue.emit(vehiculeId);
    this.showlist = false;
    this.ResRech = [];
  }
  searchCleared() {
    this.show = false;
  }
  onChangeSearch(val: string) {}

  onFocused(e) {
  }

  open(lat,lng) {
    this.show = false
    const ModalPOI = this.modalService.open(ModalComponent, { centered: true, size: 'xl' });
    ModalPOI.componentInstance.lng = lat;
    ModalPOI.componentInstance.lat = lng;
    ModalPOI.componentInstance.mapType ='Leaflet'
    ModalPOI.dismissed.subscribe(result=>{
      this.show = true
    })
  }

  sendtournees(item){
    this.tours.emit(item)
  }
  Closedpanel(){

  }
  navigateToCommande(idCommande){
    window.open("/tms/ordre-transport/"+idCommande)
  }
  navigateToPlaning(){
    window.open("tms/planing/Impression")
  }
}
