import { Injectable } from "@angular/core";  
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";  
import { Observable } from "rxjs";  
import { HistoryObject, mapService } from "./map-service";

  
@Injectable({
    providedIn: 'root',
  })
export class DataPointsResolve implements Resolve<HistoryObject[]> {  
  constructor(private mapService: mapService) {}  
  
  resolve(route: ActivatedRouteSnapshot): Observable<HistoryObject[]> {  
    return this.mapService.getPoints();  
  }  
}  