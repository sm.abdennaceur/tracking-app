import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, from, Observable, Subject, throwError } from 'rxjs';

import { catchError, filter, groupBy, map, mergeMap, reduce, tap, toArray } from 'rxjs/operators';
import { ConfigService } from '@tmsApp/login/services/environment.service';

import { HubConnection } from '@microsoft/signalr';
import * as signalR from '@microsoft/signalr';
import { group } from 'console';
import { element } from 'protractor';
import { formatDate } from '@angular/common';
import { LatLng } from '@agm/core';

@Injectable({
  providedIn: 'root',
})
export class mapService {
  private connection: signalR.HubConnection;
  connectionEstablished = new Subject<Boolean>();

  private LivePoints = new BehaviorSubject<newHistory[]>([]);
  LivePoints$ = this.LivePoints.asObservable();
  errorcase: any[] = [];
  public AppParameters: any;

  Message: SignalRObject[] = [];
  AllVehiculeRow: VehiculePositionObject[];
  AlluserSpiRow: any;

  constructor(private http: HttpClient, public configService: ConfigService) {
    this.AppParameters = this.configService.config;
  }

  signalRObservable(): Observable<SignalRObject> {
    const subject: Subject<any> = new Subject<any>();

    let url = this.AppParameters.appURl + 'TrackingHub';
    url.replace('//', '/');
    if (!this.connection) {
      this.connection = new signalR.HubConnectionBuilder()
        .withUrl(url)
        .configureLogging(signalR.LogLevel.Information)
        .build();
      this.connection
        .start()
        .then(() => {
          console.log('Hub connection started');
          this.connectionEstablished.next(true);
        })
        .catch((err) => {
          // console.log('error ',err)
          return subject.next(this.errorcase);
        });

      this.connection.on('ReceiveMessage', (data) => {
        let d = JSON.parse(data);
        // console.log('d ',d)
        if (d.evenements[0].tachygraphe.vRN != null) {
          if (d.evenements[0].geoLocalisation.position != null) {
            //d.evenements = d.evenements.filter(ev=>ev.position!=null)
            this.Message.push(d);
          }
        }
        console.log('this.Message :', this.Message);
        from(this.Message)
          .pipe(
            groupBy((object) => object.vehiculeId),
            mergeMap((group) =>
              group.pipe(
                reduce(
                  (groupMessage, message) => {
                    groupMessage.values.push(message);
                    return groupMessage;
                  },
                  { vehiculeId: group.key, values: [] }
                )
              )
            ),
            toArray()
          )
          .pipe(map((data) => data.sort((a, b) => (a.values.length > b.values.length ? -1 : 1))))
          .subscribe((newData: any) => {
            subject.next(newData);
            this.LivePoints.next([...newData]);
          });
      });
    }

    this.connection.onclose((err?: Error) => {
      if (err) {
        console.log('err', err);
        subject.error(err);
      } else {
        subject.complete();
        this.LivePoints.complete();
      }
    });

    return subject;
  }

  getDatafromSignalRbyId(id: string) {
    return this.LivePoints$.pipe(map((data) => data.filter((points) => points.vehiculeId === id)));
  }

  GetHistoriqueVehiculeId(id: string) {
    return this.AllVehiculeRow.filter((point) => point.id === id);
  }
  GetHistoriqueUserSpi(id: string) {
    return this.AlluserSpiRow.filter((point) => point.id === id);
  }

  disconnect() {
    if (this.connection) {
      this.connection.stop();
      this.connection = null;
    }
  }

  getAddress(lng, lat) {
    return fetch(
      `https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?f=pjson&langCode=fr&location=${lng},${lat}`
    );
  }
  async getPositionXY(adresse): Promise<any> {
    const promise = fetch(
      `https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/findAddressCandidates?SingleLine=${adresse}&category=&outFields=*&forStorage=false&f=pjson`
    )
      .then((res) => res.json())
      .then((data) => {
        return data;
      });
    return promise;
  }
  getCountryInfo(code: string) {
    return this.CountryCodes[code];
  }

  // getTournees(id): Observable<any> {
  //   return this.http.get<any>(this.AppParameters.apiUrl + 'api/tra/api/Vehicule/GetTournesFromTMS', {
  //     headers: {
  //       Authorization: 'Bearer ' + localStorage.getItem('Strada_Token'),
  //     },
  //     params: {
  //       vehiculeId: id,
  //       dateDebut: formatDate('01-01-2020', 'yyyy-MM-dd', 'en_US'),
  //       dateFin: formatDate(Date.now().toString(), 'yyyy-MM-dd', 'en_US'),
  //     },
  //   });
  // }
  async getTournees(id): Promise<any> {
    const promise = await this.http
      .get<any>(this.AppParameters.apiUrl + 'api/tra/api/Vehicule/GetTournesFromTMS', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('Strada_Token'),
        },
        params: {
          vehiculeId: id,
          dateDebut: formatDate('01-01-2020', 'yyyy-MM-dd', 'en_US'),
          dateFin: formatDate(Date.now().toString(), 'yyyy-MM-dd', 'en_US'),
        },
      }).pipe(map(data=>{
        data.forEach(el => {
          el.dateDebut = formatDate(el.dateDebut, 'yyyy-MM-dd', 'en_US')
          el.dateFin = formatDate(el.dateFin, 'yyyy-MM-dd', 'en_US')
          el.enlevementLivraisons.forEach(el => {
            if (!el.isEnlevement) {
              el.livraison.dateSouhaite = formatDate(el.livraison.dateSouhaite, 'yyyy-MM-dd', 'en_US')
            }else{
              el.expedition.dateSouhaite = formatDate(el.expedition.dateSouhaite, 'yyyy-MM-dd', 'en_US')
            }
          });
        });
         return data
      }))
      .toPromise()
      .catch((error) => {
        console.log(error.status);
        return null;
      });

    return promise;
  }

  getGetLast5HistoriqueVehicule(id) {
    return this.http.get<VehiculePositionObject[]>(
      this.AppParameters.apiUrl + 'api/tra/api/HistoriqueVehicule/GetLast5HistoriqueVehicule',
      {
        params: {
          IdVehicule: id,
        },
      }
    );
  }
  GetLast5PositionByUtilisateurId(id) {
    return this.http.get<any[]>(
      this.AppParameters.apiUrl + 'api/tra/api/HistoriqueVehicule/GetLast5PositionByUtilisateurId',
      {
        params: {
          IdUtilisateur: id,
        },
      }
    );
  }
  getPoints(): Observable<any> {
    return this.http
      .get<VehiculePositionObject[]>(this.AppParameters.apiUrl + 'api/tra/api/HistoriqueVehicule/GetHistoriqueVehicule')
      .pipe(
        tap((res: any) =>
          res.vehiculeRow.forEach((element) => {
            if (element.vrn) {
              let data: SignalRObject = {
                boitierId: null,
                compagnieId: null,
                creationDate: element.dateReception,
                id: element.id,
                ischecked: null,
                organisationId: null,
                ressourceId: null,
                tenantNamespace: null,
                vehiculeId: element.id,
                version: null,
                evenements: [
                  {
                    capteurs: null,
                    geoLocalisation: {
                      compteurKm: null,
                      date: null,
                      hasApresContact: null,
                      isEnMouvement: null,
                      position: {
                        cap: null,
                        latitude: +element.latitude,
                        longitude: +element.longitude,
                        vitesse: null,
                      },
                    },
                    tachygraphe: {
                      compteurKm: null,
                      conducteur: null,
                      date: null,
                      passager: null,
                      vIN: element.vin,
                      vRN: element.vrn,
                    },
                  },
                ],
              };
              this.Message.push(data);

              this.errorcase.push({ vehiculeId: data.vehiculeId, values: [data] });
            }
          })
        ),
        tap((res) => (this.AllVehiculeRow = res.vehiculeRow)),
        tap((res) => (this.AlluserSpiRow = res.userSpiRow))
        // map((data: VehiculePositionObject[]) => {
        //   console.log(data)
        //   return data.map((d) => {
        //     console.log(d)
        //     if (d.vrn) {
        //       return {
        //         AlreadyPushed: false,
        //         id: d.id,
        //         dateReception: d.dateReception,
        //         latitude: d.latitude,
        //         longitude: d.longitude,
        //         vin: d.vin,
        //         vrn: d.vrn,
        //       };
        //     } else {
        //       return false;
        //     }
        //   });
        // })
      );
  }
  getPointsByid(id, start, end): Observable<any> {
    return this.http.get<any>(this.AppParameters.apiUrl + 'api/tra/api/HistoriqueVehicule/GetAllHistoriqueVehicule', {
      params: {
        IdVehicule: id,
        startDate: start,
        endDate: end,
      },
    });
  }

  async getALLPoints(): Promise<any> {
    const promise = await this.http
      .get<any>(this.AppParameters.apiUrl + 'api/tra/api/HistoriqueVehicule/GetHistoriqueVehicule')
      .toPromise();
    return promise;
  }

  getPositionFromAdress(adresse): Observable<any> {
    const header = {
      headers: new HttpHeaders().set(`Access-Control-Allow-Origin`, `*`),
    };
    return (
      this.http
        .get<any>(
          `https://maps.googleapis.com/maps/api/geocode/json?address=` +
            adresse +
            `&key=AIzaSyCWP09i8wWlzRwgvDXOhyYWnDOxnozNjTA`,
          header
        )
        // return this.http.get<any>(`http://localhost:3001/delivery/relaypoint` ) ///read
        .pipe(
          map((q) => {
            return q;
          })
        )
    );
  }

  CountryCodes = {
    AFG: 'Afghanistan',
    ALB: 'Albania',
    DZA: 'Algeria',
    ASM: 'American Samoa',
    AND: 'Andorra',
    AGO: 'Angola',
    AIA: 'Anguilla',
    ATA: 'Antarctica',
    ATG: 'Antigua and Barbuda',
    ARG: 'Argentina',
    ARM: 'Armenia',
    ABW: 'Aruba',
    AUS: 'Australia',
    AUT: 'Austria',
    AZE: 'Azerbaijan',
    BHS: 'Bahamas (the)',
    BHR: 'Bahrain',
    BGD: 'Bangladesh',
    BRB: 'Barbados',
    BLR: 'Belarus',
    BEL: 'Belgium',
    BLZ: 'Belize',
    BEN: 'Benin',
    BMU: 'Bermuda',
    BTN: 'Bhutan',
    BOL: 'Bolivia (Plurinational State of)',
    BES: 'Bonaire, Sint Eustatius and Saba',
    BIH: 'Bosnia and Herzegovina',
    BWA: 'Botswana',
    BVT: 'Bouvet Island',
    BRA: 'Brazil',
    IOT: 'British Indian Ocean Territory (the)',
    BRN: 'Brunei Darussalam',
    BGR: 'Bulgaria',
    BFA: 'Burkina Faso',
    BDI: 'Burundi',
    CPV: 'Cabo Verde',
    KHM: 'Cambodia',
    CMR: 'Cameroon',
    CAN: 'Canada',
    CYM: 'Cayman Islands (the)',
    CAF: 'Central African Republic (the)',
    TCD: 'Chad',
    CHL: 'Chile',
    CHN: 'China',
    CXR: 'Christmas Island',
    CCK: 'Cocos (Keeling) Islands (the)',
    COL: 'Colombia',
    COM: 'Comoros (the)',
    COD: 'Congo (the Democratic Republic of the)',
    COG: 'Congo (the)',
    COK: 'Cook Islands (the)',
    CRI: 'Costa Rica',
    HRV: 'Croatia',
    CUB: 'Cuba',
    CUW: 'Curaçao',
    CYP: 'Cyprus',
    CZE: 'Czechia',
    CIV: "Côte d'Ivoire",
    DNK: 'Denmark',
    DJI: 'Djibouti',
    DMA: 'Dominica',
    DOM: 'Dominican Republic (the)',
    ECU: 'Ecuador',
    EGY: 'Egypt',
    SLV: 'El Salvador',
    GNQ: 'Equatorial Guinea',
    ERI: 'Eritrea',
    EST: 'Estonia',
    SWZ: 'Eswatini',
    ETH: 'Ethiopia',
    FLK: 'Falkland Islands (the) [Malvinas]',
    FRO: 'Faroe Islands (the)',
    FJI: 'Fiji',
    FIN: 'Finland',
    FRA: 'France',
    GUF: 'French Guiana',
    PYF: 'French Polynesia',
    ATF: 'French Southern Territories (the)',
    GAB: 'Gabon',
    GMB: 'Gambia (the)',
    GEO: 'Georgia',
    DEU: 'Germany',
    GHA: 'Ghana',
    GIB: 'Gibraltar',
    GRC: 'Greece',
    GRL: 'Greenland',
    GRD: 'Grenada',
    GLP: 'Guadeloupe',
    GUM: 'Guam',
    GTM: 'Guatemala',
    GGY: 'Guernsey',
    GIN: 'Guinea',
    GNB: 'Guinea-Bissau',
    GUY: 'Guyana',
    HTI: 'Haiti',
    HMD: 'Heard Island and McDonald Islands',
    VAT: 'Holy See (the)',
    HND: 'Honduras',
    HKG: 'Hong Kong',
    HUN: 'Hungary',
    ISL: 'Iceland',
    IND: 'India',
    IDN: 'Indonesia',
    IRN: 'Iran (Islamic Republic of)',
    IRQ: 'Iraq',
    IRL: 'Ireland',
    IMN: 'Isle of Man',
    ISR: 'Israel',
    ITA: 'Italy',
    JAM: 'Jamaica',
    JPN: 'Japan',
    JEY: 'Jersey',
    JOR: 'Jordan',
    KAZ: 'Kazakhstan',
    KEN: 'Kenya',
    KIR: 'Kiribati',
    PRK: "Korea (the Democratic People's Republic of)",
    KOR: 'Korea (the Republic of)',
    KWT: 'Kuwait',
    KGZ: 'Kyrgyzstan',
    LAO: "Lao People's Democratic Republic (the)",
    LVA: 'Latvia',
    LBN: 'Lebanon',
    LSO: 'Lesotho',
    LBR: 'Liberia',
    LBY: 'Libya',
    LIE: 'Liechtenstein',
    LTU: 'Lithuania',
    LUX: 'Luxembourg',
    MAC: 'Macao',
    MDG: 'Madagascar',
    MWI: 'Malawi',
    MYS: 'Malaysia',
    MDV: 'Maldives',
    MLI: 'Mali',
    MLT: 'Malta',
    MHL: 'Marshall Islands (the)',
    MTQ: 'Martinique',
    MRT: 'Mauritania',
    MUS: 'Mauritius',
    MYT: 'Mayotte',
    MEX: 'Mexico',
    FSM: 'Micronesia (Federated States of)',
    MDA: 'Moldova (the Republic of)',
    MCO: 'Monaco',
    MNG: 'Mongolia',
    MNE: 'Montenegro',
    MSR: 'Montserrat',
    MAR: 'Morocco',
    MOZ: 'Mozambique',
    MMR: 'Myanmar',
    NAM: 'Namibia',
    NRU: 'Nauru',
    NPL: 'Nepal',
    NLD: 'Netherlands (the)',
    NCL: 'New Caledonia',
    NZL: 'New Zealand',
    NIC: 'Nicaragua',
    NER: 'Niger (the)',
    NGA: 'Nigeria',
    NIU: 'Niue',
    NFK: 'Norfolk Island',
    MNP: 'Northern Mariana Islands (the)',
    NOR: 'Norway',
    OMN: 'Oman',
    PAK: 'Pakistan',
    PLW: 'Palau',
    PSE: 'Palestine, State of',
    PAN: 'Panama',
    PNG: 'Papua New Guinea',
    PRY: 'Paraguay',
    PER: 'Peru',
    PHL: 'Philippines (the)',
    PCN: 'Pitcairn',
    POL: 'Poland',
    PRT: 'Portugal',
    PRI: 'Puerto Rico',
    QAT: 'Qatar',
    MKD: 'Republic of North Macedonia',
    ROU: 'Romania',
    RUS: 'Russian Federation (the)',
    RWA: 'Rwanda',
    REU: 'Réunion',
    BLM: 'Saint Barthélemy',
    SHN: 'Saint Helena, Ascension and Tristan da Cunha',
    KNA: 'Saint Kitts and Nevis',
    LCA: 'Saint Lucia',
    MAF: 'Saint Martin (French part)',
    SPM: 'Saint Pierre and Miquelon',
    VCT: 'Saint Vincent and the Grenadines',
    WSM: 'Samoa',
    SMR: 'San Marino',
    STP: 'Sao Tome and Principe',
    SAU: 'Saudi Arabia',
    SEN: 'Senegal',
    SRB: 'Serbia',
    SYC: 'Seychelles',
    SLE: 'Sierra Leone',
    SGP: 'Singapore',
    SXM: 'Sint Maarten (Dutch part)',
    SVK: 'Slovakia',
    SVN: 'Slovenia',
    SLB: 'Solomon Islands',
    SOM: 'Somalia',
    ZAF: 'South Africa',
    SGS: 'South Georgia and the South Sandwich Islands',
    SSD: 'South Sudan',
    ESP: 'Spain',
    LKA: 'Sri Lanka',
    SDN: 'Sudan (the)',
    SUR: 'Suriname',
    SJM: 'Svalbard and Jan Mayen',
    SWE: 'Sweden',
    CHE: 'Switzerland',
    SYR: 'Syrian Arab Republic',
    TWN: 'Taiwan (Province of China)',
    TJK: 'Tajikistan',
    TZA: 'Tanzania, United Republic of',
    THA: 'Thailand',
    TLS: 'Timor-Leste',
    TGO: 'Togo',
    TKL: 'Tokelau',
    TON: 'Tonga',
    TTO: 'Trinidad and Tobago',
    TUN: 'Tunisia',
    TUR: 'Turkey',
    TKM: 'Turkmenistan',
    TCA: 'Turks and Caicos Islands (the)',
    TUV: 'Tuvalu',
    UGA: 'Uganda',
    UKR: 'Ukraine',
    ARE: 'United Arab Emirates (the)',
    GBR: 'United Kingdom of Great Britain and Northern Ireland (the)',
    UMI: 'United States Minor Outlying Islands (the)',
    USA: 'United States of America (the)',
    URY: 'Uruguay',
    UZB: 'Uzbekistan',
    VUT: 'Vanuatu',
    VEN: 'Venezuela (Bolivarian Republic of)',
    VNM: 'Viet Nam',
    VGB: 'Virgin Islands (British)',
    VIR: 'Virgin Islands (U.S.)',
    WLF: 'Wallis and Futuna',
    ESH: 'Western Sahara',
    YEM: 'Yemen',
    ZMB: 'Zambia',
    ZWE: 'Zimbabwe',
    ALA: 'Åland Islands',
  };
}
export interface HistoryObject {
  idVehicule: string;
  listHistorique: PointHistoryObject;
  AlreadyPushed: boolean;
  vehiculeRow: vehiculeRowObject;
}
export interface vehiculeRowObject {
  dateReception: string;
  id: string;
  isVehicule: boolean;
  latitude: string;
  longitude: string;
  numConducteur: string;
  vin: string;
  vrn: string;
}
export interface userSpiRowobject {
  dateReception: string;
  id: string;
  isVehicule: boolean;
  latitude: string;
  longitude: string;
  nom: string;
  prenom: string;
}
export interface PointHistoryObject {
  conducteur: string;
  conducteurId: string;
  createdBy: string;
  createdById: string;
  createdDate: string;
  dateReception: string;
  id: string;
  latitude: number;
  longitude: number;
  modifiedBy: string;
  modifiedById: string;
  modifiedDate: string;
  vehicule: string;
  vehiculeId: string;
}
export interface SignalRObject {
  compagnieId: string;
  organisationId: string;
  vehiculeId: string;
  ressourceId: string;
  boitierId: string;
  evenements: EvenementsObject[];
  id: string;
  creationDate: string;
  version: string;
  tenantNamespace: string;
  ischecked: boolean;
  position?: PointObject[];
}
export interface EvenementsObject {
  geoLocalisation: GeoLocalisationObject;
  tachygraphe: TachygrapheObject;
  capteurs: string;
}
export interface TachygrapheObject {
  compteurKm: string;
  conducteur: string;
  date: string;
  passager: number;
  vIN: string;
  vRN: string;
}
export interface GeoLocalisationObject {
  date: Date;
  position: PositionObject;
  compteurKm: number;
  hasApresContact: boolean;
  isEnMouvement: string;
}
export interface PositionObject {
  latitude: number;
  longitude: number;
  vitesse: number;
  cap: number;
}

export interface PointObject {
  latitude: number;
  longitude: number;
}

export interface newHistory {
  vehiculeId: string;
  values?: [];
}
export interface VehiculePositionObject {
  dateReception: string;
  id: string;
  latitude: number;
  longitude: number;
  vin: string;
  vrn: string;
  AlreadyPushed?: boolean;
}
export interface UserSPIPositionObject {
  dateReception: string;
  id: string;
  isVehicule: boolean;
  latitude: string;
  longitude: string;
  nom: string;
  prenom: string;
}
export interface addressObject {
  location: {
    x: number;
    y: number;
  };
  address: {
    AddNum: string;
    Addr_type: string;
    Address: string;
    Block: string;
    City: string;
    CountryCode: string;
    District: string;
    LongLabel: string;
    Match_addr: string;
    MetroArea: string;
    Neighborhood: string;
    PlaceName: string;
    Postal: string;
    PostalExt: string;
    Region: string;
    Sector: string;
    ShortLabel: string;
    Subregion: string;
    Territory: string;
  };
}
export class Infotournees {
  type?: string;
  point?: any;
}
export class ListInfotournes {
  ref?: string;
  list?: Array<Infotournees>;
}

export interface plusInfoObject {
  info: addressObject;
  VehiculesProximite: any;
}
