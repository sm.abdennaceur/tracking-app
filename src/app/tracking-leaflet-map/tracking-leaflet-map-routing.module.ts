import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeafletMapIndexComponent } from './leaflet-map-index/leaflet-map-index.component';
import { DataPointsResolve } from './services/data-points.resolve';

const routes: Routes = [
  {
    path: '',
    component: LeafletMapIndexComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrackingLeafletMapRoutingModule { }
