import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrackingLeafletMapRoutingModule } from './tracking-leaflet-map-routing.module';
import { LeafletMapIndexComponent } from './leaflet-map-index/leaflet-map-index.component';
import { SearchInputComponent } from './search-input/search-input.component';

import { LeafletMarkerClusterModule } from '@asymmetrik/ngx-leaflet-markercluster';
import {LeafletModule} from '@asymmetrik/ngx-leaflet';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatOptionModule } from '@angular/material/core';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { MoreInfoComponent } from './more-info/more-info.component';
import { TrackingModule } from '../tracking/tracking.module';


@NgModule({
  declarations: [LeafletMapIndexComponent, SearchInputComponent, MoreInfoComponent],
  imports: [
    CommonModule,
    TrackingLeafletMapRoutingModule,
    LeafletModule,
    LeafletMarkerClusterModule,
    AngularMaterialModule,
    MatOptionModule,
    MatFormFieldModule,
    TrackingModule
  ],
  exports: [MoreInfoComponent]
})
export class TrackingLeafletMapModule { }
