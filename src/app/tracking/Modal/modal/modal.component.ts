import { ThrowStmt } from '@angular/compiler';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { mapService } from '../../../tracking-leaflet-map/services/map-service';
import { isNumeric } from 'jquery';
import { InfoClient } from '../../model/Models';
import { ConfService } from '../../Services/TrackingServices';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  @Input() d: string;
  @Input() c: string;
  @Input() lng: any;
  @Input() lat: any;
  @Input() mapType: any;
  @ViewChild('nom', { static: false }) nom: ElementRef;
  @ViewChild('nomu', { static: false }) nomu: ElementRef;
  @ViewChild('adr1', { static: false }) adr1: ElementRef;
  @ViewChild('adr2', { static: false }) adr2: ElementRef;
  @ViewChild('ville', { static: false }) ville: ElementRef;
  @ViewChild('cp', { static: false }) cp: ElementRef;
  @ViewChild('pays', { static: false }) pays: ElementRef;
  @ViewChild('tel', { static: false }) tel: ElementRef;
  @ViewChild('tva', { static: false }) tva: ElementRef;
  public adress: any = '';
  public pay: any = '';
  public codep: any = '';
  public villle: any = '';
  public show: any = true;
  public valSelected: any = '0';
  public services: ConfService = null;
  constructor(public activeModal: NgbActiveModal, public service: ConfService,private mapService: mapService) {
    this.services = service;
  }
  save() {
    var c: InfoClient = new InfoClient();

    if (this.valSelected != 0) {
      if (this.valSelected == 1) {
        c.IsParticulier = true;
        c.Telephone = this.tel.nativeElement.value;
      } else if (this.valSelected == 2) {
        c.IsParticulier = false;
        c.Telephone = this.tel.nativeElement.value;
        c.NumTva = this.tva.nativeElement.value;
      } else c.IsParticulier = false;

      c.Latitude = this.lat;
      c.Longitude = this.lng;
      c.NomClient = this.nom.nativeElement.value;
      c.Adresse1 = this.adr1.nativeElement.value;
      c.Adresse2 = this.adr2.nativeElement.value;
      c.Pays = this.pays.nativeElement.value;
      c.Ville = this.ville.nativeElement.value;
      c.CodePostal = this.cp.nativeElement.value;
      this.services.AddClient(c);
      this.close();
    }
  }
  ngOnInit(): void {
    this.codeadr();
    // Get the container element
    var btnContainer = document.getElementById('myDIV');

    // Get all buttons with class="btn" inside the container
    var btns = btnContainer.getElementsByClassName('btnbtn');

    // Loop through the buttons and add the active class to the current/clicked button
    for (var i = 0; i < btns.length; i++) {
      btns[i].addEventListener('click', function () {
        var current = document.getElementsByClassName('active');
        current[0].className = current[0].className.replace(' active', '');
        this.className += ' active';
      });
    }
  }
  codeadr(): any {
    if (this.mapType && this.mapType === 'Leaflet') {
      this.mapService
      .getAddress(this.lat,this.lng)
      .then((res) => res.json())
      .then(async (address) => {
        let Country = await this.mapService.getCountryInfo(address.address.CountryCode);
        this.pay = Country;
        this.villle = address.address.City;
        this.codep = address.address.Postal;
        this.adress =address.address.Match_addr;
      })
    } else {

      var point = new google.maps.LatLng(this.lat, this.lng);
      var geocoder = new google.maps.Geocoder();
      geocoder.geocode({ location: point }, (results, status) => {
        if (status == google.maps.GeocoderStatus.OK) {
          console.log('results[1] :',results[1])
          //    this.codep = results[1].address_components['postal_code']
          var adr = results[1].formatted_address;
          var array = adr.split(',');
          this.adress = array[0];
          this.codep = array[1].substr(1, 5);
          if (Number(this.codep)) {
            this.villle = array[1].substring(7);
          } else {
            this.villle = array[1];
            this.codep = '';
          }

          console.log(this.codep);
          this.pay = array[array.length - 1];
        }
      });
    }
  }
  Show(a, event) {
    this.show = a;
  }
  close() {
    this.activeModal.dismiss();
  }
  Isselected(val) {
    this.valSelected = val;
  }
}
