import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { InfoClient, InfoConducteur, InfoVehicule } from '../model/Models';
import { ConfigService } from '@tmsApp/login/services/environment.service';
import { formatDate } from '@angular/common';
import { AuthentificationEnvService } from '@strada/lib-authentification';
@Injectable({
  providedIn: 'root',
})
export class ConfService {
  public AppParameters: any;
  constructor(private http: HttpClient, public configService: ConfigService) {
    this.AppParameters = this.configService.config;
   
  }
  
  // get info vehicules par id
  async get(id): Promise<InfoVehicule> {
    if (id == "" || id == "00000000-0000-0000-0000-000000000000") {
      return null;
    }
    const promise = await this.http.get<InfoVehicule>(this.AppParameters.apiUrl + 'api/tra/api/Vehicule/GetVehiculeById', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("Strada_Token")}`,
      },
      params: {
        VehiculeId: id
      }
    }).toPromise().catch((error) => { console.log(error.status); return null; });
    return promise;
  }
  //get info conducteur par id
  async getConducteur(id): Promise<InfoConducteur> {
    const promise = await this.http.get<InfoConducteur>(this.AppParameters.apiUrl + 'api/tra/api/Conducteur/GetConducteurByCardNumberFromEam', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("Strada_Token")}`,
      },
      params: {
        CardNumber: id
      }
    }).toPromise().catch((error) => { console.log(error.status); return null; });
    return promise;
  }
  async getConducteur1(Numcard): Promise<InfoConducteur> {
    const promise = await this.http.get<InfoConducteur>(this.AppParameters.apiUrl + 'api/tra/api/Conducteur/GetConducteurByCardNumber', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("Strada_Token")}`,
      },
      params: {
        CardNumber: Numcard
      }
    }).toPromise().catch((error) => { console.log(error.status); return null; });
    return promise;
  }
  // get all vehicules avec les 5 derniers points
  async getPoints(): Promise<any> {
    console.log(this.AppParameters.ddd)
    const promise = await this.http.get<InfoConducteur>(this.AppParameters.apiUrl + 'api/tra/api/HistoriqueVehicule/GetHistoriqueVehicule').toPromise()
    return promise;
  }
  //get historique vehicules with date debut et fin
  async getPointsByid(id, start, end): Promise<any> {

    const promise = await this.http.get<any>(this.AppParameters.apiUrl + 'api/tra/api/HistoriqueVehicule/GetAllHistoriqueVehicule', {
      params: {
        IdVehicule: id,
        startDate: start,
        endDate: end
      }
    }).toPromise().catch((error) => { //console.log(error.status);
       return null; });
    return promise;
  }
  async getPointsUserByid(id, start, end): Promise<any> {

    const promise = await this.http.get<any>(this.AppParameters.apiUrl + 'api/tra/api/HistoriqueVehicule/GetAllHistoriqueUser', {
      params: {
        IdVehicule: id,
        startDate: start,
        endDate: end
      }
    }).toPromise().catch((error) => { console.log(error.status); return null; });
    return promise;
  }
  // get last 5 points with id vehicule
  async getlast5PointsByid(id): Promise<any> {

    const promise = await this.http.get<any>(this.AppParameters.apiUrl + 'api/tra/api/HistoriqueVehicule/GetLast5HistoriqueVehicule', {
      params: {
        IdVehicule: id
      }
    }).toPromise().catch((error) => { console.log(error.status); return null; });
    return promise;
  }
  // get last 5 points with id User
  async getlast5PointsUserByid(id): Promise<any> {

      const promise = await this.http.get<any>(this.AppParameters.apiUrl + 'api/tra/api/HistoriqueVehicule/GetLast5PositionByUtilisateurId', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('Strada_Token'),
        },
        params: {
          IdUtilisateur: id
        }
      }).toPromise().catch((error) => { console.log(error.status); return null; });
      return promise;
  }
  //get list client tracking
  async getClients(): Promise<any> {
    console.log
    const promise = await this.http.get<any>(this.AppParameters.apiUrl + 'api/tra/api/Client/GetListClient',{ 
      headers: {
      //Authorization: 'Bearer ' + localStorage.getItem('Strada_Token'),
      Authorization: `Bearer ${localStorage.getItem("Strada_Token")}`
        }
    })
    .toPromise()
    .catch((error) => { console.log(error.status); return null; });
    return promise;
  }
  // add client tracking
  async AddClient(Client: InfoClient): Promise<any> {
    Client.ReferenceClient = Client.Telephone
    Client.Adresse3 = "adr3"
    const promise = await this.http.post<any>(this.AppParameters.apiUrl + 'api/tra/api/Client/AddClient', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("Strada_Token")}`,
      },
      "isParticulier": Client.IsParticulier,
      "referenceClient": Client.ReferenceClient,
      "adresse1": Client.Adresse1,
      "adresse2": Client.Adresse2,
      "adresse3": Client.Adresse3,
      "codePostal": Client.CodePostal,
      "ville": Client.Ville,
      "pays": Client.Pays,
      "longitude": Client.Longitude.toString(),
      "latitude": Client.Latitude.toString(),
      "nomClient": Client.NomClient,
      "numTva": Client.NumTva,
      "telephone": Client.Telephone
    }).toPromise().catch((error) => { console.log(error.status); return null; })

  }
  //get list tournees par id vehicule
  async getTournees(id): Promise<any> {
    const promise = await this.http.get<any>(this.AppParameters.apiUrl + 'api/tra/api/Vehicule/GetTournesFromTMS',
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("Strada_Token")}`,
        },
        params: {
          vehiculeId: id,
          dateDebut:formatDate('01-01-2020', 'yyyy-MM-dd', 'en_US'),
          dateFin:formatDate(Date.now().toString(), 'yyyy-MM-dd', 'en_US'),
        }
      }
    ).toPromise()
    .catch((error) => {
      console.log(error.status);
      return null;
    });

    return promise;
  }
  //get list tournees par id vehicule
  async getTourneeswithdate(id,datedebut,datefin): Promise<any> {
    const promise = await this.http.get<any>(this.AppParameters.apiUrl + 'api/tra/api/Vehicule/GetTournesFromTMS',
      {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('Strada_Token'),
        },
        params: {
          vehiculeId: id,
          dateDebut:formatDate('01-01-2020', 'yyyy-MM-dd', 'en_US'),
          dateFin:formatDate(Date.now().toString(), 'yyyy-MM-dd', 'en_US'),
        }
      }
    ).toPromise()
    .catch((error) => {
      console.log(error.status);
      return null;
    });

    return promise;
  }

}
