import { Injectable, OnInit } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
@Injectable({
  providedIn: 'root',
})
export class GQLService {
  private apo: Apollo;
  constructor(apoo: Apollo) {
    this.apo = apoo;
  }
  rates: any[];
  loading = true;
  error: any;
  killpross() {
    this.apo.client.stop();
  }
  getHistorique(DateDebut, DateFin, vrn): any {
    return this.apo
      .query({
        query: gql`
        query
        gethistorique($Vrn: UUID,$dateDebut:DateTime,$dateFin:DateTime){ trackings(
           
              where:{  
               ressourceId:{
                             eq: $Vrn
                       }
                       creationDate:  {
                                 lte  : $dateFin 
                                 gte  : $dateDebut
                               }
                    
                   }
              first:1000
                 )
                  {
             nodes{
                       evenements{
                         tachygraphe{
                           vRN 
                           vIN 
                           compteurKm
                           conducteur{
                             activite
                           }
                           } 
                           geoLocalisation{
                             position {
                               latitude 
                               longitude 
                               vitesse
                               }  
                               isEnMouvement 
                               date 
                               compteurKm
                           }
                         }
                     } 
                   }
            }
        `,
        variables: {
          Vrn: vrn,
          dateFin: DateFin,
          dateDebut: DateDebut
        }
      })
  }
  async getlast5points(Id): Promise<any> {
    const a = await this.apo
      .query({
        query: gql`
       query getLast5points($Vrn: UUID){
        trackings(
          first:5
          where:{
          ressourceId:{
                         eq: $Vrn
                   }
        }
         order: {
          creationDate: DESC
          }
        
        ){
       nodes{
          evenements{
                      tachygraphe{
                        vRN 
                        vIN 
                        compteurKm
                        } 
                        geoLocalisation{
                          position {
                            latitude 
                            longitude 
                            vitesse
                            }  
                            isEnMouvement 
                            date 
                            compteurKm
                        }
                      }
                   
       }
        }
           }
         `,
        variables: {
          Vrn: Id
        }
      }).toPromise()
    // this.killpross();
    this.apo.client.resetStore()
    return a;
  }
  buildHistorique(evenement: any, boitier: any, vrn: any): any {

    let histoItem = {

      vehiculeId: vrn,
      conducteurId: null,
      dateReception: evenement.geoLocalisation.date,
      longitude: evenement.geoLocalisation.position.longitude,
      latitude: evenement.geoLocalisation.position.latitude,
      boitierId: boitier,
      vitesse: evenement.geoLocalisation.position.vitesse,
      compteurKm: evenement.geoLocalisation.compteurKm,
      capteurId: null,
      temperature: null,
      numeroCarte: evenement.tachygraphe?.conducteur != null ? evenement.tachygraphe?.conducteur.numeroCarte : '',
      activite: evenement.tachygraphe?.conducteur != null ? evenement.tachygraphe?.conducteur.activite : 0

    }

    return histoItem;


  }
  buildHistoriqueLast5(evenement: any): any {

    let histoItem = {
      dateReception: evenement.geoLocalisation.date,
      longitude: evenement.geoLocalisation.position.longitude,
      latitude: evenement.geoLocalisation.position.latitude,


    }

    return histoItem;


  }
  buildAllHisortiqueVehiculeLast5(data) {
    let allEventList = [];
    var nodes = data.data.trackings.nodes;

    if (nodes.length > 0) {

      nodes.forEach(node => {
        let nodeVehiculeEvents = node.evenements;
        if (nodeVehiculeEvents && nodeVehiculeEvents.length > 0) {
          nodeVehiculeEvents.forEach(evennement => {
            if (evennement.geoLocalisation && evennement.geoLocalisation.position) {

              let histoItem = this.buildHistoriqueLast5(evennement);
              allEventList.push(histoItem);


            }
          });
        }
      });
    }
    // trier par ordre croissant la list des evennements
    if (allEventList.length > 1) {
      allEventList = allEventList.sort((a, b) => a.dateReception.localeCompare(b.dateReception));
    }
    let result = {
      listHistorique: allEventList.slice(-5).reverse()
    };

    return result;
  }
  buildAllHisortiqueVehicule(data, vrn) {

    let allEventList = [];
    var nodes = data.data.trackings.nodes;

    if (nodes.length > 0) {

      nodes.forEach(node => {
        let nodeVehiculeEvents = node.evenements;
        let boitierId: any = node.boitierId;
        //let vehiculeId = vrn;

        if (nodeVehiculeEvents && nodeVehiculeEvents.length > 0) {
          nodeVehiculeEvents.forEach(evennement => {
            if (evennement.geoLocalisation && evennement.geoLocalisation.position) {
              if (evennement.tachygraphe.conducteur != null) {
                let histoItem = this.buildHistorique(evennement, boitierId, vrn);
                allEventList.push(histoItem);
              }


            }
          });
        }
      });
    }
    // trier par ordre croissant la list des evennements
    if (allEventList.length > 1) {
      allEventList = allEventList.sort((a, b) => a.dateReception.localeCompare(b.dateReception));
    }
    let result = {
      idVehicule: vrn,
      listHistorique: allEventList
    };

    return result;

  }

}