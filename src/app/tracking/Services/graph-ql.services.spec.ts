import { HttpHeaders } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { ApolloLink, createHttpLink, InMemoryCache } from '@apollo/client/core';

import { Apollo } from 'apollo-angular';
import { GQLService } from './graph-ql.service';


describe('GrapgQlService', () => {
    let apo: Apollo;
    let service: any;
    let link: ApolloLink;
    let mimo: InMemoryCache;

    beforeEach(() => {

        TestBed.configureTestingModule({
            providers: [Apollo],
        }).compileComponents();
        mimo = new InMemoryCache()
        link = createHttpLink({
            uri: `https://apigqltra.dev.stradatms.net/graphql`,
            headers: new HttpHeaders().set('Authorization', 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlJFRTJPVUV4UlRnMU5EYzVSa0kzUlVVek16QTJPRFZGUlRBMU1VWTVRVEEyTTBaRFJUWkRNdyJ9.eyJuaWNrbmFtZSI6Im1laGRpLnJvdWlzc2kiLCJuYW1lIjoibWVoZGkucm91aXNzaUBhZGRpbm4uY29tIiwicGljdHVyZSI6Imh0dHBzOi8vcy5ncmF2YXRhci5jb20vYXZhdGFyL2ZhMTM3OTk3ZDAzYzFiMDU4MGVlZWUzNGIxMTJiZGU0P3M9NDgwJnI9cGcmZD1odHRwcyUzQSUyRiUyRmNkbi5hdXRoMC5jb20lMkZhdmF0YXJzJTJGbWUucG5nIiwidXBkYXRlZF9hdCI6IjIwMjItMDItMjNUMTQ6MDY6MzcuOTc3WiIsImVtYWlsIjoibWVoZGkucm91aXNzaUBhZGRpbm4uY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImlzcyI6Imh0dHBzOi8vc3RyYWRhLWRldi5ldS5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NjBiZGUwMzRiMDI2NzkwMDZmNjgyNDI5IiwiYXVkIjoiZlFTbUhqRnBNbGY5NUxoc1J0NG52Q0I0QnJ0SGdxQ0QiLCJpYXQiOjE2NDU2MjUxOTgsImV4cCI6MTY0NTY2MTE5OCwiYXRfaGFzaCI6IlYtTjZjMXhaZXAxVVRmZDRRYnItSHciLCJub25jZSI6IkNlY0dfR0pnUnZUNk5rfk9UTS5GbWk3UHRRcFRtc3o2In0.AMjD3ep8-pncb0N5Iv5D8sPPUABoqQpBsQoDHd0RMjv0KzYtXef1DnawTKDBgc35TXU5AsUASk2UHMrnqHDRR_l1lhWAbl9uYPPOkQjUiRjJiEiU_jLX8befHyXJnr8rQgWYDWifCCp6dgYsne5b_9PU4hY8YIa7PZyypYOIhix9_wXsVrnTYgitQdif2tMszL15tB-IAfnk6-whI6hcFsAZbj3SjUPqqR2NFhLiHwOnmRE50DX5ttO_3cQ0KSXOmNxt0hemISxRog2t5xIL38S3of17_7JDzcsB9cSFbSFEigaeKRexq_ufHs9gRPayKbPw190Jo6cOQA6leD_KQg'),
        }),
            apo = TestBed.inject(Apollo)
        apo.create({
            link: link,
            cache: mimo,
        })
        service = new GQLService(apo);


    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
    it('should return value from observable',
        (done: DoneFn) => {
            service.getHistorique("2022-02-08T23:59", "2022-02-08T00:00:00.000Z", "ff76fbe6-2193-47aa-88bc-dd431e8f7c23").subscribe((dog) => {
                expect(dog).not.toBeNull()
                done();
            });
        });
});
