import { Component, EventEmitter, Input, OnInit, Output, Renderer2, SimpleChanges, ViewChild } from '@angular/core';
import { NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbInputDatepicker, NgbTimepickerConfig, NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { BehaviorSubject } from 'rxjs';
declare var jQuery: any;
const WEEK_DAYS = [
  'Dimanche',
  'Lundi',
  'Mardi',
  'Mercredi',
  'Jeudi',
  'Vendredi',
  'Samedi',
];
@Component({
  selector: 'app-datepiker',
  templateUrl: './datepiker.component.html',
  styleUrls: ['./datepiker.component.scss']
})
export class DatepikerComponent implements OnInit {
  usercdata = new BehaviorSubject([]);

  activityDayEmployee: NgbDate[];
  activityDayVehicule: NgbDate[];
  @Input() range: boolean = false;
  @Input() WeekRange: boolean = false;
  @Output() dateSelected = new EventEmitter();
  time: NgbTimeStruct = { hour: 13, minute: 30, second: 0 };
  @Input() startTime = '00:00';
  @Output() startTimeChanged = new EventEmitter();
  @Input() endTime = '23:59';
  @Output() endTimeChanged = new EventEmitter();
  hoveredDate: NgbDate | null = null;
  timePicker: boolean = false;
  @Input() fromDate: NgbDate | null;
  @Input() toDate: NgbDate | null;
  @Output() fromDateChange = new EventEmitter<NgbDate>();
  @Output() toDateChange = new EventEmitter<NgbDate>();
  @Output() addDate = new EventEmitter<NgbDate>();
  @Output() lessdate = new EventEmitter<NgbDate>();
  @ViewChild('datepicker', { read: NgbInputDatepicker, static: false })
  datepicker: NgbInputDatepicker;
  getformDate: NgbDateStruct;
  gettoDate: NgbDateStruct;
  today = this.calendar.getToday();
  readonly DT_FORMAT = 'DD/MM/YYYY';
  QUICK_ACCESS_DAY_LIST = [
    { label: 'Aujourd’hui', value: { fromDate: this.today, toDate: null } },
    {
      label: 'Hier',
      value: {
        fromDate: this.calendar.getPrev(this.today, 'd', 1),
        toDate: null,
      },
    },
    {
      label: 'Lundi',
      value: {
        fromDate: this.calendar.getPrev(
          this.today,
          'd',
          this.calendar.getWeekday(this.today) - 1
        ),
        toDate: null,
      },
    },
    {
      label: 'Mardi',
      value: {
        fromDate: this.calendar.getPrev(
          this.today,
          'd',
          this.calendar.getWeekday(this.today) - 2
        ),
        toDate: null,
      },
    },
    {
      label: 'Mercredi',
      value: {
        fromDate: this.calendar.getPrev(
          this.today,
          'd',
          this.calendar.getWeekday(this.today) - 3
        ),
        toDate: null,
      },
    },
    {
      label: 'Jeudi',
      value: {
        fromDate: this.calendar.getPrev(
          this.today,
          'd',
          this.calendar.getWeekday(this.today) - 4
        ),
        toDate: null,
      },
    },
    {
      label: 'Vendredi',
      value: {
        fromDate: this.calendar.getPrev(
          this.today,
          'd',
          this.calendar.getWeekday(this.today) - 5
        ),
        toDate: null,
      },
    },
    {
      label: 'Samedi',
      value: {
        fromDate: this.calendar.getPrev(
          this.today,
          'd',
          this.calendar.getWeekday(this.today) - 6
        ),
        toDate: null,
      },
    },
    {
      label: 'Dimanche',
      value: {
        fromDate: this.calendar.getPrev(
          this.today,
          'd',
          this.calendar.getWeekday(this.today) - 7
        ),
        toDate: null,
      },
    },
  ];
  QUICK_ACCESS_WEEK_LIST = [
    {
      label: '7 derniers jours',
      value: {
        fromDate: this.calendar.getPrev(this.today, 'd', 6),
        toDate: this.today,
      },
    },
    // {label:'30 derniers jours', value:{fromDate:this.calendar.getPrev(this.today,'d',30),toDate:this.today}},
    {
      label: 'Semaine en cours',
      value: {
        fromDate: this.calendar.getPrev(
          this.today,
          'd',
          this.calendar.getWeekday(this.today) - 1
        ),
        toDate: this.calendar.getPrev(
          this.today,
          'd',
          this.calendar.getWeekday(this.today) - 7
        ),
      },
    },
    {
      label: 'La semaine dernière',
      value: {
        fromDate: this.calendar.getPrev(
          this.today,
          'd',
          this.calendar.getWeekday(this.today) + 6
        ),
        toDate: this.calendar.getPrev(
          this.today,
          'd',
          this.calendar.getWeekday(this.today)
        ),
      },
    },
  ];
  QUICK_ACCESS_MONTH_LIST = [
    {
      label: 'Mois en cours',
      value: {
        fromDate: { ...this.today, day: 1 },
        toDate: {
          ...this.today,
          day: new Date(this.today.year, this.today.month, 0).getDate(),
        },
      },
    },
    {
      label: 'Le mois dernier',
      value: {
        fromDate: { ...this.calendar.getPrev(this.today, 'm', 1), day: 1 },
        toDate: {
          ...this.calendar.getPrev(this.today, 'm', 1),
          day: new Date(
            this.calendar.getPrev(this.today, 'm', 1).year,
            this.calendar.getPrev(this.today, 'm', 1).month,
            0
          ).getDate(),
        },
      },
    },
  ];
  constructor(
    config: NgbTimepickerConfig,
    public calendar: NgbCalendar,
    public formatter: NgbDateParserFormatter,
    public renderer: Renderer2,
  ) {
    config.meridian = true;
    config.seconds = false;
    config.spinners = true;
    this.fromDate = calendar.getToday();
    this.toDate = this.range
      ? calendar.getNext(calendar.getToday(), 'd', 10)
      : calendar.getToday();
  }
  ngOnInit() {
    this.fromDate = this.calendar.getToday();   
    (function ($) {
      $(document).ready(function () {
        jQuery('.clockpicker').clockpicker({ donetext: 'valider' });
      });
    })(jQuery);
    this.quickList();
    this.handleEmmition();
    this.employeeActivite();
  }
  employeeActivite() { }

  compareDate(dateOne: NgbDate, datetow: NgbDate) {
    return dateOne.equals(datetow);
  }

  parse(value: string): NgbDateStruct {
    if (value) {
      value = value.trim();
      let mdt = moment(value, this.DT_FORMAT);
      return <NgbDateStruct>{
        year: mdt.year(),
        month: mdt.month() + 1,
        day: mdt.date(),
      };
    }
    return null;
  }
  format(date: NgbDateStruct): string {


    if (!date) return '';
    let mdt = moment([date.year, date.month - 1, date.day]);
    if (!mdt.isValid()) return '';
    return mdt.format(this.DT_FORMAT);
  }
  formatFromDate(date: NgbDateStruct): string {
    this.getformDate = date;

    if (!date) return '';
    let mdt = moment([date.year, date.month - 1, date.day]);
    if (!mdt.isValid()) return '';
    return mdt.format(this.DT_FORMAT);
  }
  formatToDate(date: NgbDateStruct): string {
    this.gettoDate = date;

    if (!date) return '';
    let mdt = moment([date.year, date.month - 1, date.day]);
    if (!mdt.isValid()) return '';
    return mdt.format(this.DT_FORMAT);
  }

  getAddDate() { }

  getLessDate() { }

  changeByDayLeft() {
    this.fromDate = this.calendar.getPrev(this.fromDate, 'd', 1);
    this.handleEmmition();

  }
  changeByDayRight() {
    this.fromDate = this.calendar.getNext(this.fromDate, 'd', 1);
    this.handleEmmition();

  }
  changeByPeriodLeft() {
    if (this.WeekRange == true) {
      this.fromDate = this.calendar.getPrev(this.fromDate, 'd', 6);
      this.toDate = this.calendar.getPrev(this.fromDate, 'd', 6);
      this.handleEmmition();
    } else {
      let jours31 = this.fromDate.month;
      if (jours31 == 3) {
        this.toDate = this.calendar.getPrev(this.fromDate, 'd', 1);
        this.fromDate = this.calendar.getPrev(this.fromDate, 'd', 28);
      } else if (
        jours31 == 2 ||
        jours31 == 4 ||
        jours31 == 6 ||
        jours31 == 7 ||
        jours31 == 9 ||
        jours31 == 11
      ) {
        this.toDate = this.calendar.getPrev(this.fromDate, 'd', 1);
        this.fromDate = this.calendar.getPrev(this.fromDate, 'd', 31);
      } else {
        this.toDate = this.calendar.getPrev(this.fromDate, 'd', 1);
        this.fromDate = this.calendar.getPrev(this.fromDate, 'd', 30);
      }
      this.handleEmmition();
    }
  }
  changeByPeriodRight() {
    if (this.WeekRange == true) {
      this.fromDate = this.calendar.getNext(this.fromDate, 'd', 6);
      this.toDate = this.calendar.getNext(this.fromDate, 'd', 6);
      this.handleEmmition();
    } else {
      let jours31 = this.toDate.month;
      this.toDate = this.calendar.getNext(this.toDate, 'd', 1);
      this.fromDate = this.toDate;
      if (jours31 == 1) {
        this.toDate = this.calendar.getNext(this.toDate, 'd', 27);
      } else if (
        jours31 == 2 ||
        jours31 == 4 ||
        jours31 == 6 ||
        jours31 == 7 ||
        jours31 == 9 ||
        jours31 == 11
      ) {
        this.toDate = this.calendar.getNext(this.toDate, 'd', 30);
      } else {
        this.toDate = this.calendar.getNext(this.toDate, 'd', 29);
      }
      this.handleEmmition();
    }
  }

  frenchFormat(date: NgbDateStruct): string {
    if (!date) return '';
    let mdt = moment([date.year, date.month - 1, date.day]);
    if (!mdt.isValid()) return '';
    return mdt.format(this.DT_FORMAT);
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    if (
      changes.hasOwnProperty('range') ||
      changes.hasOwnProperty('WeekRange')
    ) {
      // if(changes.range.previousValue)
      setTimeout(() => {
        this.datepicker.toggle();
        this.datepicker.toggle();
        this.handleOpening();
      });
    }
  }
  quickList() {
    this.QUICK_ACCESS_DAY_LIST = [];
    for (let index = 0; index < 7; index++) {
      const date = this.calendar.getPrev(this.today, 'd', index);
      this.QUICK_ACCESS_DAY_LIST.push({
        label:
          index === 0
            ? 'Aujourd’hui'
            : WEEK_DAYS[new Date(date.year, date.month - 1, date.day).getDay()],
        value: {
          fromDate: date,
          toDate: null,
        },
      });
    }
  }
  onDateSelection(date: NgbDate) {
    if (this.range) this.HandleRange(date);
    else this.HandleSingleDate(date);
  }
  HandleRange(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (
      this.fromDate &&
      !this.toDate &&
      date &&
      date.after(this.fromDate)
    ) {
      if (
        date.before(this.calendar.getNext(this.fromDate, 'd', 6)) ||
        !this.WeekRange
      ) {
        this.toDate = date;
      } else {
        this.toDate = this.calendar.getNext(this.fromDate, 'd', 6);
      }
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }
  HandleSingleDate(date: NgbDate) {
    this.fromDate = date;
    this.toDate = date;
  }
  isHovered(date: NgbDate) {
    if (!this.range) {
      return (
        this.fromDate &&
        !this.toDate &&
        this.hoveredDate &&
        date.after(this.fromDate) &&
        date.before(this.hoveredDate)
      );
    } else {
      const valid =
        this.fromDate &&
        !this.toDate &&
        this.hoveredDate &&
        date.after(this.fromDate) &&
        date.before(this.hoveredDate);
      return (
        valid &&
        (date.before(this.calendar.getNext(this.fromDate, 'd', 6)) ||
          !this.WeekRange)
      );
    }
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return (
      date.equals(this.fromDate) ||
      (this.toDate && date.equals(this.toDate)) ||
      this.isInside(date) ||
      this.isHovered(date)
    );
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed))
      ? NgbDate.from(parsed)
      : currentValue;
    // return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }
  asyncValidateInput(currentValue: NgbDate | null, event, input: string) {
  }
  BlurFromDate(event, input: string) {
    const parsed = this.parse(input);
    this.fromDate = NgbDate.from(parsed);
    return this.format(parsed);
  }

  BlurtoDate(event, input: string) {
    const parsed = this.parse(input);
    this.toDate = NgbDate.from(parsed);
    if (this.fromDate.after(this.toDate)) {
      this.fromDate = this.calendar.getPrev(this.toDate, 'd', 6);
    }
    event.target.value = this.format(parsed);
    if (this.calendar.isValid(NgbDate.from(parsed))) {
      this.handleEmmition();
    }
    return this.format(parsed);
  }

  asyncValidateToDateInput(currentValue: NgbDate | null, event, input: string) {
    if (event.keyCode === 13) {
      const parsed = this.parse(input);
      // if( NgbDate.from(parsed).after(this.fromDate)){
      this.toDate =
        parsed && this.calendar.isValid(NgbDate.from(parsed))
          ? NgbDate.from(parsed)
          : currentValue;
      // }
      if (this.fromDate.after(this.toDate)) {
        this.fromDate = this.calendar.getPrev(this.toDate, 'd', 6);
      }
      event.target.value = this.format(currentValue);
      if (this.calendar.isValid(NgbDate.from(parsed))) {
        this.validateCallBack();
      }
      if (this.calendar.isValid(NgbDate.from(parsed))) {
        this.validateCallBack();
      }
      if (this.range && this.WeekRange) {
        if (this.fromDate.before(this.calendar.getPrev(this.toDate, 'd', 6)))
          this.fromDate = this.calendar.getPrev(this.toDate, 'd', 6);
      }
    }
  }

  noConfirmedValue: { sd: NgbDate; ed: NgbDate; et: string; st: string };
  handleOpening(event?) {
    const header = document.querySelector('.ngb-dp-header');
    if (header) {
      this.noConfirmedValue = {
        sd: this.fromDate,
        ed: this.toDate,
        st: this.startTime,
        et: this.endTime,
      };
      // Creating the checkbox
      const check = this.renderer.createElement('input');
      check.setAttribute('type', 'checkbox');
      const checkdiv = this.renderer.createElement('div');
      const inputsWrapper = this.renderer.createElement('div');
      const p = this.renderer.createElement('p');
      p.innerHTML = 'Toute la journée';
      checkdiv.classList.add('d-flex');
      checkdiv.classList.add('checkbox-field');
      inputsWrapper.classList.add('inputs-wrapper');
      this.renderer.appendChild(checkdiv, check);
      this.renderer.appendChild(checkdiv, p);
      // Creating the checkbox Ends

      const wrapper = this.renderer.createElement('div');
      const startTime = this.renderer.createElement('input');
      const startTimeDiv = this.renderer.createElement('div');
      const endTimeDiv = this.renderer.createElement('div');
      const startTimeLabel = this.renderer.createElement('h6');
      const endTimeLabel = this.renderer.createElement('h6');
      const endTime = this.renderer.createElement('input');
      wrapper.classList.add('clockpicker-wrapper');
      if (!this.timePicker) {
        check.setAttribute('checked', 'true');
        startTime.classList.add('d-none');
        startTimeLabel.classList.add('d-none');
        endTimeLabel.classList.add('d-none');
        endTime.classList.add('d-none');
        check.checked = true;
      }
      startTime.classList.add('start-clockpicker');
      endTime.classList.add('end-clockpicker');
      startTimeLabel.innerHTML = 'Heure Début';
      endTimeLabel.innerHTML = 'Heure Fin';
      this.renderer.appendChild(startTimeDiv, startTimeLabel);
      this.renderer.appendChild(startTimeDiv, startTime);
      this.renderer.appendChild(inputsWrapper, startTimeDiv);
      this.renderer.appendChild(endTimeDiv, endTimeLabel);
      this.renderer.appendChild(endTimeDiv, endTime);
      this.renderer.appendChild(inputsWrapper, endTimeDiv);
      this.renderer.appendChild(wrapper, inputsWrapper);
      this.renderer.appendChild(wrapper, checkdiv);
      this.renderer.insertBefore(header, wrapper, header.firstChild);
      check.addEventListener(
        'click',
        this.timePickerToggle.bind(this, startTime, endTime)
      );

      startTime.value = this.startTime;
      endTime.value = this.endTime;
      jQuery('.start-clockpicker').clockpicker({
        donetext: 'valider',
        afterDone: this.timeChange.bind(this, startTime, endTime),
      });
      jQuery('.end-clockpicker').clockpicker({
        donetext: 'valider',
        afterDone: this.timeChange.bind(this, startTime, endTime),
      });
    }
    this.addQuickSelector(header);
    this.addConfirm();
  }
  timeChange(startTime, endTime) {
    this.startTime = startTime.value;
    const s = new Date(
      this.fromDate.year +
      '-' +
      this.fromDate.month +
      '-' +
      this.fromDate.day +
      ' ' +
      this.startTime
    ).getTime();
    const e = new Date(
      this.toDate.year +
      '-' +
      this.toDate.month +
      '-' +
      this.toDate.day +
      ' ' +
      endTime.value
    ).getTime();
    if (s < e) {
      this.endTime = endTime.value;
    } else {
      this.endTime = this.startTime;
      endTime.value = this.startTime;
    }
  }

  addConfirm() {
    const body = document.querySelector('.dropdown-menu');
    const btnWrapper = this.renderer.createElement('div');
    btnWrapper.classList.add('btn-wrapper');

    const cancelBtn = this.renderer.createElement('input');
    cancelBtn.setAttribute('type', 'button');
    cancelBtn.value = 'Annuler';
    cancelBtn.classList.add('cancel-btn');
    this.renderer.appendChild(btnWrapper, cancelBtn);
    cancelBtn.addEventListener('click', this.cancelCallBack.bind(this));
    /************************************************************ */
    const validateBtn = this.renderer.createElement('input');
    validateBtn.setAttribute('type', 'button');
    validateBtn.value = 'Appliquer';
    validateBtn.classList.add('validate-btn');
    this.renderer.appendChild(btnWrapper, validateBtn);
    validateBtn.addEventListener('click', this.validateCallBack.bind(this));

    this.renderer.appendChild(body, btnWrapper);
  }

  addQuickSelector(before) {
    const body = document.querySelector('.dropdown-menu');
    const quickDiv = this.renderer.createElement('div');
    quickDiv.classList.add('quick-date');
    this.renderer.insertBefore(body, quickDiv, before);
    let btnsList;
    if (!this.range) {
      btnsList = this.QUICK_ACCESS_DAY_LIST;
    } else if (this.WeekRange) {
      btnsList = this.QUICK_ACCESS_WEEK_LIST;
    } else {
      btnsList = this.QUICK_ACCESS_MONTH_LIST;
    }
    btnsList.forEach((el) => {
      const btn = this.renderer.createElement('button');
      const span = this.renderer.createElement('span');
      btn.classList.add('quick-btn');
      span.innerHTML = el.label;
      this.renderer.appendChild(btn, span);
      this.renderer.appendChild(quickDiv, btn);
      btn.addEventListener('click', (e) => {
        let target = e.target;
        if (!target.classList.contains('quick-btn')) target = target.parentNode;
        const childrenlist = target.parentNode.children;
        for (let index = 0; index < childrenlist.length; index++) {
          childrenlist[index].classList.remove('active');
        }
        target.classList.add('active');
        this.fromDate = el.value.fromDate;
        // this.onDateSelection(el.value.toDate: NgbDate)
        this.toDate = el.value.toDate;
        document.getElementById('date-navigation').click();
      });
    });
  }

  validateCallBack(e?) {
    if (this.toDate == null) {
      this.toDate = this.fromDate;
    }
    this.handleEmmition();
    (<HTMLElement>document.querySelector('#close')).click();
  }
  cancelCallBack(e, resetAll: boolean = true) {
    if (resetAll) {
      this.startTime = this.noConfirmedValue.st;
      this.endTime = this.noConfirmedValue.et;
      this.fromDate = this.noConfirmedValue.sd;
      this.toDate = this.noConfirmedValue.ed;
    }
    (<HTMLElement>document.querySelector('#close')).click();
  }
  handleClosing(e) {
    this.cancelCallBack(e, false);
  }
  timePickerToggle(start, end, e) {
    this.timePicker = !e.target.checked;
    const st = document.querySelector(
      '.clockpicker-wrapper .start-clockpicker'
    );
    const lbs = document.querySelectorAll('.clockpicker-wrapper h6');
    const et = document.querySelector('.clockpicker-wrapper .end-clockpicker');
    if (e.target.checked) {
      this.startTime = '00:00';
      this.endTime = '23:59';
      start.value = '00:00';
      end.value = '23:59';
      st.classList.add('d-none');
      et.classList.add('d-none');
      lbs.forEach((element) => {
        element.classList.add('d-none');
      });
    } else {
      st.classList.remove('d-none');
      et.classList.remove('d-none');
      lbs.forEach((element) => {
        element.classList.remove('d-none');
      });
    }
    if (this.toDate != null) this.handleEmmition();
  }
  handleEmmition() {
    const startDate = new Date(
      this.fromDate.year +
      '-' +
      this.fromDate.month +
      '-' +
      this.fromDate.day +
      ' ' +
      this.startTime
    );
    const endDate = new Date(
      this.toDate.year +
      '-' +
      this.toDate.month +
      '-' +
      this.toDate.day +
      ' ' +
      this.endTime
    );
    this.dateSelected.emit({
      startDate: startDate,
      endDate: endDate,
      fromDate: this.fromDate,
      startTime: this.startTime,
      endTime: this.endTime,
    });
    this.fromDateChange.emit(this.fromDate);
    this.toDateChange.emit(this.toDate);
    this.startTimeChanged.emit(this.startTime);
    this.endTimeChanged.emit(this.endTime);
  }

}
