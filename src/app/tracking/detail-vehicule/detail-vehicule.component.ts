import { formatDate } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../Modal/modal/modal.component';
import { ConducteurRechercherItem, Infotournees, ListInfotournes, ModelDetailVehicule, VehiculeRechercherItem } from '../model/Models';
import { ConfService } from '../Services/TrackingServices';
import { filter } from 'rxjs/operators';
import { GQLService } from '../Services/graph-ql.service';
import { MapFunctionService } from '../Services/ServicesMapGoogle/map-function.service';
@Component({
  selector: 'app-detail-vehicule',
  templateUrl: './detail-vehicule.component.html',
  styleUrls: ['./detail-vehicule.component.scss']
})
export class DetailVehiculeComponent implements OnInit {
  @Input() Vehicule: ModelDetailVehicule = new ModelDetailVehicule();
  @Input() VehiculesAproxlist: Array<any>; // list  de vehicule aproximite 
  @Input() isSPI: any = false; // il faut le mettre true si un SPI
  @Input() show: any = false;// il faut le mettre false si un evenement de click sur la map
  @Input() Cumule: any;// il faut le mettre false si un evenement de click sur la map
  @Output() Close = new EventEmitter();//
  @Output() tours = new EventEmitter<ListInfotournes>();
  @Output() GetSelectedVehciuleVRN = new EventEmitter<string>();
  public dateJourCurrent: any;
  public showlist: any = false
  public showlist2: any = false
  public conducteur: any = '';
  public list = [];
  public keyword = 'name';
  public vehiculepayload: any;
  public services: ConfService = null;
  public adr: Array<any> = [];
  public tournees: any;
  public adrApro: Array<any> = [];
  public route: any;
  public adress: any = '';
  constructor(
    public service: ConfService,
    config: NgbModalConfig,
    private modalService: NgbModal
    , private router: Router,
    public graph: GQLService,
    public mapFn: MapFunctionService) {
    this.services = service
    config.backdrop = 'static';
    config.keyboard = false;
    router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    )
      .subscribe(event => {
        this.route = event;
      });
  }
  Closedpanel() {
    this.tours.emit(null)
  }
  sendtournees(ref) {
    this.tours.emit(ref)


  }
  clock() {
    var d = new Date();
    var date = d.getDate();
    var year = d.getFullYear();
    var month = d.getMonth();
    var monthArr = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
    var monthString = monthArr[month];
    this.dateJourCurrent = date + " " + monthString + ", " + year;
  }
  async fetchCumuleActivite() {
    var dt1 = formatDate(new Date(), 'yyyy-MM-dd', 'en_US') + "T00:00:00.000Z"
    var dt2 = formatDate(new Date(), 'yyyy-MM-dd', 'en_US') + "T23:59:59.000Z";
    await this.graph.getHistorique(dt1, dt2, this.Vehicule.id)
      .subscribe(

        async (data: any) => {
          console.log(data)
          var b = this.graph.buildAllHisortiqueVehicule(data, this.Vehicule.id);
          this.Cumule = this.mapFn.cumuleAcitivite(b.listHistorique);


        }
      )
  }

  geocodeadrtournees(tournees): Array<ListInfotournes> {
    var geocoder = new google.maps.Geocoder();

    var res: Array<ListInfotournes> = []
    this.tournes = []
    tournees.forEach(el => {
      var a = new ListInfotournes();
      var re: Array<Infotournees> = []
      el.enlevementLivraisons.forEach(el => {
        var adr = el.adresse1Client + " " + el.cpClient + " " + el.villeClient

        var point: any;
        if (!el.isEnlevement) {
          geocoder.geocode({ 'address': adr }, (results, status) => {
            if (status == google.maps.GeocoderStatus.OK) {

              var lat = results[0].geometry.location.lat();
              var lng = results[0].geometry.location.lng()
              var a = new Infotournees();
              a.point = new google.maps.LatLng({ lat: lat, lng: lng })
              a.type = 'Enl'
              re.push(a);
            }


          })
        }
        else {
          geocoder.geocode({ 'address': adr }, (results, status) => {
            if (status == google.maps.GeocoderStatus.OK) {

              var lat = results[0].geometry.location.lat();
              var lng = results[0].geometry.location.lng()

              var a = new Infotournees();
              a.point = new google.maps.LatLng({ lat: lat, lng: lng })
              a.type = 'Liv'
              re.push(a);
            }
          })

        }
      })

      a.ref = el.referenceOT;
      a.list = re;
      res.push(a);
    });


    return res;
  }
  navigateToCommande(idCommande) {
    window.open("/tms/ordre-transport/" + idCommande)
  }
  navigateToPlaning() {
    window.open("tms/planing/Impression")
  }
  public tournes: Array<ListInfotournes> = [];
  async gettournees() {
    if (this.Vehicule.id == undefined || this.Vehicule == undefined)
      return
    this.tournees = await this.services.getTournees(this.Vehicule.id)

    if (this.tournees != null)
      if (this.tournees != null)
        this.tournees.forEach(el => {
          el.dateDebut = formatDate(el.dateDebut, 'yyyy-MM-dd', 'en_US')
          el.dateFin = formatDate(el.dateFin, 'yyyy-MM-dd', 'en_US')
          el.enlevementLivraisons.forEach(el => {
            if (!el.isEnlevement) {
              el.livraison.dateSouhaite = formatDate(el.livraison.dateSouhaite, 'yyyy-MM-dd', 'en_US')
            }
            else {
              el.expedition.dateSouhaite = formatDate(el.expedition.dateSouhaite, 'yyyy-MM-dd', 'en_US')
            }

          });

        });
  }
  apro() {

    this.adrApro = []
    this.adr.forEach(el => {

      var dis = this.distance(el.lat, el.lng, this.Vehicule.positionX, this.Vehicule.positionY, 'k');

      if (dis < 100) {
        this.adrApro.push(el);
      }
    })

  }
  distance(lat1, lon1, lat2, lon2, unit) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
      return 0;
    } else {
      var radlat1 = Math.PI * lat1 / 180;
      var radlat2 = Math.PI * lat2 / 180;
      var theta = lon1 - lon2;
      var radtheta = Math.PI * theta / 180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = dist * 180 / Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit == "K") {
        dist = dist * 1.609344
      }
      if (unit == "N") {
        dist = dist * 0.8684
      }
      return dist;
    }
  }
  async listadr() {
    await this.services.getClients().then(async (el) => {
      for await (var a of el) {
        await this.codeadr(a.address, a.nomClient, a)
      }
    });
  }
  async ngOnChanges(changes: SimpleChanges) {

    if (this.Vehicule.id != undefined && this.Vehicule != undefined) {
      this.geodecode(this.Vehicule.positionX, this.Vehicule.positionY)
      this.gettournees();
      this.apro()
    }
    if ((changes.Vehicule != null) && (changes.Vehicule != undefined)) {
      this.conducteur = changes.Vehicule.currentValue.NomC
    }
    if (changes.show != undefined) {
      if (changes.show.currentValue == false) {
        this.showlist = false
      }
    }
  }

  geodecode(lat, lag) {

    var geocoder = new google.maps.Geocoder
    const point = new google.maps.LatLng(lat, lag)
    geocoder.geocode({ 'location': point }, (results, status) => {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          var address = (results[1].formatted_address);
          var array = address.split(',');
          array.shift()
          this.adress = array.join(' ');

        }
      }
    })


  }
  ngOnInit() {
    this.listadr();
    this.clock()
  }
  open(lng, lat) {
    const a = this.modalService.open(ModalComponent, { centered: true, size: 'xl' });
    a.componentInstance.lng = lng;
    a.componentInstance.lat = lat;
  }
  codeadr(adr, nomdeclient, ob): any {
    var geocoder = new google.maps.Geocoder();
    if (ob.latitude != null) {

      this.adr.push({ nomclient: nomdeclient, adr: adr, lat: Number(ob.latitude), lng: Number(ob.longitude) })
    }
    else if ((adr != "") && (adr != null)) {
      if (this.adr.indexOf(nomdeclient) == -1) {
        geocoder.geocode({ 'address': adr }, (results, status) => {
          if (status == google.maps.GeocoderStatus.OK) {
            this.adr.push({ nomclient: nomdeclient, adr: adr, lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng() })
          }
        });
      }
    }

  }
  openDetailVehicule(vrn) {
    this.GetSelectedVehciuleVRN.emit(vrn);
  }
}
