import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from '../Modal/modal/modal.component';

@Component({
  selector: 'app-menu-context',
  templateUrl: './menu-context.component.html',
  styleUrls: ['./menu-context.component.scss']
})
export class MenuContextComponent implements OnInit {
  @Input() positionX: number;
  @Input() positionY: number;
  @Output() RValue = new EventEmitter<any>();
  @Output() RValue2 = new EventEmitter<any>();
  @Output() RValuePlusInfo = new EventEmitter<any>();
  @Output() RValueAddPOI = new EventEmitter<any>();
  public CoordClick:any=false;
  coord1: any;
  coord2: any;

  public a: any[] = [];
  constructor(config: NgbModalConfig, private modalService: NgbModal) {
    config.backdrop = 'static';
    config.keyboard = false;
  }
  ngOnInit() {
  }
  Showcoords()
  {
    this.CoordClick=!this.CoordClick;
  }
  ngOnChanges(changes: SimpleChanges){
this.CoordClick=false
  }
  getsecondcoords() {
      this.coord2 = {
        lat: this.positionX,
        lng: this.positionY
      }
      this.RValue2.emit(this.coord2);
      this.coord2=null
      return
    
    
  }
  getfirstcoords() {

    this.coord1 = {
      lat: this.positionX,
      lng: this.positionY
    }
    this.RValue.emit(this.coord1);
    this.coord1=null
  }
  getinfocoords() {
    var coord = {
      lat: this.positionX,
      lng: this.positionY
    }
    this.RValuePlusInfo.emit(coord);
  }
  open() {
 var coord = {
      lat: this.positionX,
      lng: this.positionY
    }
    this.RValueAddPOI.emit(coord);
  }
}
