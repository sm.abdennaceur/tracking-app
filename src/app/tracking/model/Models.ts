import { PositionObject } from "../../tracking-leaflet-map/services/map-service";
export class Infotournees {
  type?: string;
  point?: google.maps.LatLng;

}
export class ListInfotournes {
  ref?: string;
  list?: Array<Infotournees>;
}
export class ModelDetailVehicule {
  id: string;
  positionX: number;
  positionY: number;
  activite: number;
  vin: string;
  vrn: string;
  numC: string;
  NomC: string;
}

export class VehiculeRechercherItem {
  name: string;
  type: string;
  activite: number;
  isAct: boolean;
  vitesse: number;
  constructor() {
    this.type = 'Vehicule'
  }
}
export class ConducteurRechercherItem {
  num: string
  type: string;//
  name: string;
  activite:number
  constructor() {
    this.type = 'Conducteur'
  }
}
export class vehiculeModel {
  vehiculeId: string;
  vRN: string;
  VIN: string
  activite: number;
  NomC: string;
  position: PositionObject[];
  address: string
  VehiculesProximite?: any
  isSPI: boolean
  tournees: any
}

export class VehiculeMarker {
  id: string;
  marker: any;
  data?: any;
  type?: string;
}

export class GeoLocalisation {
  date?: string;
  position?: Position;
  compteurKm?: number;
  hasApresContact?: boolean;
  isEnMouvement?: boolean;
}

export class Position {
  latitude?: number;
  longitude?: number;
  vitesse?: number;
  cap?: number;
}
export class InfoVehicule {
  id?: string;
  nomAffichage?: string;
  vin?: string;
  vrn?: string;

}
export class InfoClient {
  id?: string;
  IsParticulier?: boolean;
  ReferenceClient?: string;
  NomClient?: string;
  NumTva?: string;
  Telephone?: string;
  Adresse1?: string;
  Adresse2?: string;
  Adresse3?: string;
  CodePostal?: string;
  Ville?: string;
  Pays?: string;
  Longitude?: string;
  Latitude?: string;

}
export class InfoConducteur {
  id?: string;
  firstName?: string;
  lastName?: string;
  displayName?: string;

}
export class Personne {
  hasCarte?: boolean;
  numeroCarte?: string;
  activite?: number;
}
export class Tachygraphe {
  date?: string;
  compteurKm?: number;
  conducteur?: Personne;
  passager?: Personne;
  vRN?: string;
  vIN?: string;
}
export class Evenement {
  geoLocalisation?: GeoLocalisation;
  tachygraphe?: Tachygraphe;
}
export class VehiculePayload {
  compagnieId?: string;
  vehiculeId?: string;
  boitierId?: string;
  ressourceId?: string;
  evenements?: Array<Evenement>;
  id?: string;
  creationDate?: string;
  version?: number;
  tenantNamespace?: string;

}
