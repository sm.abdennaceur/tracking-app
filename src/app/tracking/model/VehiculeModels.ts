export class ConducteurVehicule{
    numCartConducteur?:string='';
    NomConducteur?:string='';
    Activite?:number=1;
  
  }
  export class PositionVehicule{
    Lat:number=0;
    Lng:number=0;
    DateReception?:string='';
    Vitesse?:number=0;
    IsEnMouvement?:boolean=false;
    cap?:number=0;

  }
  export class Immatriculation{
    Vrn?:string='';
    Vin?:string='';
  
  }
  export class VehiculeModel{
    idVehicule:string='';
    Conducteur?:ConducteurVehicule;
    Position:PositionVehicule;
    Immatriculation?:Immatriculation;
    IsVehicule?:boolean=true;

  }
