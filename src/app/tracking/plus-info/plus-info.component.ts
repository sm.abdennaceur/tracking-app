import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';


@Component({
  selector: 'app-plus-info',
  templateUrl: './plus-info.component.html',
  styleUrls: ['./plus-info.component.scss']
})
export class PlusInfoComponent implements OnInit {
  @Input() infoendroit: Array<any>;
  @Input() aproxinfo: Array<any>;
  @Output() AddPoi = new EventEmitter();
  @Output() GetSelectedVehciuleVRN = new EventEmitter<string>();
   constructor() { }
  ngOnInit(): void {
   
  }
  openDetailVehicule(vrn)
  {
    this.GetSelectedVehciuleVRN.emit(vrn);
  }
}
