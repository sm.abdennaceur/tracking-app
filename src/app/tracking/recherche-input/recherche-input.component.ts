import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { ConducteurRechercherItem, VehiculeRechercherItem } from '../model/Models';

@Component({
  selector: 'app-recherche-input',
  templateUrl: './recherche-input.component.html',
  styleUrls: ['./recherche-input.component.scss']
})
export class RechercheInputComponent implements OnInit {
  public showlist: any = false
  public showlist2: any = false
  public ResRech: Array<any> = [];
  public ResRech1: Array<any> = [];
  @Input() listRech: Array<ConducteurRechercherItem | VehiculeRechercherItem>;
  @Input() HideRechercheList: any = -1; // incrementer la valeur ++  pour fermer la recherche list 
  @Input() show: any = false;// il faut le mettre false si un evenement de click sur la map
  @Output() Close = new EventEmitter();//
  @Output() RValue = new EventEmitter<string>(); // si on a clicker sur une Vehicule return VRN si un Conducteur retrun NumConducteur"Numc"
  @Output() filtreactivite = new EventEmitter<number>();
  constructor() { }

  ngOnInit(): void {
  }
  async Search(val) {

    if (val.target.value == "") {
      this.showlist = true
      this.ResRech1 = []
      return
    }
    this.ResRech1 = []
    this.ResRech1 = this.listRech.filter((x: any) => x.name.toLocaleLowerCase().indexOf(val.target.value.toLocaleLowerCase()) > -1);
    document.getElementById('listg').classList.add('list-group');
    this.showlist = false

  }
  Rechercher(val) {
    if (val == 1) {
      this.ResRech = []
      this.listRech.forEach((el:ConducteurRechercherItem | VehiculeRechercherItem) => {
        if (el.type == 'Vehicule') {

          this.ResRech.push(el)
        }

      });
      this.showlist = false;
      // this.showlist2 = true;
      document.getElementById('listgroup54').classList.add('list-group');
      document.getElementById('listgroup54').focus();
      this.filtreactivite.emit(0);
    }
    if (val == 0) {
      this.ResRech = []
      console.log(this.listRech)
      this.listRech.forEach(el => {
        if (el.type == 'Conducteur' && el.name != "") {

          this.ResRech.push(el)
        }

      });
      console.log(this.ResRech)
      this.showlist = false;
      // this.showlist2 = true;
      document.getElementById('listgroup54').classList.add('list-group');
      this.filtreactivite.emit(0);
    }
    if (val == 3) {
      this.ResRech = []
      this.listRech.forEach((el:VehiculeRechercherItem) => {
        if (el.activite == 3) {
          this.ResRech.push(el)
        }
      })
      this.showlist = false;
      document.getElementById('listgroup54').classList.add('list-group');
      this.filtreactivite.emit(3);
    }
    if (val == 2) {

      this.ResRech = []
      this.listRech.forEach((el:VehiculeRechercherItem) => {
        if (el.activite == 2) {
          if(el.name!='')
          this.ResRech.push(el)
        }

      });
      this.showlist = false;
      //this.showlist2 = true;
      document.getElementById('listgroup54').classList.add('list-group');
      this.filtreactivite.emit(2);

    }
    if (val == 5) {
      this.ResRech = []
      this.listRech.forEach((el:VehiculeRechercherItem) => {
        if (el.activite == 5) {
          this.ResRech.push(el)
        }

      });
      this.showlist = false;
      //this.showlist2 = true;
      document.getElementById('listgroup54').classList.add('list-group');
      this.filtreactivite.emit(5);
    }
    if (val == 4) {
      this.ResRech = []
      this.listRech.forEach((el:VehiculeRechercherItem) => {
        if (el.activite == 4) {

          this.ResRech.push(el)
        }

      });
      this.showlist = false;
      //this.showlist2 = true;
      document.getElementById('listgroup54').classList.add('list-group');
      this.filtreactivite.emit(4);
    }
    if (val == 3) {
      this.ResRech = []
      this.listRech.forEach((el:VehiculeRechercherItem) => {
        if (el.activite == 3) {

          this.ResRech.push(el)
        }

      });
      this.showlist = false;
      this.showlist2 = true;
      this.filtreactivite.emit(3);
    }
  }
  async Showlist1(res, event) {
    event.target.value = ""
    this.ResRech = []

    this.showlist2 = false
    await new Promise(f => setTimeout(f, 200));
    this.showlist = false
    this.ResRech1 = []


  }
  async Showlist(res, event) {
    this.Close.emit();
    event.target.value = ""
    this.ResRech = []
    if (res) {
      this.showlist = true
    }
    else {
      await new Promise(f => setTimeout(f, 200));
      this.showlist = false
    }
  }
  async ngOnChanges(changes: SimpleChanges) {
    if (changes.show != undefined) {
      if (changes.show.currentValue == false) {
        this.showlist = false
      }
    }
    if (changes.HideRechercheList != undefined) {
      this.ResRech = []
     /* var ul = document.getElementById('listgroup54');
      ul.hidden=true*/
    }
  }
  clickBody() {
    if (this.showlist)
      this.showlist = false
    this.showlist2 = false
  }
  selectEvent(item) {
    this.showlist = false
    this.showlist2 = false;
    this.ResRech = []
    this.ResRech1 = []


    if (item.type == 'Conducteur')
      this.RValue.emit(item.num);
    else
      this.RValue.emit(item.name);
  }
  searchCleared() {
    this.show = false;
  }
}
