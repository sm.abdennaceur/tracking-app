import { Component, EventEmitter, OnInit, Output, Renderer2 } from '@angular/core';
import { NgbCalendar, NgbDate } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-switshdate',
  templateUrl: './switshdate.component.html',
  styleUrls: ['./switshdate.component.scss']
})
export class SwitshdateComponent implements OnInit {

  dateRange: any = {};
  dayDate;
  fromDate: NgbDate = null;
  toDate: NgbDate = null;
  startTime: string = '00:00';
  endTime: string = '23:59';
  view: string = 'day';
  timePicker: boolean = true;
  @Output() changeView = new EventEmitter();

  constructor(
    
    public renderer: Renderer2,
    public calendar: NgbCalendar
  ) {
    this.fromDate = <NgbDate>{
      year: 2010,
      month: 10,
      day: 11,
    };
    this.toDate = <NgbDate>{
      year: 2010,
      month: 10,
      day: 11,
    };
    this.dayDate = this.fromDate;
  }
  ngOnInit() {


    this.dateRange.startDate = new Date();
    this.dateRange.endDate = new Date();
    this.fromDate = <NgbDate>{
      year: this.dateRange.startDate.getFullYear(),
      month: this.dateRange.startDate.getMonth(),
      day: this.dateRange.startDate.getDate(),
    };
  }
  NgbDateSelected(date) {
    this.dayDate = date.fromDate;
    this.dateRange = date;
    setTimeout(() => {
      this.dateRange.interval = this.view;
      this.dateRange.startDate = new Date(
        this.fromDate.year +
          '-' +
          this.fromDate.month +
          '-' +
          this.fromDate.day +
          ' ' +
          this.startTime
      );
      this.dateRange.endDate = new Date(
        this.toDate.year +
          '-' +
          this.toDate.month +
          '-' +
          this.toDate.day +
          ' ' +
          this.endTime
      );
      const params = { view: this.view, dateRange: this.dateRange };
      this.changeView.emit(params);
    });
  }

  changeViewByInterval(view: string) {
    
    this.view = view;
    switch (view) {
      case 'day':
        this.dateRange.startDate = new Date(this.dayDate);
        this.dateRange.endDate = new Date(this.dayDate);
        this.fromDate = this.dayDate;
        this.toDate = this.dayDate;
        break;
      case 'week':
        this.fromDate = this.calendar.getPrev(
          this.dayDate,
          'd',
          this.calendar.getWeekday(this.dayDate) - 1
        );
        this.toDate = this.calendar.getNext(this.fromDate, 'd', 6);
        break;
      case 'month':
        this.fromDate = this.calendar.getPrev(
          this.dayDate,
          'd',
          this.dayDate.day - 1
        );
        this.toDate = this.calendar.getPrev(
          this.calendar.getNext(this.fromDate, 'm', 1),
          'd',
          1
        );
        break;
    }
    setTimeout(() => {
      this.dateRange.interval = view;
      this.dateRange.startDate = new Date(
        this.fromDate.year +
          '-' +
          this.fromDate.month +
          '-' +
          this.fromDate.day +
          ' ' +
          this.startTime
      );
      this.dateRange.endDate = new Date(
        this.toDate.year +
          '-' +
          this.toDate.month +
          '-' +
          this.toDate.day +
          ' ' +
          this.endTime
      );
      const params = { view: view, dateRange: this.dateRange };
    //  this.changeView.emit(params);
    });
  }
}
