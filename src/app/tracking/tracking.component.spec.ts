import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NavbarService } from '../core/services/navbar.service';
import { EmployeesService } from '../core/services/employees.service';
import { TrackingComponent } from './tracking.component';
/*describe('TrackingComponent', () => {
  let component: TrackingComponent;
  let fixture: ComponentFixture<TrackingComponent>;
  beforeEach(() => {
    const navbarServiceStub = () => ({
      click: { pipe: () => ({ subscribe: f => f({}) }) },
      setNavBar: object => ({})
    });
    const employeesServiceStub = () => ({});
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [TrackingComponent],
      providers: [
        { provide: NavbarService, useFactory: navbarServiceStub },
        { provide: EmployeesService, useFactory: employeesServiceStub }
      ]
    });
    spyOn(TrackingComponent.prototype, 'fetch');
    fixture = TestBed.createComponent(TrackingComponent);
    component = fixture.componentInstance;
  });
  it('can load instance', () => {
    expect(component).toBeTruthy();
  });
  describe('constructor', () => {
    it('makes expected calls', () => {
      expect(TrackingComponent.prototype.fetch).toHaveBeenCalled();
    });
  });
  describe('ngOnInit', () => {
    it('makes expected calls', () => {
      const navbarServiceStub: NavbarService = fixture.debugElement.injector.get(
        NavbarService
      );
      spyOn(navbarServiceStub, 'setNavBar').and.callThrough();
      component.ngOnInit();
      expect(navbarServiceStub.setNavBar).toHaveBeenCalled();
    });
  });
});*/
