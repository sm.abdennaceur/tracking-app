import { AfterViewChecked, AfterViewInit, Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NavbarService } from '../core/services/navbar.service';
import { EmployeesService } from '../core/services/employees.service';
import { BehaviorSubject } from 'rxjs';
import { HubConnection } from '@microsoft/signalr';
import * as signalR from '@microsoft/signalr';
import MarkerClusterer from '@googlemaps/markerclustererplus';
import {
  ConducteurRechercherItem,
  ModelDetailVehicule,
  VehiculeMarker,
  VehiculePayload,
  VehiculeRechercherItem,
} from './model/Models';
import { ConfService } from './Services/TrackingServices';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ConfigService } from '@tmsApp/login/services/environment.service';
import { formatDate } from '@angular/common';
import { ModalComponent } from './Modal/modal/modal.component';
import { ConducteurVehicule, Immatriculation, PositionVehicule, VehiculeModel } from './model/VehiculeModels';
import { GQLService } from './Services/graph-ql.service';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { IconeService } from './Services/IconeService';
import { IconePersonneService } from './Services/IconePersonneServices';
import { MapFunctionService } from './Services/ServicesMapGoogle/map-function.service';

@Component({
  selector: 'app-tracking',
  templateUrl: './tracking.component.html',
  styleUrls: ['./tracking.component.scss'],
  providers: [NgbModalConfig, NgbModal],
})
export class TrackingComponent implements OnInit, AfterViewInit, OnDestroy, AfterViewChecked {

  constructor(
    public navbar: NavbarService,
    public emp: EmployeesService,
    public service: ConfService,
    config: NgbModalConfig,
    private modalService: NgbModal,
    public configService: ConfigService,
    public IconeVehiculeService: IconeService,
    public IconePersonneService: IconePersonneService,
    graph: GQLService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public mapFn: MapFunctionService
  ) {
    this.IconeVService = IconeVehiculeService,
      this.IconePService = IconePersonneService,
      this.AppParameters = this.configService.config;
    this.services = service;
    this.datacalendar = formatDate(Date.now(), 'yyyy-MM-dd', 'en_US');
    this.datecalendarend = formatDate(Date.now(), 'yyyy-MM-dd', 'en_US');
    this.graphservice = graph;
    this.matIconRegistry.addSvgIcon(
      "unicorn",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../assets/thermometerIcon.svg")
    );
  }
  //#region Declaration
  @ViewChild('alert', { static: true }) alert: ElementRef;
  @ViewChild('pop', { static: true }) pop: ElementRef;
  @ViewChild('range', { static: true }) range: ElementRef;
  @ViewChild('iconplayer', { static: true }) iconplayer: ElementRef;
  @ViewChild('menufiltre', { static: true }) menufiltre: ElementRef;
  public JsonCulumeActiviteInput: any = {
    c: "00:00",
    t: "00:00",
    m: "00:00",
    cp: "00:00"
  }
  public IconeVService: IconeService = null;
  public IconePService: IconePersonneService = null;
  public graphservice: GQLService = null;
  public blockcontrole: any = false;
  public listVehiculesItem: Array<VehiculeModel> = []
  public tempcalque: boolean = false;
  public myMapType: google.maps.ImageMapType;
  public isMobile: boolean;
  public services: ConfService = null;
  public AppParameters: any;
  public listpointsHistorique: any;
  public countlistpointsHistorique: any = 0;
  public datehistorique: any = "00-00-0000";
  public infoendroit: Array<string> = [];
  public directionsService: any;
  public directionsRenderer: any;
  public recherchlist: Array<any> = [];
  public vehiculepayload: VehiculePayload = null;
  private map1: google.maps.Map = null;
  public ClusterMaker: any;
  public heat: google.maps.visualization.HeatmapLayer = new google.maps.visualization.HeatmapLayer();
  private _hubConnection: HubConnection | undefined;
  message = '';
  messages: string[] = [];
  hubConnection: HubConnection;
  disable$ = new BehaviorSubject([]);
  public isSPI: any = false;
  public startIcon: any;
  public endIcon: any;
  public listv: VehiculeMarker[] = [];
  public PolyObj: any;
  public PolyOp: any;
  public PolyObjStart: any;
  public PolyObjEnd: any;
  public changeText: any = false;
  public color: Array<string> = ['#ff2e96', '#6e00c2', '#0000c2', '#00c2c2', '#721d1d', '#00FF00', '#FFFF00'];
  public center: any;
  public start: any;
  public end: any;
  public op: any;
  public way: Array<any> = [];
  public marker: any;
  public a: any;
  public tourneesMarkerHistorique: Array<google.maps.Marker> = [];
  public ob: Array<any> = [];
  public aprox: Array<any> = [];
  public aproxinfo: Array<any> = [];
  public inputdd: ModelDetailVehicule = new ModelDetailVehicule();
  public CurrentV: any;
  public menuContext: boolean = false;
  public RightClickLat: any = 0;
  public RightClickLng: any = 0;
  public recherche: boolean = false;
  public RechO: any;
  public listVehicules: Array<any> = [];
  public listConducteurs: Array<any> = [];
  public icon: any;
  public traffic: any = false;
  public trafficLayer: any;
  public polyAnni: google.maps.Polyline = null;
  public RechSHOW: any = false;
  public RechSHOW2: any = 0;
  public stationmarkerArray: Array<google.maps.Marker> = []
  public TourneesmarkerArray: Array<google.maps.Marker> = []
  public renderArray: Array<google.maps.DirectionsRenderer> = []
  public polylineArray: Array<google.maps.Polyline> = []
  public mrk: google.maps.Marker;
  public showsniper = false;
  public liveMode: any = false;
  public icontrafic: any = 'traffic';
  public menuhistorique: any = false;
  public datacalendar: any;
  public datecalendarend: any;
  public sidebar = document.getElementById('sidebar');
  public iconTemp = "../../assets/img/thermometerIcon.svg"
  public fullscreenmode: any = false;
  public intervale: any;
  public styles: any = [
    {
      height: 25,
      url: '../assets/img/cercle1.png',
      width: 25,
    },
  ];
  public showMapView: any = false;
  public arrow: any = '../../assets/img/view2.png';
  public zoom: any = 3;
  public screenOptions: any = {
    position: 6,
  };
  public streetViewControlOptions: any = {
    position: 6,
  };
  public mapType: any = 'roadmap';
  public arrowIcon: any = {
    path: '',
    fillColor: 'black',
    fillOpacity: 1,
    strokeColor: 'red',
    strokeWeight: 1,
    scale: 3,
  };
  public HistoriqueIcon: any = {
    path: '',
    fillColor: 'black',
    fillOpacity: 1,
    strokeColor: 'red',
    strokeWeight: 1,
    scale: 3,
  };
  //#endregion
  //#region Functions
  //#region filtre par activite
  filtreactivite(val) {
    if (!this.liveMode) {
      if (val == 0) {
        if (this.ClusterMaker.getMap() == this.map1) {
          return
        }
        setTimeout(() => {
          this.listv.forEach(el => {

            el.marker.setMap(this.map1);

          })
        })
        this.ClusterMaker.setMap(this.map1);
        this.RedrawClusterMarker();
        return
      }
      this.ClusterMaker.setMap(null);
      this.listv.forEach(el => {
        if (el.data.Conducteur.Activite != val) {
          el.marker.setMap(null);
        }
        if (el.data.Conducteur.Activite == val) {
          el.marker.setMap(this.map1);
        }
      })
    }

  }
  //#endregion
  //#region annimation et historique
  public pause: boolean = true;
  public timer: Array<any> = []
  public countplayer = 0;
  public timehistorique: any = '00-00-00';
  public activitehistorique: any = '';
  public pauseval = 0;
  public gradient = [
    "rgba(0, 255, 255, 0)",
    "rgba(0, 255, 255, 1)",
    "rgba(0, 191, 255, 1)",
    "rgba(0, 127, 255, 1)",
    "rgba(0, 63, 255, 1)",
    "rgba(0, 0, 255, 1)",
    "rgba(0, 0, 223, 1)",
    "rgba(0, 0, 191, 1)",
    "rgba(0, 0, 159, 1)",
    "rgba(0, 0, 127, 1)",
    "rgba(63, 0, 91, 1)",
    "rgba(127, 0, 63, 1)",
    "rgba(191, 0, 31, 1)",
    "rgba(255, 0, 0, 1)",

  ];
  moveMarker(map, marker, latlng) {
    marker.setPosition(latlng);
    map.panTo(latlng);
  }
  closeAlert() {
    this.alert.nativeElement.classList.remove('show');
  }
  closePop() {
    this.pop.nativeElement.classList.remove('show');
    this.pop.nativeElement.style.zIndex = -1
  }
  async getValueFromcalender(event) {



    this.datacalendar = formatDate(event.dateRange.startDate, 'yyyy-MM-dd', 'en_US');
    this.datecalendarend = formatDate(event.dateRange.endDate, 'yyyy-MM-dd', 'en_US');
    if (event.view == "day") {
      this.datecalendarend = this.datacalendar;
    }
    // var selectvalue = document.getElementById('Vselect') as HTMLSelectElement;
    if (this.CurrentV != null) {
      if (this.mrk) {
        this.mrk.setMap(null);
        this.clearrrtimer(0);
      }


      // var value = selectvalue.options[selectvalue.selectedIndex].value
      this.SVC(this.CurrentV)
    }
  }
  controlModeLiveMapChangeMode() {
    this.tourneesMarkerHistorique.forEach(el => {
      el.setMap(null)
    })
    this.clearrrtimer(0);
    if (this.heat != null) this.heat.setMap(null)
    this.CurrentV = null
    this.RechSHOW = 0;
    this.RechSHOW2 += 1

    this.listpointsHistorique = [];
    this.countlistpointsHistorique = 0;
    this.datehistorique = "00-00-0000"
    this.timehistorique = "00-00-00"
    this.showsniper = false
    this.polylineArray.forEach(el => {
      el.setMap(null);
    });
    this.polylineArray = []
    this.renderArray.forEach(el => {
      el.setMap(null);
    });
    this.renderArray = []
    this.stationmarkerArray.forEach(el => {
      el.setMap(null);
    });
    this.stationmarkerArray = []
    this.directionsRenderer.setMap(null);
    if (this.mrk)
      this.mrk.setMap(null);
    setTimeout(() => {
      this.listv.forEach(el => {
        el.marker.setMap(this.map1);

      })
    })

    this.ClusterMaker.setMap(this.map1);
    this.RedrawClusterMarker();
  }
  controlModeHistoriqueMapChangeMode() {
    this.directionsRenderer.setMap(null);
    this.range.nativeElement.style.background = 'darksalmon'
    setTimeout(() => {
      this.listv.forEach(el => {
        if (this.CurrentV != null) {
          if (el.id != this.CurrentV.idVehicule)
            el.marker.setMap(null);
          if (el.id == this.CurrentV.idVehicule)
            el.marker.setMap(this.map1);
        }


      })

    })
    this.TourneesmarkerArray.forEach(el => {
      el.setMap(null);
    });
    setTimeout(() => {
      this.listv.forEach(el => {
        el.marker.setMap(null);
      })
    })
    this.ClusterMaker.setMap(null);

  }
  changemode(event) {
    this.range.nativeElement.style.background = 'darksalmon'
    this.liveMode = event.target.checked;

    //  var menudetail = document.getElementById("Mdetail");
    var a = document.getElementById('sliderange');
    if (this.liveMode) {
      this.controlModeHistoriqueMapChangeMode();
      a.hidden = false;


    }
    else {
      this.controlModeLiveMapChangeMode()
      a.hidden = true;

    }

  }
  playpause() {

    if (this.pause) {
      this.pause = false;
      this.pauseval = this.range.nativeElement.value
    }

    else {

      this.pause = true
      this.clearrrtimer(this.countplayer)
      return
    }
    var restpoint = this.listpointsHistorique.slice(this.pauseval);
    for (var i = 0; i < restpoint.length; i++) {


      this.timer[i] = setTimeout((coords, i) => {

        this.moveMarker(this.map1, this.mrk, new google.maps.LatLng(Number(coords.latitude), Number(coords.longitude)))
        this.datehistorique = formatDate(restpoint[i].dateReception, 'yyyy-MM-dd', 'en_FR')
        this.timehistorique = formatDate(restpoint[i].dateReception, 'HH:mm:ss', 'en_FR')
        this.activitehistorique = this.mapFn.getValactivite(coords.activite)
        this.range.nativeElement.value = this.pauseval++
        this.countplayer = i - 1
      }, 30 * i, restpoint[i], i);

      if (i > 0) {
        clearTimeout(this.timer[i - 1])
        this.timer.splice(i - 1, 1);
      }


    }



  }
  clearrrtimer(val) {
    this.countplayer = val
    this.timer.forEach(el => {
      clearTimeout(el);
    })
  }
  async filtretourne(id, dd, df) {
    var tournes = await this.service.getTournees(id);
    var tourneefiltre = [];
    tournes.forEach(el => {
      if (new Date(formatDate(el.dateDebut, 'yyyy-MM-dd', 'en_US')) >= new Date(formatDate(dd, 'yyyy-MM-dd', 'en_US'))


      ) {
        tourneefiltre.push(el.enlevementLivraisons)
      }
    });
    return tourneefiltre;

  }
  controleMapHistorique() {
    this.range.nativeElement.style.background = 'linear-gradient(to right,while 0% 100%)'
    this.tourneesMarkerHistorique.forEach(el => {
      el.setMap(null);
    })
    this.polylineArray.forEach(el => {
      el.setMap(null);
    });
    this.polylineArray = []
    this.renderArray.forEach(el => {
      el.setMap(null);
    });
    this.renderArray = []
    this.stationmarkerArray.forEach(el => {
      el.setMap(null);
    });
    this.stationmarkerArray = []
    this.closeAlert()
    if (this.mrk != undefined) {
      this.directionsRenderer.setMap(null);

      this.mrk.setMap(null)
    }
  }
  CreateActiviteMarkerForhistorique(activite, item) {
    var url = ""
    if (activite == 0) {
      console.log(0);
      url = '../assets/img/coupoure default.svg'
      var icont = {
        url: url,
        scaledSize: new google.maps.Size(14, 14),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(7, 7),

      };
      var stationmarker1 = new google.maps.Marker({ map: this.map1, visible: true, position: new google.maps.LatLng(item.latitude, item.longitude), icon: icont })
      this.stationmarkerArray.push(stationmarker1);
      return true;
    }
    if (activite == 3) {
      url = '../assets/img/travail default.svg'
      var iconv = {
        url: url,
        scaledSize: new google.maps.Size(12, 12),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(6, 6),

      };
      var stationmarker = new google.maps.Marker({ map: this.map1, visible: true, position: new google.maps.LatLng(item.latitude, item.longitude), icon: iconv })
      this.stationmarkerArray.push(stationmarker);
      return true;
    }

    /*   if(activite==5 || activite ==0 || activite == 1 || activite == 3)
       {
           var icont = {
             url: url,
             scaledSize: new google.maps.Size(12, 12),
             origin: new google.maps.Point(0, 0),
             anchor: new google.maps.Point(6, 6),
             
           };
           var stationmarker1 = new google.maps.Marker({ map: this.map1, visible: true, position: new google.maps.LatLng(item.latitude, item.longitude), icon: icont })
           this.stationmarkerArray.push(stationmarker1);
       }*/
  }
  setmarkerhistorique(el, b, mrk, map) {
    this.mrk = null
    this.mrk = new google.maps.Marker({
      visible: true,
      icon: el.marker.getIcon()
    });
    this.mrk.setMap(map)
    this.mrk.setTitle(formatDate(b.listHistorique[b.listHistorique.length - 1].dateReception, 'yyyy-MM-dd HH:mm:ss', 'en_FR'))
    this.mrk.setPosition(new google.maps.LatLng(b.listHistorique[b.listHistorique.length - 1].latitude, b.listHistorique[b.listHistorique.length - 1].longitude))
    map.setCenter(new google.maps.LatLng(b.listHistorique[b.listHistorique.length - 1].latitude, b.listHistorique[b.listHistorique.length - 1].longitude))
    map.setZoom(7);
  }
  async SVC(event: VehiculeModel) {

    this.controleMapHistorique();
    if (event == null) {
      this.showsniper = false
      return
    }
    var b: any;
    if (event.idVehicule != "00000000-0000-0000-0000-000000000000")
      this.showsniper = true;

    await this.filtretourne(event.idVehicule, formatDate(this.datacalendar, 'yyyy-MM-dd', 'en_US'), formatDate(this.datecalendarend, 'yyyy-MM-dd', 'en_US')).then(tournees => {
      tournees.forEach(el => {
        el.forEach(el => {
          if (!el.isEnlevement) {
            var ic = {
              url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.mapFn.replace('Liv', "rgb(91, 255, 91)")),
              scaledSize: new google.maps.Size(155, 60),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(76, 42),
            };
            var point = {
              lat: Number(el.livraison.latitude),
              lng: Number(el.livraison.longitude),
            };
            var marker = new google.maps.Marker({
              position: new google.maps.LatLng(point),
              map: this.map1,
              visible: true,
              title: 'Livraison',
              icon: ic,
            });
            if (el.livraison.latitude != null)
              marker.setPosition({ lat: Number(el.livraison.latitude), lng: Number(el.livraison.longitude) });
            this.tourneesMarkerHistorique.push(marker);

          }
          if (el.isEnlevement) {
            var ic = {
              url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.mapFn.replace('Exp', "rgb(60, 119, 255)")),
              scaledSize: new google.maps.Size(155, 60),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(76, 42),
            };
            var point = {
              lat: Number(el.expedition.latitude),
              lng: Number(el.expedition.longitude),
            };
            var marker = new google.maps.Marker({
              position: new google.maps.LatLng(point),
              map: this.map1,
              visible: true,
              title: 'Expedition',
              icon: ic,
            });
            if (el.expedition.latitude != null)
              marker.setPosition({ lat: Number(el.expedition.latitude), lng: Number(el.expedition.longitude) });
            this.tourneesMarkerHistorique.push(marker);

          }
        });
      })
      this.tourneesMarkerHistorique.forEach(el => {
        el.setMap(this.map1)
      })
    })
    var type = '';
    this.listv.forEach(el => {
      if (el.id == event.idVehicule)
        type = el.type
    })
    if (type == 'V') {
      // votre code à mesurer ici
      var dt1 = formatDate(this.datacalendar, 'yyyy-MM-dd', 'en_US') + "T00:00:00.000Z"
      var dt2 = formatDate(this.datecalendarend, 'yyyy-MM-dd', 'en_US') + "T23:59:59.000Z";
      await this.graphservice.getHistorique(dt1, dt2, event.idVehicule)
        .subscribe(
          async (data: any) => {
            b = this.graphservice.buildAllHisortiqueVehicule(data, event.idVehicule);

            if (b != undefined) {
              if (this.mapFn.diffdate(dt1, dt2) <= 1)
                this.range.nativeElement.style.background = this.mapFn.RangeActiviteStyle(b);
            }

            // affichage de trage historique
            if (b.listHistorique.length == 0 || b == undefined) {
              this.alert.nativeElement.classList.add('show');

              this.showsniper = false


              return
            }
            this.listpointsHistorique = b.listHistorique;
            this.countlistpointsHistorique = b.listHistorique.length;
            this.datehistorique = formatDate(this.listpointsHistorique[this.countlistpointsHistorique - 1].dateReception, 'yyyy-MM-dd', 'en_FR')
            this.timehistorique = formatDate(this.listpointsHistorique[this.countlistpointsHistorique - 1].dateReception, 'HH:mm:ss', 'en_FR')

            var i = 0
            this.listv.forEach(async el => {
              if (el.id == event.idVehicule) {
                this.way = []
                this.setmarkerhistorique(el, b, this.mrk, this.map1);
              }
            })
            this.way = [];

            // debut fin marker
            var activite = b.listHistorique[0].activite
            for (var item of b.listHistorique) {
              console.log(item.activite)
              if (i > 0) {
                if (activite != item.activite) {
                  activite = item.activite;
                  this.CreateActiviteMarkerForhistorique(activite, item)
                }
              }

              i++
              this.way.push({
                location: {
                  lat: Number(item.latitude),
                  lng: Number(item.longitude),
                },
                stopover: false,
                activite: item.activite
              })
            }
            var path = this.way;


            this.autoRefresh(this.map1, path, this.mrk);

            var icon = {
              url: '../assets/img/depart default.svg',
              scaledSize: new google.maps.Size(12, 12),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(6, 6),
            };
            var stationmarker = new google.maps.Marker({ map: this.map1, visible: true, position: new google.maps.LatLng(b.listHistorique[0].latitude, b.listHistorique[0].longitude), icon: icon })

            this.stationmarkerArray.push(stationmarker);
            stationmarker.setOpacity(0.5);
            var icon = {
              url: '../assets/img/arrivé default.svg',
              scaledSize: new google.maps.Size(12, 12),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(6, 6),
            };
            var stationmarkerfin = new google.maps.Marker({ map: this.map1, visible: true, position: new google.maps.LatLng(b.listHistorique[this.countlistpointsHistorique - 1].latitude, b.listHistorique[this.countlistpointsHistorique - 1].longitude), icon: icon })
            stationmarkerfin.addListener('click', async () => {
              var window = await this.mapFn.Getwindowinfo(event.Immatriculation.Vrn, b);
              window.open(
                this.map1, stationmarkerfin
              );
            })
            stationmarkerfin.setOpacity(0.5);
            this.stationmarkerArray.push(stationmarkerfin);



            this.showsniper = false
          })
    }
    else
      await this.services.getPointsUserByid(event.idVehicule, formatDate(this.datacalendar, 'yyyy-MM-dd', 'en_US'), formatDate(this.datecalendarend, 'yyyy-MM-dd', 'en_US')).then(async (b) => {

        if (b.listHistorique.length == 0) {
          this.alert.nativeElement.classList.add('show');

          this.showsniper = false
          return
        }
        this.listpointsHistorique = b.listHistorique;
        this.countlistpointsHistorique = b.listHistorique.length;
        this.datehistorique = formatDate(this.listpointsHistorique[this.countlistpointsHistorique - 1].dateReception, 'yyyy-MM-dd', 'en_FR')
        this.timehistorique = formatDate(this.listpointsHistorique[this.countlistpointsHistorique - 1].dateReception, 'HH:mm:ss', 'en_FR')
        var i = 0

        this.way = [];
        var stationcompteur = 0
        // debut fin marker
        var icon = {
          url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.mapFn.replace('Debut', '#354360')),
          scaledSize: new google.maps.Size(155, 60),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(76, 42),
        };
        var stationmarker = new google.maps.Marker({ map: this.map1, visible: true, position: new google.maps.LatLng(b.listHistorique[0].latitude, b.listHistorique[0].longitude), icon: icon })

        this.stationmarkerArray.push(stationmarker);
        var icon = {
          url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.mapFn.replace('Fin', '#354360')),
          scaledSize: new google.maps.Size(155, 60),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(76, 42),
        };
        var stationmarkerfin = new google.maps.Marker({ map: this.map1, visible: true, position: new google.maps.LatLng(b.listHistorique[this.countlistpointsHistorique - 1].latitude, b.listHistorique[this.countlistpointsHistorique - 1].longitude), icon: icon })
        stationmarkerfin.addListener('click', async () => {
          var window = await this.mapFn.Getwindowinfo(event.Immatriculation.Vrn, b);
          window.open(
            this.map1, stationmarkerfin
          );
        })
        this.stationmarkerArray.push(stationmarkerfin);
        for await (var item of b.listHistorique) {

          if (i > 0) {
            var c = this.mapFn.distance(Number(item.latitude), Number(item.longitude), Number(b.listHistorique[i - 1].latitude), Number(b.listHistorique[i - 1].longitude), 'K');

            if (c < 0.07) {
              stationcompteur++;
            }
            else if (i == this.countlistpointsHistorique - 1) {
              if (stationcompteur >= 10) {

                var icon = {
                  url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.mapFn.replace('Repos', '#FF0000')),
                  scaledSize: new google.maps.Size(155, 60),
                  origin: new google.maps.Point(0, 0),
                  anchor: new google.maps.Point(76, 42),
                };
                var stationmarker = new google.maps.Marker({ map: this.map1, visible: true, title: 'Station de :' + formatDate(b.listHistorique[i - stationcompteur].dateReception, 'MM/dd HH:mm ', 'en_FR') + ' a :' + formatDate(item.dateReception, 'MM/dd HH:mm ', 'en_FR'), position: new google.maps.LatLng(item.latitude, item.longitude), icon: icon })
                this.stationmarkerArray.push(stationmarker);
                stationcompteur = 0
              }
              stationcompteur = 0
            }
            else {
              if (stationcompteur >= 10) {

                var icon = {
                  url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.mapFn.replace('Repos', '#FF0000')),
                  scaledSize: new google.maps.Size(155, 60),
                  origin: new google.maps.Point(0, 0),
                  anchor: new google.maps.Point(76, 42),
                };
                var stationmarker = new google.maps.Marker({ map: this.map1, visible: true, title: 'Station de :' + formatDate(b.listHistorique[i - stationcompteur].dateReception, 'MM/dd HH:mm ', 'en_FR') + ' a :' + formatDate(item.dateReception, 'MM/dd HH:mm ', 'en_FR'), position: new google.maps.LatLng(item.latitude, item.longitude), icon: icon })
                this.stationmarkerArray.push(stationmarker);
                stationcompteur = 0
              }
              stationcompteur = 0
            }

          }
          i++
          this.way.push({
            location: {
              lat: Number(item.latitude),
              lng: Number(item.longitude),
            },
            stopover: false,
          })
        }
        var path = this.way;
        await new Promise((f) => setTimeout(f, 500));

        this.mrk = new google.maps.Marker({
          visible: true,
        });
        /*  var selectvalue = document.getElementById('Vselect') as HTMLSelectElement;
          var value = selectvalue.options[selectvalue.selectedIndex].text*/
        this.listv.forEach(async el => {
          if (el.id == event.idVehicule) {
            this.way = []

            this.setmarkerhistorique(el, b, this.mrk, this.map1);




          }
        })
        this.autoRefresh(this.map1, path, this.mrk);
        this.showsniper = false
      }).catch(err => {
        this.alert.nativeElement.classList.add('show');

        this.showsniper = false
      });


  }
  changelecteur(event) {

    this.pause = true;
    var value = event.target.value;
    this.clearrrtimer(value);
    /*  if (event.target.value == this.countlistpointsHistorique) {
        value--
      }*/
    this.moveMarker(this.map1, this.mrk, new google.maps.LatLng(this.listpointsHistorique[value].latitude, this.listpointsHistorique[value].longitude))
    this.datehistorique = formatDate(this.listpointsHistorique[value].dateReception, 'yyyy-MM-dd', 'en_FR')
    this.timehistorique = formatDate(this.listpointsHistorique[value].dateReception, 'HH:mm:ss', 'en_FR')
    this.activitehistorique = this.mapFn.getValactivite(this.listpointsHistorique[value].activite)
  }
  autoRefresh(map, pathCoords, marker) {
    var i, route, marker;

    this.arrowIcon.path = google.maps.SymbolPath.CIRCLE;
    route = new google.maps.Polyline({
      path: [],
      geodesic: true,
      strokeColor: '#32CD32',
      strokeOpacity: 0.4,
      strokeWeight: 5,
      editable: false,
      /* icons: [
         {
           icon: this.arrowIcon,
           offset: "0",
           repeat: "20px",
         },
       ],*/
      map: map
    });
    var heatMapData = [];
    var temp = 0
    for (i = 0; i < pathCoords.length; i++) {
      var pos = new google.maps.LatLng(pathCoords[i].location.lat, pathCoords[i].location.lng)
      temp = Math.random() * (25 + 10) - 10;
      heatMapData.push({ location: pos, weight: temp })
      // setTimeout((coords, i) => {
      var pos = new google.maps.LatLng(pathCoords[i].location.lat, pathCoords[i].location.lng)
      route.getPath().push(pos);

      this.moveMarker(map, marker, pos);
      if (i == pathCoords.length - 1) {
        this.polylineArray.push(route);
        var focuspoint = new google.maps.LatLng(pathCoords[pathCoords.length - 1].location.lat, pathCoords[pathCoords.length - 1].location.lng)
        map.panTo(focuspoint);
        if (this.map1.getZoom() < 10)
          this.map1.setZoom(10);

      }

      // }, 0, pathCoords[i], i);


    }
    this.heat = new google.maps.visualization.HeatmapLayer({
      maxIntensity: 2, radius: 7,
      opacity: 0.8,
      gradient: this.gradient,
      data: heatMapData
    });
  }
  autoTracingRoute(map, pathCoords, routecolor) {
    if (routecolor == "")
      routecolor = "#354360"


    clearInterval(this.intervale);
    this.polylineArray.forEach(el => {
      el.setMap(null);
    });
    this.polylineArray = []
    var i, route
    this.arrowIcon.path = google.maps.SymbolPath.FORWARD_OPEN_ARROW;
    this.arrowIcon.fillOpacity = 1
    this.arrowIcon.fillColor = routecolor
    this.arrowIcon.strokeWeight = 1
    route = new google.maps.Polyline({
      path: [],
      geodesic: true,
      strokeColor: routecolor,
      strokeOpacity: 0.5,
      strokeWeight: 5,
      editable: false,
      icons: [
        {
          icon: this.arrowIcon,
          offset: "100%",
        },
      ],
      map: map,
      zIndex: -1
    });
    this.mapFn.animateCircle(route, this.intervale)
    this.polylineArray.push(route);
    var focuspoint = new google.maps.LatLng(pathCoords[0].location.lat, pathCoords[0].location.lng)
    map.panTo(focuspoint);
    if (this.map1.getZoom() < 11 || this.map1.getZoom() > 11)
      this.map1.setZoom(11);
    pathCoords.reverse();
    for (i = 0; i < pathCoords.length; i++) {
      setTimeout((coords, i) => {
        var pos = new google.maps.LatLng(coords.location.lat, coords.location.lng)
        route.getPath().push(pos);
      }, 0, pathCoords[i], i);
    }
  }
  openMenuHistorique() {
    this.menuhistorique = !this.menuhistorique;
  }
  //#endregion
  //#region menucontext
  open(content) {
    this.modalService.open(content, { centered: true, size: 'xl' });
  }
  openModal(event) {
    const a = this.modalService.open(ModalComponent, { centered: true, size: 'xl' });
    a.componentInstance.lng = event.lng;
    a.componentInstance.lat = event.lat;
  }
  Aproximité(coord) {
    this.aproxinfo = [];
    this.listVehiculesItem.map((e) => {
      if (e.Immatriculation.Vrn == ('' || null))
        return
      var a = this.mapFn.distance(coord.lat, coord.lng, e.Position.Lat, e.Position.Lng, 'K');
      if (a < 50) {
        this.aproxinfo.push(e);
      }

    });

  }
  async infocoords(coord) {
    this.Aproximité(coord)
    this.closePop()
    this.pop.nativeElement.style.zIndex = 0
    this.RechSHOW = false
    this.infoendroit = []
    this.infoendroit.push("List des adresses :")
    this.infoendroit.push(coord.lat.toFixed(6) + "," + coord.lng.toFixed(6));
    var listadr = await this.mapFn.geodecodeinfo(coord.lat, coord.lng);
    this.infoendroit.push(...listadr);
    if (this.infoendroit.length > 0) {
      this.pop.nativeElement.classList.add('show');

      var array = listadr[0].split(',');
      this.infoendroit.push('Pays :' + array[array.length - 1]);
    }
    var menu = document.getElementById('menuC');
    menu.hidden = true;
  }
  async setcoord(coords: any) {
    if (coords != null) {
      if (this.PolyObjStart != null) {
        this.PolyObjStart.setMap(this.map1)
        this.PolyObjStart.setPosition(new google.maps.LatLng(coords.lat, coords.lng))

      }
      else
        this.PolyObjStart = new google.maps.Marker({ map: this.map1, visible: true, position: new google.maps.LatLng(coords.lat, coords.lng), icon: this.startIcon })
    }
    if (this.PolyObjEnd) {
      if (this.PolyObjStart.getMap() != null && this.PolyObjEnd.getMap() != null)
        this.setPoly()
    }

    return

  }
  async setcoord2(coords: any) {
    if (coords != null) {
      if (this.PolyObjEnd != null) {
        this.PolyObjEnd.setMap(this.map1)
        this.PolyObjEnd.setPosition(new google.maps.LatLng(coords.lat, coords.lng))

      }
      else {
        this.PolyObjEnd = new google.maps.Marker({ map: this.map1, visible: true, position: new google.maps.LatLng(coords.lat, coords.lng), icon: this.endIcon })
      }

    }
    if (this.PolyObjStart.getMap() != null && this.PolyObjEnd.getMap() != null)
      this.setPoly()
    return

  }
  setPoly() {

    var menu = document.getElementById('menuC');
    menu.hidden = true;
    this.calculateAndDisplayRoute(this.directionsService, this.directionsRenderer, this.PolyObjStart.getPosition(), this.PolyObjEnd.getPosition(), null, [], this.PolyOp)
    this.directionsRenderer.setMap(this.map1);
  }
  //#endregion
  //#region mapcontrol

  TrafficController(b, event) {
    if (event == 3) {
      this.tempcalque = !this.tempcalque;
      if (this.tempcalque) {
        $('#b3').prop(
          'style',
          'background-color: #354360;border-radius: 5px;'
        );
        this.iconTemp = "../../assets/img/thermometerIconCliked.svg"
        this.heat.setMap(this.map1)
      }
      else {
        $('#b3').prop('style', 'background-color: rgb(255, 255, 255);border-radius: 5px;');
        this.heat.setMap(null);
        this.iconTemp = "../../assets/img/thermometerIcon.svg"
      }
      // this.SetcalqueTempurature(this.tempcalque);

      return
    }

    if (event == 2) {
      if (this.traffic) {

        $('#b2').prop('style', 'background-color: rgb(255, 255, 255);border-radius: 5px;');
        $('#Icontraffic').addClass("white-icon");
        $('#Icontraffic').removeClass("white-icon-X");

        this.trafficLayer.setMap(null);
        this.traffic = false;
        this.icontrafic = 'traffic';

      }
      else {
        $('#b2').prop(
          'style',
          'background-color: #354360;border-radius: 5px;'
        );

        $('#Icontraffic').addClass("white-icon-X");
        $('#Icontraffic').removeClass("white-icon");
        this.icontrafic = 'directions_bus';
        this.trafficLayer.setMap(this.map1);
        this.traffic = true;
      }
    }
  }
  filtreButtonHover(index) {
    if (index == 1)
      for (var i = 1; i < this.menufiltre.nativeElement.children.length; i++) {
        this.menufiltre.nativeElement.children[i].style.display = "inline-block"
      }
    if (index == 2) {
      for (var i = 1; i < this.menufiltre.nativeElement.children.length; i++) {
        this.menufiltre.nativeElement.children[i].style.display = "none"
      }
    }
  }
  mapTyperoadmap() {
    this.mapType = 'roadmap';
  }
  mapTypehybrid() {
    this.mapType = 'hybrid';
  }
  mapTypeterrain() {
    this.mapType = 'terrain';
  }
  fullscreen() {
    this.screenOptions = {
      position: 1,
    };
    var divs = document.getElementsByClassName('v');
    console.log(divs.item[1])
    var map = document.getElementById('map')
    if (this.fullscreenmode) {
      this.sidebar.style.display = 'block';
      map.style.paddingLeft = '50px'
      var collection = document.getElementsByClassName("full-screen-buttons") as HTMLCollectionOf<HTMLElement>;
      for (let i = 0; i < collection.length; i++) {
        collection[i].style.marginLeft = "55px";
      }
    }
    else {
      this.sidebar.style.display = 'none'
      map.style.paddingLeft = '0px'
      var collection = document.getElementsByClassName("full-screen-buttons") as HTMLCollectionOf<HTMLElement>;
      for (let i = 0; i < collection.length; i++) {
        collection[i].style.marginLeft = "6px";
      }

    }
    this.fullscreenmode = !this.fullscreenmode;

  }
  zoomcenter() {
    this.map1.setZoom(7);
    this.map1.setCenter(this.center);
    this.zoom = 4;
    this.CurrentV = null;
    this.directionsRenderer.setMap(null);
    this.polylineArray.forEach(el => {
      el.setMap(null);
    });
    this.polylineArray = []
  }
  zoomin() {
    if (this.zoom < 20) this.zoom += 1;
    var zoom = this.map1.getZoom();
    if (zoom < 20) zoom += 1;
    this.map1.setZoom(zoom);
  }
  zoomout() {
    if (this.zoom > 0) this.zoom -= 1;
    var zoom = this.map1.getZoom();
    if (zoom > 0) zoom -= 1;
    this.map1.setZoom(zoom);
  }
  onZoomChange(event: any) {
    this.zoom = event;
  }
  openMapsViewList() {
    if (this.showMapView) {
      this.showMapView = false;
      this.arrow = '../../assets/img/view2.png';
      this.mapType = 'roadmap';
      this.map1.setMapTypeId('roadmap');
    } else {
      this.showMapView = true;
      this.arrow = '../../assets/img/view3.png';
      this.mapType = 'hybrid';
      this.map1.setMapTypeId('hybrid');
    }
  }
  public sendMessage(): void {
    const data = `Sent: ${this.message}`;

    if (this._hubConnection) {
      this._hubConnection.invoke('Send', data);
    }
    this.messages.push(data);
  }
  RightClickChange(event: any) {

    this.RightClickLat = event.coords.lat;
    this.RightClickLng = event.coords.lng;
    this.infoendroit = [];

  }
  mapClickChange() {
    this.directionsRenderer.setMap(null);
    if (this.PolyObjStart)
      this.PolyObjStart.setMap(null);
    if (this.PolyObjEnd)
      this.PolyObjEnd.setMap(null);

    this.infoendroit = [];
    if (!this.liveMode)
      this.RechSHOW = false;
    this.closePop()
  }
  clickmap() {
    var menu = document.getElementById('menuC');
    menu.hidden = true;
    this.changeText = false;
    if (!this.liveMode) {
      this.RechSHOW = false;
      this.RechSHOW2 = this.RechSHOW2 + 1;
    }
    this.menuhistorique = false
  }
  onMap(map: google.maps.Map) {
    window.addEventListener('contextmenu', (e) => e.preventDefault());
    this.map1 = map;
    this.map1.addListener("zoom_changed", () => {
      this.listv.forEach(el => {
        this.mouseLeaveIcon(el.data, el.marker, el.type, null)
      });
    });
    this.trafficLayer = new google.maps.TrafficLayer();
    this.directionsRenderer = new google.maps.DirectionsRenderer({ map: this.map1 });
    this.directionsService = new google.maps.DirectionsService();
    this.map1.setZoom(8);

    if (this.isMobile) {
      this.map1.setOptions({
        streetViewControl: true,
        fullscreenControlOptions: this.screenOptions,
        fullscreenControl: false,
        zoomControl: false,
        disableDefaultUI: true
      });
    } else {
      this.map1.setOptions({
        streetViewControlOptions: this.streetViewControlOptions,
        fullscreenControlOptions: this.screenOptions,
        fullscreenControl: false,
        zoomControl: false,
      });
    }
    this.map1.addListener('click', () => this.mapClickChange());
    google.maps.event.addListener(this.map1, 'rightclick', (event) => {
      this.closePop()
      var width = Math.max(
        document.body.scrollWidth,
        document.documentElement.scrollWidth,
        document.body.offsetWidth,
        document.documentElement.offsetWidth,
        document.documentElement.clientWidth
      );
      var hieght = Math.max(
        document.body.scrollHeight,
        document.documentElement.scrollHeight,
        document.body.offsetHeight,
        document.documentElement.offsetHeight,
        document.documentElement.clientHeight
      );

      var menu = document.getElementById('menuC');
      menu.hidden = false;
      menu.style.top = event.pixel.y + 'px';
      menu.style.left = event.pixel.x + 'px';
      if (event.pixel.x > width - 250) {
        menu.style.left = width - 250 + 'px';
      }
      if (event.pixel.y > hieght - 170) {
        menu.style.top = hieght - 170 + 'px';
      }
    });
    google.maps.event.addListener(this.map1, 'click', () => this.clickmap());
  }
  calculateAndDisplayRoute(
    directionsService: google.maps.DirectionsService,
    directionsRenderer: google.maps.DirectionsRenderer,
    origin,
    destination,
    data,
    way: Array<any>,
    polyOption: any
  ) {
    var polylineOptions = {
      strokeColor: 'black',
      strokeOpacity: 0,
      strokeWeight: 5,
      zIndex: 2,
      geodesic: false,
      clickable: false,
      editable: false,
    };
    directionsService.route(
      {
        origin: origin,
        destination: destination,
        waypoints: way,
        optimizeWaypoints: true,
        travelMode: google.maps.TravelMode.WALKING,
      },
      (response, status) => {

        if (status === google.maps.DirectionsStatus.OK) {
          directionsRenderer.setMap(this.map1);
          directionsRenderer.setOptions({ suppressMarkers: true });

          directionsRenderer.setDirections(response,);
        }
        else {
          console.log("Route: " + status);
        }

      }
    );

  }
  //#endregion
  //#region new function

  toggleBounce(marker, id) {
    if (this.CurrentV == null) {
      setTimeout(() => {
        this.listv.forEach(el => {

          el.marker.setAnimation(null);
        })
      })
    }
    else if (this.CurrentV.idVehicule == id) {
      setTimeout(() => {
        this.listv.forEach(el => {

          el.marker.setAnimation(null);
        })
        marker.setAnimation(google.maps.Animation.BOUNCE);
      })

    }
  }

  ShowTournees(event) {
    setTimeout(() => {
      this.listv.forEach(el => {
        if (el.id == this.CurrentV.id)
          el.marker.setMap(this.map1);
      })
    })
    setTimeout(() => {
      this.listv.forEach(el => {
        if (el.id != this.CurrentV.id)
          el.marker.setMap(null);
      })
    })
    this.TourneesmarkerArray.forEach(el => {
      el.setMap(null);
    });
    this.ClusterMaker.setMap(null);
    this.TourneesmarkerArray = [];
    this.directionsRenderer.setMap(null);
    if (event == null && !this.liveMode) {
      this.ClusterMaker.setMap(this.map1)
      setTimeout(() => {
        this.listv.forEach(el => {

          el.marker.setMap(this.map1);


        })

      })
      return;
    }
    var way: Array<any> = [];
    if (event == null)
      return
    event.enlevementLivraisons.forEach(async element => {
      if (element.isEnlevement) {
        way.push({
          location: { lng: Number(element.expedition.longitude), lat: Number(element.expedition.latitude) },
          stopover: false,
        });
      }
      else {
        way.push({
          location: { lng: Number(element.livraison.longitude), lat: Number(element.livraison.latitude) },
          stopover: false,
        });
      }

    });
    var origin = way[0].location;
    var destination = way[way.length - 1].location;
    this.calculateAndDisplayRoute(this.directionsService, this.directionsRenderer, origin, destination, null, way, null);


    event.enlevementLivraisons.forEach(async element => {
      if (element.isEnlevement) {
        var ic = {
          url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.mapFn.replace("Enl", "rgb(60, 119, 255)")),
          scaledSize: new google.maps.Size(155, 60),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(76, 42),
        };

        var marker = new google.maps.Marker({
          position: new google.maps.LatLng({ lng: element.expedition.longitude, lat: element.expedition.latitude }),
          map: this.map1,
          visible: true,
          title: "tournees",
          icon: ic,
        });
        this.TourneesmarkerArray.push(marker)
      }
      else {
        var ic = {
          url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.mapFn.replace("Liv", "rgb(91, 255, 91)")),
          scaledSize: new google.maps.Size(155, 60),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(76, 42),
        };
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng({ lng: element.livraison.longitude, lat: element.livraison.latitude }),
          map: this.map1,
          visible: true,
          title: "tournees",
          icon: ic,
        });
        this.TourneesmarkerArray.push(marker)
      }
    })

  }
  //#endregion
  //#region New Tracking version recherche
  GetVrnFromPlusInfoComponent(vrn) {
    this.closePop();
    this.NewRechercheSelectedVehicule(vrn);
  }
  ChangingDataTodetailvehiculeAndTracingRoute(vehicule: VehiculeMarker) {
    //identifie type of resource (vehicule=="V" || Spi=="U")
    if (vehicule.type == 'V')
      this.isSPI = false;
    else
      this.isSPI = true;
    //setdata to detailvehicuel input "inputdd"
    this.inputdd.id = vehicule.data.idVehicule;
    this.inputdd.vrn = vehicule.data.Immatriculation.Vrn
    this.inputdd.activite = vehicule.data.Conducteur.Activite;
    this.inputdd.positionX = vehicule.data.Position.Lat;
    this.inputdd.positionY = vehicule.data.Position.Lng;
    this.inputdd.vin = vehicule.data.Immatriculation.Vin;
    this.inputdd.NomC = vehicule.data.Conducteur.NomConducteur

  }
  async LoadingDataAndTracingRoute(type, id, Activite) {
    this.showsniper = true;
    var lastpoint = null;
    if (type == 'V')
      await this.graphservice.getlast5points(id).then((data: any) => {
        lastpoint = this.graphservice.buildAllHisortiqueVehiculeLast5(data);
        console.log(lastpoint)
        this.showsniper = false;
        this.way = []

        if (lastpoint != null) {
          var reversearray = lastpoint.listHistorique
          reversearray.forEach(el => {
            this.way.push({
              location: {
                lat: Number(el.latitude),
                lng: Number(el.longitude),
              },
              stopover: false,
            });
          });
          var way = this.way
          var point = {
            lat: way[0].location.lat,
            lng: way[0].location.lng
          }
          this.listv.find(x => x.id == id).marker.setPosition(new google.maps.LatLng(point));
          this.way = []
          if (way.length > 0)
            this.autoTracingRoute(this.map1, way, '#32CD32');
        }
      }).catch((err) => {
        this.showsniper = false
        console.log("Alert :" + err.message)
      })
    else {
      lastpoint = await this.service.getlast5PointsUserByid(id)
      this.showsniper = false
    }




  }

  async NewRechercheSelectedVehicule(event: any) {
    clearInterval(this.intervale);
    this.isSPI = false;
    this.closePop()
    if (event == '') {
      this.CurrentV = null;
      this.directionsRenderer.setMap(null);
      this.polylineArray.forEach(el => {
        el.setMap(null);
      });
      this.polylineArray = []
      this.RechSHOW = false;
    } else {
      this.JsonCulumeActiviteInput = {
        c: "00:00",
        t: "00:00",
        m: "00:00",
        cp: "00:00"
      }
      // rechercher par VRN vehicule 
      var vehicule = this.listv.find(x => x.data.Immatriculation.Vrn == event)
      if (vehicule != null) {
        this.CurrentV = vehicule.data;
        //setdata to detailvehciulecomponent 
        this.ChangingDataTodetailvehiculeAndTracingRoute(vehicule)
        this.aprox = this.mapFn.FindListVehiculeAproximite(this.listVehiculesItem, this.CurrentV)
        this.RechSHOW = true;
        if (!this.liveMode) {
          //charging data and tracing route
          this.LoadingDataAndTracingRoute(vehicule.type, vehicule.id, vehicule.data.Conducteur.Activite)
        }

        if (this.liveMode) {
          this.listv.find(x => x.id == this.CurrentV.idVehicule).marker.setMap(null);
          this.SVC(this.CurrentV)
        }

        return
      }
      // rechercher par NunConducteur vehicule 
      var vehiculeParConducetur = this.listv.find(x => x.data.Conducteur.numCartConducteur == event)
      if (vehiculeParConducetur != null) {
        this.CurrentV = vehiculeParConducetur.data;
        //setdata to detailvehciulecomponent 
        this.ChangingDataTodetailvehiculeAndTracingRoute(vehiculeParConducetur)
        this.aprox = this.mapFn.FindListVehiculeAproximite(this.listVehiculesItem, this.CurrentV)
        this.RechSHOW = true;
        if (!this.liveMode) {
          //charging data and tracing route
          this.LoadingDataAndTracingRoute(vehiculeParConducetur.type, vehiculeParConducetur.id, vehiculeParConducetur.data.Conducteur.Activite)
        }

        if (this.liveMode) {
          this.listv.find(x => x.id == this.CurrentV.idVehicule).marker.setMap(null);
          this.SVC(this.CurrentV)
        }
        this.showsniper = false;
        return
      }

    }

  }
  RedrawClusterMarker() {

    this.listv.forEach(el => {
      setTimeout(() => {
        this.ClusterMaker.removeMarker(el.marker);
        this.ClusterMaker.addMarker(el.marker);
      })

    })
  }
  async NewClickMarker(event: VehiculeModel) {
    clearInterval(this.intervale);
    if (this.CurrentV == event) {
      setTimeout(() => {
        this.listv.forEach(el => {

          el.marker.setMap(this.map1);

        })
      })
      this.ClusterMaker.setMap(this.map1);
      this.TourneesmarkerArray.forEach(el => {
        el.setMap(null);
      });
      this.RechSHOW = false;
      this.directionsRenderer.setMap(null);
      this.polylineArray.forEach(el => {
        el.setMap(null);
      });
      this.polylineArray = []

      if (this.polyAnni != undefined) this.polyAnni.setMap(null);
      {
        this.directionsRenderer.setMap(null);
        this.polylineArray.forEach(el => {
          el.setMap(null);
        });
        this.polylineArray = []
      }
      this.CurrentV = null;
      this.map1.setZoom(5);
      this.zoom = 5;
      this.zoom--


      this.RedrawClusterMarker();
      this.showsniper = false;
      return
    }
    this.NewRechercheSelectedVehicule(event.Immatriculation.Vrn)

  }
  async mouseHoverIcon(Vehicule, marker, type, random) {
    var randomcolor = "#354360"
    if (Vehicule.Conducteur.Activite == 2) {
      randomcolor = "#17a2b8"
    }
    if (Vehicule.Conducteur.Activite == 3) {
      randomcolor = "#fd7e14"
    }
    if (Vehicule.Conducteur.Activite == 4) {
      randomcolor = "#FF0000"
    }
    if (Vehicule.Conducteur.Activite == 5) {
      randomcolor = "#006400"
    }
    var adr = Vehicule.Position.Lat + "" + Vehicule.Position.Lng;
    await this.mapFn.geodecodeinfo(Vehicule.Position.Lat, Vehicule.Position.Lng).then((data) => {
      adr = data[2];
    }).catch((err) => {
      return;
    })
    var ic = {
      url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.mapFn.replaceHoverIcon(Vehicule.Immatriculation.Vrn, randomcolor, Vehicule.Position.DateReception, "HOV", type, adr)),
      scaledSize: new google.maps.Size(355, 220),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(38, 135),
    };
    marker.setIcon(ic);
    marker.setZIndex(70);
  }
  async mouseLeaveIcon(Vehicule, marker, type, random) {
    var randomcolor = "#354360"
    if (Vehicule.Conducteur.Activite == 2) {
      randomcolor = "#17a2b8"
    }
    if (Vehicule.Conducteur.Activite == 3) {
      randomcolor = "#fd7e14"
    }
    if (Vehicule.Conducteur.Activite == 4) {
      randomcolor = "#FF0000"
    }
    if (Vehicule.Conducteur.Activite == 5) {
      randomcolor = "#006400"
    }
    var adr = Vehicule.Position.Lat + Vehicule.Position.Lng;
    var ic = {
      url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.mapFn.replaceHoverIcon(Vehicule.Immatriculation.Vrn, randomcolor, Vehicule.Position.DateReception, "H", type, adr)),
      scaledSize: new google.maps.Size(155, 60),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(15, 50),
    };
    marker.setIcon(ic);
    marker.setZIndex(7);
  }
  async NewSetMarkerVehiculeToMap(Vehicule: VehiculeModel, type) {
    var map1 = null
    if (this.liveMode)
      map1 = this.map1
    if (Vehicule.Immatriculation.Vrn == null || '') { return }
    if (Vehicule.Conducteur.numCartConducteur != null) {
      var itemC = new ConducteurRechercherItem
      itemC = {
        num: Vehicule.Conducteur.numCartConducteur,
        type: 'Conducteur',
        name: Vehicule.Conducteur.NomConducteur != '' ? Vehicule.Conducteur.NomConducteur : Vehicule.Conducteur.numCartConducteur,
        activite: Vehicule.Conducteur.Activite,
      }
      this.recherchlist.push(itemC);
    }
    if (Vehicule.Immatriculation.Vrn != null) {
      var item = new VehiculeRechercherItem
      item = {
        name: Vehicule.Immatriculation.Vrn,
        type: 'Vehicule',
        activite: Vehicule.Conducteur.Activite,
        isAct: Vehicule.Position.IsEnMouvement,
        vitesse: Vehicule.Position.Vitesse,
      }
      this.recherchlist.push(item);
    }
    var randomcolor = "#354360"
    if (Vehicule.Conducteur.Activite == 2) {
      randomcolor = "#17a2b8"
    }
    if (Vehicule.Conducteur.Activite == 3) {
      randomcolor = "#fd7e14"
    }
    if (Vehicule.Conducteur.Activite == 4) {
      randomcolor = "#FF0000"
    }
    if (Vehicule.Conducteur.Activite == 5) {
      randomcolor = "#006400"
    }
    var adr = Vehicule.Position.Lat + "" + Vehicule.Position.Lng;
    var ic = {
      url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.mapFn.replaceHoverIcon(Vehicule.Immatriculation.Vrn, randomcolor, Vehicule.Position.DateReception, "H", type, adr)),
      scaledSize: new google.maps.Size(155, 60),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(15, 50),
    };
    var point = {
      lat: Vehicule.Position.Lat,
      lng: Vehicule.Position.Lng,
    };
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(point),
      map: this.map1,
      visible: true,
      icon: ic,
    });
    marker.addListener('click', () => {
      this.NewClickMarker(Vehicule);
      // this.toggleBounce(marker, Vehicule.idVehicule)
    });
    marker.addListener('mouseover', async () => {
      this.listv.forEach(el => {
        this.mouseLeaveIcon(el.data, el.marker, el.type, randomcolor)
      });
      this.mouseHoverIcon(Vehicule, marker, type, randomcolor)

    });

    // assuming you also want to hide the infowindow when user mouses-out
    marker.addListener('mouseout', () => {
      this.mouseLeaveIcon(Vehicule, marker, type, randomcolor)
    });

    var vehicule: VehiculeMarker = new VehiculeMarker();
    vehicule.id = Vehicule.idVehicule;
    vehicule.marker = marker;
    vehicule.data = Vehicule;
    vehicule.type = type;
    this.listv.push(vehicule);
    var mark: google.maps.Marker[] = [];
    this.listv.forEach((element) => {
      mark.push(element.marker);
    });
    if (this.ClusterMaker != undefined)
      this.ClusterMaker.addMarkers(mark);
  }
  /* async NewLiveSetMarkerVehiculeToMap(t: VehiculePayload) {
     //#region Partie Personne
     var map1 = null
     var type: string = 'V';
     if (this.liveMode)
       map1 = this.map1
 
     if (t.vehiculeId == '00000000-0000-0000-0000-000000000000') {
       type = "U"
       var a = false;
       t.vehiculeId == t.ressourceId;
     }
     //#endregion
     //#region Partie Vehicules
     if (t.evenements[0].geoLocalisation.position == null) {
       return;
     }
     var exist = false;
     // comparaison de des dernier point et mouvement de marker
     this.listv.forEach(async (el) => {
       if (el.id == t.vehiculeId) {
         exist = true;
         var dis = this.mapFn.distance(
           el.marker.getPosition().lat(),
           el.marker.getPosition().lng(),
           t.evenements[0].geoLocalisation.position.latitude,
           t.evenements[0].geoLocalisation.position.longitude,
           'K'
         );
 
         if (dis < 0.02) {
           return;
         }
 
         var point = {
           lat: t.evenements[t.evenements.length - 1].geoLocalisation.position.latitude,
           lng: t.evenements[t.evenements.length - 1].geoLocalisation.position.longitude,
         };
         el.marker.setPosition(new google.maps.LatLng(point));
         // Mouvement de current vehicule 
         if (this.CurrentV != null && this.CurrentV.id == el.id) {
           this.directionsRenderer.setMap(null);
           this.polylineArray.forEach(el => {
             el.setMap(null);
           });
           this.polylineArray = []
           var type = ''
           this.listv.forEach(el => {
             if (el.id == el.id)
               type = el.type;
           })
           if (type == 'V')
             this.isSPI = false;
           else
             this.isSPI = true;
 
           if (!this.liveMode) {
             if (type == 'V')
               var lastpoint = await this.service.getlast5PointsByid(el.id);
             else
               var lastpoint = await this.service.getlast5PointsUserByid(el.id);
 
 
             this.way = []
             lastpoint.listHistorique.forEach(el => {
 
               this.way.push({
                 location: {
                   lat: Number(el.latitude),
                   lng: Number(el.longitude),
                 },
                 stopover: false,
               });
             });
             var way = this.way
             this.way = []
             var randomcolor = ""
             if (el.data.Conducteur.Activite == 2) {
               randomcolor = "#17a2b8"
             }
             if (el.data.Conducteur.Activite == 3) {
               randomcolor = "#fd7e14"
             }
             if (el.data.Conducteur.Activite == 4) {
               randomcolor = "#FF0000"
             }
             if (el.data.Conducteur.Activite == 5) {
               randomcolor = "#006400"
             }
             this.autoTracingRoute(this.map1, way, randomcolor);
           }
 
         }
       }
     });
     //si la vehciule est existe deja change les valeurs du conducteur,activite,mouvement....
     if (exist) {
       var V = this.listVehiculesItem.find(x => x.idVehicule = t.vehiculeId)
       var CRecherhcheitem = this.recherchlist.find((x: ConducteurRechercherItem) => x.num = V.Conducteur.numCartConducteur)
       var VRecherhcheitem = this.recherchlist.find((x: VehiculeRechercherItem) => x.name = V.Immatriculation.Vrn)
       if (t.evenements[0].tachygraphe != null) {
         if (t.evenements[0].tachygraphe.conducteur != null) {
           if (t.evenements[0].tachygraphe.conducteur.numeroCarte != null) {
 
             V.Conducteur.numCartConducteur = t.evenements[0].tachygraphe.conducteur.numeroCarte;
             CRecherhcheitem.num = t.evenements[0].tachygraphe.conducteur.numeroCarte
             if (t.evenements[0].tachygraphe.conducteur.activite != null) {
               V.Conducteur.Activite = this.mapFn.setActivite(t.evenements[0].tachygraphe.conducteur.activite)
               VRecherhcheitem.activite = this.mapFn.setActivite(t.evenements[0].tachygraphe.conducteur.activite)
               CRecherhcheitem.activite = this.mapFn.setActivite(t.evenements[0].tachygraphe.conducteur.activite)
             }
           }
         }
       }
     }
     // si une nouvelle vehicule 
     if (!exist) {
       // this.SubNewVehicuel(t.vehiculeId);
     }
     //#endregion
   }*/
  UpdateMarkerIcon(marker, Vehicule) {
    var randomcolor = "#354360"
    if (Vehicule.Conducteur.Activite == 2) {
      randomcolor = "#17a2b8"
    }
    if (Vehicule.Conducteur.Activite == 3) {
      randomcolor = "#fd7e14"
    }
    if (Vehicule.Conducteur.Activite == 4) {
      randomcolor = "#FF0000"
    }
    if (Vehicule.Conducteur.Activite == 5) {
      randomcolor = "#006400"
    }

    var ty = 'V'
    if (Vehicule.Immatriculation.Vrn == 'Krichen')
      ty = 'U'
    var adr = Vehicule.Position.Lat + "" + Vehicule.Position.Lng;
    var ic = {
      url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.mapFn.replaceHoverIcon(Vehicule.Immatriculation.Vrn, randomcolor, Vehicule.Position.DateReception, "H", ty, adr)),
      scaledSize: new google.maps.Size(155, 60),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(15, 50),
    };
    marker.setIcon(ic);
    marker.setZIndex(100000);
    // mouseIn
    marker.addListener('mouseover', async () => {
      var adr = Vehicule.Position.Lat + "" + Vehicule.Position.Lng;
      var ic = {
        url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.mapFn.replaceHoverIcon(Vehicule.Immatriculation.Vrn, randomcolor, Vehicule.Position.DateReception, "n", ty, adr)),
        scaledSize: new google.maps.Size(355, 220),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(38, 135),
      };
      marker.setIcon(ic);
      marker.setZIndex(7);
    });
    // mouseOut
    marker.addListener('mouseout', () => {
      var adr = Vehicule.Position.Lat + "" + Vehicule.Position.Lng;
      var ic = {
        url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.mapFn.replaceHoverIcon(Vehicule.Immatriculation.Vrn, randomcolor, Vehicule.Position.DateReception, "H", ty, adr)),
        scaledSize: new google.maps.Size(155, 60),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(15, 50),
      };
      marker.setIcon(ic);
      marker.setZIndex(7);
    });
  }
  async LiveSetMarkerVehiculeToMap(t) {
    //#region Partie Personne
    var map1 = null
    var type: string = 'V';
    if (this.liveMode)
      map1 = this.map1

    /*  if (t.vehiculeId == '00000000-0000-0000-0000-000000000000') {
        type = "U"
        var a = false;
        t.vehiculeId == t.ressourceId;
      }*/
    //#endregion
    //#region Partie Vehicules
    if (t.lat == null) {
      return;
    }
    var exist = false;
    var marker: any = null;
    // comparaison de des dernier point et mouvement de marker
    this.listv.forEach(async (el) => {
      if (el.id == t.idvehciule) {
        exist = true;
        var point = {
          lat: t.lat,
          lng: t.lng,
        };
        el.marker.setPosition(new google.maps.LatLng(point));
        el.data.Position.Lat = t.lat;
        el.data.Position.Lng = t.lng;
        marker = el.marker
        /* if (this.CurrentV != null && this.CurrentV.idVehicule == el.id) {
           this.NewRechercheSelectedVehicule(el.data.Immatriculation.Vrn);
         }*/
      }
    });
    //si la vehciule est existe deja change les valeurs du conducteur,activite,mouvement....
    if (exist) {
      this.listVehiculesItem.forEach(async (V) => {
        if (V.idVehicule == t.idvehciule) {
          V.Conducteur.Activite = this.mapFn.setActivite(this.mapFn.getnumactivite(t.activite))
          V.Position.Lat = t.lat;
          V.Position.Lng = t.lng;
          V.Position.DateReception = t.dateReception;
        }
      })

      var V = this.listVehiculesItem.find(x => x.idVehicule == t.idvehciule)
      this.recherchlist.forEach(async (x) => {
        if (x.name == V.Immatriculation.Vrn) {
          x.activite = this.mapFn.setActivite(this.mapFn.getnumactivite(t.activite))
        }
      })
      this.UpdateMarkerIcon(marker, V);
    }
    // si une nouvelle vehicule 
    if (!exist) {
      // this.SubNewVehicuel(t.vehiculeId);
    }
    //#endregion
  }

  SetcalqueTempurature(a: boolean) {
    if (!a) {
      this.map1.overlayMapTypes.removeAt(0)
      return
    }
    this.myMapType = new google.maps.ImageMapType({
      getTileUrl: (coord, zoom) => {
        var normalizedCoord = this.mapFn.getNormalizedCoord(coord, zoom);

        if (!normalizedCoord) {
          return null;
        }
        var bound = Math.pow(2, zoom);

        var url = "https://tile.openweathermap.org/map/temp_new/" +
          zoom + "/" + normalizedCoord.x + "/" + (normalizedCoord.y) + ".png?appid=c477e6a9fb7b3bbea69d074f28652e8b"
        return url
      },
      tileSize: new google.maps.Size(256, 256),
      maxZoom: 19,
      minZoom: 2,
      name: 'moon',
      opacity: 1
    });

    this.map1.mapTypes.set("moon", this.myMapType);
    this.map1.overlayMapTypes.insertAt(0, this.myMapType);


  }
  //#endregion 
  //#region  initialisation
  ngAfterViewInit(): void {
  }
  ngAfterViewChecked(): void {
    if (document.getElementsByClassName('gmnoprint')[0]) {
      var p = document.getElementsByClassName('gmnoprint')[0].children[0].attributes.getNamedItem('style');
      p.value = p.value + "left:6px;bottom:205px;"
    }
  }
  ngOnDestroy(): void {
    this.graphservice.killpross()
  }
  async ngOnInit() {

    // this.onResize();
    this.showsniper = true
    this.blockcontrole = true;
    await this.initIconTarget();
    await this.initvehicuel();
    this.Hubconntection()
    this.Hubconntection2()
    this.showsniper = false;
    this.blockcontrole = false;

  }
  Hubconntection() {
    let url = this.AppParameters.appURl + '/TrackingHub';
    url.replace('//', '/');
    this._hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(url)
      .configureLogging(signalR.LogLevel.Information)
      .build();

    this._hubConnection.start().catch((err) => console.error('Bus Introuvable : ' + url));
    this._hubConnection.on('ReceiveMessage', async (data) => {
      var t = JSON.parse(data);
      this.vehiculepayload = t;
      console.log(t);
      this.disable$.next(data);
      const received = `Received: ${data}`;
      this.messages.push(received);
    });

  }
  Hubconntection2() {
    let url = this.AppParameters.appURl + '/TrackingHub';
    url.replace('//', '/');
    this._hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(url)
      .configureLogging(signalR.LogLevel.Information)
      .build();

    this._hubConnection.start().catch((err) => console.error('Bus Introuvable : ' + url));
    this._hubConnection.on('ReceiveMessage1', async (data) => {
      var t = JSON.parse(data);
      var mark = this.listv.find(x => x.id == t.idvehciule)
      if (mark != null)
        mark.marker.setPosition({ lat: t.lat, lng: t.lng });
      this.LiveSetMarkerVehiculeToMap(t);
      console.log(t);
      this.disable$.next(data);
      const received = `Received: ${data}`;
      this.messages.push(received);
    });

  }
  infowindowCluster = new google.maps.InfoWindow();
  async initIconTarget() {
    var pos: any;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position: any): void => {
          pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };
          this.map1.setCenter(pos);
        },
        () => {
          console.log("Location not found");
        }
      );
    }
    await new Promise((f) => setTimeout(f, 1000));
    this.ClusterMaker = new MarkerClusterer(this.map1, null, {
      imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
    });
    this.startIcon = {
      url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.mapFn.replace('Début', '#FF0000')),
      scaledSize: new google.maps.Size(155, 60),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(76, 42),
    };
    this.endIcon = {
      url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(this.mapFn.replace('Fin', '#FF0000')),
      scaledSize: new google.maps.Size(155, 60),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(76, 42),
    };

  }

  InfoWindowCluster() {

    google.maps.event.addListener(this.ClusterMaker, "mouseover", (cluster) => {
      var test = '<p style="display:block;color:#354360;font-size:19px;font-weight: bold;">' + cluster.markers_.length + ' Vehicules :</p>'
      cluster.markers_.forEach(el => {
        test += '<p style="display:block;color:#354360;font-size:17px;font-weight: bold;">- ' + el.title + '</p>';
      });
      var offset = 0.1 / Math.pow(2, this.map1.getZoom());
      this.infowindowCluster.setContent("<div style='display:block;max-height:150px'>" + test + "<\/div>");
      this.infowindowCluster.setPosition({
        lat: cluster.center_.lat() * (1 + offset),
        lng: cluster.center_.lng()
      });
      this.infowindowCluster.open(this.map1);
    });
    google.maps.event.addListener(this.ClusterMaker, "click", (cluster) => {
      this.infowindowCluster.close();
    });
  }

  async initvehicuel() {
    var a: any = await this.services.getPoints().catch(e => {
      console.log(e);
      this.showsniper = false;
      this.blockcontrole = false;
      console.log("Service actuellement indisponible veuillez réessayer ultérieurement");
    })
    if (a == null)
      return
    //partie vehicule 
    await a.vehiculeRow.forEach(async (el) => {
      var Vehiculeitem = new VehiculeModel()
      Vehiculeitem.Conducteur = new ConducteurVehicule();
      Vehiculeitem.Position = new PositionVehicule();
      Vehiculeitem.Immatriculation = new Immatriculation();
      if (el.latitude != null) {
        var Conducteur: any = await this.services.getConducteur1(el.numConducteur)
        Vehiculeitem.idVehicule = el.id;
        Vehiculeitem.Conducteur.numCartConducteur = el.numConducteur;
        Vehiculeitem.Conducteur.NomConducteur = Conducteur != null ? Conducteur.displayName : '';
        Vehiculeitem.Conducteur.Activite = this.mapFn.setActivite(el.lastActvite);
        Vehiculeitem.Position.Lat = Number(el.latitude)
        Vehiculeitem.Position.Lng = Number(el.longitude)
        Vehiculeitem.Position.DateReception = el.dateReception;
        Vehiculeitem.Immatriculation.Vin = el.vin;
        Vehiculeitem.Immatriculation.Vrn = el.vrn;
        this.listVehiculesItem.push(Vehiculeitem);
        this.NewSetMarkerVehiculeToMap(Vehiculeitem, 'V');
      }
    });
    //partie SPI or User
    await a.userSpiRow.forEach(async (el) => {
      console.log(el);
      var Vehiculeitem = new VehiculeModel()
      Vehiculeitem.Conducteur = new ConducteurVehicule();
      Vehiculeitem.Position = new PositionVehicule();
      Vehiculeitem.Immatriculation = new Immatriculation();
      Vehiculeitem.idVehicule = el.id;
      Vehiculeitem.IsVehicule = false;
      //Vehiculeitem.Conducteur.numCartConducteur=el.numConducteur;
      Vehiculeitem.Position.Lat = Number(el.latitude)
      Vehiculeitem.Position.Lng = Number(el.longitude)
      Vehiculeitem.Position.DateReception = el.dateReception;
      Vehiculeitem.Immatriculation.Vin = el.nom;
      Vehiculeitem.Immatriculation.Vrn = el.prenom;
      this.listVehiculesItem.push(Vehiculeitem);
      this.NewSetMarkerVehiculeToMap(Vehiculeitem, 'U');
    });
  }
  //#endregion
  @HostListener('window:resize', ['$event'])
  onResize(event?: any) {
    this.isMobile = event ? event.target.innerWidth < 576 : window.innerWidth < 576;
    const sidebar = document.querySelector('.sidebar') as HTMLElement;
    const mainpanel = document.querySelector('.main-panel') as HTMLElement;
    const wrapper = document.querySelector('.wrapper') as HTMLElement;
    const trackingLocalizeButton = document.querySelector('.tracking-localize-button') as HTMLElement;
    if (this.isMobile) {
      sidebar.classList.add('d-none', 'mobile-width');
      mainpanel.classList.add('w-100', 'h-100');
      wrapper.classList.add('overflow-hidden');
      trackingLocalizeButton.classList.add('tracking-localize-button-mobile');
    } else {
      sidebar.classList.remove('d-none', 'mobile-width');
      mainpanel.classList.remove('w-100', 'h-100');
      wrapper.classList.remove('overflow-hidden');
      trackingLocalizeButton.classList.remove('tracking-localize-button-mobile');
    }
  }
  //#endregion
}
