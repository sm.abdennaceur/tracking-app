import { TestBed } from '@angular/core/testing';
import { TrackingModule } from './tracking.module';
describe('TrackingModule', () => {
  let pipe: TrackingModule;
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [TrackingModule] });
    pipe = TestBed.get(TrackingModule);
  });
  it('can load instance', () => {
    expect(pipe).toBeTruthy();
  });
});
