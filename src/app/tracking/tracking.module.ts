import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { TrackingRoutingModule } from './tracking.routing';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { SharedModule } from '../shared/shared.module';
import { AgmCoreModule } from '@agm/core';
 import { AgmDirectionModule } from 'agm-direction';
import { CoreModule } from '../core/core.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { DetailVehiculeComponent } from './detail-vehicule/detail-vehicule.component';
import { MenuContextComponent } from './menu-context/menu-context.component';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatOptionModule } from '@angular/material/core';
import { HttpClientModule, HttpHeaders, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ModalComponent } from './Modal/modal/modal.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { DatepikerComponent } from './datepiker/datepiker.component';
import { SwitshdateComponent } from './switshdate/switshdate.component';
import {Apollo, APOLLO_OPTIONS} from 'apollo-angular';
import {HttpLink} from 'apollo-angular/http';
import {InMemoryCache} from '@apollo/client/core';
import { PlusInfoComponent } from './plus-info/plus-info.component';
import { RechercheInputComponent } from './recherche-input/recherche-input.component';
import { InfoBulleComponent } from './info-bulle/info-bulle.component';


@NgModule({
  declarations: [
    TrackingRoutingModule.components,
    DetailVehiculeComponent,
    MenuContextComponent,
    ModalComponent,
    DatepikerComponent,
    SwitshdateComponent,
    PlusInfoComponent,
    RechercheInputComponent,
    InfoBulleComponent
  ],
  imports: [
    MatFormFieldModule,
    MatOptionModule,
    NgbModule,
    CommonModule,
    CoreModule,
    SharedModule,
    TrackingRoutingModule,
    AngularMaterialModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBU3fyx7thOvVj4K53uNR3N-c9cOUR_62g',
      libraries: ['visualization','geometry'],
    }),
     AgmDirectionModule,
    AgmJsMarkerClustererModule,
    HttpClientModule,
    AngularSvgIconModule,
  ],
  providers: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
  exports:[SwitshdateComponent,MenuContextComponent,PlusInfoComponent,RechercheInputComponent,DetailVehiculeComponent]

})
export class TrackingModule {

  constructor(apollo: Apollo, httpLink: HttpLink) {
    if(!apollo.client)
 {
  apollo.create({
    link: httpLink.create({
      uri:`https://apigqltra.dev.stradatms.net/graphql`,
      headers: new HttpHeaders().set('Authorization', `Bearer ${localStorage.getItem("Strada_Token")}`),
    }),
    cache: new InMemoryCache(),
  })
}
   
  }
}
