import { TestBed } from '@angular/core/testing';
import { TrackingRoutingModule } from './tracking.routing';
import { TrackingComponent } from './tracking.component';
describe('TrackingRoutingModule', () => {
  let pipe: TrackingRoutingModule;
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [TrackingRoutingModule] });
    pipe = TestBed.get(TrackingRoutingModule);
  });
  it('can load instance', () => {
    expect(pipe).toBeTruthy();
  });
  it('components defaults to: [TrackingComponent]', () => {
    expect(TrackingRoutingModule.components).toEqual([TrackingComponent]);
  });
});
