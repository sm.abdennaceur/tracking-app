export const environment = {
  production: true,
  config: '../assets/environments/config.json'

};

import * as config   from '../assets/environments/config.json'

export const AppParameters = {
  apiUrl: config.apiUrl,
  auth0ClientId: config.auth0ClientId,
  auth0Scope: config.scope,
  auth0Audience: config.audience,
  baseUrl: config.auth0BaseUrl,
  auth0ConfigId: config.auth0ConfigId,
  auth0ClientSecret: config.auth0ClientSecret
};