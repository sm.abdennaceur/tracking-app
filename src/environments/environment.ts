// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  config: '../assets/environments/config.json'

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
import * as config   from '../assets/environments/config.json'

export const AppParameters = {
  apiUrl: config.apiUrl,
  auth0ClientId: config.auth0ClientId,
  auth0Scope: config.scope,
  auth0Audience: config.audience,
  baseUrl: config.auth0BaseUrl,
  auth0ConfigId: config.auth0ConfigId,
  auth0ClientSecret: config.auth0ClientSecret
};
